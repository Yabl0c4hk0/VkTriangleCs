using System;
using System.Runtime.InteropServices;

namespace LearnVulkan.Vulkan.Binding.Native
{
    public static class Vk
    {
        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateInstance")]
        public static extern unsafe Result CreateInstance(InstanceCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, Instance* pInstance);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyInstance")]
        public static extern unsafe void DestroyInstance(Instance instance, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkEnumeratePhysicalDevices")]
        public static extern unsafe Result EnumeratePhysicalDevices(Instance instance, uint* pPhysicalDeviceCount,
            PhysicalDevice* pPhysicalDevices);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetDeviceProcAddr")]
        public static extern unsafe void* GetDeviceProcAddr(Device device, sbyte* pName);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetInstanceProcAddr")]
        public static extern unsafe void* GetInstanceProcAddr(Instance instance, sbyte* pName);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceProperties")]
        public static extern unsafe void GetPhysicalDeviceProperties(PhysicalDevice physicalDevice,
            PhysicalDeviceProperties* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceQueueFamilyProperties")]
        public static extern unsafe void GetPhysicalDeviceQueueFamilyProperties(PhysicalDevice physicalDevice,
            uint* pQueueFamilyPropertyCount, QueueFamilyProperties* pQueueFamilyProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceMemoryProperties")]
        public static extern unsafe void GetPhysicalDeviceMemoryProperties(PhysicalDevice physicalDevice,
            PhysicalDeviceMemoryProperties* pMemoryProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceFeatures")]
        public static extern unsafe void GetPhysicalDeviceFeatures(PhysicalDevice physicalDevice,
            PhysicalDeviceFeatures* pFeatures);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceFormatProperties")]
        public static extern unsafe void GetPhysicalDeviceFormatProperties(PhysicalDevice physicalDevice, Format format,
            FormatProperties* pFormatProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceImageFormatProperties")]
        public static extern unsafe Result GetPhysicalDeviceImageFormatProperties(PhysicalDevice physicalDevice,
            Format format, ImageType type, ImageTiling tiling, ImageUsageFlags usage, ImageCreateFlags flags,
            ImageFormatProperties* pImageFormatProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateDevice")]
        public static extern unsafe Result CreateDevice(PhysicalDevice physicalDevice, DeviceCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, Device* pDevice);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyDevice")]
        public static extern unsafe void DestroyDevice(Device device, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkEnumerateInstanceVersion")]
        public static extern unsafe Result EnumerateInstanceVersion(uint* pApiVersion);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkEnumerateInstanceLayerProperties")]
        public static extern unsafe Result EnumerateInstanceLayerProperties(uint* pPropertyCount,
            LayerProperties* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkEnumerateInstanceExtensionProperties")]
        public static extern unsafe Result EnumerateInstanceExtensionProperties(sbyte* pLayerName, uint* pPropertyCount,
            ExtensionProperties* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkEnumerateDeviceLayerProperties")]
        public static extern unsafe Result EnumerateDeviceLayerProperties(PhysicalDevice physicalDevice,
            uint* pPropertyCount, LayerProperties* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkEnumerateDeviceExtensionProperties")]
        public static extern unsafe Result EnumerateDeviceExtensionProperties(PhysicalDevice physicalDevice,
            sbyte* pLayerName, uint* pPropertyCount, ExtensionProperties* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetDeviceQueue")]
        public static extern unsafe void GetDeviceQueue(Device device, uint queueFamilyIndex, uint queueIndex,
            Queue* pQueue);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkQueueSubmit")]
        public static extern unsafe Result
            QueueSubmit(Queue queue, uint submitCount, SubmitInfo* pSubmits, Fence fence);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkQueueWaitIdle")]
        public static extern unsafe Result QueueWaitIdle(Queue queue);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDeviceWaitIdle")]
        public static extern unsafe Result DeviceWaitIdle(Device device);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkAllocateMemory")]
        public static extern unsafe Result AllocateMemory(Device device, MemoryAllocateInfo* pAllocateInfo,
            AllocationCallbacks* pAllocator, DeviceMemory* pMemory);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkFreeMemory")]
        public static extern unsafe void
            FreeMemory(Device device, DeviceMemory memory, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkMapMemory")]
        public static extern unsafe Result MapMemory(Device device, DeviceMemory memory, ulong offset, ulong size,
            uint flags, IntPtr ppData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkUnmapMemory")]
        public static extern unsafe void UnmapMemory(Device device, DeviceMemory memory);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkFlushMappedMemoryRanges")]
        public static extern unsafe Result FlushMappedMemoryRanges(Device device, uint memoryRangeCount,
            MappedMemoryRange* pMemoryRanges);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkInvalidateMappedMemoryRanges")]
        public static extern unsafe Result InvalidateMappedMemoryRanges(Device device, uint memoryRangeCount,
            MappedMemoryRange* pMemoryRanges);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDeviceMemoryCommitment")]
        public static extern unsafe void GetDeviceMemoryCommitment(Device device, DeviceMemory memory,
            ulong* pCommittedMemoryInBytes);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetBufferMemoryRequirements")]
        public static extern unsafe void GetBufferMemoryRequirements(Device device, Buffer buffer,
            MemoryRequirements* pMemoryRequirements);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkBindBufferMemory")]
        public static extern unsafe Result BindBufferMemory(Device device, Buffer buffer, DeviceMemory memory,
            ulong memoryOffset);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetImageMemoryRequirements")]
        public static extern unsafe void GetImageMemoryRequirements(Device device, Image image,
            MemoryRequirements* pMemoryRequirements);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkBindImageMemory")]
        public static extern unsafe Result BindImageMemory(Device device, Image image, DeviceMemory memory,
            ulong memoryOffset);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetImageSparseMemoryRequirements")]
        public static extern unsafe void GetImageSparseMemoryRequirements(Device device, Image image,
            uint* pSparseMemoryRequirementCount, SparseImageMemoryRequirements* pSparseMemoryRequirements);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSparseImageFormatProperties")]
        public static extern unsafe void GetPhysicalDeviceSparseImageFormatProperties(PhysicalDevice physicalDevice,
            Format format, ImageType type, SampleCountFlags samples, ImageUsageFlags usage, ImageTiling tiling,
            uint* pPropertyCount, SparseImageFormatProperties* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkQueueBindSparse")]
        public static extern unsafe Result QueueBindSparse(Queue queue, uint bindInfoCount, BindSparseInfo* pBindInfo,
            Fence fence);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateFence")]
        public static extern unsafe Result CreateFence(Device device, FenceCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, Fence* pFence);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyFence")]
        public static extern unsafe void DestroyFence(Device device, Fence fence, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkResetFences")]
        public static extern unsafe Result ResetFences(Device device, uint fenceCount, Fence* pFences);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetFenceStatus")]
        public static extern unsafe Result GetFenceStatus(Device device, Fence fence);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkWaitForFences")]
        public static extern unsafe Result WaitForFences(Device device, uint fenceCount, Fence* pFences, uint waitAll,
            ulong timeout);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateSemaphore")]
        public static extern unsafe Result CreateSemaphore(Device device, SemaphoreCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, Semaphore* pSemaphore);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroySemaphore")]
        public static extern unsafe void DestroySemaphore(Device device, Semaphore semaphore,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateEvent")]
        public static extern unsafe Result CreateEvent(Device device, EventCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, Event* pEvent);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyEvent")]
        public static extern unsafe void DestroyEvent(Device device, Event @event, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetEventStatus")]
        public static extern unsafe Result GetEventStatus(Device device, Event @event);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkSetEvent")]
        public static extern unsafe Result SetEvent(Device device, Event @event);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkResetEvent")]
        public static extern unsafe Result ResetEvent(Device device, Event @event);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateQueryPool")]
        public static extern unsafe Result CreateQueryPool(Device device, QueryPoolCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, QueryPool* pQueryPool);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyQueryPool")]
        public static extern unsafe void DestroyQueryPool(Device device, QueryPool queryPool,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetQueryPoolResults")]
        public static extern unsafe Result GetQueryPoolResults(Device device, QueryPool queryPool, uint firstQuery,
            uint queryCount, IntPtr dataSize, void* pData, ulong stride, QueryResultFlags flags);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkResetQueryPool")]
        public static extern unsafe void ResetQueryPool(Device device, QueryPool queryPool, uint firstQuery,
            uint queryCount);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateBuffer")]
        public static extern unsafe Result CreateBuffer(Device device, BufferCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, Buffer* pBuffer);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyBuffer")]
        public static extern unsafe void DestroyBuffer(Device device, Buffer buffer, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateBufferView")]
        public static extern unsafe Result CreateBufferView(Device device, BufferViewCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, BufferView* pView);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyBufferView")]
        public static extern unsafe void DestroyBufferView(Device device, BufferView bufferView,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateImage")]
        public static extern unsafe Result CreateImage(Device device, ImageCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, Image* pImage);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyImage")]
        public static extern unsafe void DestroyImage(Device device, Image image, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetImageSubresourceLayout")]
        public static extern unsafe void GetImageSubresourceLayout(Device device, Image image,
            ImageSubresource* pSubresource, SubresourceLayout* pLayout);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateImageView")]
        public static extern unsafe Result CreateImageView(Device device, ImageViewCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, ImageView* pView);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyImageView")]
        public static extern unsafe void DestroyImageView(Device device, ImageView imageView,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateShaderModule")]
        public static extern unsafe Result CreateShaderModule(Device device, ShaderModuleCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, ShaderModule* pShaderModule);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyShaderModule")]
        public static extern unsafe void DestroyShaderModule(Device device, ShaderModule shaderModule,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreatePipelineCache")]
        public static extern unsafe Result CreatePipelineCache(Device device, PipelineCacheCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, PipelineCache* pPipelineCache);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyPipelineCache")]
        public static extern unsafe void DestroyPipelineCache(Device device, PipelineCache pipelineCache,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetPipelineCacheData")]
        public static extern unsafe Result GetPipelineCacheData(Device device, PipelineCache pipelineCache,
            IntPtr* pDataSize, void* pData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkMergePipelineCaches")]
        public static extern unsafe Result MergePipelineCaches(Device device, PipelineCache dstCache,
            uint srcCacheCount, PipelineCache* pSrcCaches);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateGraphicsPipelines")]
        public static extern unsafe Result CreateGraphicsPipelines(Device device, PipelineCache pipelineCache,
            uint createInfoCount, GraphicsPipelineCreateInfo* pCreateInfos, AllocationCallbacks* pAllocator,
            Pipeline* pPipelines);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateComputePipelines")]
        public static extern unsafe Result CreateComputePipelines(Device device, PipelineCache pipelineCache,
            uint createInfoCount, ComputePipelineCreateInfo* pCreateInfos, AllocationCallbacks* pAllocator,
            Pipeline* pPipelines);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyPipeline")]
        public static extern unsafe void DestroyPipeline(Device device, Pipeline pipeline,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreatePipelineLayout")]
        public static extern unsafe Result CreatePipelineLayout(Device device, PipelineLayoutCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, PipelineLayout* pPipelineLayout);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyPipelineLayout")]
        public static extern unsafe void DestroyPipelineLayout(Device device, PipelineLayout pipelineLayout,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateSampler")]
        public static extern unsafe Result CreateSampler(Device device, SamplerCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, Sampler* pSampler);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroySampler")]
        public static extern unsafe void
            DestroySampler(Device device, Sampler sampler, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateDescriptorSetLayout")]
        public static extern unsafe Result CreateDescriptorSetLayout(Device device,
            DescriptorSetLayoutCreateInfo* pCreateInfo, AllocationCallbacks* pAllocator,
            DescriptorSetLayout* pSetLayout);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyDescriptorSetLayout")]
        public static extern unsafe void DestroyDescriptorSetLayout(Device device,
            DescriptorSetLayout descriptorSetLayout, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateDescriptorPool")]
        public static extern unsafe Result CreateDescriptorPool(Device device, DescriptorPoolCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, DescriptorPool* pDescriptorPool);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyDescriptorPool")]
        public static extern unsafe void DestroyDescriptorPool(Device device, DescriptorPool descriptorPool,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkResetDescriptorPool")]
        public static extern unsafe Result
            ResetDescriptorPool(Device device, DescriptorPool descriptorPool, uint flags);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkAllocateDescriptorSets")]
        public static extern unsafe Result AllocateDescriptorSets(Device device,
            DescriptorSetAllocateInfo* pAllocateInfo, DescriptorSet* pDescriptorSets);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkFreeDescriptorSets")]
        public static extern unsafe Result FreeDescriptorSets(Device device, DescriptorPool descriptorPool,
            uint descriptorSetCount, DescriptorSet* pDescriptorSets);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkUpdateDescriptorSets")]
        public static extern unsafe void UpdateDescriptorSets(Device device, uint descriptorWriteCount,
            WriteDescriptorSet* pDescriptorWrites, uint descriptorCopyCount, CopyDescriptorSet* pDescriptorCopies);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateFramebuffer")]
        public static extern unsafe Result CreateFramebuffer(Device device, FramebufferCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, Framebuffer* pFramebuffer);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyFramebuffer")]
        public static extern unsafe void DestroyFramebuffer(Device device, Framebuffer framebuffer,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateRenderPass")]
        public static extern unsafe Result CreateRenderPass(Device device, RenderPassCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, RenderPass* pRenderPass);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyRenderPass")]
        public static extern unsafe void DestroyRenderPass(Device device, RenderPass renderPass,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetRenderAreaGranularity")]
        public static extern unsafe void GetRenderAreaGranularity(Device device, RenderPass renderPass,
            Extent2d* pGranularity);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateCommandPool")]
        public static extern unsafe Result CreateCommandPool(Device device, CommandPoolCreateInfo* pCreateInfo,
            AllocationCallbacks* pAllocator, CommandPool* pCommandPool);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroyCommandPool")]
        public static extern unsafe void DestroyCommandPool(Device device, CommandPool commandPool,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkResetCommandPool")]
        public static extern unsafe Result ResetCommandPool(Device device, CommandPool commandPool,
            CommandPoolResetFlags flags);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkAllocateCommandBuffers")]
        public static extern unsafe Result AllocateCommandBuffers(Device device,
            CommandBufferAllocateInfo* pAllocateInfo, CommandBuffer* pCommandBuffers);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkFreeCommandBuffers")]
        public static extern unsafe void FreeCommandBuffers(Device device, CommandPool commandPool,
            uint commandBufferCount, CommandBuffer* pCommandBuffers);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkBeginCommandBuffer")]
        public static extern unsafe Result BeginCommandBuffer(CommandBuffer commandBuffer,
            CommandBufferBeginInfo* pBeginInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkEndCommandBuffer")]
        public static extern unsafe Result EndCommandBuffer(CommandBuffer commandBuffer);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkResetCommandBuffer")]
        public static extern unsafe Result ResetCommandBuffer(CommandBuffer commandBuffer,
            CommandBufferResetFlags flags);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdBindPipeline")]
        public static extern unsafe void CmdBindPipeline(CommandBuffer commandBuffer,
            PipelineBindPoint pipelineBindPoint, Pipeline pipeline);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetViewport")]
        public static extern unsafe void CmdSetViewport(CommandBuffer commandBuffer, uint firstViewport,
            uint viewportCount, Viewport* pViewports);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetScissor")]
        public static extern unsafe void CmdSetScissor(CommandBuffer commandBuffer, uint firstScissor,
            uint scissorCount, Rect2d* pScissors);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetLineWidth")]
        public static extern unsafe void CmdSetLineWidth(CommandBuffer commandBuffer, float lineWidth);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetDepthBias")]
        public static extern unsafe void CmdSetDepthBias(CommandBuffer commandBuffer, float depthBiasConstantFactor,
            float depthBiasClamp, float depthBiasSlopeFactor);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetBlendConstants")]
        public static extern unsafe void CmdSetBlendConstants(CommandBuffer commandBuffer, float* blendConstants);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetDepthBounds")]
        public static extern unsafe void CmdSetDepthBounds(CommandBuffer commandBuffer, float minDepthBounds,
            float maxDepthBounds);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetStencilCompareMask")]
        public static extern unsafe void CmdSetStencilCompareMask(CommandBuffer commandBuffer,
            StencilFaceFlags faceMask, uint compareMask);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetStencilWriteMask")]
        public static extern unsafe void CmdSetStencilWriteMask(CommandBuffer commandBuffer, StencilFaceFlags faceMask,
            uint writeMask);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetStencilReference")]
        public static extern unsafe void CmdSetStencilReference(CommandBuffer commandBuffer, StencilFaceFlags faceMask,
            uint reference);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBindDescriptorSets")]
        public static extern unsafe void CmdBindDescriptorSets(CommandBuffer commandBuffer,
            PipelineBindPoint pipelineBindPoint, PipelineLayout layout, uint firstSet, uint descriptorSetCount,
            DescriptorSet* pDescriptorSets, uint dynamicOffsetCount, uint* pDynamicOffsets);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdBindIndexBuffer")]
        public static extern unsafe void CmdBindIndexBuffer(CommandBuffer commandBuffer, Buffer buffer, ulong offset,
            IndexType indexType);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdBindVertexBuffers")]
        public static extern unsafe void CmdBindVertexBuffers(CommandBuffer commandBuffer, uint firstBinding,
            uint bindingCount, Buffer* pBuffers, ulong* pOffsets);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdDraw")]
        public static extern unsafe void CmdDraw(CommandBuffer commandBuffer, uint vertexCount, uint instanceCount,
            uint firstVertex, uint firstInstance);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdDrawIndexed")]
        public static extern unsafe void CmdDrawIndexed(CommandBuffer commandBuffer, uint indexCount,
            uint instanceCount, uint firstIndex, int vertexOffset, uint firstInstance);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdDrawIndirect")]
        public static extern unsafe void CmdDrawIndirect(CommandBuffer commandBuffer, Buffer buffer, ulong offset,
            uint drawCount, uint stride);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdDrawIndexedIndirect")]
        public static extern unsafe void CmdDrawIndexedIndirect(CommandBuffer commandBuffer, Buffer buffer,
            ulong offset, uint drawCount, uint stride);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdDispatch")]
        public static extern unsafe void CmdDispatch(CommandBuffer commandBuffer, uint groupCountX, uint groupCountY,
            uint groupCountZ);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdDispatchIndirect")]
        public static extern unsafe void CmdDispatchIndirect(CommandBuffer commandBuffer, Buffer buffer, ulong offset);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdCopyBuffer")]
        public static extern unsafe void CmdCopyBuffer(CommandBuffer commandBuffer, Buffer srcBuffer, Buffer dstBuffer,
            uint regionCount, BufferCopy* pRegions);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdCopyImage")]
        public static extern unsafe void CmdCopyImage(CommandBuffer commandBuffer, Image srcImage,
            ImageLayout srcImageLayout, Image dstImage, ImageLayout dstImageLayout, uint regionCount,
            ImageCopy* pRegions);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdBlitImage")]
        public static extern unsafe void CmdBlitImage(CommandBuffer commandBuffer, Image srcImage,
            ImageLayout srcImageLayout, Image dstImage, ImageLayout dstImageLayout, uint regionCount,
            ImageBlit* pRegions, Filter filter);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdCopyBufferToImage")]
        public static extern unsafe void CmdCopyBufferToImage(CommandBuffer commandBuffer, Buffer srcBuffer,
            Image dstImage, ImageLayout dstImageLayout, uint regionCount, BufferImageCopy* pRegions);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdCopyImageToBuffer")]
        public static extern unsafe void CmdCopyImageToBuffer(CommandBuffer commandBuffer, Image srcImage,
            ImageLayout srcImageLayout, Buffer dstBuffer, uint regionCount, BufferImageCopy* pRegions);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdUpdateBuffer")]
        public static extern unsafe void CmdUpdateBuffer(CommandBuffer commandBuffer, Buffer dstBuffer, ulong dstOffset,
            ulong dataSize, void* pData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdFillBuffer")]
        public static extern unsafe void CmdFillBuffer(CommandBuffer commandBuffer, Buffer dstBuffer, ulong dstOffset,
            ulong size, uint data);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdClearColorImage")]
        public static extern unsafe void CmdClearColorImage(CommandBuffer commandBuffer, Image image,
            ImageLayout imageLayout, ClearColorValue* pColor, uint rangeCount, ImageSubresourceRange* pRanges);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdClearDepthStencilImage")]
        public static extern unsafe void CmdClearDepthStencilImage(CommandBuffer commandBuffer, Image image,
            ImageLayout imageLayout, ClearDepthStencilValue* pDepthStencil, uint rangeCount,
            ImageSubresourceRange* pRanges);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdClearAttachments")]
        public static extern unsafe void CmdClearAttachments(CommandBuffer commandBuffer, uint attachmentCount,
            ClearAttachment* pAttachments, uint rectCount, ClearRect* pRects);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdResolveImage")]
        public static extern unsafe void CmdResolveImage(CommandBuffer commandBuffer, Image srcImage,
            ImageLayout srcImageLayout, Image dstImage, ImageLayout dstImageLayout, uint regionCount,
            ImageResolve* pRegions);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetEvent")]
        public static extern unsafe void CmdSetEvent(CommandBuffer commandBuffer, Event @event,
            PipelineStageFlags stageMask);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdResetEvent")]
        public static extern unsafe void CmdResetEvent(CommandBuffer commandBuffer, Event @event,
            PipelineStageFlags stageMask);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdWaitEvents")]
        public static extern unsafe void CmdWaitEvents(CommandBuffer commandBuffer, uint eventCount, Event* pEvents,
            PipelineStageFlags srcStageMask, PipelineStageFlags dstStageMask, uint memoryBarrierCount,
            MemoryBarrier* pMemoryBarriers, uint bufferMemoryBarrierCount, BufferMemoryBarrier* pBufferMemoryBarriers,
            uint imageMemoryBarrierCount, ImageMemoryBarrier* pImageMemoryBarriers);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdPipelineBarrier")]
        public static extern unsafe void CmdPipelineBarrier(CommandBuffer commandBuffer,
            PipelineStageFlags srcStageMask, PipelineStageFlags dstStageMask, DependencyFlags dependencyFlags,
            uint memoryBarrierCount, MemoryBarrier* pMemoryBarriers, uint bufferMemoryBarrierCount,
            BufferMemoryBarrier* pBufferMemoryBarriers, uint imageMemoryBarrierCount,
            ImageMemoryBarrier* pImageMemoryBarriers);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdBeginQuery")]
        public static extern unsafe void CmdBeginQuery(CommandBuffer commandBuffer, QueryPool queryPool, uint query,
            QueryControlFlags flags);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdEndQuery")]
        public static extern unsafe void CmdEndQuery(CommandBuffer commandBuffer, QueryPool queryPool, uint query);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBeginConditionalRenderingEXT")]
        public static extern unsafe void CmdBeginConditionalRenderingExt(CommandBuffer commandBuffer,
            ConditionalRenderingBeginInfoExt* pConditionalRenderingBegin);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdEndConditionalRenderingEXT")]
        public static extern unsafe void CmdEndConditionalRenderingExt(CommandBuffer commandBuffer);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdResetQueryPool")]
        public static extern unsafe void CmdResetQueryPool(CommandBuffer commandBuffer, QueryPool queryPool,
            uint firstQuery, uint queryCount);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdWriteTimestamp")]
        public static extern unsafe void CmdWriteTimestamp(CommandBuffer commandBuffer,
            PipelineStageFlags pipelineStage, QueryPool queryPool, uint query);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdCopyQueryPoolResults")]
        public static extern unsafe void CmdCopyQueryPoolResults(CommandBuffer commandBuffer, QueryPool queryPool,
            uint firstQuery, uint queryCount, Buffer dstBuffer, ulong dstOffset, ulong stride, QueryResultFlags flags);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdPushConstants")]
        public static extern unsafe void CmdPushConstants(CommandBuffer commandBuffer, PipelineLayout layout,
            ShaderStageFlags stageFlags, uint offset, uint size, void* pValues);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdBeginRenderPass")]
        public static extern unsafe void CmdBeginRenderPass(CommandBuffer commandBuffer,
            RenderPassBeginInfo* pRenderPassBegin, SubpassContents contents);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdNextSubpass")]
        public static extern unsafe void CmdNextSubpass(CommandBuffer commandBuffer, SubpassContents contents);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdEndRenderPass")]
        public static extern unsafe void CmdEndRenderPass(CommandBuffer commandBuffer);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdExecuteCommands")]
        public static extern unsafe void CmdExecuteCommands(CommandBuffer commandBuffer, uint commandBufferCount,
            CommandBuffer* pCommandBuffers);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateAndroidSurfaceKHR")]
        public static extern unsafe Result CreateAndroidSurfaceKhr(Instance instance,
            AndroidSurfaceCreateInfoKhr* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceDisplayPropertiesKHR")]
        public static extern unsafe Result GetPhysicalDeviceDisplayPropertiesKhr(PhysicalDevice physicalDevice,
            uint* pPropertyCount, DisplayPropertiesKhr* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceDisplayPlanePropertiesKHR")]
        public static extern unsafe Result GetPhysicalDeviceDisplayPlanePropertiesKhr(PhysicalDevice physicalDevice,
            uint* pPropertyCount, DisplayPlanePropertiesKhr* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDisplayPlaneSupportedDisplaysKHR")]
        public static extern unsafe Result GetDisplayPlaneSupportedDisplaysKhr(PhysicalDevice physicalDevice,
            uint planeIndex, uint* pDisplayCount, DisplayKhr* pDisplays);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDisplayModePropertiesKHR")]
        public static extern unsafe Result GetDisplayModePropertiesKhr(PhysicalDevice physicalDevice,
            DisplayKhr display, uint* pPropertyCount, DisplayModePropertiesKhr* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateDisplayModeKHR")]
        public static extern unsafe Result CreateDisplayModeKhr(PhysicalDevice physicalDevice, DisplayKhr display,
            DisplayModeCreateInfoKhr* pCreateInfo, AllocationCallbacks* pAllocator, DisplayModeKhr* pMode);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDisplayPlaneCapabilitiesKHR")]
        public static extern unsafe Result GetDisplayPlaneCapabilitiesKhr(PhysicalDevice physicalDevice,
            DisplayModeKhr mode, uint planeIndex, DisplayPlaneCapabilitiesKhr* pCapabilities);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateDisplayPlaneSurfaceKHR")]
        public static extern unsafe Result CreateDisplayPlaneSurfaceKhr(Instance instance,
            DisplaySurfaceCreateInfoKhr* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateSharedSwapchainsKHR")]
        public static extern unsafe Result CreateSharedSwapchainsKhr(Device device, uint swapchainCount,
            SwapchainCreateInfoKhr* pCreateInfos, AllocationCallbacks* pAllocator, SwapchainKhr* pSwapchains);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroySurfaceKHR")]
        public static extern unsafe void DestroySurfaceKhr(Instance instance, SurfaceKhr surface,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSurfaceSupportKHR")]
        public static extern unsafe Result GetPhysicalDeviceSurfaceSupportKhr(PhysicalDevice physicalDevice,
            uint queueFamilyIndex, SurfaceKhr surface, uint* pSupported);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSurfaceCapabilitiesKHR")]
        public static extern unsafe Result GetPhysicalDeviceSurfaceCapabilitiesKhr(PhysicalDevice physicalDevice,
            SurfaceKhr surface, SurfaceCapabilitiesKhr* pSurfaceCapabilities);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSurfaceFormatsKHR")]
        public static extern unsafe Result GetPhysicalDeviceSurfaceFormatsKhr(PhysicalDevice physicalDevice,
            SurfaceKhr surface, uint* pSurfaceFormatCount, SurfaceFormatKhr* pSurfaceFormats);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSurfacePresentModesKHR")]
        public static extern unsafe Result GetPhysicalDeviceSurfacePresentModesKhr(PhysicalDevice physicalDevice,
            SurfaceKhr surface, uint* pPresentModeCount, PresentModeKhr* pPresentModes);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateSwapchainKHR")]
        public static extern unsafe Result CreateSwapchainKhr(Device device, SwapchainCreateInfoKhr* pCreateInfo,
            AllocationCallbacks* pAllocator, SwapchainKhr* pSwapchain);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkDestroySwapchainKHR")]
        public static extern unsafe void DestroySwapchainKhr(Device device, SwapchainKhr swapchain,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetSwapchainImagesKHR")]
        public static extern unsafe Result GetSwapchainImagesKhr(Device device, SwapchainKhr swapchain,
            uint* pSwapchainImageCount, Image* pSwapchainImages);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkAcquireNextImageKHR")]
        public static extern unsafe Result AcquireNextImageKhr(Device device, SwapchainKhr swapchain, ulong timeout,
            Semaphore semaphore, Fence fence, out uint pImageIndex);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkQueuePresentKHR")]
        public static extern unsafe Result QueuePresentKhr(Queue queue, PresentInfoKhr* pPresentInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateViSurfaceNN")]
        public static extern unsafe Result CreateViSurfaceNn(Instance instance, ViSurfaceCreateInfoNn* pCreateInfo,
            AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateWaylandSurfaceKHR")]
        public static extern unsafe Result CreateWaylandSurfaceKhr(Instance instance,
            WaylandSurfaceCreateInfoKhr* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceWaylandPresentationSupportKHR")]
        public static extern unsafe uint GetPhysicalDeviceWaylandPresentationSupportKhr(PhysicalDevice physicalDevice,
            uint queueFamilyIndex, IntPtr display);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateWin32SurfaceKHR")]
        public static extern unsafe Result CreateWin32SurfaceKhr(Instance instance,
            Win32SurfaceCreateInfoKhr* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceWin32PresentationSupportKHR")]
        public static extern unsafe uint GetPhysicalDeviceWin32PresentationSupportKhr(PhysicalDevice physicalDevice,
            uint queueFamilyIndex);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateXlibSurfaceKHR")]
        public static extern unsafe Result CreateXlibSurfaceKhr(Instance instance,
            XlibSurfaceCreateInfoKhr* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceXlibPresentationSupportKHR")]
        public static extern unsafe uint GetPhysicalDeviceXlibPresentationSupportKhr(PhysicalDevice physicalDevice,
            uint queueFamilyIndex, IntPtr dpy, void* visualID);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateXcbSurfaceKHR")]
        public static extern unsafe Result CreateXcbSurfaceKhr(Instance instance, XcbSurfaceCreateInfoKhr* pCreateInfo,
            AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceXcbPresentationSupportKHR")]
        public static extern unsafe uint GetPhysicalDeviceXcbPresentationSupportKhr(PhysicalDevice physicalDevice,
            uint queueFamilyIndex, IntPtr connection, IntPtr visual_id);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateDirectFBSurfaceEXT")]
        public static extern unsafe Result CreateDirectFBSurfaceExt(Instance instance,
            DirectFBSurfaceCreateInfoExt* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceDirectFBPresentationSupportEXT")]
        public static extern unsafe uint GetPhysicalDeviceDirectFBPresentationSupportExt(PhysicalDevice physicalDevice,
            uint queueFamilyIndex, void* dfb);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateImagePipeSurfaceFUCHSIA")]
        public static extern unsafe Result CreateImagePipeSurfaceFuchsia(Instance instance,
            ImagePipeSurfaceCreateInfoFuchsia* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateStreamDescriptorSurfaceGGP")]
        public static extern unsafe Result CreateStreamDescriptorSurfaceGgp(Instance instance,
            StreamDescriptorSurfaceCreateInfoGgp* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateDebugReportCallbackEXT")]
        public static extern unsafe Result CreateDebugReportCallbackExt(Instance instance,
            DebugReportCallbackCreateInfoExt* pCreateInfo, AllocationCallbacks* pAllocator,
            DebugReportCallbackExt* pCallback);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyDebugReportCallbackEXT")]
        public static extern unsafe void DestroyDebugReportCallbackExt(Instance instance,
            DebugReportCallbackExt callback, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDebugReportMessageEXT")]
        public static extern unsafe void DebugReportMessageExt(Instance instance, DebugReportFlagsExt flags,
            DebugReportObjectTypeExt objectType, ulong @object, IntPtr location, int messageCode, sbyte* pLayerPrefix,
            sbyte* pMessage);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDebugMarkerSetObjectNameEXT")]
        public static extern unsafe Result DebugMarkerSetObjectNameExt(Device device,
            DebugMarkerObjectNameInfoExt* pNameInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDebugMarkerSetObjectTagEXT")]
        public static extern unsafe Result DebugMarkerSetObjectTagExt(Device device,
            DebugMarkerObjectTagInfoExt* pTagInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdDebugMarkerBeginEXT")]
        public static extern unsafe void CmdDebugMarkerBeginExt(CommandBuffer commandBuffer,
            DebugMarkerMarkerInfoExt* pMarkerInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdDebugMarkerEndEXT")]
        public static extern unsafe void CmdDebugMarkerEndExt(CommandBuffer commandBuffer);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdDebugMarkerInsertEXT")]
        public static extern unsafe void CmdDebugMarkerInsertExt(CommandBuffer commandBuffer,
            DebugMarkerMarkerInfoExt* pMarkerInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceExternalImageFormatPropertiesNV")]
        public static extern unsafe Result GetPhysicalDeviceExternalImageFormatPropertiesNv(
            PhysicalDevice physicalDevice, Format format, ImageType type, ImageTiling tiling, ImageUsageFlags usage,
            ImageCreateFlags flags, ExternalMemoryHandleTypeFlagsNv externalHandleType,
            ExternalImageFormatPropertiesNv* pExternalImageFormatProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetMemoryWin32HandleNV")]
        public static extern unsafe Result GetMemoryWin32HandleNv(Device device, DeviceMemory memory,
            ExternalMemoryHandleTypeFlagsNv handleType, IntPtr pHandle);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdExecuteGeneratedCommandsNV")]
        public static extern unsafe void CmdExecuteGeneratedCommandsNv(CommandBuffer commandBuffer, uint isPreprocessed,
            GeneratedCommandsInfoNv* pGeneratedCommandsInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdPreprocessGeneratedCommandsNV")]
        public static extern unsafe void CmdPreprocessGeneratedCommandsNv(CommandBuffer commandBuffer,
            GeneratedCommandsInfoNv* pGeneratedCommandsInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBindPipelineShaderGroupNV")]
        public static extern unsafe void CmdBindPipelineShaderGroupNv(CommandBuffer commandBuffer,
            PipelineBindPoint pipelineBindPoint, Pipeline pipeline, uint groupIndex);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetGeneratedCommandsMemoryRequirementsNV")]
        public static extern unsafe void GetGeneratedCommandsMemoryRequirementsNv(Device device,
            GeneratedCommandsMemoryRequirementsInfoNv* pInfo, MemoryRequirements2* pMemoryRequirements);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateIndirectCommandsLayoutNV")]
        public static extern unsafe Result CreateIndirectCommandsLayoutNv(Device device,
            IndirectCommandsLayoutCreateInfoNv* pCreateInfo, AllocationCallbacks* pAllocator,
            IndirectCommandsLayoutNv* pIndirectCommandsLayout);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyIndirectCommandsLayoutNV")]
        public static extern unsafe void DestroyIndirectCommandsLayoutNv(Device device,
            IndirectCommandsLayoutNv indirectCommandsLayout, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceFeatures2")]
        public static extern unsafe void GetPhysicalDeviceFeatures2(PhysicalDevice physicalDevice,
            PhysicalDeviceFeatures2* pFeatures);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceProperties2")]
        public static extern unsafe void GetPhysicalDeviceProperties2(PhysicalDevice physicalDevice,
            PhysicalDeviceProperties2* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceFormatProperties2")]
        public static extern unsafe void GetPhysicalDeviceFormatProperties2(PhysicalDevice physicalDevice,
            Format format, FormatProperties2* pFormatProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceImageFormatProperties2")]
        public static extern unsafe Result GetPhysicalDeviceImageFormatProperties2(PhysicalDevice physicalDevice,
            PhysicalDeviceImageFormatInfo2* pImageFormatInfo, ImageFormatProperties2* pImageFormatProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceQueueFamilyProperties2")]
        public static extern unsafe void GetPhysicalDeviceQueueFamilyProperties2(PhysicalDevice physicalDevice,
            uint* pQueueFamilyPropertyCount, QueueFamilyProperties2* pQueueFamilyProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceMemoryProperties2")]
        public static extern unsafe void GetPhysicalDeviceMemoryProperties2(PhysicalDevice physicalDevice,
            PhysicalDeviceMemoryProperties2* pMemoryProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSparseImageFormatProperties2")]
        public static extern unsafe void GetPhysicalDeviceSparseImageFormatProperties2(PhysicalDevice physicalDevice,
            PhysicalDeviceSparseImageFormatInfo2* pFormatInfo, uint* pPropertyCount,
            SparseImageFormatProperties2* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdPushDescriptorSetKHR")]
        public static extern unsafe void CmdPushDescriptorSetKhr(CommandBuffer commandBuffer,
            PipelineBindPoint pipelineBindPoint, PipelineLayout layout, uint set, uint descriptorWriteCount,
            WriteDescriptorSet* pDescriptorWrites);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkTrimCommandPool")]
        public static extern unsafe void TrimCommandPool(Device device, CommandPool commandPool, uint flags);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceExternalBufferProperties")]
        public static extern unsafe void GetPhysicalDeviceExternalBufferProperties(PhysicalDevice physicalDevice,
            PhysicalDeviceExternalBufferInfo* pExternalBufferInfo, ExternalBufferProperties* pExternalBufferProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetMemoryWin32HandleKHR")]
        public static extern unsafe Result GetMemoryWin32HandleKhr(Device device,
            MemoryGetWin32HandleInfoKhr* pGetWin32HandleInfo, IntPtr pHandle);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetMemoryWin32HandlePropertiesKHR")]
        public static extern unsafe Result GetMemoryWin32HandlePropertiesKhr(Device device,
            ExternalMemoryHandleTypeFlags handleType, IntPtr handle,
            MemoryWin32HandlePropertiesKhr* pMemoryWin32HandleProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetMemoryFdKHR")]
        public static extern unsafe Result GetMemoryFdKhr(Device device, MemoryGetFdInfoKhr* pGetFdInfo, int* pFd);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetMemoryFdPropertiesKHR")]
        public static extern unsafe Result GetMemoryFdPropertiesKhr(Device device,
            ExternalMemoryHandleTypeFlags handleType, int fd, MemoryFdPropertiesKhr* pMemoryFdProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceExternalSemaphoreProperties")]
        public static extern unsafe void GetPhysicalDeviceExternalSemaphoreProperties(PhysicalDevice physicalDevice,
            PhysicalDeviceExternalSemaphoreInfo* pExternalSemaphoreInfo,
            ExternalSemaphoreProperties* pExternalSemaphoreProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetSemaphoreWin32HandleKHR")]
        public static extern unsafe Result GetSemaphoreWin32HandleKhr(Device device,
            SemaphoreGetWin32HandleInfoKhr* pGetWin32HandleInfo, IntPtr pHandle);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkImportSemaphoreWin32HandleKHR")]
        public static extern unsafe Result ImportSemaphoreWin32HandleKhr(Device device,
            ImportSemaphoreWin32HandleInfoKhr* pImportSemaphoreWin32HandleInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetSemaphoreFdKHR")]
        public static extern unsafe Result GetSemaphoreFdKhr(Device device, SemaphoreGetFdInfoKhr* pGetFdInfo,
            int* pFd);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkImportSemaphoreFdKHR")]
        public static extern unsafe Result ImportSemaphoreFdKhr(Device device,
            ImportSemaphoreFdInfoKhr* pImportSemaphoreFdInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceExternalFenceProperties")]
        public static extern unsafe void GetPhysicalDeviceExternalFenceProperties(PhysicalDevice physicalDevice,
            PhysicalDeviceExternalFenceInfo* pExternalFenceInfo, ExternalFenceProperties* pExternalFenceProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetFenceWin32HandleKHR")]
        public static extern unsafe Result GetFenceWin32HandleKhr(Device device,
            FenceGetWin32HandleInfoKhr* pGetWin32HandleInfo, IntPtr pHandle);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkImportFenceWin32HandleKHR")]
        public static extern unsafe Result ImportFenceWin32HandleKhr(Device device,
            ImportFenceWin32HandleInfoKhr* pImportFenceWin32HandleInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetFenceFdKHR")]
        public static extern unsafe Result GetFenceFdKhr(Device device, FenceGetFdInfoKhr* pGetFdInfo, int* pFd);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkImportFenceFdKHR")]
        public static extern unsafe Result ImportFenceFdKhr(Device device, ImportFenceFdInfoKhr* pImportFenceFdInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkReleaseDisplayEXT")]
        public static extern unsafe Result ReleaseDisplayExt(PhysicalDevice physicalDevice, DisplayKhr display);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkAcquireXlibDisplayEXT")]
        public static extern unsafe Result AcquireXlibDisplayExt(PhysicalDevice physicalDevice, IntPtr dpy,
            DisplayKhr display);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetRandROutputDisplayEXT")]
        public static extern unsafe Result GetRandROutputDisplayExt(PhysicalDevice physicalDevice, IntPtr dpy,
            IntPtr rrOutput, DisplayKhr* pDisplay);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDisplayPowerControlEXT")]
        public static extern unsafe Result DisplayPowerControlExt(Device device, DisplayKhr display,
            DisplayPowerInfoExt* pDisplayPowerInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkRegisterDeviceEventEXT")]
        public static extern unsafe Result RegisterDeviceEventExt(Device device, DeviceEventInfoExt* pDeviceEventInfo,
            AllocationCallbacks* pAllocator, Fence* pFence);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkRegisterDisplayEventEXT")]
        public static extern unsafe Result RegisterDisplayEventExt(Device device, DisplayKhr display,
            DisplayEventInfoExt* pDisplayEventInfo, AllocationCallbacks* pAllocator, Fence* pFence);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetSwapchainCounterEXT")]
        public static extern unsafe Result GetSwapchainCounterExt(Device device, SwapchainKhr swapchain,
            SurfaceCounterFlagsExt counter, ulong* pCounterValue);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSurfaceCapabilities2EXT")]
        public static extern unsafe Result GetPhysicalDeviceSurfaceCapabilities2ext(PhysicalDevice physicalDevice,
            SurfaceKhr surface, SurfaceCapabilities2ext* pSurfaceCapabilities);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkEnumeratePhysicalDeviceGroups")]
        public static extern unsafe Result EnumeratePhysicalDeviceGroups(Instance instance,
            uint* pPhysicalDeviceGroupCount, PhysicalDeviceGroupProperties* pPhysicalDeviceGroupProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDeviceGroupPeerMemoryFeatures")]
        public static extern unsafe void GetDeviceGroupPeerMemoryFeatures(Device device, uint heapIndex,
            uint localDeviceIndex, uint remoteDeviceIndex, PeerMemoryFeatureFlags* pPeerMemoryFeatures);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkBindBufferMemory2")]
        public static extern unsafe Result BindBufferMemory2(Device device, uint bindInfoCount,
            BindBufferMemoryInfo* pBindInfos);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkBindImageMemory2")]
        public static extern unsafe Result BindImageMemory2(Device device, uint bindInfoCount,
            BindImageMemoryInfo* pBindInfos);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetDeviceMask")]
        public static extern unsafe void CmdSetDeviceMask(CommandBuffer commandBuffer, uint deviceMask);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDeviceGroupPresentCapabilitiesKHR")]
        public static extern unsafe Result GetDeviceGroupPresentCapabilitiesKhr(Device device,
            DeviceGroupPresentCapabilitiesKhr* pDeviceGroupPresentCapabilities);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDeviceGroupSurfacePresentModesKHR")]
        public static extern unsafe Result GetDeviceGroupSurfacePresentModesKhr(Device device, SurfaceKhr surface,
            DeviceGroupPresentModeFlagsKhr* pModes);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkAcquireNextImage2KHR")]
        public static extern unsafe Result AcquireNextImage2khr(Device device, AcquireNextImageInfoKhr* pAcquireInfo,
            uint* pImageIndex);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdDispatchBase")]
        public static extern unsafe void CmdDispatchBase(CommandBuffer commandBuffer, uint baseGroupX, uint baseGroupY,
            uint baseGroupZ, uint groupCountX, uint groupCountY, uint groupCountZ);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDevicePresentRectanglesKHR")]
        public static extern unsafe Result GetPhysicalDevicePresentRectanglesKhr(PhysicalDevice physicalDevice,
            SurfaceKhr surface, uint* pRectCount, Rect2d* pRects);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateDescriptorUpdateTemplate")]
        public static extern unsafe Result CreateDescriptorUpdateTemplate(Device device,
            DescriptorUpdateTemplateCreateInfo* pCreateInfo, AllocationCallbacks* pAllocator,
            DescriptorUpdateTemplate* pDescriptorUpdateTemplate);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyDescriptorUpdateTemplate")]
        public static extern unsafe void DestroyDescriptorUpdateTemplate(Device device,
            DescriptorUpdateTemplate descriptorUpdateTemplate, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkUpdateDescriptorSetWithTemplate")]
        public static extern unsafe void UpdateDescriptorSetWithTemplate(Device device, DescriptorSet descriptorSet,
            DescriptorUpdateTemplate descriptorUpdateTemplate, void* pData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdPushDescriptorSetWithTemplateKHR")]
        public static extern unsafe void CmdPushDescriptorSetWithTemplateKhr(CommandBuffer commandBuffer,
            DescriptorUpdateTemplate descriptorUpdateTemplate, PipelineLayout layout, uint set, void* pData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkSetHdrMetadataEXT")]
        public static extern unsafe void SetHdrMetadataExt(Device device, uint swapchainCount,
            SwapchainKhr* pSwapchains, HdrMetadataExt* pMetadata);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetSwapchainStatusKHR")]
        public static extern unsafe Result GetSwapchainStatusKhr(Device device, SwapchainKhr swapchain);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetRefreshCycleDurationGOOGLE")]
        public static extern unsafe Result GetRefreshCycleDurationGoogle(Device device, SwapchainKhr swapchain,
            RefreshCycleDurationGoogle* pDisplayTimingProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPastPresentationTimingGOOGLE")]
        public static extern unsafe Result GetPastPresentationTimingGoogle(Device device, SwapchainKhr swapchain,
            uint* pPresentationTimingCount, PastPresentationTimingGoogle* pPresentationTimings);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateIOSSurfaceMVK")]
        public static extern unsafe Result CreateIOSSurfaceMvk(Instance instance, IOSSurfaceCreateInfoMvk* pCreateInfo,
            AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateMacOSSurfaceMVK")]
        public static extern unsafe Result CreateMacOSSurfaceMvk(Instance instance,
            MacOSSurfaceCreateInfoMvk* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateMetalSurfaceEXT")]
        public static extern unsafe Result CreateMetalSurfaceExt(Instance instance,
            MetalSurfaceCreateInfoExt* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetViewportWScalingNV")]
        public static extern unsafe void CmdSetViewportWScalingNv(CommandBuffer commandBuffer, uint firstViewport,
            uint viewportCount, ViewportWScalingNv* pViewportWScalings);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetDiscardRectangleEXT")]
        public static extern unsafe void CmdSetDiscardRectangleExt(CommandBuffer commandBuffer,
            uint firstDiscardRectangle, uint discardRectangleCount, Rect2d* pDiscardRectangles);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetSampleLocationsEXT")]
        public static extern unsafe void CmdSetSampleLocationsExt(CommandBuffer commandBuffer,
            SampleLocationsInfoExt* pSampleLocationsInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceMultisamplePropertiesEXT")]
        public static extern unsafe void GetPhysicalDeviceMultisamplePropertiesExt(PhysicalDevice physicalDevice,
            SampleCountFlags samples, MultisamplePropertiesExt* pMultisampleProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSurfaceCapabilities2KHR")]
        public static extern unsafe Result GetPhysicalDeviceSurfaceCapabilities2khr(PhysicalDevice physicalDevice,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo, SurfaceCapabilities2khr* pSurfaceCapabilities);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSurfaceFormats2KHR")]
        public static extern unsafe Result GetPhysicalDeviceSurfaceFormats2khr(PhysicalDevice physicalDevice,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo, uint* pSurfaceFormatCount, SurfaceFormat2khr* pSurfaceFormats);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceDisplayProperties2KHR")]
        public static extern unsafe Result GetPhysicalDeviceDisplayProperties2khr(PhysicalDevice physicalDevice,
            uint* pPropertyCount, DisplayProperties2khr* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceDisplayPlaneProperties2KHR")]
        public static extern unsafe Result GetPhysicalDeviceDisplayPlaneProperties2khr(PhysicalDevice physicalDevice,
            uint* pPropertyCount, DisplayPlaneProperties2khr* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDisplayModeProperties2KHR")]
        public static extern unsafe Result GetDisplayModeProperties2khr(PhysicalDevice physicalDevice,
            DisplayKhr display, uint* pPropertyCount, DisplayModeProperties2khr* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDisplayPlaneCapabilities2KHR")]
        public static extern unsafe Result GetDisplayPlaneCapabilities2khr(PhysicalDevice physicalDevice,
            DisplayPlaneInfo2khr* pDisplayPlaneInfo, DisplayPlaneCapabilities2khr* pCapabilities);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetBufferMemoryRequirements2")]
        public static extern unsafe void GetBufferMemoryRequirements2(Device device,
            BufferMemoryRequirementsInfo2* pInfo, MemoryRequirements2* pMemoryRequirements);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetImageMemoryRequirements2")]
        public static extern unsafe void GetImageMemoryRequirements2(Device device, ImageMemoryRequirementsInfo2* pInfo,
            MemoryRequirements2* pMemoryRequirements);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetImageSparseMemoryRequirements2")]
        public static extern unsafe void GetImageSparseMemoryRequirements2(Device device,
            ImageSparseMemoryRequirementsInfo2* pInfo, uint* pSparseMemoryRequirementCount,
            SparseImageMemoryRequirements2* pSparseMemoryRequirements);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateSamplerYcbcrConversion")]
        public static extern unsafe Result CreateSamplerYcbcrConversion(Device device,
            SamplerYcbcrConversionCreateInfo* pCreateInfo, AllocationCallbacks* pAllocator,
            SamplerYcbcrConversion* pYcbcrConversion);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroySamplerYcbcrConversion")]
        public static extern unsafe void DestroySamplerYcbcrConversion(Device device,
            SamplerYcbcrConversion ycbcrConversion, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetDeviceQueue2")]
        public static extern unsafe void GetDeviceQueue2(Device device, DeviceQueueInfo2* pQueueInfo, Queue* pQueue);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateValidationCacheEXT")]
        public static extern unsafe Result CreateValidationCacheExt(Device device,
            ValidationCacheCreateInfoExt* pCreateInfo, AllocationCallbacks* pAllocator,
            ValidationCacheExt* pValidationCache);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyValidationCacheEXT")]
        public static extern unsafe void DestroyValidationCacheExt(Device device, ValidationCacheExt validationCache,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetValidationCacheDataEXT")]
        public static extern unsafe Result GetValidationCacheDataExt(Device device, ValidationCacheExt validationCache,
            IntPtr* pDataSize, void* pData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkMergeValidationCachesEXT")]
        public static extern unsafe Result MergeValidationCachesExt(Device device, ValidationCacheExt dstCache,
            uint srcCacheCount, ValidationCacheExt* pSrcCaches);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDescriptorSetLayoutSupport")]
        public static extern unsafe void GetDescriptorSetLayoutSupport(Device device,
            DescriptorSetLayoutCreateInfo* pCreateInfo, DescriptorSetLayoutSupport* pSupport);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetSwapchainGrallocUsageANDROID")]
        public static extern unsafe Result GetSwapchainGrallocUsageAndroid(Device device, Format format,
            ImageUsageFlags imageUsage, IntPtr grallocUsage);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetSwapchainGrallocUsage2ANDROID")]
        public static extern unsafe Result GetSwapchainGrallocUsage2android(Device device, Format format,
            ImageUsageFlags imageUsage, SwapchainImageUsageFlagsAndroid swapchainImageUsage,
            ulong* grallocConsumerUsage, ulong* grallocProducerUsage);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkAcquireImageANDROID")]
        public static extern unsafe Result AcquireImageAndroid(Device device, Image image, IntPtr nativeFenceFd,
            Semaphore semaphore, Fence fence);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkQueueSignalReleaseImageANDROID")]
        public static extern unsafe Result QueueSignalReleaseImageAndroid(Queue queue, uint waitSemaphoreCount,
            Semaphore* pWaitSemaphores, Image image, IntPtr pNativeFenceFd);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetShaderInfoAMD")]
        public static extern unsafe Result GetShaderInfoAmd(Device device, Pipeline pipeline,
            ShaderStageFlags shaderStage, ShaderInfoTypeAmd infoType, IntPtr* pInfoSize, void* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkSetLocalDimmingAMD")]
        public static extern unsafe void SetLocalDimmingAmd(Device device, SwapchainKhr swapChain,
            uint localDimmingEnable);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceCalibrateableTimeDomainsEXT")]
        public static extern unsafe Result GetPhysicalDeviceCalibrateableTimeDomainsExt(PhysicalDevice physicalDevice,
            uint* pTimeDomainCount, TimeDomainExt* pTimeDomains);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetCalibratedTimestampsEXT")]
        public static extern unsafe Result GetCalibratedTimestampsExt(Device device, uint timestampCount,
            CalibratedTimestampInfoExt* pTimestampInfos, ulong* pTimestamps, ulong* pMaxDeviation);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkSetDebugUtilsObjectNameEXT")]
        public static extern unsafe Result SetDebugUtilsObjectNameExt(Device device,
            DebugUtilsObjectNameInfoExt* pNameInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkSetDebugUtilsObjectTagEXT")]
        public static extern unsafe Result SetDebugUtilsObjectTagExt(Device device,
            DebugUtilsObjectTagInfoExt* pTagInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkQueueBeginDebugUtilsLabelEXT")]
        public static extern unsafe void QueueBeginDebugUtilsLabelExt(Queue queue, DebugUtilsLabelExt* pLabelInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkQueueEndDebugUtilsLabelEXT")]
        public static extern unsafe void QueueEndDebugUtilsLabelExt(Queue queue);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkQueueInsertDebugUtilsLabelEXT")]
        public static extern unsafe void QueueInsertDebugUtilsLabelExt(Queue queue, DebugUtilsLabelExt* pLabelInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBeginDebugUtilsLabelEXT")]
        public static extern unsafe void CmdBeginDebugUtilsLabelExt(CommandBuffer commandBuffer,
            DebugUtilsLabelExt* pLabelInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdEndDebugUtilsLabelEXT")]
        public static extern unsafe void CmdEndDebugUtilsLabelExt(CommandBuffer commandBuffer);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdInsertDebugUtilsLabelEXT")]
        public static extern unsafe void CmdInsertDebugUtilsLabelExt(CommandBuffer commandBuffer,
            DebugUtilsLabelExt* pLabelInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateDebugUtilsMessengerEXT")]
        public static extern unsafe Result CreateDebugUtilsMessengerExt(Instance instance,
            DebugUtilsMessengerCreateInfoExt* pCreateInfo, AllocationCallbacks* pAllocator,
            DebugUtilsMessengerExt* pMessenger);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyDebugUtilsMessengerEXT")]
        public static extern unsafe void DestroyDebugUtilsMessengerExt(Instance instance,
            DebugUtilsMessengerExt messenger, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkSubmitDebugUtilsMessageEXT")]
        public static extern unsafe void SubmitDebugUtilsMessageExt(Instance instance,
            DebugUtilsMessageSeverityFlagsExt messageSeverity, DebugUtilsMessageTypeFlagsExt messageTypes,
            DebugUtilsMessengerCallbackDataExt* pCallbackData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetMemoryHostPointerPropertiesEXT")]
        public static extern unsafe Result GetMemoryHostPointerPropertiesExt(Device device,
            ExternalMemoryHandleTypeFlags handleType, void* pHostPointer,
            MemoryHostPointerPropertiesExt* pMemoryHostPointerProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdWriteBufferMarkerAMD")]
        public static extern unsafe void CmdWriteBufferMarkerAmd(CommandBuffer commandBuffer,
            PipelineStageFlags pipelineStage, Buffer dstBuffer, ulong dstOffset, uint marker);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCreateRenderPass2")]
        public static extern unsafe Result CreateRenderPass2(Device device, RenderPassCreateInfo2* pCreateInfo,
            AllocationCallbacks* pAllocator, RenderPass* pRenderPass);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdBeginRenderPass2")]
        public static extern unsafe void CmdBeginRenderPass2(CommandBuffer commandBuffer,
            RenderPassBeginInfo* pRenderPassBegin, SubpassBeginInfo* pSubpassBeginInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdNextSubpass2")]
        public static extern unsafe void CmdNextSubpass2(CommandBuffer commandBuffer,
            SubpassBeginInfo* pSubpassBeginInfo, SubpassEndInfo* pSubpassEndInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdEndRenderPass2")]
        public static extern unsafe void
            CmdEndRenderPass2(CommandBuffer commandBuffer, SubpassEndInfo* pSubpassEndInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetSemaphoreCounterValue")]
        public static extern unsafe Result GetSemaphoreCounterValue(Device device, Semaphore semaphore, ulong* pValue);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkWaitSemaphores")]
        public static extern unsafe Result WaitSemaphores(Device device, SemaphoreWaitInfo* pWaitInfo, ulong timeout);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkSignalSemaphore")]
        public static extern unsafe Result SignalSemaphore(Device device, SemaphoreSignalInfo* pSignalInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetAndroidHardwareBufferPropertiesANDROID")]
        public static extern unsafe Result GetAndroidHardwareBufferPropertiesAndroid(Device device, IntPtr buffer,
            AndroidHardwareBufferPropertiesAndroid* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetMemoryAndroidHardwareBufferANDROID")]
        public static extern unsafe Result GetMemoryAndroidHardwareBufferAndroid(Device device,
            MemoryGetAndroidHardwareBufferInfoAndroid* pInfo, IntPtr pBuffer);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdDrawIndirectCount")]
        public static extern unsafe void CmdDrawIndirectCount(CommandBuffer commandBuffer, Buffer buffer, ulong offset,
            Buffer countBuffer, ulong countBufferOffset, uint maxDrawCount, uint stride);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdDrawIndexedIndirectCount")]
        public static extern unsafe void CmdDrawIndexedIndirectCount(CommandBuffer commandBuffer, Buffer buffer,
            ulong offset, Buffer countBuffer, ulong countBufferOffset, uint maxDrawCount, uint stride);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetCheckpointNV")]
        public static extern unsafe void CmdSetCheckpointNv(CommandBuffer commandBuffer, void* pCheckpointMarker);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetQueueCheckpointDataNV")]
        public static extern unsafe void GetQueueCheckpointDataNv(Queue queue, uint* pCheckpointDataCount,
            CheckpointDataNv* pCheckpointData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBindTransformFeedbackBuffersEXT")]
        public static extern unsafe void CmdBindTransformFeedbackBuffersExt(CommandBuffer commandBuffer,
            uint firstBinding, uint bindingCount, Buffer* pBuffers, ulong* pOffsets, ulong* pSizes);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBeginTransformFeedbackEXT")]
        public static extern unsafe void CmdBeginTransformFeedbackExt(CommandBuffer commandBuffer,
            uint firstCounterBuffer, uint counterBufferCount, Buffer* pCounterBuffers, ulong* pCounterBufferOffsets);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdEndTransformFeedbackEXT")]
        public static extern unsafe void CmdEndTransformFeedbackExt(CommandBuffer commandBuffer,
            uint firstCounterBuffer, uint counterBufferCount, Buffer* pCounterBuffers, ulong* pCounterBufferOffsets);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBeginQueryIndexedEXT")]
        public static extern unsafe void CmdBeginQueryIndexedExt(CommandBuffer commandBuffer, QueryPool queryPool,
            uint query, QueryControlFlags flags, uint index);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdEndQueryIndexedEXT")]
        public static extern unsafe void CmdEndQueryIndexedExt(CommandBuffer commandBuffer, QueryPool queryPool,
            uint query, uint index);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdDrawIndirectByteCountEXT")]
        public static extern unsafe void CmdDrawIndirectByteCountExt(CommandBuffer commandBuffer, uint instanceCount,
            uint firstInstance, Buffer counterBuffer, ulong counterBufferOffset, uint counterOffset, uint vertexStride);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetExclusiveScissorNV")]
        public static extern unsafe void CmdSetExclusiveScissorNv(CommandBuffer commandBuffer,
            uint firstExclusiveScissor, uint exclusiveScissorCount, Rect2d* pExclusiveScissors);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBindShadingRateImageNV")]
        public static extern unsafe void CmdBindShadingRateImageNv(CommandBuffer commandBuffer, ImageView imageView,
            ImageLayout imageLayout);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetViewportShadingRatePaletteNV")]
        public static extern unsafe void CmdSetViewportShadingRatePaletteNv(CommandBuffer commandBuffer,
            uint firstViewport, uint viewportCount, ShadingRatePaletteNv* pShadingRatePalettes);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetCoarseSampleOrderNV")]
        public static extern unsafe void CmdSetCoarseSampleOrderNv(CommandBuffer commandBuffer,
            CoarseSampleOrderTypeNv sampleOrderType, uint customSampleOrderCount,
            CoarseSampleOrderCustomNv* pCustomSampleOrders);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdDrawMeshTasksNV")]
        public static extern unsafe void
            CmdDrawMeshTasksNv(CommandBuffer commandBuffer, uint taskCount, uint firstTask);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdDrawMeshTasksIndirectNV")]
        public static extern unsafe void CmdDrawMeshTasksIndirectNv(CommandBuffer commandBuffer, Buffer buffer,
            ulong offset, uint drawCount, uint stride);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdDrawMeshTasksIndirectCountNV")]
        public static extern unsafe void CmdDrawMeshTasksIndirectCountNv(CommandBuffer commandBuffer, Buffer buffer,
            ulong offset, Buffer countBuffer, ulong countBufferOffset, uint maxDrawCount, uint stride);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCompileDeferredNV")]
        public static extern unsafe Result CompileDeferredNv(Device device, Pipeline pipeline, uint shader);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateAccelerationStructureNV")]
        public static extern unsafe Result CreateAccelerationStructureNv(Device device,
            AccelerationStructureCreateInfoNv* pCreateInfo, AllocationCallbacks* pAllocator,
            AccelerationStructureKhr* pAccelerationStructure);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyAccelerationStructureKHR")]
        public static extern unsafe void DestroyAccelerationStructureKhr(Device device,
            AccelerationStructureKhr accelerationStructure, AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetAccelerationStructureMemoryRequirementsKHR")]
        public static extern unsafe void GetAccelerationStructureMemoryRequirementsKhr(Device device,
            AccelerationStructureMemoryRequirementsInfoKhr* pInfo, MemoryRequirements2* pMemoryRequirements);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetAccelerationStructureMemoryRequirementsNV")]
        public static extern unsafe void GetAccelerationStructureMemoryRequirementsNv(Device device,
            AccelerationStructureMemoryRequirementsInfoNv* pInfo, MemoryRequirements2khr* pMemoryRequirements);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkBindAccelerationStructureMemoryKHR")]
        public static extern unsafe Result BindAccelerationStructureMemoryKhr(Device device, uint bindInfoCount,
            BindAccelerationStructureMemoryInfoKhr* pBindInfos);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdCopyAccelerationStructureNV")]
        public static extern unsafe void CmdCopyAccelerationStructureNv(CommandBuffer commandBuffer,
            AccelerationStructureKhr dst, AccelerationStructureKhr src, CopyAccelerationStructureModeKhr mode);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdCopyAccelerationStructureKHR")]
        public static extern unsafe void CmdCopyAccelerationStructureKhr(CommandBuffer commandBuffer,
            CopyAccelerationStructureInfoKhr* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCopyAccelerationStructureKHR")]
        public static extern unsafe Result CopyAccelerationStructureKhr(Device device,
            CopyAccelerationStructureInfoKhr* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdCopyAccelerationStructureToMemoryKHR")]
        public static extern unsafe void CmdCopyAccelerationStructureToMemoryKhr(CommandBuffer commandBuffer,
            CopyAccelerationStructureToMemoryInfoKhr* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCopyAccelerationStructureToMemoryKHR")]
        public static extern unsafe Result CopyAccelerationStructureToMemoryKhr(Device device,
            CopyAccelerationStructureToMemoryInfoKhr* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdCopyMemoryToAccelerationStructureKHR")]
        public static extern unsafe void CmdCopyMemoryToAccelerationStructureKhr(CommandBuffer commandBuffer,
            CopyMemoryToAccelerationStructureInfoKhr* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCopyMemoryToAccelerationStructureKHR")]
        public static extern unsafe Result CopyMemoryToAccelerationStructureKhr(Device device,
            CopyMemoryToAccelerationStructureInfoKhr* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdWriteAccelerationStructuresPropertiesKHR")]
        public static extern unsafe void CmdWriteAccelerationStructuresPropertiesKhr(CommandBuffer commandBuffer,
            uint accelerationStructureCount, AccelerationStructureKhr* pAccelerationStructures, QueryType queryType,
            QueryPool queryPool, uint firstQuery);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBuildAccelerationStructureNV")]
        public static extern unsafe void CmdBuildAccelerationStructureNv(CommandBuffer commandBuffer,
            AccelerationStructureInfoNv* pInfo, Buffer instanceData, ulong instanceOffset, uint update,
            AccelerationStructureKhr dst, AccelerationStructureKhr src, Buffer scratch, ulong scratchOffset);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkWriteAccelerationStructuresPropertiesKHR")]
        public static extern unsafe Result WriteAccelerationStructuresPropertiesKhr(Device device,
            uint accelerationStructureCount, AccelerationStructureKhr* pAccelerationStructures, QueryType queryType,
            IntPtr dataSize, void* pData, IntPtr stride);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdTraceRaysKHR")]
        public static extern unsafe void CmdTraceRaysKhr(CommandBuffer commandBuffer,
            StridedBufferRegionKhr* pRaygenShaderBindingTable, StridedBufferRegionKhr* pMissShaderBindingTable,
            StridedBufferRegionKhr* pHitShaderBindingTable, StridedBufferRegionKhr* pCallableShaderBindingTable,
            uint width, uint height, uint depth);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdTraceRaysNV")]
        public static extern unsafe void CmdTraceRaysNv(CommandBuffer commandBuffer,
            Buffer raygenShaderBindingTableBuffer, ulong raygenShaderBindingOffset, Buffer missShaderBindingTableBuffer,
            ulong missShaderBindingOffset, ulong missShaderBindingStride, Buffer hitShaderBindingTableBuffer,
            ulong hitShaderBindingOffset, ulong hitShaderBindingStride, Buffer callableShaderBindingTableBuffer,
            ulong callableShaderBindingOffset, ulong callableShaderBindingStride, uint width, uint height, uint depth);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetRayTracingShaderGroupHandlesKHR")]
        public static extern unsafe Result GetRayTracingShaderGroupHandlesKhr(Device device, Pipeline pipeline,
            uint firstGroup, uint groupCount, IntPtr dataSize, void* pData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetRayTracingCaptureReplayShaderGroupHandlesKHR")]
        public static extern unsafe Result GetRayTracingCaptureReplayShaderGroupHandlesKhr(Device device,
            Pipeline pipeline, uint firstGroup, uint groupCount, IntPtr dataSize, void* pData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetAccelerationStructureHandleNV")]
        public static extern unsafe Result GetAccelerationStructureHandleNv(Device device,
            AccelerationStructureKhr accelerationStructure, IntPtr dataSize, void* pData);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateRayTracingPipelinesNV")]
        public static extern unsafe Result CreateRayTracingPipelinesNv(Device device, PipelineCache pipelineCache,
            uint createInfoCount, RayTracingPipelineCreateInfoNv* pCreateInfos, AllocationCallbacks* pAllocator,
            Pipeline* pPipelines);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateRayTracingPipelinesKHR")]
        public static extern unsafe Result CreateRayTracingPipelinesKhr(Device device, PipelineCache pipelineCache,
            uint createInfoCount, RayTracingPipelineCreateInfoKhr* pCreateInfos, AllocationCallbacks* pAllocator,
            Pipeline* pPipelines);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceCooperativeMatrixPropertiesNV")]
        public static extern unsafe Result GetPhysicalDeviceCooperativeMatrixPropertiesNv(PhysicalDevice physicalDevice,
            uint* pPropertyCount, CooperativeMatrixPropertiesNv* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdTraceRaysIndirectKHR")]
        public static extern unsafe void CmdTraceRaysIndirectKhr(CommandBuffer commandBuffer,
            StridedBufferRegionKhr* pRaygenShaderBindingTable, StridedBufferRegionKhr* pMissShaderBindingTable,
            StridedBufferRegionKhr* pHitShaderBindingTable, StridedBufferRegionKhr* pCallableShaderBindingTable,
            Buffer buffer, ulong offset);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDeviceAccelerationStructureCompatibilityKHR")]
        public static extern unsafe Result GetDeviceAccelerationStructureCompatibilityKhr(Device device,
            AccelerationStructureVersionKhr* version);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetImageViewHandleNVX")]
        public static extern unsafe uint GetImageViewHandleNvx(Device device, ImageViewHandleInfoNvx* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetImageViewAddressNVX")]
        public static extern unsafe Result GetImageViewAddressNvx(Device device, ImageView imageView,
            ImageViewAddressPropertiesNvx* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSurfacePresentModes2EXT")]
        public static extern unsafe Result GetPhysicalDeviceSurfacePresentModes2ext(PhysicalDevice physicalDevice,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo, uint* pPresentModeCount, PresentModeKhr* pPresentModes);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDeviceGroupSurfacePresentModes2EXT")]
        public static extern unsafe Result GetDeviceGroupSurfacePresentModes2ext(Device device,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo, DeviceGroupPresentModeFlagsKhr* pModes);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkAcquireFullScreenExclusiveModeEXT")]
        public static extern unsafe Result AcquireFullScreenExclusiveModeExt(Device device, SwapchainKhr swapchain);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkReleaseFullScreenExclusiveModeEXT")]
        public static extern unsafe Result ReleaseFullScreenExclusiveModeExt(Device device, SwapchainKhr swapchain);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR")]
        public static extern unsafe Result EnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKhr(
            PhysicalDevice physicalDevice, uint queueFamilyIndex, uint* pCounterCount, PerformanceCounterKhr* pCounters,
            PerformanceCounterDescriptionKhr* pCounterDescriptions);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR")]
        public static extern unsafe void GetPhysicalDeviceQueueFamilyPerformanceQueryPassesKhr(
            PhysicalDevice physicalDevice, QueryPoolPerformanceCreateInfoKhr* pPerformanceQueryCreateInfo,
            uint* pNumPasses);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkAcquireProfilingLockKHR")]
        public static extern unsafe Result AcquireProfilingLockKhr(Device device, AcquireProfilingLockInfoKhr* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkReleaseProfilingLockKHR")]
        public static extern unsafe void ReleaseProfilingLockKhr(Device device);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetImageDrmFormatModifierPropertiesEXT")]
        public static extern unsafe Result GetImageDrmFormatModifierPropertiesExt(Device device, Image image,
            ImageDrmFormatModifierPropertiesExt* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetBufferOpaqueCaptureAddress")]
        public static extern unsafe ulong GetBufferOpaqueCaptureAddress(Device device, BufferDeviceAddressInfo* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetBufferDeviceAddress")]
        public static extern unsafe ulong GetBufferDeviceAddress(Device device, BufferDeviceAddressInfo* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateHeadlessSurfaceEXT")]
        public static extern unsafe Result CreateHeadlessSurfaceExt(Instance instance,
            HeadlessSurfaceCreateInfoExt* pCreateInfo, AllocationCallbacks* pAllocator, SurfaceKhr* pSurface);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV")]
        public static extern unsafe Result GetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNv(
            PhysicalDevice physicalDevice, uint* pCombinationCount,
            FramebufferMixedSamplesCombinationNv* pCombinations);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkInitializePerformanceApiINTEL")]
        public static extern unsafe Result InitializePerformanceApiIntel(Device device,
            InitializePerformanceApiInfoIntel* pInitializeInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkUninitializePerformanceApiINTEL")]
        public static extern unsafe void UninitializePerformanceApiIntel(Device device);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetPerformanceMarkerINTEL")]
        public static extern unsafe Result CmdSetPerformanceMarkerIntel(CommandBuffer commandBuffer,
            PerformanceMarkerInfoIntel* pMarkerInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetPerformanceStreamMarkerINTEL")]
        public static extern unsafe Result CmdSetPerformanceStreamMarkerIntel(CommandBuffer commandBuffer,
            PerformanceStreamMarkerInfoIntel* pMarkerInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetPerformanceOverrideINTEL")]
        public static extern unsafe Result CmdSetPerformanceOverrideIntel(CommandBuffer commandBuffer,
            PerformanceOverrideInfoIntel* pOverrideInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkAcquirePerformanceConfigurationINTEL")]
        public static extern unsafe Result AcquirePerformanceConfigurationIntel(Device device,
            PerformanceConfigurationAcquireInfoIntel* pAcquireInfo, PerformanceConfigurationIntel* pConfiguration);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkReleasePerformanceConfigurationINTEL")]
        public static extern unsafe Result ReleasePerformanceConfigurationIntel(Device device,
            PerformanceConfigurationIntel configuration);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkQueueSetPerformanceConfigurationINTEL")]
        public static extern unsafe Result QueueSetPerformanceConfigurationIntel(Queue queue,
            PerformanceConfigurationIntel configuration);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPerformanceParameterINTEL")]
        public static extern unsafe Result GetPerformanceParameterIntel(Device device,
            PerformanceParameterTypeIntel parameter, PerformanceValueIntel* pValue);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDeviceMemoryOpaqueCaptureAddress")]
        public static extern unsafe ulong GetDeviceMemoryOpaqueCaptureAddress(Device device,
            DeviceMemoryOpaqueCaptureAddressInfo* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPipelineExecutablePropertiesKHR")]
        public static extern unsafe Result GetPipelineExecutablePropertiesKhr(Device device,
            PipelineInfoKhr* pPipelineInfo, uint* pExecutableCount, PipelineExecutablePropertiesKhr* pProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPipelineExecutableStatisticsKHR")]
        public static extern unsafe Result GetPipelineExecutableStatisticsKhr(Device device,
            PipelineExecutableInfoKhr* pExecutableInfo, uint* pStatisticCount,
            PipelineExecutableStatisticKhr* pStatistics);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPipelineExecutableInternalRepresentationsKHR")]
        public static extern unsafe Result GetPipelineExecutableInternalRepresentationsKhr(Device device,
            PipelineExecutableInfoKhr* pExecutableInfo, uint* pInternalRepresentationCount,
            PipelineExecutableInternalRepresentationKhr* pInternalRepresentations);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetLineStippleEXT")]
        public static extern unsafe void CmdSetLineStippleExt(CommandBuffer commandBuffer, uint lineStippleFactor,
            ushort lineStipplePattern);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetPhysicalDeviceToolPropertiesEXT")]
        public static extern unsafe Result GetPhysicalDeviceToolPropertiesExt(PhysicalDevice physicalDevice,
            uint* pToolCount, PhysicalDeviceToolPropertiesExt* pToolProperties);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateAccelerationStructureKHR")]
        public static extern unsafe Result CreateAccelerationStructureKhr(Device device,
            AccelerationStructureCreateInfoKhr* pCreateInfo, AllocationCallbacks* pAllocator,
            AccelerationStructureKhr* pAccelerationStructure);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBuildAccelerationStructureKHR")]
        public static extern unsafe void CmdBuildAccelerationStructureKhr(CommandBuffer commandBuffer, uint infoCount,
            AccelerationStructureBuildGeometryInfoKhr* pInfos,
            AccelerationStructureBuildOffsetInfoKhr* ppOffsetInfos);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBuildAccelerationStructureIndirectKHR")]
        public static extern unsafe void CmdBuildAccelerationStructureIndirectKhr(CommandBuffer commandBuffer,
            AccelerationStructureBuildGeometryInfoKhr* pInfo, Buffer indirectBuffer, ulong indirectOffset,
            uint indirectStride);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkBuildAccelerationStructureKHR")]
        public static extern unsafe Result BuildAccelerationStructureKhr(Device device, uint infoCount,
            AccelerationStructureBuildGeometryInfoKhr* pInfos,
            AccelerationStructureBuildOffsetInfoKhr* ppOffsetInfos);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetAccelerationStructureDeviceAddressKHR")]
        public static extern unsafe ulong GetAccelerationStructureDeviceAddressKhr(Device device,
            AccelerationStructureDeviceAddressInfoKhr* pInfo);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreateDeferredOperationKHR")]
        public static extern unsafe Result CreateDeferredOperationKhr(Device device, AllocationCallbacks* pAllocator,
            DeferredOperationKhr* pDeferredOperation);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyDeferredOperationKHR")]
        public static extern unsafe void DestroyDeferredOperationKhr(Device device, DeferredOperationKhr operation,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDeferredOperationMaxConcurrencyKHR")]
        public static extern unsafe uint GetDeferredOperationMaxConcurrencyKhr(Device device,
            DeferredOperationKhr operation);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkGetDeferredOperationResultKHR")]
        public static extern unsafe Result GetDeferredOperationResultKhr(Device device, DeferredOperationKhr operation);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDeferredOperationJoinKHR")]
        public static extern unsafe Result DeferredOperationJoinKhr(Device device, DeferredOperationKhr operation);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetCullModeEXT")]
        public static extern unsafe void CmdSetCullModeExt(CommandBuffer commandBuffer, CullModeFlags cullMode);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetFrontFaceEXT")]
        public static extern unsafe void CmdSetFrontFaceExt(CommandBuffer commandBuffer, FrontFace frontFace);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetPrimitiveTopologyEXT")]
        public static extern unsafe void CmdSetPrimitiveTopologyExt(CommandBuffer commandBuffer,
            PrimitiveTopology primitiveTopology);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetViewportWithCountEXT")]
        public static extern unsafe void CmdSetViewportWithCountExt(CommandBuffer commandBuffer, uint viewportCount,
            Viewport* pViewports);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetScissorWithCountEXT")]
        public static extern unsafe void CmdSetScissorWithCountExt(CommandBuffer commandBuffer, uint scissorCount,
            Rect2d* pScissors);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdBindVertexBuffers2EXT")]
        public static extern unsafe void CmdBindVertexBuffers2ext(CommandBuffer commandBuffer, uint firstBinding,
            uint bindingCount, Buffer* pBuffers, ulong* pOffsets, ulong* pSizes, ulong* pStrides);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetDepthTestEnableEXT")]
        public static extern unsafe void CmdSetDepthTestEnableExt(CommandBuffer commandBuffer, uint depthTestEnable);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetDepthWriteEnableEXT")]
        public static extern unsafe void CmdSetDepthWriteEnableExt(CommandBuffer commandBuffer, uint depthWriteEnable);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetDepthCompareOpEXT")]
        public static extern unsafe void CmdSetDepthCompareOpExt(CommandBuffer commandBuffer, CompareOp depthCompareOp);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetDepthBoundsTestEnableEXT")]
        public static extern unsafe void CmdSetDepthBoundsTestEnableExt(CommandBuffer commandBuffer,
            uint depthBoundsTestEnable);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCmdSetStencilTestEnableEXT")]
        public static extern unsafe void
            CmdSetStencilTestEnableExt(CommandBuffer commandBuffer, uint stencilTestEnable);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkCmdSetStencilOpEXT")]
        public static extern unsafe void CmdSetStencilOpExt(CommandBuffer commandBuffer, StencilFaceFlags faceMask,
            StencilOp failOp, StencilOp passOp, StencilOp depthFailOp, CompareOp compareOp);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkCreatePrivateDataSlotEXT")]
        public static extern unsafe Result CreatePrivateDataSlotExt(Device device,
            PrivateDataSlotCreateInfoExt* pCreateInfo, AllocationCallbacks* pAllocator,
            PrivateDataSlotExt* pPrivateDataSlot);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi,
            EntryPoint = "vkDestroyPrivateDataSlotEXT")]
        public static extern unsafe void DestroyPrivateDataSlotExt(Device device, PrivateDataSlotExt privateDataSlot,
            AllocationCallbacks* pAllocator = null);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkSetPrivateDataEXT")]
        public static extern unsafe Result SetPrivateDataExt(Device device, ObjectType objectType, ulong objectHandle,
            PrivateDataSlotExt privateDataSlot, ulong data);

        [DllImport("vulkan-1.dll", CallingConvention = CallingConvention.Winapi, EntryPoint = "vkGetPrivateDataEXT")]
        public static extern unsafe void GetPrivateDataExt(Device device, ObjectType objectType, ulong objectHandle,
            PrivateDataSlotExt privateDataSlot, ulong* pData);
    }
}