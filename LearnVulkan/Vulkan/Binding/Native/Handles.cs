using System;

namespace LearnVulkan.Vulkan.Binding.Native
{
    public struct Instance
    {
        public IntPtr Handle;
    }

    public struct PhysicalDevice
    {
        public IntPtr Handle;
    }

    public struct Device
    {
        public IntPtr Handle;
    }

    public struct Queue
    {
        public IntPtr Handle;
    }

    public struct CommandBuffer
    {
        public IntPtr Handle;
    }

    public struct DeviceMemory
    {
        public IntPtr Handle;
    }

    public struct CommandPool
    {
        public IntPtr Handle;
    }

    public struct Buffer
    {
        public IntPtr Handle;
    }

    public struct BufferView
    {
        public IntPtr Handle;
    }

    public struct Image
    {
        public IntPtr Handle;
    }

    public struct ImageView
    {
        public IntPtr Handle;
    }

    public struct ShaderModule
    {
        public IntPtr Handle;
    }

    public struct Pipeline
    {
        public IntPtr Handle;
    }

    public struct PipelineLayout
    {
        public IntPtr Handle;
    }

    public struct Sampler
    {
        public IntPtr Handle;
    }

    public struct DescriptorSet
    {
        public IntPtr Handle;
    }

    public struct DescriptorSetLayout
    {
        public IntPtr Handle;
    }

    public struct DescriptorPool
    {
        public IntPtr Handle;
    }

    public struct Fence
    {
        public IntPtr Handle;
    }

    public struct Semaphore
    {
        public IntPtr Handle;
    }

    public struct Event
    {
        public IntPtr Handle;
    }

    public struct QueryPool
    {
        public IntPtr Handle;
    }

    public struct Framebuffer
    {
        public IntPtr Handle;
    }

    public struct RenderPass
    {
        public IntPtr Handle;
    }

    public struct PipelineCache
    {
        public IntPtr Handle;
    }

    public struct IndirectCommandsLayoutNv
    {
        public IntPtr Handle;
    }

    public struct DescriptorUpdateTemplate
    {
        public IntPtr Handle;
    }

    public struct SamplerYcbcrConversion
    {
        public IntPtr Handle;
    }

    public struct ValidationCacheExt
    {
        public IntPtr Handle;
    }

    public struct AccelerationStructureKhr
    {
        public IntPtr Handle;
    }

    public struct PerformanceConfigurationIntel
    {
        public IntPtr Handle;
    }

    public struct DeferredOperationKhr
    {
        public IntPtr Handle;
    }

    public struct PrivateDataSlotExt
    {
        public IntPtr Handle;
    }

    public struct DisplayKhr
    {
        public IntPtr Handle;
    }

    public struct DisplayModeKhr
    {
        public IntPtr Handle;
    }

    public struct SurfaceKhr
    {
        public IntPtr Handle;
    }

    public struct SwapchainKhr
    {
        public IntPtr Handle;
    }

    public struct DebugReportCallbackExt
    {
        public IntPtr Handle;
    }

    public struct DebugUtilsMessengerExt
    {
        public IntPtr Handle;
    }
}