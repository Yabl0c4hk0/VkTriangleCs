using System;

namespace LearnVulkan.Vulkan.Binding.Native
{
    public enum ApiConstants : ulong
    {
        MaxPhysicalDeviceNameSize = 256,
        UuidSize = 16,
        LuidSize = 8,
        LuidSizeKhr = LuidSize,
        MaxExtensionNameSize = 256,
        MaxDescriptionSize = 256,
        MaxMemoryTypes = 32,
        MaxMemoryHeaps = 16,
        RemainingMipLevels = (~0U),
        RemainingArrayLayers = (~0U),
        WholeSize = 18446744073709551615,
        AttachmentUnused = (~0U),
        True = 1,
        False = 0,
        QueueFamilyIgnored = (~0U),
        QueueFamilyExternal = (~0U - 1),
        QueueFamilyExternalKhr = QueueFamilyExternal,
        QueueFamilyForeignExt = (~0U - 2),
        SubpassExternal = (~0U),
        MaxDeviceGroupSize = 32,
        MaxDeviceGroupSizeKhr = MaxDeviceGroupSize,
        MaxDriverNameSize = 256,
        MaxDriverNameSizeKhr = MaxDriverNameSize,
        MaxDriverInfoSize = 256,
        MaxDriverInfoSizeKhr = MaxDriverInfoSize,
        ShaderUnusedKhr = (~0U),
        ShaderUnusedNv = ShaderUnusedKhr
    }

    public enum ImageLayout : int
    {
        Undefined = 0,
        General = 1,
        ColorAttachmentOptimal = 2,
        DepthStencilAttachmentOptimal = 3,
        DepthStencilReadOnlyOptimal = 4,
        ShaderReadOnlyOptimal = 5,
        TransferSrcOptimal = 6,
        TransferDstOptimal = 7,
        Preinitialized = 8,
        PresentSrcKhr = 1000001002,
        SharedPresentKhr = 1000111000,
        ShadingRateOptimalNv = 1000164003,
        FragmentDensityMapOptimalExt = 1000218000
    }

    public enum AttachmentLoadOp : int
    {
        Load = 0,
        Clear = 1,
        DontCare = 2
    }

    public enum AttachmentStoreOp : int
    {
        Store = 0,
        DontCare = 1,
        NoneQcom = 1000301000
    }

    public enum ImageType : int
    {
        N1d = 0,
        N2d = 1,
        N3d = 2
    }

    public enum ImageTiling : int
    {
        Optimal = 0,
        Linear = 1,
        DrmFormatModifierExt = 1000158000
    }

    public enum ImageViewType : int
    {
        N1d = 0,
        N2d = 1,
        N3d = 2,
        Cube = 3,
        N1dArray = 4,
        N2dArray = 5,
        CubeArray = 6
    }

    public enum CommandBufferLevel : int
    {
        Primary = 0,
        Secondary = 1
    }

    public enum ComponentSwizzle : int
    {
        Identity = 0,
        Zero = 1,
        One = 2,
        R = 3,
        G = 4,
        B = 5,
        A = 6
    }

    public enum DescriptorType : int
    {
        Sampler = 0,
        CombinedImageSampler = 1,
        SampledImage = 2,
        StorageImage = 3,
        UniformTexelBuffer = 4,
        StorageTexelBuffer = 5,
        UniformBuffer = 6,
        StorageBuffer = 7,
        UniformBufferDynamic = 8,
        StorageBufferDynamic = 9,
        InputAttachment = 10,
        InlineUniformBlockExt = 1000138000,
        AccelerationStructureKhr = 1000150000
    }

    public enum QueryType : int
    {
        Occlusion = 0,
        PipelineStatistics = 1,
        Timestamp = 2,
        Reserved8 = 1000023008,
        Reserved4 = 1000024004,
        TransformFeedbackStreamExt = 1000028004,
        PerformanceQueryKhr = 1000116000,
        AccelerationStructureCompactedSizeKhr = 1000150000,
        AccelerationStructureSerializationSizeKhr = 1000150000,
        PerformanceQueryIntel = 1000210000
    }

    public enum BorderColor : int
    {
        FloatTransparentBlack = 0,
        IntTransparentBlack = 1,
        FloatOpaqueBlack = 2,
        IntOpaqueBlack = 3,
        FloatOpaqueWhite = 4,
        IntOpaqueWhite = 5,
        FloatCustomExt = 1000287003,
        IntCustomExt = 1000287004
    }

    public enum PipelineBindPoint : int
    {
        Graphics = 0,
        Compute = 1,
        RayTracingKhr = 1000150000
    }

    public enum PipelineCacheHeaderVersion : int
    {
        One = 1
    }

    [Flags]
    public enum PipelineCacheCreateFlags : int
    {
        Reserved1BitExt = 2,
        ExternallySynchronizedBitExt = 1,
        Reserved2BitExt = 4
    }

    public enum PrimitiveTopology : int
    {
        PointList = 0,
        LineList = 1,
        LineStrip = 2,
        TriangleList = 3,
        TriangleStrip = 4,
        TriangleFan = 5,
        LineListWithAdjacency = 6,
        LineStripWithAdjacency = 7,
        TriangleListWithAdjacency = 8,
        TriangleStripWithAdjacency = 9,
        PatchList = 10
    }

    public enum SharingMode : int
    {
        Exclusive = 0,
        Concurrent = 1
    }

    public enum IndexType : int
    {
        Uint16 = 0,
        Uint32 = 1,
        NoneKhr = 1000150000,
        Uint8Ext = 1000265000
    }

    public enum Filter : int
    {
        Nearest = 0,
        Linear = 1,
        CubicImg = 1000015000
    }

    public enum SamplerMipmapMode : int
    {
        Nearest = 0,
        Linear = 1
    }

    public enum SamplerAddressMode : int
    {
        Repeat = 0,
        MirroredRepeat = 1,
        ClampToEdge = 2,
        ClampToBorder = 3,
        MirrorClampToEdge = 4
    }

    public enum CompareOp : int
    {
        Never = 0,
        Less = 1,
        Equal = 2,
        LessOrEqual = 3,
        Greater = 4,
        NotEqual = 5,
        GreaterOrEqual = 6,
        Always = 7
    }

    public enum PolygonMode : int
    {
        Fill = 0,
        Line = 1,
        Point = 2,
        FillRectangleNv = 1000153000
    }

    [Flags]
    public enum CullModeFlags : int
    {
        None = 0,
        FrontBit = 1 << 0,
        BackBit = 1 << 1,
        FrontAndBack = 0x00000003
    }

    public enum FrontFace : int
    {
        CounterClockwise = 0,
        Clockwise = 1
    }

    public enum BlendFactor : int
    {
        Zero = 0,
        One = 1,
        SrcColor = 2,
        OneMinusSrcColor = 3,
        DstColor = 4,
        OneMinusDstColor = 5,
        SrcAlpha = 6,
        OneMinusSrcAlpha = 7,
        DstAlpha = 8,
        OneMinusDstAlpha = 9,
        ConstantColor = 10,
        OneMinusConstantColor = 11,
        ConstantAlpha = 12,
        OneMinusConstantAlpha = 13,
        SrcAlphaSaturate = 14,
        Src1Color = 15,
        OneMinusSrc1Color = 16,
        Src1Alpha = 17,
        OneMinusSrc1Alpha = 18
    }

    public enum BlendOp : int
    {
        Add = 0,
        Subtract = 1,
        ReverseSubtract = 2,
        Min = 3,
        Max = 4,
        ZeroExt = 1000148000,
        SrcExt = 1000148001,
        DstExt = 1000148002,
        SrcOverExt = 1000148003,
        DstOverExt = 1000148004,
        SrcInExt = 1000148005,
        DstInExt = 1000148006,
        SrcOutExt = 1000148007,
        DstOutExt = 1000148008,
        SrcAtopExt = 1000148009,
        DstAtopExt = 1000148010,
        XorExt = 1000148011,
        MultiplyExt = 1000148012,
        ScreenExt = 1000148013,
        OverlayExt = 1000148014,
        DarkenExt = 1000148015,
        LightenExt = 1000148016,
        ColordodgeExt = 1000148017,
        ColorburnExt = 1000148018,
        HardlightExt = 1000148019,
        SoftlightExt = 1000148020,
        DifferenceExt = 1000148021,
        ExclusionExt = 1000148022,
        InvertExt = 1000148023,
        InvertRgbExt = 1000148024,
        LineardodgeExt = 1000148025,
        LinearburnExt = 1000148026,
        VividlightExt = 1000148027,
        LinearlightExt = 1000148028,
        PinlightExt = 1000148029,
        HardmixExt = 1000148030,
        HslHueExt = 1000148031,
        HslSaturationExt = 1000148032,
        HslColorExt = 1000148033,
        HslLuminosityExt = 1000148034,
        PlusExt = 1000148035,
        PlusClampedExt = 1000148036,
        PlusClampedAlphaExt = 1000148037,
        PlusDarkerExt = 1000148038,
        MinusExt = 1000148039,
        MinusClampedExt = 1000148040,
        ContrastExt = 1000148041,
        InvertOvgExt = 1000148042,
        RedExt = 1000148043,
        GreenExt = 1000148044,
        BlueExt = 1000148045
    }

    public enum StencilOp : int
    {
        Keep = 0,
        Zero = 1,
        Replace = 2,
        IncrementAndClamp = 3,
        DecrementAndClamp = 4,
        Invert = 5,
        IncrementAndWrap = 6,
        DecrementAndWrap = 7
    }

    public enum LogicOp : int
    {
        Clear = 0,
        And = 1,
        AndReverse = 2,
        Copy = 3,
        AndInverted = 4,
        NoOp = 5,
        Xor = 6,
        Or = 7,
        Nor = 8,
        Equivalent = 9,
        Invert = 10,
        OrReverse = 11,
        CopyInverted = 12,
        OrInverted = 13,
        Nand = 14,
        Set = 15
    }

    public enum InternalAllocationType : int
    {
        Executable = 0
    }

    public enum SystemAllocationScope : int
    {
        Command = 0,
        Object = 1,
        Cache = 2,
        Device = 3,
        Instance = 4
    }

    public enum PhysicalDeviceType : int
    {
        Other = 0,
        IntegratedGpu = 1,
        DiscreteGpu = 2,
        VirtualGpu = 3,
        Cpu = 4
    }

    public enum VertexInputRate : int
    {
        Vertex = 0,
        Instance = 1
    }

    public enum Format : int
    {
        Undefined = 0,
        R4g4UnormPack8 = 1,
        R4g4b4a4UnormPack16 = 2,
        B4g4r4a4UnormPack16 = 3,
        R5g6b5UnormPack16 = 4,
        B5g6r5UnormPack16 = 5,
        R5g5b5a1UnormPack16 = 6,
        B5g5r5a1UnormPack16 = 7,
        A1r5g5b5UnormPack16 = 8,
        R8Unorm = 9,
        R8Snorm = 10,
        R8Uscaled = 11,
        R8Sscaled = 12,
        R8Uint = 13,
        R8Sint = 14,
        R8Srgb = 15,
        R8g8Unorm = 16,
        R8g8Snorm = 17,
        R8g8Uscaled = 18,
        R8g8Sscaled = 19,
        R8g8Uint = 20,
        R8g8Sint = 21,
        R8g8Srgb = 22,
        R8g8b8Unorm = 23,
        R8g8b8Snorm = 24,
        R8g8b8Uscaled = 25,
        R8g8b8Sscaled = 26,
        R8g8b8Uint = 27,
        R8g8b8Sint = 28,
        R8g8b8Srgb = 29,
        B8g8r8Unorm = 30,
        B8g8r8Snorm = 31,
        B8g8r8Uscaled = 32,
        B8g8r8Sscaled = 33,
        B8g8r8Uint = 34,
        B8g8r8Sint = 35,
        B8g8r8Srgb = 36,
        R8g8b8a8Unorm = 37,
        R8g8b8a8Snorm = 38,
        R8g8b8a8Uscaled = 39,
        R8g8b8a8Sscaled = 40,
        R8g8b8a8Uint = 41,
        R8g8b8a8Sint = 42,
        R8g8b8a8Srgb = 43,
        B8g8r8a8Unorm = 44,
        B8g8r8a8Snorm = 45,
        B8g8r8a8Uscaled = 46,
        B8g8r8a8Sscaled = 47,
        B8g8r8a8Uint = 48,
        B8g8r8a8Sint = 49,
        B8g8r8a8Srgb = 50,
        A8b8g8r8UnormPack32 = 51,
        A8b8g8r8SnormPack32 = 52,
        A8b8g8r8UscaledPack32 = 53,
        A8b8g8r8SscaledPack32 = 54,
        A8b8g8r8UintPack32 = 55,
        A8b8g8r8SintPack32 = 56,
        A8b8g8r8SrgbPack32 = 57,
        A2r10g10b10UnormPack32 = 58,
        A2r10g10b10SnormPack32 = 59,
        A2r10g10b10UscaledPack32 = 60,
        A2r10g10b10SscaledPack32 = 61,
        A2r10g10b10UintPack32 = 62,
        A2r10g10b10SintPack32 = 63,
        A2b10g10r10UnormPack32 = 64,
        A2b10g10r10SnormPack32 = 65,
        A2b10g10r10UscaledPack32 = 66,
        A2b10g10r10SscaledPack32 = 67,
        A2b10g10r10UintPack32 = 68,
        A2b10g10r10SintPack32 = 69,
        R16Unorm = 70,
        R16Snorm = 71,
        R16Uscaled = 72,
        R16Sscaled = 73,
        R16Uint = 74,
        R16Sint = 75,
        R16Sfloat = 76,
        R16g16Unorm = 77,
        R16g16Snorm = 78,
        R16g16Uscaled = 79,
        R16g16Sscaled = 80,
        R16g16Uint = 81,
        R16g16Sint = 82,
        R16g16Sfloat = 83,
        R16g16b16Unorm = 84,
        R16g16b16Snorm = 85,
        R16g16b16Uscaled = 86,
        R16g16b16Sscaled = 87,
        R16g16b16Uint = 88,
        R16g16b16Sint = 89,
        R16g16b16Sfloat = 90,
        R16g16b16a16Unorm = 91,
        R16g16b16a16Snorm = 92,
        R16g16b16a16Uscaled = 93,
        R16g16b16a16Sscaled = 94,
        R16g16b16a16Uint = 95,
        R16g16b16a16Sint = 96,
        R16g16b16a16Sfloat = 97,
        R32Uint = 98,
        R32Sint = 99,
        R32Sfloat = 100,
        R32g32Uint = 101,
        R32g32Sint = 102,
        R32g32Sfloat = 103,
        R32g32b32Uint = 104,
        R32g32b32Sint = 105,
        R32g32b32Sfloat = 106,
        R32g32b32a32Uint = 107,
        R32g32b32a32Sint = 108,
        R32g32b32a32Sfloat = 109,
        R64Uint = 110,
        R64Sint = 111,
        R64Sfloat = 112,
        R64g64Uint = 113,
        R64g64Sint = 114,
        R64g64Sfloat = 115,
        R64g64b64Uint = 116,
        R64g64b64Sint = 117,
        R64g64b64Sfloat = 118,
        R64g64b64a64Uint = 119,
        R64g64b64a64Sint = 120,
        R64g64b64a64Sfloat = 121,
        B10g11r11UfloatPack32 = 122,
        E5b9g9r9UfloatPack32 = 123,
        D16Unorm = 124,
        X8D24UnormPack32 = 125,
        D32Sfloat = 126,
        S8Uint = 127,
        D16UnormS8Uint = 128,
        D24UnormS8Uint = 129,
        D32SfloatS8Uint = 130,
        Bc1RgbUnormBlock = 131,
        Bc1RgbSrgbBlock = 132,
        Bc1RgbaUnormBlock = 133,
        Bc1RgbaSrgbBlock = 134,
        Bc2UnormBlock = 135,
        Bc2SrgbBlock = 136,
        Bc3UnormBlock = 137,
        Bc3SrgbBlock = 138,
        Bc4UnormBlock = 139,
        Bc4SnormBlock = 140,
        Bc5UnormBlock = 141,
        Bc5SnormBlock = 142,
        Bc6hUfloatBlock = 143,
        Bc6hSfloatBlock = 144,
        Bc7UnormBlock = 145,
        Bc7SrgbBlock = 146,
        Etc2R8g8b8UnormBlock = 147,
        Etc2R8g8b8SrgbBlock = 148,
        Etc2R8g8b8a1UnormBlock = 149,
        Etc2R8g8b8a1SrgbBlock = 150,
        Etc2R8g8b8a8UnormBlock = 151,
        Etc2R8g8b8a8SrgbBlock = 152,
        EacR11UnormBlock = 153,
        EacR11SnormBlock = 154,
        EacR11g11UnormBlock = 155,
        EacR11g11SnormBlock = 156,
        Astc4x4UnormBlock = 157,
        Astc4x4SrgbBlock = 158,
        Astc5x4UnormBlock = 159,
        Astc5x4SrgbBlock = 160,
        Astc5x5UnormBlock = 161,
        Astc5x5SrgbBlock = 162,
        Astc6x5UnormBlock = 163,
        Astc6x5SrgbBlock = 164,
        Astc6x6UnormBlock = 165,
        Astc6x6SrgbBlock = 166,
        Astc8x5UnormBlock = 167,
        Astc8x5SrgbBlock = 168,
        Astc8x6UnormBlock = 169,
        Astc8x6SrgbBlock = 170,
        Astc8x8UnormBlock = 171,
        Astc8x8SrgbBlock = 172,
        Astc10x5UnormBlock = 173,
        Astc10x5SrgbBlock = 174,
        Astc10x6UnormBlock = 175,
        Astc10x6SrgbBlock = 176,
        Astc10x8UnormBlock = 177,
        Astc10x8SrgbBlock = 178,
        Astc10x10UnormBlock = 179,
        Astc10x10SrgbBlock = 180,
        Astc12x10UnormBlock = 181,
        Astc12x10SrgbBlock = 182,
        Astc12x12UnormBlock = 183,
        Astc12x12SrgbBlock = 184,
        Pvrtc12bppUnormBlockImg = 1000054000,
        Pvrtc14bppUnormBlockImg = 1000054001,
        Pvrtc22bppUnormBlockImg = 1000054002,
        Pvrtc24bppUnormBlockImg = 1000054003,
        Pvrtc12bppSrgbBlockImg = 1000054004,
        Pvrtc14bppSrgbBlockImg = 1000054005,
        Pvrtc22bppSrgbBlockImg = 1000054006,
        Pvrtc24bppSrgbBlockImg = 1000054007,
        Astc4x4SfloatBlockExt = 1000066000,
        Astc5x4SfloatBlockExt = 1000066001,
        Astc5x5SfloatBlockExt = 1000066002,
        Astc6x5SfloatBlockExt = 1000066003,
        Astc6x6SfloatBlockExt = 1000066004,
        Astc8x5SfloatBlockExt = 1000066005,
        Astc8x6SfloatBlockExt = 1000066006,
        Astc8x8SfloatBlockExt = 1000066007,
        Astc10x5SfloatBlockExt = 1000066008,
        Astc10x6SfloatBlockExt = 1000066009,
        Astc10x8SfloatBlockExt = 1000066010,
        Astc10x10SfloatBlockExt = 1000066011,
        Astc12x10SfloatBlockExt = 1000066012,
        Astc12x12SfloatBlockExt = 1000066013,
        Astc3x3x3UnormBlockExt = 1000288000,
        Astc3x3x3SrgbBlockExt = 1000288001,
        Astc3x3x3SfloatBlockExt = 1000288002,
        Astc4x3x3UnormBlockExt = 1000288003,
        Astc4x3x3SrgbBlockExt = 1000288004,
        Astc4x3x3SfloatBlockExt = 1000288005,
        Astc4x4x3UnormBlockExt = 1000288006,
        Astc4x4x3SrgbBlockExt = 1000288007,
        Astc4x4x3SfloatBlockExt = 1000288008,
        Astc4x4x4UnormBlockExt = 1000288009,
        Astc4x4x4SrgbBlockExt = 1000288010,
        Astc4x4x4SfloatBlockExt = 1000288011,
        Astc5x4x4UnormBlockExt = 1000288012,
        Astc5x4x4SrgbBlockExt = 1000288013,
        Astc5x4x4SfloatBlockExt = 1000288014,
        Astc5x5x4UnormBlockExt = 1000288015,
        Astc5x5x4SrgbBlockExt = 1000288016,
        Astc5x5x4SfloatBlockExt = 1000288017,
        Astc5x5x5UnormBlockExt = 1000288018,
        Astc5x5x5SrgbBlockExt = 1000288019,
        Astc5x5x5SfloatBlockExt = 1000288020,
        Astc6x5x5UnormBlockExt = 1000288021,
        Astc6x5x5SrgbBlockExt = 1000288022,
        Astc6x5x5SfloatBlockExt = 1000288023,
        Astc6x6x5UnormBlockExt = 1000288024,
        Astc6x6x5SrgbBlockExt = 1000288025,
        Astc6x6x5SfloatBlockExt = 1000288026,
        Astc6x6x6UnormBlockExt = 1000288027,
        Astc6x6x6SrgbBlockExt = 1000288028,
        Astc6x6x6SfloatBlockExt = 1000288029,
        A4r4g4b4UnormPack16Ext = 1000340000,
        A4b4g4r4UnormPack16Ext = 1000340001
    }

    public enum StructureType : int
    {
        ApplicationInfo = 0,
        InstanceCreateInfo = 1,
        DeviceQueueCreateInfo = 2,
        DeviceCreateInfo = 3,
        SubmitInfo = 4,
        MemoryAllocateInfo = 5,
        MappedMemoryRange = 6,
        BindSparseInfo = 7,
        FenceCreateInfo = 8,
        SemaphoreCreateInfo = 9,
        EventCreateInfo = 10,
        QueryPoolCreateInfo = 11,
        BufferCreateInfo = 12,
        BufferViewCreateInfo = 13,
        ImageCreateInfo = 14,
        ImageViewCreateInfo = 15,
        ShaderModuleCreateInfo = 16,
        PipelineCacheCreateInfo = 17,
        PipelineShaderStageCreateInfo = 18,
        PipelineVertexInputStateCreateInfo = 19,
        PipelineInputAssemblyStateCreateInfo = 20,
        PipelineTessellationStateCreateInfo = 21,
        PipelineViewportStateCreateInfo = 22,
        PipelineRasterizationStateCreateInfo = 23,
        PipelineMultisampleStateCreateInfo = 24,
        PipelineDepthStencilStateCreateInfo = 25,
        PipelineColorBlendStateCreateInfo = 26,
        PipelineDynamicStateCreateInfo = 27,
        GraphicsPipelineCreateInfo = 28,
        ComputePipelineCreateInfo = 29,
        PipelineLayoutCreateInfo = 30,
        SamplerCreateInfo = 31,
        DescriptorSetLayoutCreateInfo = 32,
        DescriptorPoolCreateInfo = 33,
        DescriptorSetAllocateInfo = 34,
        WriteDescriptorSet = 35,
        CopyDescriptorSet = 36,
        FramebufferCreateInfo = 37,
        RenderPassCreateInfo = 38,
        CommandPoolCreateInfo = 39,
        CommandBufferAllocateInfo = 40,
        CommandBufferInheritanceInfo = 41,
        CommandBufferBeginInfo = 42,
        RenderPassBeginInfo = 43,
        BufferMemoryBarrier = 44,
        ImageMemoryBarrier = 45,
        MemoryBarrier = 46,
        LoaderInstanceCreateInfo = 47,
        LoaderDeviceCreateInfo = 48,
        SwapchainCreateInfoKhr = 1000001000,
        PresentInfoKhr = 1000001001,
        DisplayModeCreateInfoKhr = 1000002000,
        DisplaySurfaceCreateInfoKhr = 1000002001,
        DisplayPresentInfoKhr = 1000003000,
        XlibSurfaceCreateInfoKhr = 1000004000,
        XcbSurfaceCreateInfoKhr = 1000005000,
        WaylandSurfaceCreateInfoKhr = 1000006000,
        AndroidSurfaceCreateInfoKhr = 1000008000,
        Win32SurfaceCreateInfoKhr = 1000009000,
        NativeBufferAndroid = 1000010000,
        SwapchainImageCreateInfoAndroid = 1000010001,
        PhysicalDevicePresentationPropertiesAndroid = 1000010002,
        PipelineRasterizationStateRasterizationOrderAmd = 1000018000,
        DebugMarkerObjectNameInfoExt = 1000022000,
        DebugMarkerObjectTagInfoExt = 1000022001,
        DebugMarkerMarkerInfoExt = 1000022002,
        DedicatedAllocationImageCreateInfoNv = 1000026000,
        DedicatedAllocationBufferCreateInfoNv = 1000026001,
        DedicatedAllocationMemoryAllocateInfoNv = 1000026002,
        PhysicalDeviceTransformFeedbackFeaturesExt = 1000028000,
        PhysicalDeviceTransformFeedbackPropertiesExt = 1000028001,
        PipelineRasterizationStateStreamCreateInfoExt = 1000028002,
        ImageViewHandleInfoNvx = 1000030000,
        ImageViewAddressPropertiesNvx = 1000030001,
        TextureLodGatherFormatPropertiesAmd = 1000041000,
        StreamDescriptorSurfaceCreateInfoGgp = 1000049000,
        PhysicalDeviceCornerSampledImageFeaturesNv = 1000050000,
        ExternalMemoryImageCreateInfoNv = 1000056000,
        ExportMemoryAllocateInfoNv = 1000056001,
        ImportMemoryWin32HandleInfoNv = 1000057000,
        ExportMemoryWin32HandleInfoNv = 1000057001,
        Win32KeyedMutexAcquireReleaseInfoNv = 1000058000,
        ValidationFlagsExt = 1000061000,
        ViSurfaceCreateInfoNn = 1000062000,
        PhysicalDeviceTextureCompressionAstcHdrFeaturesExt = 1000066000,
        ImageViewAstcDecodeModeExt = 1000067000,
        PhysicalDeviceAstcDecodeFeaturesExt = 1000067001,
        ImportMemoryWin32HandleInfoKhr = 1000073000,
        ExportMemoryWin32HandleInfoKhr = 1000073001,
        MemoryWin32HandlePropertiesKhr = 1000073002,
        MemoryGetWin32HandleInfoKhr = 1000073003,
        ImportMemoryFdInfoKhr = 1000074000,
        MemoryFdPropertiesKhr = 1000074001,
        MemoryGetFdInfoKhr = 1000074002,
        Win32KeyedMutexAcquireReleaseInfoKhr = 1000075000,
        ImportSemaphoreWin32HandleInfoKhr = 1000078000,
        ExportSemaphoreWin32HandleInfoKhr = 1000078001,
        D3d12FenceSubmitInfoKhr = 1000078002,
        SemaphoreGetWin32HandleInfoKhr = 1000078003,
        ImportSemaphoreFdInfoKhr = 1000079000,
        SemaphoreGetFdInfoKhr = 1000079001,
        CommandBufferInheritanceConditionalRenderingInfoExt = 1000081000,
        PhysicalDeviceConditionalRenderingFeaturesExt = 1000081001,
        ConditionalRenderingBeginInfoExt = 1000081002,
        PresentRegionsKhr = 1000084000,
        PipelineViewportWScalingStateCreateInfoNv = 1000087000,
        SurfaceCapabilities2Ext = 1000090000,
        DisplayPowerInfoExt = 1000091000,
        DeviceEventInfoExt = 1000091001,
        DisplayEventInfoExt = 1000091002,
        SwapchainCounterCreateInfoExt = 1000091003,
        PresentTimesInfoGoogle = 1000092000,
        PhysicalDeviceMultiviewPerViewAttributesPropertiesNvx = 1000097000,
        PipelineViewportSwizzleStateCreateInfoNv = 1000098000,
        PhysicalDeviceDiscardRectanglePropertiesExt = 1000099000,
        PipelineDiscardRectangleStateCreateInfoExt = 1000099001,
        PhysicalDeviceConservativeRasterizationPropertiesExt = 1000101000,
        PipelineRasterizationConservativeStateCreateInfoExt = 1000101001,
        PhysicalDeviceDepthClipEnableFeaturesExt = 1000102000,
        PipelineRasterizationDepthClipStateCreateInfoExt = 1000102001,
        HdrMetadataExt = 1000105000,
        SharedPresentSurfaceCapabilitiesKhr = 1000111000,
        ImportFenceWin32HandleInfoKhr = 1000114000,
        ExportFenceWin32HandleInfoKhr = 1000114001,
        FenceGetWin32HandleInfoKhr = 1000114002,
        ImportFenceFdInfoKhr = 1000115000,
        FenceGetFdInfoKhr = 1000115001,
        PhysicalDevicePerformanceQueryFeaturesKhr = 1000116000,
        PhysicalDevicePerformanceQueryPropertiesKhr = 1000116001,
        QueryPoolPerformanceCreateInfoKhr = 1000116002,
        PerformanceQuerySubmitInfoKhr = 1000116003,
        AcquireProfilingLockInfoKhr = 1000116004,
        PerformanceCounterKhr = 1000116005,
        PerformanceCounterDescriptionKhr = 1000116006,
        PhysicalDeviceSurfaceInfo2Khr = 1000119000,
        SurfaceCapabilities2Khr = 1000119001,
        SurfaceFormat2Khr = 1000119002,
        DisplayProperties2Khr = 1000121000,
        DisplayPlaneProperties2Khr = 1000121001,
        DisplayModeProperties2Khr = 1000121002,
        DisplayPlaneInfo2Khr = 1000121003,
        DisplayPlaneCapabilities2Khr = 1000121004,
        IosSurfaceCreateInfoMvk = 1000122000,
        MacosSurfaceCreateInfoMvk = 1000123000,
        DebugUtilsObjectNameInfoExt = 1000128000,
        DebugUtilsObjectTagInfoExt = 1000128001,
        DebugUtilsLabelExt = 1000128002,
        DebugUtilsMessengerCallbackDataExt = 1000128003,
        DebugUtilsMessengerCreateInfoExt = 1000128004,
        AndroidHardwareBufferUsageAndroid = 1000129000,
        AndroidHardwareBufferPropertiesAndroid = 1000129001,
        AndroidHardwareBufferFormatPropertiesAndroid = 1000129002,
        ImportAndroidHardwareBufferInfoAndroid = 1000129003,
        MemoryGetAndroidHardwareBufferInfoAndroid = 1000129004,
        ExternalFormatAndroid = 1000129005,
        PhysicalDeviceInlineUniformBlockFeaturesExt = 1000138000,
        PhysicalDeviceInlineUniformBlockPropertiesExt = 1000138001,
        WriteDescriptorSetInlineUniformBlockExt = 1000138002,
        DescriptorPoolInlineUniformBlockCreateInfoExt = 1000138003,
        SampleLocationsInfoExt = 1000143000,
        RenderPassSampleLocationsBeginInfoExt = 1000143001,
        PipelineSampleLocationsStateCreateInfoExt = 1000143002,
        PhysicalDeviceSampleLocationsPropertiesExt = 1000143003,
        MultisamplePropertiesExt = 1000143004,
        PhysicalDeviceBlendOperationAdvancedFeaturesExt = 1000148000,
        PhysicalDeviceBlendOperationAdvancedPropertiesExt = 1000148001,
        PipelineColorBlendAdvancedStateCreateInfoExt = 1000148002,
        PipelineCoverageToColorStateCreateInfoNv = 1000149000,
        BindAccelerationStructureMemoryInfoKhr = 1000150006,
        WriteDescriptorSetAccelerationStructureKhr = 1000150007,
        AccelerationStructureBuildGeometryInfoKhr = 1000150000,
        AccelerationStructureCreateGeometryTypeInfoKhr = 1000150001,
        AccelerationStructureDeviceAddressInfoKhr = 1000150002,
        AccelerationStructureGeometryAabbsDataKhr = 1000150003,
        AccelerationStructureGeometryInstancesDataKhr = 1000150004,
        AccelerationStructureGeometryTrianglesDataKhr = 1000150005,
        AccelerationStructureGeometryKhr = 1000150006,
        AccelerationStructureMemoryRequirementsInfoKhr = 1000150008,
        AccelerationStructureVersionKhr = 1000150009,
        CopyAccelerationStructureInfoKhr = 1000150010,
        CopyAccelerationStructureToMemoryInfoKhr = 1000150011,
        CopyMemoryToAccelerationStructureInfoKhr = 1000150012,
        PhysicalDeviceRayTracingFeaturesKhr = 1000150013,
        PhysicalDeviceRayTracingPropertiesKhr = 1000150014,
        RayTracingPipelineCreateInfoKhr = 1000150015,
        RayTracingShaderGroupCreateInfoKhr = 1000150016,
        AccelerationStructureCreateInfoKhr = 1000150017,
        RayTracingPipelineInterfaceCreateInfoKhr = 1000150018,
        PipelineCoverageModulationStateCreateInfoNv = 1000152000,
        PhysicalDeviceShaderSmBuiltinsFeaturesNv = 1000154000,
        PhysicalDeviceShaderSmBuiltinsPropertiesNv = 1000154001,
        DrmFormatModifierPropertiesListExt = 1000158000,
        DrmFormatModifierPropertiesExt = 1000158001,
        PhysicalDeviceImageDrmFormatModifierInfoExt = 1000158002,
        ImageDrmFormatModifierListCreateInfoExt = 1000158003,
        ImageDrmFormatModifierExplicitCreateInfoExt = 1000158004,
        ImageDrmFormatModifierPropertiesExt = 1000158005,
        ValidationCacheCreateInfoExt = 1000160000,
        ShaderModuleValidationCacheCreateInfoExt = 1000160001,
        PipelineViewportShadingRateImageStateCreateInfoNv = 1000164000,
        PhysicalDeviceShadingRateImageFeaturesNv = 1000164001,
        PhysicalDeviceShadingRateImagePropertiesNv = 1000164002,
        PipelineViewportCoarseSampleOrderStateCreateInfoNv = 1000164005,
        RayTracingPipelineCreateInfoNv = 1000165000,
        AccelerationStructureCreateInfoNv = 1000165001,
        GeometryNv = 1000165003,
        GeometryTrianglesNv = 1000165004,
        GeometryAabbNv = 1000165005,
        AccelerationStructureMemoryRequirementsInfoNv = 1000165008,
        PhysicalDeviceRayTracingPropertiesNv = 1000165009,
        RayTracingShaderGroupCreateInfoNv = 1000165011,
        AccelerationStructureInfoNv = 1000165012,
        PhysicalDeviceRepresentativeFragmentTestFeaturesNv = 1000166000,
        PipelineRepresentativeFragmentTestStateCreateInfoNv = 1000166001,
        PhysicalDeviceImageViewImageFormatInfoExt = 1000170000,
        FilterCubicImageViewImageFormatPropertiesExt = 1000170001,
        DeviceQueueGlobalPriorityCreateInfoExt = 1000174000,
        ImportMemoryHostPointerInfoExt = 1000178000,
        MemoryHostPointerPropertiesExt = 1000178001,
        PhysicalDeviceExternalMemoryHostPropertiesExt = 1000178002,
        PhysicalDeviceShaderClockFeaturesKhr = 1000181000,
        PipelineCompilerControlCreateInfoAmd = 1000183000,
        CalibratedTimestampInfoExt = 1000184000,
        PhysicalDeviceShaderCorePropertiesAmd = 1000185000,
        DeviceMemoryOverallocationCreateInfoAmd = 1000189000,
        PhysicalDeviceVertexAttributeDivisorPropertiesExt = 1000190000,
        PipelineVertexInputDivisorStateCreateInfoExt = 1000190001,
        PhysicalDeviceVertexAttributeDivisorFeaturesExt = 1000190002,
        PresentFrameTokenGgp = 1000191000,
        PipelineCreationFeedbackCreateInfoExt = 1000192000,
        PhysicalDeviceComputeShaderDerivativesFeaturesNv = 1000201000,
        PhysicalDeviceMeshShaderFeaturesNv = 1000202000,
        PhysicalDeviceMeshShaderPropertiesNv = 1000202001,
        PhysicalDeviceFragmentShaderBarycentricFeaturesNv = 1000203000,
        PhysicalDeviceShaderImageFootprintFeaturesNv = 1000204000,
        PipelineViewportExclusiveScissorStateCreateInfoNv = 1000205000,
        PhysicalDeviceExclusiveScissorFeaturesNv = 1000205002,
        CheckpointDataNv = 1000206000,
        QueueFamilyCheckpointPropertiesNv = 1000206001,
        PhysicalDeviceShaderIntegerFunctions2FeaturesIntel = 1000209000,
        QueryPoolPerformanceQueryCreateInfoIntel = 1000210000,
        InitializePerformanceApiInfoIntel = 1000210001,
        PerformanceMarkerInfoIntel = 1000210002,
        PerformanceStreamMarkerInfoIntel = 1000210003,
        PerformanceOverrideInfoIntel = 1000210004,
        PerformanceConfigurationAcquireInfoIntel = 1000210005,
        PhysicalDevicePciBusInfoPropertiesExt = 1000212000,
        DisplayNativeHdrSurfaceCapabilitiesAmd = 1000213000,
        SwapchainDisplayNativeHdrCreateInfoAmd = 1000213001,
        ImagepipeSurfaceCreateInfoFuchsia = 1000214000,
        MetalSurfaceCreateInfoExt = 1000217000,
        PhysicalDeviceFragmentDensityMapFeaturesExt = 1000218000,
        PhysicalDeviceFragmentDensityMapPropertiesExt = 1000218001,
        RenderPassFragmentDensityMapCreateInfoExt = 1000218002,
        PhysicalDeviceSubgroupSizeControlPropertiesExt = 1000225000,
        PipelineShaderStageRequiredSubgroupSizeCreateInfoExt = 1000225001,
        PhysicalDeviceSubgroupSizeControlFeaturesExt = 1000225002,
        PhysicalDeviceShaderCoreProperties2Amd = 1000227000,
        PhysicalDeviceCoherentMemoryFeaturesAmd = 1000229000,
        PhysicalDeviceMemoryBudgetPropertiesExt = 1000237000,
        PhysicalDeviceMemoryPriorityFeaturesExt = 1000238000,
        MemoryPriorityAllocateInfoExt = 1000238001,
        SurfaceProtectedCapabilitiesKhr = 1000239000,
        PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNv = 1000240000,
        PhysicalDeviceBufferDeviceAddressFeaturesExt = 1000244000,
        BufferDeviceAddressCreateInfoExt = 1000244002,
        ValidationFeaturesExt = 1000247000,
        PhysicalDeviceCooperativeMatrixFeaturesNv = 1000249000,
        CooperativeMatrixPropertiesNv = 1000249001,
        PhysicalDeviceCooperativeMatrixPropertiesNv = 1000249002,
        PhysicalDeviceCoverageReductionModeFeaturesNv = 1000250000,
        PipelineCoverageReductionStateCreateInfoNv = 1000250001,
        FramebufferMixedSamplesCombinationNv = 1000250002,
        PhysicalDeviceFragmentShaderInterlockFeaturesExt = 1000251000,
        PhysicalDeviceYcbcrImageArraysFeaturesExt = 1000252000,
        HeadlessSurfaceCreateInfoExt = 1000256000,
        PhysicalDeviceLineRasterizationFeaturesExt = 1000259000,
        PipelineRasterizationLineStateCreateInfoExt = 1000259001,
        PhysicalDeviceLineRasterizationPropertiesExt = 1000259002,
        PhysicalDeviceShaderAtomicFloatFeaturesExt = 1000260000,
        PhysicalDeviceIndexTypeUint8FeaturesExt = 1000265000,
        PhysicalDeviceExtendedDynamicStateFeaturesExt = 1000267000,
        DeferredOperationInfoKhr = 1000268000,
        PhysicalDevicePipelineExecutablePropertiesFeaturesKhr = 1000269000,
        PipelineInfoKhr = 1000269001,
        PipelineExecutablePropertiesKhr = 1000269002,
        PipelineExecutableInfoKhr = 1000269003,
        PipelineExecutableStatisticKhr = 1000269004,
        PipelineExecutableInternalRepresentationKhr = 1000269005,
        PhysicalDeviceShaderDemoteToHelperInvocationFeaturesExt = 1000276000,
        PhysicalDeviceDeviceGeneratedCommandsPropertiesNv = 1000277000,
        GraphicsShaderGroupCreateInfoNv = 1000277001,
        GraphicsPipelineShaderGroupsCreateInfoNv = 1000277002,
        IndirectCommandsLayoutTokenNv = 1000277003,
        IndirectCommandsLayoutCreateInfoNv = 1000277004,
        GeneratedCommandsInfoNv = 1000277005,
        GeneratedCommandsMemoryRequirementsInfoNv = 1000277006,
        PhysicalDeviceDeviceGeneratedCommandsFeaturesNv = 1000277007,
        PhysicalDeviceTexelBufferAlignmentFeaturesExt = 1000281000,
        PhysicalDeviceTexelBufferAlignmentPropertiesExt = 1000281001,
        CommandBufferInheritanceRenderPassTransformInfoQcom = 1000282000,
        RenderPassTransformBeginInfoQcom = 1000282001,
        PhysicalDeviceRobustness2FeaturesExt = 1000286000,
        PhysicalDeviceRobustness2PropertiesExt = 1000286001,
        SamplerCustomBorderColorCreateInfoExt = 1000287000,
        PhysicalDeviceCustomBorderColorPropertiesExt = 1000287001,
        PhysicalDeviceCustomBorderColorFeaturesExt = 1000287002,
        PipelineLibraryCreateInfoKhr = 1000290000,
        PhysicalDevicePrivateDataFeaturesExt = 1000295000,
        DevicePrivateDataCreateInfoExt = 1000295001,
        PrivateDataSlotCreateInfoExt = 1000295002,
        PhysicalDevicePipelineCreationCacheControlFeaturesExt = 1000297000,
        PhysicalDeviceDiagnosticsConfigFeaturesNv = 1000300000,
        DeviceDiagnosticsConfigCreateInfoNv = 1000300001,
        ReservedQcom = 1000309000,
        PhysicalDeviceFragmentDensityMap2FeaturesExt = 1000332000,
        PhysicalDeviceFragmentDensityMap2PropertiesExt = 1000332001,
        PhysicalDeviceImageRobustnessFeaturesExt = 1000335000,
        PhysicalDevice4444FormatsFeaturesExt = 1000340000,
        DirectfbSurfaceCreateInfoExt = 1000346000
    }

    public enum SubpassContents : int
    {
        Inline = 0,
        SecondaryCommandBuffers = 1
    }

    public enum Result : int
    {
        Success = 0,
        NotReady = 1,
        Timeout = 2,
        EventSet = 3,
        EventReset = 4,
        Incomplete = 5,
        ErrorOutOfHostMemory = -1,
        ErrorOutOfDeviceMemory = -2,
        ErrorInitializationFailed = -3,
        ErrorDeviceLost = -4,
        ErrorMemoryMapFailed = -5,
        ErrorLayerNotPresent = -6,
        ErrorExtensionNotPresent = -7,
        ErrorFeatureNotPresent = -8,
        ErrorIncompatibleDriver = -9,
        ErrorTooManyObjects = -10,
        ErrorFormatNotSupported = -11,
        ErrorFragmentedPool = -12,
        ErrorUnknown = -13,
        ErrorSurfaceLostKhr = 1000000000,
        ErrorNativeWindowInUseKhr = 1000000001,
        ErrorIncompatibleDisplayKhr = 1000003001,
        ErrorInvalidShaderNv = 1000012000,
        ErrorIncompatibleVersionKhr = 1000150000,
        ErrorInvalidDrmFormatModifierPlaneLayoutExt = 1000158000,
        ErrorNotPermittedExt = 1000174001,
        ThreadIdleKhr = 1000268000,
        ThreadDoneKhr = 1000268001,
        OperationDeferredKhr = 1000268002,
        OperationNotDeferredKhr = 1000268003,
        PipelineCompileRequiredExt = 1000297000
    }

    public enum DynamicState : int
    {
        Viewport = 0,
        Scissor = 1,
        LineWidth = 2,
        DepthBias = 3,
        BlendConstants = 4,
        DepthBounds = 5,
        StencilCompareMask = 6,
        StencilWriteMask = 7,
        StencilReference = 8,
        ViewportWScalingNv = 1000087000,
        DiscardRectangleExt = 1000099000,
        SampleLocationsExt = 1000143000,
        ViewportShadingRatePaletteNv = 1000164004,
        ViewportCoarseSampleOrderNv = 1000164006,
        ExclusiveScissorNv = 1000205001,
        LineStippleExt = 1000259000,
        CullModeExt = 1000267000,
        FrontFaceExt = 1000267001,
        PrimitiveTopologyExt = 1000267002,
        ViewportWithCountExt = 1000267003,
        ScissorWithCountExt = 1000267004,
        VertexInputBindingStrideExt = 1000267005,
        DepthTestEnableExt = 1000267006,
        DepthWriteEnableExt = 1000267007,
        DepthCompareOpExt = 1000267008,
        DepthBoundsTestEnableExt = 1000267009,
        StencilTestEnableExt = 1000267010,
        StencilOpExt = 1000267011
    }

    public enum DescriptorUpdateTemplateType : int
    {
        Set = 0
    }

    public enum ObjectType : int
    {
        Unknown = 0,
        Instance = 1,
        PhysicalDevice = 2,
        Device = 3,
        Queue = 4,
        Semaphore = 5,
        CommandBuffer = 6,
        Fence = 7,
        DeviceMemory = 8,
        Buffer = 9,
        Image = 10,
        Event = 11,
        QueryPool = 12,
        BufferView = 13,
        ImageView = 14,
        ShaderModule = 15,
        PipelineCache = 16,
        PipelineLayout = 17,
        RenderPass = 18,
        Pipeline = 19,
        DescriptorSetLayout = 20,
        Sampler = 21,
        DescriptorPool = 22,
        DescriptorSet = 23,
        Framebuffer = 24,
        CommandPool = 25,
        SurfaceKhr = 1000000000,
        DisplayKhr = 1000002000,
        DisplayModeKhr = 1000002001,
        DebugUtilsMessengerExt = 1000128000,
        AccelerationStructureKhr = 1000150000,
        ValidationCacheExt = 1000160000,
        PerformanceConfigurationIntel = 1000210000,
        DeferredOperationKhr = 1000268000,
        IndirectCommandsLayoutNv = 1000277000,
        PrivateDataSlotExt = 1000295000
    }

    [Flags]
    public enum QueueFlags : int
    {
        GraphicsBit = 1 << 0,
        ComputeBit = 1 << 1,
        TransferBit = 1 << 2,
        SparseBindingBit = 1 << 3,
        Reserved6BitKhr = 64,
        Reserved5BitKhr = 32
    }

    [Flags]
    public enum RenderPassCreateFlags : int
    {
        Reserved0BitKhr = 1,
        TransformBitQcom = 2
    }

    [Flags]
    public enum DeviceQueueCreateFlags : int
    {
    }

    [Flags]
    public enum MemoryPropertyFlags : int
    {
        DeviceLocalBit = 1 << 0,
        HostVisibleBit = 1 << 1,
        HostCoherentBit = 1 << 2,
        HostCachedBit = 1 << 3,
        LazilyAllocatedBit = 1 << 4,
        DeviceCoherentBitAmd = 64,
        DeviceUncachedBitAmd = 128
    }

    [Flags]
    public enum MemoryHeapFlags : int
    {
        DeviceLocalBit = 1 << 0,
        Reserved2BitKhr = 4
    }

    [Flags]
    public enum AccessFlags : int
    {
        IndirectCommandReadBit = 1 << 0,
        IndexReadBit = 1 << 1,
        VertexAttributeReadBit = 1 << 2,
        UniformReadBit = 1 << 3,
        InputAttachmentReadBit = 1 << 4,
        ShaderReadBit = 1 << 5,
        ShaderWriteBit = 1 << 6,
        ColorAttachmentReadBit = 1 << 7,
        ColorAttachmentWriteBit = 1 << 8,
        DepthStencilAttachmentReadBit = 1 << 9,
        DepthStencilAttachmentWriteBit = 1 << 10,
        TransferReadBit = 1 << 11,
        TransferWriteBit = 1 << 12,
        HostReadBit = 1 << 13,
        HostWriteBit = 1 << 14,
        MemoryReadBit = 1 << 15,
        MemoryWriteBit = 1 << 16,
        Reserved30BitKhr = 1073741824,
        Reserved28BitKhr = 268435456,
        Reserved29BitKhr = 536870912,
        TransformFeedbackWriteBitExt = 33554432,
        TransformFeedbackCounterReadBitExt = 67108864,
        TransformFeedbackCounterWriteBitExt = 134217728,
        ConditionalRenderingReadBitExt = 1048576,
        ColorAttachmentReadNoncoherentBitExt = 524288,
        AccelerationStructureReadBitKhr = 2097152,
        AccelerationStructureWriteBitKhr = 4194304,
        ShadingRateImageReadBitNv = 8388608,
        FragmentDensityMapReadBitExt = 16777216,
        CommandPreprocessReadBitNv = 131072,
        CommandPreprocessWriteBitNv = 262144
    }

    [Flags]
    public enum BufferUsageFlags : int
    {
        TransferSrcBit = 1 << 0,
        TransferDstBit = 1 << 1,
        UniformTexelBufferBit = 1 << 2,
        StorageTexelBufferBit = 1 << 3,
        UniformBufferBit = 1 << 4,
        StorageBufferBit = 1 << 5,
        IndexBufferBit = 1 << 6,
        VertexBufferBit = 1 << 7,
        IndirectBufferBit = 1 << 8,
        Reserved15BitKhr = 32768,
        Reserved16BitKhr = 65536,
        Reserved13BitKhr = 8192,
        Reserved14BitKhr = 16384,
        TransformFeedbackBufferBitExt = 2048,
        TransformFeedbackCounterBufferBitExt = 4096,
        ConditionalRenderingBitExt = 512,
        RayTracingBitKhr = 1024,
        Reserved19BitKhr = 524288,
        Reserved20BitKhr = 1048576,
        Reserved18BitQcom = 262144
    }

    [Flags]
    public enum BufferCreateFlags : int
    {
        BindingBit = 1 << 0,
        ResidencyBit = 1 << 1,
        AliasedBit = 1 << 2
    }

    [Flags]
    public enum ShaderStageFlags : int
    {
        VertexBit = 1 << 0,
        TessellationControlBit = 1 << 1,
        TessellationEvaluationBit = 1 << 2,
        GeometryBit = 1 << 3,
        FragmentBit = 1 << 4,
        ComputeBit = 1 << 5,
        AllGraphics = 0x0000001f,
        All = 0x7fffffff,
        RaygenBitKhr = 256,
        AnyHitBitKhr = 512,
        ClosestHitBitKhr = 1024,
        MissBitKhr = 2048,
        IntersectionBitKhr = 4096,
        CallableBitKhr = 8192,
        TaskBitNv = 64,
        MeshBitNv = 128
    }

    [Flags]
    public enum ImageUsageFlags : int
    {
        TransferSrcBit = 1 << 0,
        TransferDstBit = 1 << 1,
        SampledBit = 1 << 2,
        StorageBit = 1 << 3,
        ColorAttachmentBit = 1 << 4,
        DepthStencilAttachmentBit = 1 << 5,
        TransientAttachmentBit = 1 << 6,
        InputAttachmentBit = 1 << 7,
        Reserved13BitKhr = 8192,
        Reserved14BitKhr = 16384,
        Reserved15BitKhr = 32768,
        Reserved10BitKhr = 1024,
        Reserved11BitKhr = 2048,
        Reserved12BitKhr = 4096,
        ShadingRateImageBitNv = 256,
        Reserved16BitQcom = 65536,
        Reserved17BitQcom = 131072,
        FragmentDensityMapBitExt = 512
    }

    [Flags]
    public enum ImageCreateFlags : int
    {
        SparseBindingBit = 1 << 0,
        SparseResidencyBit = 1 << 1,
        SparseAliasedBit = 1 << 2,
        MutableFormatBit = 1 << 3,
        CubeCompatibleBit = 1 << 4,
        CornerSampledBitNv = 8192,
        SampleLocationsCompatibleDepthBitExt = 4096,
        SubsampledBitExt = 16384
    }

    [Flags]
    public enum ImageViewCreateFlags : int
    {
        DynamicBitExt = 1,
        DeferredBitExt = 2
    }

    [Flags]
    public enum SamplerCreateFlags : int
    {
        BitExt = 1,
        CoarseReconstructionBitExt = 2
    }

    [Flags]
    public enum PipelineCreateFlags : int
    {
        DisableOptimizationBit = 1 << 0,
        AllowDerivativesBit = 1 << 1,
        DerivativeBit = 1 << 2,
        RayTracingNoNullAnyHitShadersBitKhr = 16384,
        RayTracingNoNullClosestHitShadersBitKhr = 32768,
        RayTracingNoNullMissShadersBitKhr = 65536,
        RayTracingNoNullIntersectionShadersBitKhr = 131072,
        RayTracingSkipTrianglesBitKhr = 4096,
        RayTracingSkipAabbsBitKhr = 8192,
        DeferCompileBitNv = 32,
        Reserved19BitKhr = 524288,
        CaptureStatisticsBitKhr = 64,
        CaptureInternalRepresentationsBitKhr = 128,
        IndirectBindableBitNv = 262144,
        LibraryBitKhr = 2048,
        FailOnPipelineCompileRequiredBitExt = 256,
        EarlyReturnOnFailureBitExt = 512
    }

    [Flags]
    public enum PipelineShaderStageCreateFlags : int
    {
        Reserved2BitNv = 4,
        AllowVaryingSubgroupSizeBitExt = 1,
        RequireFullSubgroupsBitExt = 2,
        Reserved3BitKhr = 8
    }

    [Flags]
    public enum ColorComponentFlags : int
    {
        RBit = 1 << 0,
        GBit = 1 << 1,
        BBit = 1 << 2,
        ABit = 1 << 3
    }

    [Flags]
    public enum FenceCreateFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum SemaphoreCreateFlags : int
    {
    }

    [Flags]
    public enum FormatFeatureFlags : int
    {
        SampledImageBit = 1 << 0,
        StorageImageBit = 1 << 1,
        StorageImageAtomicBit = 1 << 2,
        UniformTexelBufferBit = 1 << 3,
        StorageTexelBufferBit = 1 << 4,
        StorageTexelBufferAtomicBit = 1 << 5,
        VertexBufferBit = 1 << 6,
        ColorAttachmentBit = 1 << 7,
        ColorAttachmentBlendBit = 1 << 8,
        DepthStencilAttachmentBit = 1 << 9,
        BlitSrcBit = 1 << 10,
        BlitDstBit = 1 << 11,
        SampledImageFilterLinearBit = 1 << 12,
        SampledImageFilterCubicBitImg = 8192,
        Reserved27BitKhr = 134217728,
        Reserved28BitKhr = 268435456,
        Reserved25BitKhr = 33554432,
        Reserved26BitKhr = 67108864,
        AccelerationStructureVertexBufferBitKhr = 536870912,
        FragmentDensityMapBitExt = 16777216,
        AmdReserved30Bit = 1073741824
    }

    [Flags]
    public enum QueryControlFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum QueryResultFlags : int
    {
        N64Bit = 1 << 0,
        WaitBit = 1 << 1,
        WithAvailabilityBit = 1 << 2,
        PartialBit = 1 << 3
    }

    [Flags]
    public enum CommandBufferUsageFlags : int
    {
        OneTimeSubmitBit = 1 << 0,
        RenderPassContinueBit = 1 << 1,
        SimultaneousUseBit = 1 << 2
    }

    [Flags]
    public enum QueryPipelineStatisticFlags : int
    {
        InputAssemblyVerticesBit = 1 << 0,
        InputAssemblyPrimitivesBit = 1 << 1,
        VertexShaderInvocationsBit = 1 << 2,
        GeometryShaderInvocationsBit = 1 << 3,
        GeometryShaderPrimitivesBit = 1 << 4,
        ClippingInvocationsBit = 1 << 5,
        ClippingPrimitivesBit = 1 << 6,
        FragmentShaderInvocationsBit = 1 << 7,
        TessellationControlShaderPatchesBit = 1 << 8,
        TessellationEvaluationShaderInvocationsBit = 1 << 9,
        ComputeShaderInvocationsBit = 1 << 10
    }

    [Flags]
    public enum ImageAspectFlags : int
    {
        ColorBit = 1 << 0,
        DepthBit = 1 << 1,
        StencilBit = 1 << 2,
        MetadataBit = 1 << 3,
        MemoryPlane0BitExt = 128,
        MemoryPlane1BitExt = 256,
        MemoryPlane2BitExt = 512,
        MemoryPlane3BitExt = 1024
    }

    [Flags]
    public enum SparseImageFormatFlags : int
    {
        SingleMiptailBit = 1 << 0,
        AlignedMipSizeBit = 1 << 1,
        NonstandardBlockSizeBit = 1 << 2
    }

    [Flags]
    public enum SparseMemoryBindFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum PipelineStageFlags : int
    {
        TopOfPipeBit = 1 << 0,
        DrawIndirectBit = 1 << 1,
        VertexInputBit = 1 << 2,
        VertexShaderBit = 1 << 3,
        TessellationControlShaderBit = 1 << 4,
        TessellationEvaluationShaderBit = 1 << 5,
        GeometryShaderBit = 1 << 6,
        FragmentShaderBit = 1 << 7,
        EarlyFragmentTestsBit = 1 << 8,
        LateFragmentTestsBit = 1 << 9,
        ColorAttachmentOutputBit = 1 << 10,
        ComputeShaderBit = 1 << 11,
        TransferBit = 1 << 12,
        BottomOfPipeBit = 1 << 13,
        HostBit = 1 << 14,
        AllGraphicsBit = 1 << 15,
        AllCommandsBit = 1 << 16,
        Reserved27BitKhr = 134217728,
        Reserved26BitKhr = 67108864,
        TransformFeedbackBitExt = 16777216,
        ConditionalRenderingBitExt = 262144,
        RayTracingShaderBitKhr = 2097152,
        AccelerationStructureBuildBitKhr = 33554432,
        ShadingRateImageBitNv = 4194304,
        TaskShaderBitNv = 524288,
        MeshShaderBitNv = 1048576,
        FragmentDensityProcessBitExt = 8388608,
        CommandPreprocessBitNv = 131072
    }

    [Flags]
    public enum CommandPoolCreateFlags : int
    {
        TransientBit = 1 << 0,
        ResetCommandBufferBit = 1 << 1
    }

    [Flags]
    public enum CommandPoolResetFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum CommandBufferResetFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum SampleCountFlags : int
    {
        N1Bit = 1 << 0,
        N2Bit = 1 << 1,
        N4Bit = 1 << 2,
        N8Bit = 1 << 3,
        N16Bit = 1 << 4,
        N32Bit = 1 << 5,
        N64Bit = 1 << 6
    }

    [Flags]
    public enum AttachmentDescriptionFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum StencilFaceFlags : int
    {
        FaceFrontBit = 1 << 0,
        FaceBackBit = 1 << 1,
        FaceFrontAndBack = 0x00000003,
        FrontAndBack = FaceFrontAndBack
    }

    [Flags]
    public enum DescriptorPoolCreateFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum DependencyFlags : int
    {
        Bit = 1 << 0
    }

    public enum SemaphoreType : int
    {
        Binary = 0,
        Timeline = 1
    }

    [Flags]
    public enum SemaphoreWaitFlags : int
    {
        Bit = 1 << 0
    }

    public enum PresentModeKhr : int
    {
        ImmediateKhr = 0,
        MailboxKhr = 1,
        FifoKhr = 2,
        FifoRelaxedKhr = 3,
        SharedDemandRefreshKhr = 1000111000,
        SharedContinuousRefreshKhr = 1000111001
    }

    public enum ColorSpaceKhr : int
    {
        ColorSpaceSrgbNonlinearKhr = 0,
        ColorspaceSrgbNonlinearKhr = ColorSpaceSrgbNonlinearKhr,
        ColorSpaceDisplayP3NonlinearExt = 1000104001,
        ColorSpaceExtendedSrgbLinearExt = 1000104002,
        ColorSpaceDisplayP3LinearExt = 1000104003,
        ColorSpaceDciP3NonlinearExt = 1000104004,
        ColorSpaceBt709LinearExt = 1000104005,
        ColorSpaceBt709NonlinearExt = 1000104006,
        ColorSpaceBt2020LinearExt = 1000104007,
        ColorSpaceHdr10St2084Ext = 1000104008,
        ColorSpaceDolbyvisionExt = 1000104009,
        ColorSpaceHdr10HlgExt = 1000104010,
        ColorSpaceAdobergbLinearExt = 1000104011,
        ColorSpaceAdobergbNonlinearExt = 1000104012,
        ColorSpacePassThroughExt = 1000104013,
        ColorSpaceExtendedSrgbNonlinearExt = 1000104014,
        ColorSpaceDisplayNativeAmd = 1000213000
    }

    [Flags]
    public enum DisplayPlaneAlphaFlagsKhr : int
    {
        OpaqueBitKhr = 1 << 0,
        GlobalBitKhr = 1 << 1,
        PerPixelBitKhr = 1 << 2,
        PerPixelPremultipliedBitKhr = 1 << 3
    }

    [Flags]
    public enum CompositeAlphaFlagsKhr : int
    {
        OpaqueBitKhr = 1 << 0,
        PreMultipliedBitKhr = 1 << 1,
        PostMultipliedBitKhr = 1 << 2,
        InheritBitKhr = 1 << 3
    }

    [Flags]
    public enum SurfaceTransformFlagsKhr : int
    {
        IdentityBitKhr = 1 << 0,
        Rotate90BitKhr = 1 << 1,
        Rotate180BitKhr = 1 << 2,
        Rotate270BitKhr = 1 << 3,
        HorizontalMirrorBitKhr = 1 << 4,
        HorizontalMirrorRotate90BitKhr = 1 << 5,
        HorizontalMirrorRotate180BitKhr = 1 << 6,
        HorizontalMirrorRotate270BitKhr = 1 << 7,
        InheritBitKhr = 1 << 8
    }

    [Flags]
    public enum SwapchainImageUsageFlagsAndroid : int
    {
        Android = 1 << 0
    }

    public enum TimeDomainExt : int
    {
        DeviceExt = 0,
        ClockMonotonicExt = 1,
        ClockMonotonicRawExt = 2,
        QueryPerformanceCounterExt = 3
    }

    [Flags]
    public enum DebugReportFlagsExt : int
    {
        InformationBitExt = 1 << 0,
        WarningBitExt = 1 << 1,
        PerformanceWarningBitExt = 1 << 2,
        ErrorBitExt = 1 << 3,
        DebugBitExt = 1 << 4
    }

    public enum DebugReportObjectTypeExt : int
    {
        UnknownExt = 0,
        InstanceExt = 1,
        PhysicalDeviceExt = 2,
        DeviceExt = 3,
        QueueExt = 4,
        SemaphoreExt = 5,
        CommandBufferExt = 6,
        FenceExt = 7,
        DeviceMemoryExt = 8,
        BufferExt = 9,
        ImageExt = 10,
        EventExt = 11,
        QueryPoolExt = 12,
        BufferViewExt = 13,
        ImageViewExt = 14,
        ShaderModuleExt = 15,
        PipelineCacheExt = 16,
        PipelineLayoutExt = 17,
        RenderPassExt = 18,
        PipelineExt = 19,
        DescriptorSetLayoutExt = 20,
        SamplerExt = 21,
        DescriptorPoolExt = 22,
        DescriptorSetExt = 23,
        FramebufferExt = 24,
        CommandPoolExt = 25,
        SurfaceKhrExt = 26,
        SwapchainKhrExt = 27,
        DebugReportCallbackExtExt = 28,
        DebugReportExt = DebugReportCallbackExtExt,
        DisplayKhrExt = 29,
        DisplayModeKhrExt = 30,
        ValidationCacheExtExt = 33,
        ValidationCacheExt = ValidationCacheExtExt,
        AccelerationStructureKhrExt = 1000150000
    }

    public enum RasterizationOrderAmd : int
    {
        StrictAmd = 0,
        RelaxedAmd = 1
    }

    [Flags]
    public enum ExternalMemoryHandleTypeFlagsNv : int
    {
        OpaqueWin32BitNv = 1 << 0,
        OpaqueWin32KmtBitNv = 1 << 1,
        D3d11ImageBitNv = 1 << 2,
        D3d11ImageKmtBitNv = 1 << 3
    }

    [Flags]
    public enum ExternalMemoryFeatureFlagsNv : int
    {
        DedicatedOnlyBitNv = 1 << 0,
        ExportableBitNv = 1 << 1,
        ImportableBitNv = 1 << 2
    }

    public enum ValidationCheckExt : int
    {
        AllExt = 0,
        ShadersExt = 1
    }

    public enum ValidationFeatureEnableExt : int
    {
        GpuAssistedExt = 0,
        GpuAssistedReserveBindingSlotExt = 1,
        BestPracticesExt = 2,
        DebugPrintfExt = 3,
        SynchronizationValidationExt = 4
    }

    public enum ValidationFeatureDisableExt : int
    {
        AllExt = 0,
        ShadersExt = 1,
        ThreadSafetyExt = 2,
        ApiParametersExt = 3,
        ObjectLifetimesExt = 4,
        CoreChecksExt = 5,
        UniqueHandlesExt = 6
    }

    [Flags]
    public enum SubgroupFeatureFlags : int
    {
        BasicBit = 1 << 0,
        VoteBit = 1 << 1,
        ArithmeticBit = 1 << 2,
        BallotBit = 1 << 3,
        ShuffleBit = 1 << 4,
        ShuffleRelativeBit = 1 << 5,
        ClusteredBit = 1 << 6,
        QuadBit = 1 << 7,
        PartitionedBitNv = 256
    }

    [Flags]
    public enum IndirectCommandsLayoutUsageFlagsNv : int
    {
        ExplicitPreprocessBitNv = 1 << 0,
        IndexedSequencesBitNv = 1 << 1,
        UnorderedSequencesBitNv = 1 << 2
    }

    [Flags]
    public enum IndirectStateFlagsNv : int
    {
        Nv = 1 << 0
    }

    public enum IndirectCommandsTokenTypeNv : int
    {
        ShaderGroupNv = 0,
        StateFlagsNv = 1,
        IndexBufferNv = 2,
        VertexBufferNv = 3,
        PushConstantNv = 4,
        DrawIndexedNv = 5,
        DrawNv = 6,
        DrawTasksNv = 7
    }

    [Flags]
    public enum PrivateDataSlotCreateFlagsExt : int
    {
    }

    [Flags]
    public enum DescriptorSetLayoutCreateFlags : int
    {
    }

    [Flags]
    public enum ExternalMemoryHandleTypeFlags : int
    {
        OpaqueFdBit = 1 << 0,
        OpaqueWin32Bit = 1 << 1,
        OpaqueWin32KmtBit = 1 << 2,
        D3d11TextureBit = 1 << 3,
        D3d11TextureKmtBit = 1 << 4,
        D3d12HeapBit = 1 << 5,
        D3d12ResourceBit = 1 << 6,
        DmaBufBitExt = 512,
        AndroidHardwareBufferBitAndroid = 1024,
        HostAllocationBitExt = 128,
        HostMappedForeignMemoryBitExt = 256
    }

    [Flags]
    public enum ExternalMemoryFeatureFlags : int
    {
        DedicatedOnlyBit = 1 << 0,
        ExportableBit = 1 << 1,
        ImportableBit = 1 << 2
    }

    [Flags]
    public enum ExternalSemaphoreHandleTypeFlags : int
    {
        OpaqueFdBit = 1 << 0,
        OpaqueWin32Bit = 1 << 1,
        OpaqueWin32KmtBit = 1 << 2,
        D3d12FenceBit = 1 << 3,
        D3d11FenceBit = D3d12FenceBit,
        SyncFdBit = 1 << 4
    }

    [Flags]
    public enum ExternalSemaphoreFeatureFlags : int
    {
        ExportableBit = 1 << 0,
        ImportableBit = 1 << 1
    }

    [Flags]
    public enum SemaphoreImportFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum ExternalFenceHandleTypeFlags : int
    {
        OpaqueFdBit = 1 << 0,
        OpaqueWin32Bit = 1 << 1,
        OpaqueWin32KmtBit = 1 << 2,
        SyncFdBit = 1 << 3
    }

    [Flags]
    public enum ExternalFenceFeatureFlags : int
    {
        ExportableBit = 1 << 0,
        ImportableBit = 1 << 1
    }

    [Flags]
    public enum FenceImportFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum SurfaceCounterFlagsExt : int
    {
        Ext = 1 << 0
    }

    public enum DisplayPowerStateExt : int
    {
        OffExt = 0,
        SuspendExt = 1,
        OnExt = 2
    }

    public enum DeviceEventTypeExt : int
    {
        Ext = 0
    }

    public enum DisplayEventTypeExt : int
    {
        Ext = 0
    }

    [Flags]
    public enum PeerMemoryFeatureFlags : int
    {
        CopySrcBit = 1 << 0,
        CopyDstBit = 1 << 1,
        GenericSrcBit = 1 << 2,
        GenericDstBit = 1 << 3
    }

    [Flags]
    public enum MemoryAllocateFlags : int
    {
        Bit = 1 << 0
    }

    [Flags]
    public enum DeviceGroupPresentModeFlagsKhr : int
    {
        LocalBitKhr = 1 << 0,
        RemoteBitKhr = 1 << 1,
        SumBitKhr = 1 << 2,
        LocalMultiDeviceBitKhr = 1 << 3
    }

    [Flags]
    public enum SwapchainCreateFlagsKhr : int
    {
        Khr = 4
    }

    public enum ViewportCoordinateSwizzleNv : int
    {
        PositiveXNv = 0,
        NegativeXNv = 1,
        PositiveYNv = 2,
        NegativeYNv = 3,
        PositiveZNv = 4,
        NegativeZNv = 5,
        PositiveWNv = 6,
        NegativeWNv = 7
    }

    public enum DiscardRectangleModeExt : int
    {
        InclusiveExt = 0,
        ExclusiveExt = 1
    }

    [Flags]
    public enum SubpassDescriptionFlags : int
    {
        PerViewAttributesBitNvx = 1,
        PerViewPositionXOnlyBitNvx = 2,
        FragmentRegionBitQcom = 4,
        ShaderResolveBitQcom = 8
    }

    public enum PointClippingBehavior : int
    {
        AllClipPlanes = 0,
        UserClipPlanesOnly = 1
    }

    public enum SamplerReductionMode : int
    {
        WeightedAverage = 0,
        Min = 1,
        Max = 2
    }

    public enum TessellationDomainOrigin : int
    {
        UpperLeft = 0,
        LowerLeft = 1
    }

    public enum SamplerYcbcrModelConversion : int
    {
        RgbIdentity = 0,
        YcbcrIdentity = 1,
        Ycbcr709 = 2,
        Ycbcr601 = 3,
        Ycbcr2020 = 4
    }

    public enum SamplerYcbcrRange : int
    {
        Full = 0,
        Narrow = 1
    }

    public enum ChromaLocation : int
    {
        CositedEven = 0,
        Midpoint = 1
    }

    public enum BlendOverlapExt : int
    {
        UncorrelatedExt = 0,
        DisjointExt = 1,
        ConjointExt = 2
    }

    public enum CoverageModulationModeNv : int
    {
        NoneNv = 0,
        RgbNv = 1,
        AlphaNv = 2,
        RgbaNv = 3
    }

    public enum CoverageReductionModeNv : int
    {
        MergeNv = 0,
        TruncateNv = 1
    }

    public enum ValidationCacheHeaderVersionExt : int
    {
        Ext = 1
    }

    public enum ShaderInfoTypeAmd : int
    {
        StatisticsAmd = 0,
        BinaryAmd = 1,
        DisassemblyAmd = 2
    }

    public enum QueueGlobalPriorityExt : int
    {
        LowExt = 128,
        MediumExt = 256,
        HighExt = 512,
        RealtimeExt = 1024
    }

    [Flags]
    public enum DebugUtilsMessageSeverityFlagsExt : int
    {
        VerboseBitExt = 1 << 0,
        InfoBitExt = 1 << 4,
        WarningBitExt = 1 << 8,
        ErrorBitExt = 1 << 12
    }

    [Flags]
    public enum DebugUtilsMessageTypeFlagsExt : int
    {
        GeneralBitExt = 1 << 0,
        ValidationBitExt = 1 << 1,
        PerformanceBitExt = 1 << 2
    }

    public enum ConservativeRasterizationModeExt : int
    {
        DisabledExt = 0,
        OverestimateExt = 1,
        UnderestimateExt = 2
    }

    [Flags]
    public enum DescriptorBindingFlags : int
    {
        UpdateAfterBindBit = 1 << 0,
        UpdateUnusedWhilePendingBit = 1 << 1,
        PartiallyBoundBit = 1 << 2,
        VariableDescriptorCountBit = 1 << 3
    }

    public enum VendorId : int
    {
        Viv = 0x10001,
        Vsi = 0x10002,
        Kazan = 0x10003,
        Codeplay = 0x10004,
        Mesa = 0x10005
    }

    public enum DriverId : int
    {
        AmdProprietary = 1,
        AmdOpenSource = 2,
        MesaRadv = 3,
        NvidiaProprietary = 4,
        IntelProprietaryWindows = 5,
        IntelOpenSourceMesa = 6,
        ImaginationProprietary = 7,
        QualcommProprietary = 8,
        ArmProprietary = 9,
        GoogleSwiftshader = 10,
        GgpProprietary = 11,
        BroadcomProprietary = 12,
        MesaLlvmpipe = 13,
        Moltenvk = 14
    }

    [Flags]
    public enum ConditionalRenderingFlagsExt : int
    {
        Ext = 1 << 0
    }

    [Flags]
    public enum ResolveModeFlags : int
    {
        None = 0,
        SampleZeroBit = 1 << 0,
        AverageBit = 1 << 1,
        MinBit = 1 << 2,
        MaxBit = 1 << 3
    }

    public enum ShadingRatePaletteEntryNv : int
    {
        NoInvocationsNv = 0,
        N16InvocationsPerPixelNv = 1,
        N8InvocationsPerPixelNv = 2,
        N4InvocationsPerPixelNv = 3,
        N2InvocationsPerPixelNv = 4,
        N1InvocationPerPixelNv = 5,
        N1InvocationPer2x1PixelsNv = 6,
        N1InvocationPer1x2PixelsNv = 7,
        N1InvocationPer2x2PixelsNv = 8,
        N1InvocationPer4x2PixelsNv = 9,
        N1InvocationPer2x4PixelsNv = 10,
        N1InvocationPer4x4PixelsNv = 11
    }

    public enum CoarseSampleOrderTypeNv : int
    {
        DefaultNv = 0,
        CustomNv = 1,
        PixelMajorNv = 2,
        SampleMajorNv = 3
    }

    [Flags]
    public enum GeometryInstanceFlagsKhr : int
    {
        TriangleFacingCullDisableBitKhr = 1 << 0,
        TriangleFrontCounterclockwiseBitKhr = 1 << 1,
        ForceOpaqueBitKhr = 1 << 2,
        ForceNoOpaqueBitKhr = 1 << 3
    }

    [Flags]
    public enum GeometryFlagsKhr : int
    {
        OpaqueBitKhr = 1 << 0,
        NoDuplicateAnyHitInvocationBitKhr = 1 << 1
    }

    [Flags]
    public enum BuildAccelerationStructureFlagsKhr : int
    {
        AllowUpdateBitKhr = 1 << 0,
        AllowCompactionBitKhr = 1 << 1,
        PreferFastTraceBitKhr = 1 << 2,
        PreferFastBuildBitKhr = 1 << 3,
        LowMemoryBitKhr = 1 << 4
    }

    public enum CopyAccelerationStructureModeKhr : int
    {
        CloneKhr = 0,
        CompactKhr = 1,
        SerializeKhr = 2,
        DeserializeKhr = 3
    }

    public enum AccelerationStructureTypeKhr : int
    {
        TopLevelKhr = 0,
        BottomLevelKhr = 1
    }

    public enum GeometryTypeKhr : int
    {
        TrianglesKhr = 0,
        AabbsKhr = 1,
        InstancesKhr = 1000150000
    }

    public enum AccelerationStructureMemoryRequirementsTypeKhr : int
    {
        ObjectKhr = 0,
        BuildScratchKhr = 1,
        UpdateScratchKhr = 2
    }

    public enum AccelerationStructureBuildTypeKhr : int
    {
        HostKhr = 0,
        DeviceKhr = 1,
        HostOrDeviceKhr = 2
    }

    public enum RayTracingShaderGroupTypeKhr : int
    {
        GeneralKhr = 0,
        TrianglesHitGroupKhr = 1,
        ProceduralHitGroupKhr = 2
    }

    public enum MemoryOverallocationBehaviorAmd : int
    {
        DefaultAmd = 0,
        AllowedAmd = 1,
        DisallowedAmd = 2
    }

    [Flags]
    public enum FramebufferCreateFlags : int
    {
    }

    public enum ScopeNv : int
    {
        DeviceNv = 1,
        WorkgroupNv = 2,
        SubgroupNv = 3,
        QueueFamilyNv = 5
    }

    public enum ComponentTypeNv : int
    {
        Float16Nv = 0,
        Float32Nv = 1,
        Float64Nv = 2,
        Sint8Nv = 3,
        Sint16Nv = 4,
        Sint32Nv = 5,
        Sint64Nv = 6,
        Uint8Nv = 7,
        Uint16Nv = 8,
        Uint32Nv = 9,
        Uint64Nv = 10
    }

    [Flags]
    public enum DeviceDiagnosticsConfigFlagsNv : int
    {
        ShaderDebugInfoBitNv = 1 << 0,
        ResourceTrackingBitNv = 1 << 1,
        AutomaticCheckpointsBitNv = 1 << 2
    }

    [Flags]
    public enum PipelineCreationFeedbackFlagsExt : int
    {
        ValidBitExt = 1 << 0,
        ApplicationPipelineCacheHitBitExt = 1 << 1,
        BasePipelineAccelerationBitExt = 1 << 2
    }

    public enum FullScreenExclusiveExt : int
    {
        DefaultExt = 0,
        AllowedExt = 1,
        DisallowedExt = 2,
        ApplicationControlledExt = 3
    }

    public enum PerformanceCounterScopeKhr : int
    {
        PerformanceCounterScopeCommandBufferKhr = 0,
        PerformanceCounterScopeRenderPassKhr = 1,
        PerformanceCounterScopeCommandKhr = 2,
        QueryScopeCommandBufferKhr = PerformanceCounterScopeCommandBufferKhr,
        QueryScopeRenderPassKhr = PerformanceCounterScopeRenderPassKhr,
        QueryScopeCommandKhr = PerformanceCounterScopeCommandKhr
    }

    public enum PerformanceCounterUnitKhr : int
    {
        GenericKhr = 0,
        PercentageKhr = 1,
        NanosecondsKhr = 2,
        BytesKhr = 3,
        BytesPerSecondKhr = 4,
        KelvinKhr = 5,
        WattsKhr = 6,
        VoltsKhr = 7,
        AmpsKhr = 8,
        HertzKhr = 9,
        CyclesKhr = 10
    }

    public enum PerformanceCounterStorageKhr : int
    {
        Int32Khr = 0,
        Int64Khr = 1,
        Uint32Khr = 2,
        Uint64Khr = 3,
        Float32Khr = 4,
        Float64Khr = 5
    }

    [Flags]
    public enum PerformanceCounterDescriptionFlagsKhr : int
    {
        PerformanceImpactingKhr = 1 << 0,
        ConcurrentlyImpactedKhr = 1 << 1
    }

    [Flags]
    public enum AcquireProfilingLockFlagsKhr : int
    {
    }

    [Flags]
    public enum ShaderCorePropertiesFlagsAmd : int
    {
    }

    public enum PerformanceConfigurationTypeIntel : int
    {
        Intel = 0
    }

    public enum QueryPoolSamplingModeIntel : int
    {
        Intel = 0
    }

    public enum PerformanceOverrideTypeIntel : int
    {
        NullHardwareIntel = 0,
        FlushGpuCachesIntel = 1
    }

    public enum PerformanceParameterTypeIntel : int
    {
        HwCountersSupportedIntel = 0,
        StreamMarkerValidBitsIntel = 1
    }

    public enum PerformanceValueTypeIntel : int
    {
        Uint32Intel = 0,
        Uint64Intel = 1,
        FloatIntel = 2,
        BoolIntel = 3,
        StringIntel = 4
    }

    public enum ShaderFloatControlsIndependence : int
    {
        N32BitOnly = 0,
        All = 1,
        None = 2
    }

    public enum PipelineExecutableStatisticFormatKhr : int
    {
        Bool32Khr = 0,
        Int64Khr = 1,
        Uint64Khr = 2,
        Float64Khr = 3
    }

    public enum LineRasterizationModeExt : int
    {
        DefaultExt = 0,
        RectangularExt = 1,
        BresenhamExt = 2,
        RectangularSmoothExt = 3
    }

    [Flags]
    public enum ShaderModuleCreateFlags : int
    {
        Nv = 1
    }

    [Flags]
    public enum PipelineCompilerControlFlagsAmd : int
    {
    }

    [Flags]
    public enum ToolPurposeFlagsExt : int
    {
        ValidationBitExt = 1 << 0,
        ProfilingBitExt = 1 << 1,
        TracingBitExt = 1 << 2,
        AdditionalFeaturesBitExt = 1 << 3,
        ModifyingFeaturesBitExt = 1 << 4
    }

// another structs

    public enum MetalSurfaceCreateFlagsExt
    {
    }

    public enum XcbSurfaceCreateFlagsKhr
    {
    }

    public enum PipelineInputAssemblyStateCreateFlags
    {
    }

    public enum PipelineCoverageModulationStateCreateFlagsNv
    {
    }

    public enum DirectFBSurfaceCreateFlagsExt
    {
    }

    public enum HeadlessSurfaceCreateFlagsExt
    {
    }

    public enum PipelineDynamicStateCreateFlags
    {
    }

    public enum BufferViewCreateFlags
    {
    }

    public enum DeviceCreateFlags
    {
    }

    public enum QueryPoolCreateFlags
    {
    }

    public enum PipelineTessellationStateCreateFlags
    {
    }

    public enum StreamDescriptorSurfaceCreateFlagsGgp
    {
    }

    public enum PipelineMultisampleStateCreateFlags
    {
    }

    public enum WaylandSurfaceCreateFlagsKhr
    {
    }

    public enum PipelineCoverageReductionStateCreateFlagsNv
    {
    }

    public enum IOSSurfaceCreateFlagsMvk
    {
    }

    public enum PipelineVertexInputStateCreateFlags
    {
    }

    public enum PipelineRasterizationStateCreateFlags
    {
    }

    public enum PipelineDepthStencilStateCreateFlags
    {
    }

    public enum PipelineDiscardRectangleStateCreateFlagsExt
    {
    }

    public enum PipelineRasterizationDepthClipStateCreateFlagsExt
    {
    }

    public enum ValidationCacheCreateFlagsExt
    {
    }

    public enum InstanceCreateFlags
    {
    }

    public enum Win32SurfaceCreateFlagsKhr
    {
    }

    public enum PipelineViewportSwizzleStateCreateFlagsNv
    {
    }

    public enum DebugUtilsMessengerCreateFlagsExt
    {
    }

    public enum PipelineColorBlendStateCreateFlags
    {
    }

    public enum AndroidSurfaceCreateFlagsKhr
    {
    }

    public enum EventCreateFlags
    {
    }

    public enum PipelineLayoutCreateFlags
    {
    }

    public enum MacOSSurfaceCreateFlagsMvk
    {
    }

    public enum PipelineRasterizationConservativeStateCreateFlagsExt
    {
    }

    public enum DisplayModeCreateFlagsKhr
    {
    }

    public enum ImagePipeSurfaceCreateFlagsFuchsia
    {
    }

    public enum DisplaySurfaceCreateFlagsKhr
    {
    }

    public enum PipelineViewportStateCreateFlags
    {
    }

    public enum XlibSurfaceCreateFlagsKhr
    {
    }

    public enum PipelineRasterizationStateStreamCreateFlagsExt
    {
    }

    public enum ViSurfaceCreateFlagsNn
    {
    }

    public enum DescriptorUpdateTemplateCreateFlags
    {
    }

    public enum PipelineCoverageToColorStateCreateFlagsNv
    {
    }
}