using System;
using System.Runtime.InteropServices;
using LearnVulkan.Util;

namespace LearnVulkan.Vulkan.Binding.Native
{
    // Original Definition: 
// struct VkBaseOutStructure 
// {
//     VkStructureType sType;
//     VkBaseOutStructure* pNext;
// }
    public unsafe struct BaseOutStructure
    {
        public StructureType SType;
        public BaseOutStructure* PNext;
    }

// Original Definition: 
// struct VkBaseInStructure 
// {
//     VkStructureType sType;
//     VkBaseInStructure* pNext;
// }
    public unsafe struct BaseInStructure
    {
        public StructureType SType;
        public BaseInStructure* PNext;
    }

// Original Definition: 
// struct VkOffset2D 
// {
//     int32_t x;
//     int32_t y;
// }
    public unsafe struct Offset2d
    {
        public int X;
        public int Y;
    }

// Original Definition: 
// struct VkOffset3D 
// {
//     int32_t x;
//     int32_t y;
//     int32_t z;
// }
    public unsafe struct Offset3d
    {
        public int X;
        public int Y;
        public int Z;
    }

// Original Definition: 
// struct VkExtent2D 
// {
//     uint32_t width;
//     uint32_t height;
// }
    public unsafe struct Extent2d
    {
        public uint Width;
        public uint Height;
    }

// Original Definition: 
// struct VkExtent3D 
// {
//     uint32_t width;
//     uint32_t height;
//     uint32_t depth;
// }
    public unsafe struct Extent3d
    {
        public uint Width;
        public uint Height;
        public uint Depth;
    }

// Original Definition: 
// struct VkViewport 
// {
//     float x;
//     float y;
//     float width;
//     float height;
//     float minDepth;
//     float maxDepth;
// }
    public unsafe struct Viewport
    {
        public float X;
        public float Y;
        public float Width;
        public float Height;
        public float MinDepth;
        public float MaxDepth;
    }

// Original Definition: 
// struct VkRect2D 
// {
//     VkOffset2D offset;
//     VkExtent2D extent;
// }
    public unsafe struct Rect2d
    {
        public Offset2d Offset;
        public Extent2d Extent;
    }

// Original Definition: 
// struct VkClearRect 
// {
//     VkRect2D rect;
//     uint32_t baseArrayLayer;
//     uint32_t layerCount;
// }
    public unsafe struct ClearRect
    {
        public Rect2d Rect;
        public uint BaseArrayLayer;
        public uint LayerCount;
    }

// Original Definition: 
// struct VkComponentMapping 
// {
//     VkComponentSwizzle r;
//     VkComponentSwizzle g;
//     VkComponentSwizzle b;
//     VkComponentSwizzle a;
// }
    public unsafe struct ComponentMapping
    {
        public ComponentSwizzle R;
        public ComponentSwizzle G;
        public ComponentSwizzle B;
        public ComponentSwizzle A;
    }

// Original Definition: 
// struct VkPhysicalDeviceProperties 
// {
//     uint32_t apiVersion;
//     uint32_t driverVersion;
//     uint32_t vendorID;
//     uint32_t deviceID;
//     VkPhysicalDeviceType deviceType;
//     char deviceName[(int)ApiConstants.MaxPhysicalDeviceNameSize];
//     uint8_t pipelineCacheUUID[(int)ApiConstants.UuidSize];
//     VkPhysicalDeviceLimits limits;
//     VkPhysicalDeviceSparseProperties sparseProperties;
// }
    public unsafe struct PhysicalDeviceProperties
    {
        public uint ApiVersion;
        public uint DriverVersion;
        public uint VendorID;
        public uint DeviceID;
        public PhysicalDeviceType DeviceType;
        public fixed sbyte DeviceName_[(int) ApiConstants.MaxPhysicalDeviceNameSize];
        public fixed byte PipelineCacheUUID[(int) ApiConstants.UuidSize];
        public PhysicalDeviceLimits Limits;
        public PhysicalDeviceSparseProperties SparseProperties;

        public string DeviceName
        {
            get
            {
                fixed (sbyte* ptr = DeviceName_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }

// Original Definition: 
// struct VkExtensionProperties 
// {
//     char extensionName[(int)ApiConstants.MaxExtensionNameSize];
//     uint32_t specVersion;
// }
    public unsafe struct ExtensionProperties
    {
        public fixed sbyte ExtensionName_[(int) ApiConstants.MaxExtensionNameSize];
        public uint SpecVersion;

        public string ExtensionName
        {
            get
            {
                fixed (sbyte* ptr = ExtensionName_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }

// Original Definition: 
// struct VkLayerProperties 
// {
//     char layerName[(int)ApiConstants.MaxExtensionNameSize];
//     uint32_t specVersion;
//     uint32_t implementationVersion;
//     char description[(int)ApiConstants.MaxDescriptionSize];
// }
    public unsafe struct LayerProperties
    {
        public fixed sbyte LayerName_[(int) ApiConstants.MaxExtensionNameSize];
        public uint SpecVersion;
        public uint ImplementationVersion;
        public fixed sbyte Description_[(int) ApiConstants.MaxDescriptionSize];

        public string LayerName
        {
            get
            {
                fixed (sbyte* ptr = LayerName_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string Description
        {
            get
            {
                fixed (sbyte* ptr = Description_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }

// Original Definition: 
// struct VkApplicationInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     char* pApplicationName;
//     uint32_t applicationVersion;
//     char* pEngineName;
//     uint32_t engineVersion;
//     uint32_t apiVersion;
// }
    public unsafe struct ApplicationInfo
    {
        public StructureType SType;
        public void* PNext;
        public sbyte* PApplicationName;
        public uint ApplicationVersion;
        public sbyte* PEngineName;
        public uint EngineVersion;
        public uint ApiVersion;
    }

// Original Definition: 
// struct VkAllocationCallbacks 
// {
//     void* pUserData;
//     PFN_vkAllocationFunction pfnAllocation;
//     PFN_vkReallocationFunction pfnReallocation;
//     PFN_vkFreeFunction pfnFree;
//     PFN_vkInternalAllocationNotification pfnInternalAllocation;
//     PFN_vkInternalFreeNotification pfnInternalFree;
// }
    public unsafe struct AllocationCallbacks
    {
        public void* PUserData;
        public void* PfnAllocation;
        public void* PfnReallocation;
        public void* PfnFree;
        public void* PfnInternalAllocation;
        public void* PfnInternalFree;
    }

// Original Definition: 
// struct VkDeviceQueueCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceQueueCreateFlags flags;
//     uint32_t queueFamilyIndex;
//     uint32_t queueCount;
//     float* pQueuePriorities;
// }
    public unsafe struct DeviceQueueCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public DeviceQueueCreateFlags Flags;
        public uint QueueFamilyIndex;
        public uint QueueCount;
        public float* PQueuePriorities;
    }

// Original Definition: 
// struct VkDeviceCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceCreateFlags flags;
//     uint32_t queueCreateInfoCount;
//     VkDeviceQueueCreateInfo* pQueueCreateInfos;
//     uint32_t enabledLayerCount;
//     char* const* ppEnabledLayerNames;
//     uint32_t enabledExtensionCount;
//     char* const* ppEnabledExtensionNames;
//     VkPhysicalDeviceFeatures* pEnabledFeatures;
// }
    public unsafe struct DeviceCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public DeviceCreateFlags Flags;
        public uint QueueCreateInfoCount;
        public DeviceQueueCreateInfo* PQueueCreateInfos;
        public uint EnabledLayerCount;
        public sbyte** PpEnabledLayerNames;
        public uint EnabledExtensionCount;
        public sbyte** PpEnabledExtensionNames;
        public PhysicalDeviceFeatures* PEnabledFeatures;
    }

// Original Definition: 
// struct VkInstanceCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkInstanceCreateFlags flags;
//     VkApplicationInfo* pApplicationInfo;
//     uint32_t enabledLayerCount;
//     char* const* ppEnabledLayerNames;
//     uint32_t enabledExtensionCount;
//     char* const* ppEnabledExtensionNames;
// }
    public unsafe struct InstanceCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public InstanceCreateFlags Flags;
        public ApplicationInfo* PApplicationInfo;
        public uint EnabledLayerCount;
        public sbyte** PpEnabledLayerNames;
        public uint EnabledExtensionCount;
        public sbyte** PpEnabledExtensionNames;
    }

// Original Definition: 
// struct VkQueueFamilyProperties 
// {
//     VkQueueFlags queueFlags;
//     uint32_t queueCount;
//     uint32_t timestampValidBits;
//     VkExtent3D minImageTransferGranularity;
// }
    public unsafe struct QueueFamilyProperties
    {
        public QueueFlags QueueFlags;
        public uint QueueCount;
        public uint TimestampValidBits;
        public Extent3d MinImageTransferGranularity;
    }

// Original Definition: 
// struct VkPhysicalDeviceMemoryProperties 
// {
//     uint32_t memoryTypeCount;
//     VkMemoryType memoryTypes[(int)ApiConstants.MaxMemoryTypes];
//     uint32_t memoryHeapCount;
//     VkMemoryHeap memoryHeaps[(int)ApiConstants.MaxMemoryHeaps];
// }
    public unsafe struct PhysicalDeviceMemoryProperties
    {
        public uint MemoryTypeCount;
        public fixed byte MemoryTypes_[(int) ApiConstants.MaxMemoryTypes * 8]; // * sizeof(MemoryType)
        public uint MemoryHeapCount;
        public fixed byte MemoryHeaps_[(int) ApiConstants.MaxMemoryHeaps * 16]; // * sizeof(MemoryHeap)

        public MemoryType[] MemoryTypes
        {
            get
            {
                var types = new MemoryType[MemoryTypeCount];
                fixed (byte* bytePtr = MemoryTypes_)
                {
                    var ptr = (MemoryType*) bytePtr;

                    for (var i = 0; i < MemoryTypeCount; ++i)
                        types[i] = ptr[i];
                }

                return types;
            }
        }

        public MemoryHeap[] MemoryHeaps
        {
            get
            {
                var heaps = new MemoryHeap[MemoryHeapCount];
                fixed (byte* bytePtr = MemoryHeaps_)
                {
                    var ptr = (MemoryHeap*) bytePtr;

                    for (var i = 0; i < MemoryHeapCount; ++i)
                        heaps[i] = ptr[i];
                }

                return heaps;
            }
        }
    }

// Original Definition: 
// struct VkMemoryAllocateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceSize allocationSize;
//     uint32_t memoryTypeIndex;
// }
    public unsafe struct MemoryAllocateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ulong AllocationSize;
        public uint MemoryTypeIndex;
    }

// Original Definition: 
// struct VkMemoryRequirements 
// {
//     VkDeviceSize size;
//     VkDeviceSize alignment;
//     uint32_t memoryTypeBits;
// }
    public unsafe struct MemoryRequirements
    {
        public ulong Size;
        public ulong Alignment;
        public uint MemoryTypeBits;
    }

// Original Definition: 
// struct VkSparseImageFormatProperties 
// {
//     VkImageAspectFlags aspectMask;
//     VkExtent3D imageGranularity;
//     VkSparseImageFormatFlags flags;
// }
    public unsafe struct SparseImageFormatProperties
    {
        public ImageAspectFlags AspectMask;
        public Extent3d ImageGranularity;
        public SparseImageFormatFlags Flags;
    }

// Original Definition: 
// struct VkSparseImageMemoryRequirements 
// {
//     VkSparseImageFormatProperties formatProperties;
//     uint32_t imageMipTailFirstLod;
//     VkDeviceSize imageMipTailSize;
//     VkDeviceSize imageMipTailOffset;
//     VkDeviceSize imageMipTailStride;
// }
    public unsafe struct SparseImageMemoryRequirements
    {
        public SparseImageFormatProperties FormatProperties;
        public uint ImageMipTailFirstLod;
        public ulong ImageMipTailSize;
        public ulong ImageMipTailOffset;
        public ulong ImageMipTailStride;
    }

// Original Definition: 
// struct VkMemoryType 
// {
//     VkMemoryPropertyFlags propertyFlags;
//     uint32_t heapIndex;
// }
    public unsafe struct MemoryType
    {
        public MemoryPropertyFlags PropertyFlags;
        public uint HeapIndex;
    }

// Original Definition: 
// struct VkMemoryHeap 
// {
//     VkDeviceSize size;
//     VkMemoryHeapFlags flags;
// }
    public unsafe struct MemoryHeap
    {
        public ulong Size;
        public MemoryHeapFlags Flags;
    }

// Original Definition: 
// struct VkMappedMemoryRange 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceMemory memory;
//     VkDeviceSize offset;
//     VkDeviceSize size;
// }
    public unsafe struct MappedMemoryRange
    {
        public StructureType SType;
        public void* PNext;
        public DeviceMemory Memory;
        public ulong Offset;
        public ulong Size;
    }

// Original Definition: 
// struct VkFormatProperties 
// {
//     VkFormatFeatureFlags linearTilingFeatures;
//     VkFormatFeatureFlags optimalTilingFeatures;
//     VkFormatFeatureFlags bufferFeatures;
// }
    public unsafe struct FormatProperties
    {
        public FormatFeatureFlags LinearTilingFeatures;
        public FormatFeatureFlags OptimalTilingFeatures;
        public FormatFeatureFlags BufferFeatures;
    }

// Original Definition: 
// struct VkImageFormatProperties 
// {
//     VkExtent3D maxExtent;
//     uint32_t maxMipLevels;
//     uint32_t maxArrayLayers;
//     VkSampleCountFlags sampleCounts;
//     VkDeviceSize maxResourceSize;
// }
    public unsafe struct ImageFormatProperties
    {
        public Extent3d MaxExtent;
        public uint MaxMipLevels;
        public uint MaxArrayLayers;
        public SampleCountFlags SampleCounts;
        public ulong MaxResourceSize;
    }

// Original Definition: 
// struct VkDescriptorBufferInfo 
// {
//     VkBuffer buffer;
//     VkDeviceSize offset;
//     VkDeviceSize range;
// }
    public unsafe struct DescriptorBufferInfo
    {
        public Buffer Buffer;
        public ulong Offset;
        public ulong Range;
    }

// Original Definition: 
// struct VkDescriptorImageInfo 
// {
//     VkSampler sampler;
//     VkImageView imageView;
//     VkImageLayout imageLayout;
// }
    public unsafe struct DescriptorImageInfo
    {
        public Sampler Sampler;
        public ImageView ImageView;
        public ImageLayout ImageLayout;
    }

// Original Definition: 
// struct VkWriteDescriptorSet 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDescriptorSet dstSet;
//     uint32_t dstBinding;
//     uint32_t dstArrayElement;
//     uint32_t descriptorCount;
//     VkDescriptorType descriptorType;
//     VkDescriptorImageInfo* pImageInfo;
//     VkDescriptorBufferInfo* pBufferInfo;
//     VkBufferView* pTexelBufferView;
// }
    public unsafe struct WriteDescriptorSet
    {
        public StructureType SType;
        public void* PNext;
        public DescriptorSet DstSet;
        public uint DstBinding;
        public uint DstArrayElement;
        public uint DescriptorCount;
        public DescriptorType DescriptorType;
        public DescriptorImageInfo* PImageInfo;
        public DescriptorBufferInfo* PBufferInfo;
        public BufferView* PTexelBufferView;
    }

// Original Definition: 
// struct VkCopyDescriptorSet 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDescriptorSet srcSet;
//     uint32_t srcBinding;
//     uint32_t srcArrayElement;
//     VkDescriptorSet dstSet;
//     uint32_t dstBinding;
//     uint32_t dstArrayElement;
//     uint32_t descriptorCount;
// }
    public unsafe struct CopyDescriptorSet
    {
        public StructureType SType;
        public void* PNext;
        public DescriptorSet SrcSet;
        public uint SrcBinding;
        public uint SrcArrayElement;
        public DescriptorSet DstSet;
        public uint DstBinding;
        public uint DstArrayElement;
        public uint DescriptorCount;
    }

// Original Definition: 
// struct VkBufferCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBufferCreateFlags flags;
//     VkDeviceSize size;
//     VkBufferUsageFlags usage;
//     VkSharingMode sharingMode;
//     uint32_t queueFamilyIndexCount;
//     uint32_t* pQueueFamilyIndices;
// }
    public unsafe struct BufferCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public BufferCreateFlags Flags;
        public ulong Size;
        public BufferUsageFlags Usage;
        public SharingMode SharingMode;
        public uint QueueFamilyIndexCount;
        public uint* PQueueFamilyIndices;
    }

// Original Definition: 
// struct VkBufferViewCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBufferViewCreateFlags flags;
//     VkBuffer buffer;
//     VkFormat format;
//     VkDeviceSize offset;
//     VkDeviceSize range;
// }
    public unsafe struct BufferViewCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public BufferViewCreateFlags Flags;
        public Buffer Buffer;
        public Format Format;
        public ulong Offset;
        public ulong Range;
    }

// Original Definition: 
// struct VkImageSubresource 
// {
//     VkImageAspectFlags aspectMask;
//     uint32_t mipLevel;
//     uint32_t arrayLayer;
// }
    public unsafe struct ImageSubresource
    {
        public ImageAspectFlags AspectMask;
        public uint MipLevel;
        public uint ArrayLayer;
    }

// Original Definition: 
// struct VkImageSubresourceLayers 
// {
//     VkImageAspectFlags aspectMask;
//     uint32_t mipLevel;
//     uint32_t baseArrayLayer;
//     uint32_t layerCount;
// }
    public unsafe struct ImageSubresourceLayers
    {
        public ImageAspectFlags AspectMask;
        public uint MipLevel;
        public uint BaseArrayLayer;
        public uint LayerCount;
    }

// Original Definition: 
// struct VkImageSubresourceRange 
// {
//     VkImageAspectFlags aspectMask;
//     uint32_t baseMipLevel;
//     uint32_t levelCount;
//     uint32_t baseArrayLayer;
//     uint32_t layerCount;
// }
    public unsafe struct ImageSubresourceRange
    {
        public ImageAspectFlags AspectMask;
        public uint BaseMipLevel;
        public uint LevelCount;
        public uint BaseArrayLayer;
        public uint LayerCount;
    }

// Original Definition: 
// struct VkMemoryBarrier 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccessFlags srcAccessMask;
//     VkAccessFlags dstAccessMask;
// }
    public unsafe struct MemoryBarrier
    {
        public StructureType SType;
        public void* PNext;
        public AccessFlags SrcAccessMask;
        public AccessFlags DstAccessMask;
    }

// Original Definition: 
// struct VkBufferMemoryBarrier 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccessFlags srcAccessMask;
//     VkAccessFlags dstAccessMask;
//     uint32_t srcQueueFamilyIndex;
//     uint32_t dstQueueFamilyIndex;
//     VkBuffer buffer;
//     VkDeviceSize offset;
//     VkDeviceSize size;
// }
    public unsafe struct BufferMemoryBarrier
    {
        public StructureType SType;
        public void* PNext;
        public AccessFlags SrcAccessMask;
        public AccessFlags DstAccessMask;
        public uint SrcQueueFamilyIndex;
        public uint DstQueueFamilyIndex;
        public Buffer Buffer;
        public ulong Offset;
        public ulong Size;
    }

// Original Definition: 
// struct VkImageMemoryBarrier 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccessFlags srcAccessMask;
//     VkAccessFlags dstAccessMask;
//     VkImageLayout oldLayout;
//     VkImageLayout newLayout;
//     uint32_t srcQueueFamilyIndex;
//     uint32_t dstQueueFamilyIndex;
//     VkImage image;
//     VkImageSubresourceRange subresourceRange;
// }
    public unsafe struct ImageMemoryBarrier
    {
        public StructureType SType;
        public void* PNext;
        public AccessFlags SrcAccessMask;
        public AccessFlags DstAccessMask;
        public ImageLayout OldLayout;
        public ImageLayout NewLayout;
        public uint SrcQueueFamilyIndex;
        public uint DstQueueFamilyIndex;
        public Image Image;
        public ImageSubresourceRange SubresourceRange;
    }

// Original Definition: 
// struct VkImageCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageCreateFlags flags;
//     VkImageType imageType;
//     VkFormat format;
//     VkExtent3D extent;
//     uint32_t mipLevels;
//     uint32_t arrayLayers;
//     VkSampleCountFlagBits samples;
//     VkImageTiling tiling;
//     VkImageUsageFlags usage;
//     VkSharingMode sharingMode;
//     uint32_t queueFamilyIndexCount;
//     uint32_t* pQueueFamilyIndices;
//     VkImageLayout initialLayout;
// }
    public unsafe struct ImageCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ImageCreateFlags Flags;
        public ImageType ImageType;
        public Format Format;
        public Extent3d Extent;
        public uint MipLevels;
        public uint ArrayLayers;
        public SampleCountFlags Samples;
        public ImageTiling Tiling;
        public ImageUsageFlags Usage;
        public SharingMode SharingMode;
        public uint QueueFamilyIndexCount;
        public uint* PQueueFamilyIndices;
        public ImageLayout InitialLayout;
    }

// Original Definition: 
// struct VkSubresourceLayout 
// {
//     VkDeviceSize offset;
//     VkDeviceSize size;
//     VkDeviceSize rowPitch;
//     VkDeviceSize arrayPitch;
//     VkDeviceSize depthPitch;
// }
    public unsafe struct SubresourceLayout
    {
        public ulong Offset;
        public ulong Size;
        public ulong RowPitch;
        public ulong ArrayPitch;
        public ulong DepthPitch;
    }

// Original Definition: 
// struct VkImageViewCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageViewCreateFlags flags;
//     VkImage image;
//     VkImageViewType viewType;
//     VkFormat format;
//     VkComponentMapping components;
//     VkImageSubresourceRange subresourceRange;
// }
    public unsafe struct ImageViewCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ImageViewCreateFlags Flags;
        public Image Image;
        public ImageViewType ViewType;
        public Format Format;
        public ComponentMapping Components;
        public ImageSubresourceRange SubresourceRange;
    }

// Original Definition: 
// struct VkBufferCopy 
// {
//     VkDeviceSize srcOffset;
//     VkDeviceSize dstOffset;
//     VkDeviceSize size;
// }
    public unsafe struct BufferCopy
    {
        public ulong SrcOffset;
        public ulong DstOffset;
        public ulong Size;
    }

// Original Definition: 
// struct VkSparseMemoryBind 
// {
//     VkDeviceSize resourceOffset;
//     VkDeviceSize size;
//     VkDeviceMemory memory;
//     VkDeviceSize memoryOffset;
//     VkSparseMemoryBindFlags flags;
// }
    public unsafe struct SparseMemoryBind
    {
        public ulong ResourceOffset;
        public ulong Size;
        public DeviceMemory Memory;
        public ulong MemoryOffset;
        public SparseMemoryBindFlags Flags;
    }

// Original Definition: 
// struct VkSparseImageMemoryBind 
// {
//     VkImageSubresource subresource;
//     VkOffset3D offset;
//     VkExtent3D extent;
//     VkDeviceMemory memory;
//     VkDeviceSize memoryOffset;
//     VkSparseMemoryBindFlags flags;
// }
    public unsafe struct SparseImageMemoryBind
    {
        public ImageSubresource Subresource;
        public Offset3d Offset;
        public Extent3d Extent;
        public DeviceMemory Memory;
        public ulong MemoryOffset;
        public SparseMemoryBindFlags Flags;
    }

// Original Definition: 
// struct VkSparseBufferMemoryBindInfo 
// {
//     VkBuffer buffer;
//     uint32_t bindCount;
//     VkSparseMemoryBind* pBinds;
// }
    public unsafe struct SparseBufferMemoryBindInfo
    {
        public Buffer Buffer;
        public uint BindCount;
        public SparseMemoryBind* PBinds;
    }

// Original Definition: 
// struct VkSparseImageOpaqueMemoryBindInfo 
// {
//     VkImage image;
//     uint32_t bindCount;
//     VkSparseMemoryBind* pBinds;
// }
    public unsafe struct SparseImageOpaqueMemoryBindInfo
    {
        public Image Image;
        public uint BindCount;
        public SparseMemoryBind* PBinds;
    }

// Original Definition: 
// struct VkSparseImageMemoryBindInfo 
// {
//     VkImage image;
//     uint32_t bindCount;
//     VkSparseImageMemoryBind* pBinds;
// }
    public unsafe struct SparseImageMemoryBindInfo
    {
        public Image Image;
        public uint BindCount;
        public SparseImageMemoryBind* PBinds;
    }

// Original Definition: 
// struct VkBindSparseInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t waitSemaphoreCount;
//     VkSemaphore* pWaitSemaphores;
//     uint32_t bufferBindCount;
//     VkSparseBufferMemoryBindInfo* pBufferBinds;
//     uint32_t imageOpaqueBindCount;
//     VkSparseImageOpaqueMemoryBindInfo* pImageOpaqueBinds;
//     uint32_t imageBindCount;
//     VkSparseImageMemoryBindInfo* pImageBinds;
//     uint32_t signalSemaphoreCount;
//     VkSemaphore* pSignalSemaphores;
// }
    public unsafe struct BindSparseInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint WaitSemaphoreCount;
        public Semaphore* PWaitSemaphores;
        public uint BufferBindCount;
        public SparseBufferMemoryBindInfo* PBufferBinds;
        public uint ImageOpaqueBindCount;
        public SparseImageOpaqueMemoryBindInfo* PImageOpaqueBinds;
        public uint ImageBindCount;
        public SparseImageMemoryBindInfo* PImageBinds;
        public uint SignalSemaphoreCount;
        public Semaphore* PSignalSemaphores;
    }

// Original Definition: 
// struct VkImageCopy 
// {
//     VkImageSubresourceLayers srcSubresource;
//     VkOffset3D srcOffset;
//     VkImageSubresourceLayers dstSubresource;
//     VkOffset3D dstOffset;
//     VkExtent3D extent;
// }
    public unsafe struct ImageCopy
    {
        public ImageSubresourceLayers SrcSubresource;
        public Offset3d SrcOffset;
        public ImageSubresourceLayers DstSubresource;
        public Offset3d DstOffset;
        public Extent3d Extent;
    }

// Original Definition: 
// struct VkImageBlit 
// {
//     VkImageSubresourceLayers srcSubresource;
//     VkOffset3D srcOffsets;
//     VkImageSubresourceLayers dstSubresource;
//     VkOffset3D dstOffsets;
// }
    public unsafe struct ImageBlit
    {
        public ImageSubresourceLayers SrcSubresource;
        public Offset3d SrcOffsets;
        public ImageSubresourceLayers DstSubresource;
        public Offset3d DstOffsets;
    }

// Original Definition: 
// struct VkBufferImageCopy 
// {
//     VkDeviceSize bufferOffset;
//     uint32_t bufferRowLength;
//     uint32_t bufferImageHeight;
//     VkImageSubresourceLayers imageSubresource;
//     VkOffset3D imageOffset;
//     VkExtent3D imageExtent;
// }
    public unsafe struct BufferImageCopy
    {
        public ulong BufferOffset;
        public uint BufferRowLength;
        public uint BufferImageHeight;
        public ImageSubresourceLayers ImageSubresource;
        public Offset3d ImageOffset;
        public Extent3d ImageExtent;
    }

// Original Definition: 
// struct VkImageResolve 
// {
//     VkImageSubresourceLayers srcSubresource;
//     VkOffset3D srcOffset;
//     VkImageSubresourceLayers dstSubresource;
//     VkOffset3D dstOffset;
//     VkExtent3D extent;
// }
    public unsafe struct ImageResolve
    {
        public ImageSubresourceLayers SrcSubresource;
        public Offset3d SrcOffset;
        public ImageSubresourceLayers DstSubresource;
        public Offset3d DstOffset;
        public Extent3d Extent;
    }

// Original Definition: 
// struct VkShaderModuleCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkShaderModuleCreateFlags flags;
//     size_t codeSize;
//     uint32_t* pCode;
// }
    public unsafe struct ShaderModuleCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ShaderModuleCreateFlags Flags;
        public IntPtr CodeSize;
        public uint* PCode;
    }

// Original Definition: 
// struct VkDescriptorSetLayoutBinding 
// {
//     uint32_t binding;
//     VkDescriptorType descriptorType;
//     uint32_t descriptorCount;
//     VkShaderStageFlags stageFlags;
//     VkSampler* pImmutableSamplers;
// }
    public unsafe struct DescriptorSetLayoutBinding
    {
        public uint Binding;
        public DescriptorType DescriptorType;
        public uint DescriptorCount;
        public ShaderStageFlags StageFlags;
        public Sampler* PImmutableSamplers;
    }

// Original Definition: 
// struct VkDescriptorSetLayoutCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDescriptorSetLayoutCreateFlags flags;
//     uint32_t bindingCount;
//     VkDescriptorSetLayoutBinding* pBindings;
// }
    public unsafe struct DescriptorSetLayoutCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public DescriptorSetLayoutCreateFlags Flags;
        public uint BindingCount;
        public DescriptorSetLayoutBinding* PBindings;
    }

// Original Definition: 
// struct VkDescriptorPoolSize 
// {
//     VkDescriptorType type;
//     uint32_t descriptorCount;
// }
    public unsafe struct DescriptorPoolSize
    {
        public DescriptorType Type;
        public uint DescriptorCount;
    }

// Original Definition: 
// struct VkDescriptorPoolCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDescriptorPoolCreateFlags flags;
//     uint32_t maxSets;
//     uint32_t poolSizeCount;
//     VkDescriptorPoolSize* pPoolSizes;
// }
    public unsafe struct DescriptorPoolCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public DescriptorPoolCreateFlags Flags;
        public uint MaxSets;
        public uint PoolSizeCount;
        public DescriptorPoolSize* PPoolSizes;
    }

// Original Definition: 
// struct VkDescriptorSetAllocateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDescriptorPool descriptorPool;
//     uint32_t descriptorSetCount;
//     VkDescriptorSetLayout* pSetLayouts;
// }
    public unsafe struct DescriptorSetAllocateInfo
    {
        public StructureType SType;
        public void* PNext;
        public DescriptorPool DescriptorPool;
        public uint DescriptorSetCount;
        public DescriptorSetLayout* PSetLayouts;
    }

// Original Definition: 
// struct VkSpecializationMapEntry 
// {
//     uint32_t constantID;
//     uint32_t offset;
//     size_t size;
// }
    public unsafe struct SpecializationMapEntry
    {
        public uint ConstantID;
        public uint Offset;
        public IntPtr Size;
    }

// Original Definition: 
// struct VkSpecializationInfo 
// {
//     uint32_t mapEntryCount;
//     VkSpecializationMapEntry* pMapEntries;
//     size_t dataSize;
//     void* pData;
// }
    public unsafe struct SpecializationInfo
    {
        public uint MapEntryCount;
        public SpecializationMapEntry* PMapEntries;
        public IntPtr DataSize;
        public void* PData;
    }

// Original Definition: 
// struct VkPipelineShaderStageCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineShaderStageCreateFlags flags;
//     VkShaderStageFlagBits stage;
//     VkShaderModule module;
//     char* pName;
//     VkSpecializationInfo* pSpecializationInfo;
// }
    public unsafe struct PipelineShaderStageCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineShaderStageCreateFlags Flags;
        public ShaderStageFlags Stage;
        public ShaderModule Module;
        public sbyte* PName;
        public SpecializationInfo* PSpecializationInfo;
    }

// Original Definition: 
// struct VkComputePipelineCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCreateFlags flags;
//     VkPipelineShaderStageCreateInfo stage;
//     VkPipelineLayout layout;
//     VkPipeline basePipelineHandle;
//     int32_t basePipelineIndex;
// }
    public unsafe struct ComputePipelineCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCreateFlags Flags;
        public PipelineShaderStageCreateInfo Stage;
        public PipelineLayout Layout;
        public Pipeline BasePipelineHandle;
        public int BasePipelineIndex;
    }

// Original Definition: 
// struct VkVertexInputBindingDescription 
// {
//     uint32_t binding;
//     uint32_t stride;
//     VkVertexInputRate inputRate;
// }
    public unsafe struct VertexInputBindingDescription
    {
        public uint Binding;
        public uint Stride;
        public VertexInputRate InputRate;
    }

// Original Definition: 
// struct VkVertexInputAttributeDescription 
// {
//     uint32_t location;
//     uint32_t binding;
//     VkFormat format;
//     uint32_t offset;
// }
    public unsafe struct VertexInputAttributeDescription
    {
        public uint Location;
        public uint Binding;
        public Format Format;
        public uint Offset;
    }

// Original Definition: 
// struct VkPipelineVertexInputStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineVertexInputStateCreateFlags flags;
//     uint32_t vertexBindingDescriptionCount;
//     VkVertexInputBindingDescription* pVertexBindingDescriptions;
//     uint32_t vertexAttributeDescriptionCount;
//     VkVertexInputAttributeDescription* pVertexAttributeDescriptions;
// }
    public unsafe struct PipelineVertexInputStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineVertexInputStateCreateFlags Flags;
        public uint VertexBindingDescriptionCount;
        public VertexInputBindingDescription* PVertexBindingDescriptions;
        public uint VertexAttributeDescriptionCount;
        public VertexInputAttributeDescription* PVertexAttributeDescriptions;
    }

// Original Definition: 
// struct VkPipelineInputAssemblyStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineInputAssemblyStateCreateFlags flags;
//     VkPrimitiveTopology topology;
//     VkBool32 primitiveRestartEnable;
// }
    public unsafe struct PipelineInputAssemblyStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineInputAssemblyStateCreateFlags Flags;
        public PrimitiveTopology Topology;
        public uint PrimitiveRestartEnable;
    }

// Original Definition: 
// struct VkPipelineTessellationStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineTessellationStateCreateFlags flags;
//     uint32_t patchControlPoints;
// }
    public unsafe struct PipelineTessellationStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineTessellationStateCreateFlags Flags;
        public uint PatchControlPoints;
    }

// Original Definition: 
// struct VkPipelineViewportStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineViewportStateCreateFlags flags;
//     uint32_t viewportCount;
//     VkViewport* pViewports;
//     uint32_t scissorCount;
//     VkRect2D* pScissors;
// }
    public unsafe struct PipelineViewportStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineViewportStateCreateFlags Flags;
        public uint ViewportCount;
        public Viewport* PViewports;
        public uint ScissorCount;
        public Rect2d* PScissors;
    }

// Original Definition: 
// struct VkPipelineRasterizationStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineRasterizationStateCreateFlags flags;
//     VkBool32 depthClampEnable;
//     VkBool32 rasterizerDiscardEnable;
//     VkPolygonMode polygonMode;
//     VkCullModeFlags cullMode;
//     VkFrontFace frontFace;
//     VkBool32 depthBiasEnable;
//     float depthBiasConstantFactor;
//     float depthBiasClamp;
//     float depthBiasSlopeFactor;
//     float lineWidth;
// }
    public unsafe struct PipelineRasterizationStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineRasterizationStateCreateFlags Flags;
        public uint DepthClampEnable;
        public uint RasterizerDiscardEnable;
        public PolygonMode PolygonMode;
        public CullModeFlags CullMode;
        public FrontFace FrontFace;
        public uint DepthBiasEnable;
        public float DepthBiasConstantFactor;
        public float DepthBiasClamp;
        public float DepthBiasSlopeFactor;
        public float LineWidth;
    }

// Original Definition: 
// struct VkPipelineMultisampleStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineMultisampleStateCreateFlags flags;
//     VkSampleCountFlagBits rasterizationSamples;
//     VkBool32 sampleShadingEnable;
//     float minSampleShading;
//     VkSampleMask* pSampleMask;
//     VkBool32 alphaToCoverageEnable;
//     VkBool32 alphaToOneEnable;
// }
    public unsafe struct PipelineMultisampleStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineMultisampleStateCreateFlags Flags;
        public SampleCountFlags RasterizationSamples;
        public uint SampleShadingEnable;
        public float MinSampleShading;
        public uint* PSampleMask;
        public uint AlphaToCoverageEnable;
        public uint AlphaToOneEnable;
    }

// Original Definition: 
// struct VkPipelineColorBlendAttachmentState 
// {
//     VkBool32 blendEnable;
//     VkBlendFactor srcColorBlendFactor;
//     VkBlendFactor dstColorBlendFactor;
//     VkBlendOp colorBlendOp;
//     VkBlendFactor srcAlphaBlendFactor;
//     VkBlendFactor dstAlphaBlendFactor;
//     VkBlendOp alphaBlendOp;
//     VkColorComponentFlags colorWriteMask;
// }
    public unsafe struct PipelineColorBlendAttachmentState
    {
        public uint BlendEnable;
        public BlendFactor SrcColorBlendFactor;
        public BlendFactor DstColorBlendFactor;
        public BlendOp ColorBlendOp;
        public BlendFactor SrcAlphaBlendFactor;
        public BlendFactor DstAlphaBlendFactor;
        public BlendOp AlphaBlendOp;
        public ColorComponentFlags ColorWriteMask;
    }

// Original Definition: 
// struct VkPipelineColorBlendStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineColorBlendStateCreateFlags flags;
//     VkBool32 logicOpEnable;
//     VkLogicOp logicOp;
//     uint32_t attachmentCount;
//     VkPipelineColorBlendAttachmentState* pAttachments;
//     float blendConstants;
// }
    public unsafe struct PipelineColorBlendStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineColorBlendStateCreateFlags Flags;
        public uint LogicOpEnable;
        public LogicOp LogicOp;
        public uint AttachmentCount;
        public PipelineColorBlendAttachmentState* PAttachments;
        public float* BlendConstants4;
    }

// Original Definition: 
// struct VkPipelineDynamicStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineDynamicStateCreateFlags flags;
//     uint32_t dynamicStateCount;
//     VkDynamicState* pDynamicStates;
// }
    public unsafe struct PipelineDynamicStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineDynamicStateCreateFlags Flags;
        public uint DynamicStateCount;
        public DynamicState* PDynamicStates;
    }

// Original Definition: 
// struct VkStencilOpState 
// {
//     VkStencilOp failOp;
//     VkStencilOp passOp;
//     VkStencilOp depthFailOp;
//     VkCompareOp compareOp;
//     uint32_t compareMask;
//     uint32_t writeMask;
//     uint32_t reference;
// }
    public unsafe struct StencilOpState
    {
        public StencilOp FailOp;
        public StencilOp PassOp;
        public StencilOp DepthFailOp;
        public CompareOp CompareOp;
        public uint CompareMask;
        public uint WriteMask;
        public uint Reference;
    }

// Original Definition: 
// struct VkPipelineDepthStencilStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineDepthStencilStateCreateFlags flags;
//     VkBool32 depthTestEnable;
//     VkBool32 depthWriteEnable;
//     VkCompareOp depthCompareOp;
//     VkBool32 depthBoundsTestEnable;
//     VkBool32 stencilTestEnable;
//     VkStencilOpState front;
//     VkStencilOpState back;
//     float minDepthBounds;
//     float maxDepthBounds;
// }
    public unsafe struct PipelineDepthStencilStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineDepthStencilStateCreateFlags Flags;
        public uint DepthTestEnable;
        public uint DepthWriteEnable;
        public CompareOp DepthCompareOp;
        public uint DepthBoundsTestEnable;
        public uint StencilTestEnable;
        public StencilOpState Front;
        public StencilOpState Back;
        public float MinDepthBounds;
        public float MaxDepthBounds;
    }

// Original Definition: 
// struct VkGraphicsPipelineCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCreateFlags flags;
//     uint32_t stageCount;
//     VkPipelineShaderStageCreateInfo* pStages;
//     VkPipelineVertexInputStateCreateInfo* pVertexInputState;
//     VkPipelineInputAssemblyStateCreateInfo* pInputAssemblyState;
//     VkPipelineTessellationStateCreateInfo* pTessellationState;
//     VkPipelineViewportStateCreateInfo* pViewportState;
//     VkPipelineRasterizationStateCreateInfo* pRasterizationState;
//     VkPipelineMultisampleStateCreateInfo* pMultisampleState;
//     VkPipelineDepthStencilStateCreateInfo* pDepthStencilState;
//     VkPipelineColorBlendStateCreateInfo* pColorBlendState;
//     VkPipelineDynamicStateCreateInfo* pDynamicState;
//     VkPipelineLayout layout;
//     VkRenderPass renderPass;
//     uint32_t subpass;
//     VkPipeline basePipelineHandle;
//     int32_t basePipelineIndex;
// }
    public unsafe struct GraphicsPipelineCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCreateFlags Flags;
        public uint StageCount;
        public PipelineShaderStageCreateInfo* PStages;
        public PipelineVertexInputStateCreateInfo* PVertexInputState;
        public PipelineInputAssemblyStateCreateInfo* PInputAssemblyState;
        public PipelineTessellationStateCreateInfo* PTessellationState;
        public PipelineViewportStateCreateInfo* PViewportState;
        public PipelineRasterizationStateCreateInfo* PRasterizationState;
        public PipelineMultisampleStateCreateInfo* PMultisampleState;
        public PipelineDepthStencilStateCreateInfo* PDepthStencilState;
        public PipelineColorBlendStateCreateInfo* PColorBlendState;
        public PipelineDynamicStateCreateInfo* PDynamicState;
        public PipelineLayout Layout;
        public RenderPass RenderPass;
        public uint Subpass;
        public Pipeline BasePipelineHandle;
        public int BasePipelineIndex;
    }

// Original Definition: 
// struct VkPipelineCacheCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCacheCreateFlags flags;
//     size_t initialDataSize;
//     void* pInitialData;
// }
    public unsafe struct PipelineCacheCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCacheCreateFlags Flags;
        public IntPtr InitialDataSize;
        public void* PInitialData;
    }

// Original Definition: 
// struct VkPushConstantRange 
// {
//     VkShaderStageFlags stageFlags;
//     uint32_t offset;
//     uint32_t size;
// }
    public unsafe struct PushConstantRange
    {
        public ShaderStageFlags StageFlags;
        public uint Offset;
        public uint Size;
    }

// Original Definition: 
// struct VkPipelineLayoutCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineLayoutCreateFlags flags;
//     uint32_t setLayoutCount;
//     VkDescriptorSetLayout* pSetLayouts;
//     uint32_t pushConstantRangeCount;
//     VkPushConstantRange* pPushConstantRanges;
// }
    public unsafe struct PipelineLayoutCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public PipelineLayoutCreateFlags Flags;
        public uint SetLayoutCount;
        public DescriptorSetLayout* PSetLayouts;
        public uint PushConstantRangeCount;
        public PushConstantRange* PPushConstantRanges;
    }

// Original Definition: 
// struct VkSamplerCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSamplerCreateFlags flags;
//     VkFilter magFilter;
//     VkFilter minFilter;
//     VkSamplerMipmapMode mipmapMode;
//     VkSamplerAddressMode addressModeU;
//     VkSamplerAddressMode addressModeV;
//     VkSamplerAddressMode addressModeW;
//     float mipLodBias;
//     VkBool32 anisotropyEnable;
//     float maxAnisotropy;
//     VkBool32 compareEnable;
//     VkCompareOp compareOp;
//     float minLod;
//     float maxLod;
//     VkBorderColor borderColor;
//     VkBool32 unnormalizedCoordinates;
// }
    public unsafe struct SamplerCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public SamplerCreateFlags Flags;
        public Filter MagFilter;
        public Filter MinFilter;
        public SamplerMipmapMode MipmapMode;
        public SamplerAddressMode AddressModeU;
        public SamplerAddressMode AddressModeV;
        public SamplerAddressMode AddressModeW;
        public float MipLodBias;
        public uint AnisotropyEnable;
        public float MaxAnisotropy;
        public uint CompareEnable;
        public CompareOp CompareOp;
        public float MinLod;
        public float MaxLod;
        public BorderColor BorderColor;
        public uint UnnormalizedCoordinates;
    }

// Original Definition: 
// struct VkCommandPoolCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkCommandPoolCreateFlags flags;
//     uint32_t queueFamilyIndex;
// }
    public unsafe struct CommandPoolCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public CommandPoolCreateFlags Flags;
        public uint QueueFamilyIndex;
    }

// Original Definition: 
// struct VkCommandBufferAllocateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkCommandPool commandPool;
//     VkCommandBufferLevel level;
//     uint32_t commandBufferCount;
// }
    public unsafe struct CommandBufferAllocateInfo
    {
        public StructureType SType;
        public void* PNext;
        public CommandPool CommandPool;
        public CommandBufferLevel Level;
        public uint CommandBufferCount;
    }

// Original Definition: 
// struct VkCommandBufferInheritanceInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkRenderPass renderPass;
//     uint32_t subpass;
//     VkFramebuffer framebuffer;
//     VkBool32 occlusionQueryEnable;
//     VkQueryControlFlags queryFlags;
//     VkQueryPipelineStatisticFlags pipelineStatistics;
// }
    public unsafe struct CommandBufferInheritanceInfo
    {
        public StructureType SType;
        public void* PNext;
        public RenderPass RenderPass;
        public uint Subpass;
        public Framebuffer Framebuffer;
        public uint OcclusionQueryEnable;
        public QueryControlFlags QueryFlags;
        public QueryPipelineStatisticFlags PipelineStatistics;
    }

// Original Definition: 
// struct VkCommandBufferBeginInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkCommandBufferUsageFlags flags;
//     VkCommandBufferInheritanceInfo* pInheritanceInfo;
// }
    public unsafe struct CommandBufferBeginInfo
    {
        public StructureType SType;
        public void* PNext;
        public CommandBufferUsageFlags Flags;
        public CommandBufferInheritanceInfo* PInheritanceInfo;
    }

// Original Definition: 
// struct VkRenderPassBeginInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkRenderPass renderPass;
//     VkFramebuffer framebuffer;
//     VkRect2D renderArea;
//     uint32_t clearValueCount;
//     VkClearValue* pClearValues;
// }
    public unsafe struct RenderPassBeginInfo
    {
        public StructureType SType;
        public void* PNext;
        public RenderPass RenderPass;
        public Framebuffer Framebuffer;
        public Rect2d RenderArea;
        public uint ClearValueCount;
        public ClearValue* PClearValues;
    }

    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct ClearColorValue
    {
        [FieldOffset(0)] public fixed float Float32[4];
        [FieldOffset(0)] public fixed int Int32[4];
        [FieldOffset(0)] public fixed uint Uint32[4];
    }

// Original Definition: 
// struct VkClearDepthStencilValue 
// {
//     float depth;
//     uint32_t stencil;
// }
    public unsafe struct ClearDepthStencilValue
    {
        public float Depth;
        public uint Stencil;
    }

    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct ClearValue
    {
        [FieldOffset(0)] public ClearColorValue Color;
        [FieldOffset(0)] public ClearDepthStencilValue DepthStencil;
    }

// Original Definition: 
// struct VkClearAttachment 
// {
//     VkImageAspectFlags aspectMask;
//     uint32_t colorAttachment;
//     VkClearValue clearValue;
// }
    public unsafe struct ClearAttachment
    {
        public ImageAspectFlags AspectMask;
        public uint ColorAttachment;
        public ClearValue ClearValue;
    }

// Original Definition: 
// struct VkAttachmentDescription 
// {
//     VkAttachmentDescriptionFlags flags;
//     VkFormat format;
//     VkSampleCountFlagBits samples;
//     VkAttachmentLoadOp loadOp;
//     VkAttachmentStoreOp storeOp;
//     VkAttachmentLoadOp stencilLoadOp;
//     VkAttachmentStoreOp stencilStoreOp;
//     VkImageLayout initialLayout;
//     VkImageLayout finalLayout;
// }
    public unsafe struct AttachmentDescription
    {
        public AttachmentDescriptionFlags Flags;
        public Format Format;
        public SampleCountFlags Samples;
        public AttachmentLoadOp LoadOp;
        public AttachmentStoreOp StoreOp;
        public AttachmentLoadOp StencilLoadOp;
        public AttachmentStoreOp StencilStoreOp;
        public ImageLayout InitialLayout;
        public ImageLayout FinalLayout;
    }

// Original Definition: 
// struct VkAttachmentReference 
// {
//     uint32_t attachment;
//     VkImageLayout layout;
// }
    public unsafe struct AttachmentReference
    {
        public uint Attachment;
        public ImageLayout Layout;
    }

// Original Definition: 
// struct VkSubpassDescription 
// {
//     VkSubpassDescriptionFlags flags;
//     VkPipelineBindPoint pipelineBindPoint;
//     uint32_t inputAttachmentCount;
//     VkAttachmentReference* pInputAttachments;
//     uint32_t colorAttachmentCount;
//     VkAttachmentReference* pColorAttachments;
//     VkAttachmentReference* pResolveAttachments;
//     VkAttachmentReference* pDepthStencilAttachment;
//     uint32_t preserveAttachmentCount;
//     uint32_t* pPreserveAttachments;
// }
    public unsafe struct SubpassDescription
    {
        public SubpassDescriptionFlags Flags;
        public PipelineBindPoint PipelineBindPoint;
        public uint InputAttachmentCount;
        public AttachmentReference* PInputAttachments;
        public uint ColorAttachmentCount;
        public AttachmentReference* PColorAttachments;
        public AttachmentReference* PResolveAttachments;
        public AttachmentReference* PDepthStencilAttachment;
        public uint PreserveAttachmentCount;
        public uint* PPreserveAttachments;
    }

// Original Definition: 
// struct VkSubpassDependency 
// {
//     uint32_t srcSubpass;
//     uint32_t dstSubpass;
//     VkPipelineStageFlags srcStageMask;
//     VkPipelineStageFlags dstStageMask;
//     VkAccessFlags srcAccessMask;
//     VkAccessFlags dstAccessMask;
//     VkDependencyFlags dependencyFlags;
// }
    public unsafe struct SubpassDependency
    {
        public uint SrcSubpass;
        public uint DstSubpass;
        public PipelineStageFlags SrcStageMask;
        public PipelineStageFlags DstStageMask;
        public AccessFlags SrcAccessMask;
        public AccessFlags DstAccessMask;
        public DependencyFlags DependencyFlags;
    }

// Original Definition: 
// struct VkRenderPassCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkRenderPassCreateFlags flags;
//     uint32_t attachmentCount;
//     VkAttachmentDescription* pAttachments;
//     uint32_t subpassCount;
//     VkSubpassDescription* pSubpasses;
//     uint32_t dependencyCount;
//     VkSubpassDependency* pDependencies;
// }
    public unsafe struct RenderPassCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public RenderPassCreateFlags Flags;
        public uint AttachmentCount;
        public AttachmentDescription* PAttachments;
        public uint SubpassCount;
        public SubpassDescription* PSubpasses;
        public uint DependencyCount;
        public SubpassDependency* PDependencies;
    }

// Original Definition: 
// struct VkEventCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkEventCreateFlags flags;
// }
    public unsafe struct EventCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public EventCreateFlags Flags;
    }

// Original Definition: 
// struct VkFenceCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFenceCreateFlags flags;
// }
    public unsafe struct FenceCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public FenceCreateFlags Flags;
    }

// Original Definition: 
// struct VkPhysicalDeviceFeatures 
// {
//     VkBool32 robustBufferAccess;
//     VkBool32 fullDrawIndexUint32;
//     VkBool32 imageCubeArray;
//     VkBool32 independentBlend;
//     VkBool32 geometryShader;
//     VkBool32 tessellationShader;
//     VkBool32 sampleRateShading;
//     VkBool32 dualSrcBlend;
//     VkBool32 logicOp;
//     VkBool32 multiDrawIndirect;
//     VkBool32 drawIndirectFirstInstance;
//     VkBool32 depthClamp;
//     VkBool32 depthBiasClamp;
//     VkBool32 fillModeNonSolid;
//     VkBool32 depthBounds;
//     VkBool32 wideLines;
//     VkBool32 largePoints;
//     VkBool32 alphaToOne;
//     VkBool32 multiViewport;
//     VkBool32 samplerAnisotropy;
//     VkBool32 textureCompressionETC2;
//     VkBool32 textureCompressionASTC_LDR;
//     VkBool32 textureCompressionBC;
//     VkBool32 occlusionQueryPrecise;
//     VkBool32 pipelineStatisticsQuery;
//     VkBool32 vertexPipelineStoresAndAtomics;
//     VkBool32 fragmentStoresAndAtomics;
//     VkBool32 shaderTessellationAndGeometryPointSize;
//     VkBool32 shaderImageGatherExtended;
//     VkBool32 shaderStorageImageExtendedFormats;
//     VkBool32 shaderStorageImageMultisample;
//     VkBool32 shaderStorageImageReadWithoutFormat;
//     VkBool32 shaderStorageImageWriteWithoutFormat;
//     VkBool32 shaderUniformBufferArrayDynamicIndexing;
//     VkBool32 shaderSampledImageArrayDynamicIndexing;
//     VkBool32 shaderStorageBufferArrayDynamicIndexing;
//     VkBool32 shaderStorageImageArrayDynamicIndexing;
//     VkBool32 shaderClipDistance;
//     VkBool32 shaderCullDistance;
//     VkBool32 shaderFloat64;
//     VkBool32 shaderInt64;
//     VkBool32 shaderInt16;
//     VkBool32 shaderResourceResidency;
//     VkBool32 shaderResourceMinLod;
//     VkBool32 sparseBinding;
//     VkBool32 sparseResidencyBuffer;
//     VkBool32 sparseResidencyImage2D;
//     VkBool32 sparseResidencyImage3D;
//     VkBool32 sparseResidency2Samples;
//     VkBool32 sparseResidency4Samples;
//     VkBool32 sparseResidency8Samples;
//     VkBool32 sparseResidency16Samples;
//     VkBool32 sparseResidencyAliased;
//     VkBool32 variableMultisampleRate;
//     VkBool32 inheritedQueries;
// }
    public unsafe struct PhysicalDeviceFeatures
    {
        public uint RobustBufferAccess;
        public uint FullDrawIndexUint32;
        public uint ImageCubeArray;
        public uint IndependentBlend;
        public uint GeometryShader;
        public uint TessellationShader;
        public uint SampleRateShading;
        public uint DualSrcBlend;
        public uint LogicOp;
        public uint MultiDrawIndirect;
        public uint DrawIndirectFirstInstance;
        public uint DepthClamp;
        public uint DepthBiasClamp;
        public uint FillModeNonSolid;
        public uint DepthBounds;
        public uint WideLines;
        public uint LargePoints;
        public uint AlphaToOne;
        public uint MultiViewport;
        public uint SamplerAnisotropy;
        public uint TextureCompressionETC2;
        public uint TextureCompressionASTC_LDR;
        public uint TextureCompressionBC;
        public uint OcclusionQueryPrecise;
        public uint PipelineStatisticsQuery;
        public uint VertexPipelineStoresAndAtomics;
        public uint FragmentStoresAndAtomics;
        public uint ShaderTessellationAndGeometryPointSize;
        public uint ShaderImageGatherExtended;
        public uint ShaderStorageImageExtendedFormats;
        public uint ShaderStorageImageMultisample;
        public uint ShaderStorageImageReadWithoutFormat;
        public uint ShaderStorageImageWriteWithoutFormat;
        public uint ShaderUniformBufferArrayDynamicIndexing;
        public uint ShaderSampledImageArrayDynamicIndexing;
        public uint ShaderStorageBufferArrayDynamicIndexing;
        public uint ShaderStorageImageArrayDynamicIndexing;
        public uint ShaderClipDistance;
        public uint ShaderCullDistance;
        public uint ShaderFloat64;
        public uint ShaderInt64;
        public uint ShaderInt16;
        public uint ShaderResourceResidency;
        public uint ShaderResourceMinLod;
        public uint SparseBinding;
        public uint SparseResidencyBuffer;
        public uint SparseResidencyImage2D;
        public uint SparseResidencyImage3D;
        public uint SparseResidency2Samples;
        public uint SparseResidency4Samples;
        public uint SparseResidency8Samples;
        public uint SparseResidency16Samples;
        public uint SparseResidencyAliased;
        public uint VariableMultisampleRate;
        public uint InheritedQueries;
    }

// Original Definition: 
// struct VkPhysicalDeviceSparseProperties 
// {
//     VkBool32 residencyStandard2DBlockShape;
//     VkBool32 residencyStandard2DMultisampleBlockShape;
//     VkBool32 residencyStandard3DBlockShape;
//     VkBool32 residencyAlignedMipSize;
//     VkBool32 residencyNonResidentStrict;
// }
    public unsafe struct PhysicalDeviceSparseProperties
    {
        public uint ResidencyStandard2DBlockShape;
        public uint ResidencyStandard2DMultisampleBlockShape;
        public uint ResidencyStandard3DBlockShape;
        public uint ResidencyAlignedMipSize;
        public uint ResidencyNonResidentStrict;
    }

// Original Definition: 
// struct VkPhysicalDeviceLimits 
// {
//     uint32_t maxImageDimension1D;
//     uint32_t maxImageDimension2D;
//     uint32_t maxImageDimension3D;
//     uint32_t maxImageDimensionCube;
//     uint32_t maxImageArrayLayers;
//     uint32_t maxTexelBufferElements;
//     uint32_t maxUniformBufferRange;
//     uint32_t maxStorageBufferRange;
//     uint32_t maxPushConstantsSize;
//     uint32_t maxMemoryAllocationCount;
//     uint32_t maxSamplerAllocationCount;
//     VkDeviceSize bufferImageGranularity;
//     VkDeviceSize sparseAddressSpaceSize;
//     uint32_t maxBoundDescriptorSets;
//     uint32_t maxPerStageDescriptorSamplers;
//     uint32_t maxPerStageDescriptorUniformBuffers;
//     uint32_t maxPerStageDescriptorStorageBuffers;
//     uint32_t maxPerStageDescriptorSampledImages;
//     uint32_t maxPerStageDescriptorStorageImages;
//     uint32_t maxPerStageDescriptorInputAttachments;
//     uint32_t maxPerStageResources;
//     uint32_t maxDescriptorSetSamplers;
//     uint32_t maxDescriptorSetUniformBuffers;
//     uint32_t maxDescriptorSetUniformBuffersDynamic;
//     uint32_t maxDescriptorSetStorageBuffers;
//     uint32_t maxDescriptorSetStorageBuffersDynamic;
//     uint32_t maxDescriptorSetSampledImages;
//     uint32_t maxDescriptorSetStorageImages;
//     uint32_t maxDescriptorSetInputAttachments;
//     uint32_t maxVertexInputAttributes;
//     uint32_t maxVertexInputBindings;
//     uint32_t maxVertexInputAttributeOffset;
//     uint32_t maxVertexInputBindingStride;
//     uint32_t maxVertexOutputComponents;
//     uint32_t maxTessellationGenerationLevel;
//     uint32_t maxTessellationPatchSize;
//     uint32_t maxTessellationControlPerVertexInputComponents;
//     uint32_t maxTessellationControlPerVertexOutputComponents;
//     uint32_t maxTessellationControlPerPatchOutputComponents;
//     uint32_t maxTessellationControlTotalOutputComponents;
//     uint32_t maxTessellationEvaluationInputComponents;
//     uint32_t maxTessellationEvaluationOutputComponents;
//     uint32_t maxGeometryShaderInvocations;
//     uint32_t maxGeometryInputComponents;
//     uint32_t maxGeometryOutputComponents;
//     uint32_t maxGeometryOutputVertices;
//     uint32_t maxGeometryTotalOutputComponents;
//     uint32_t maxFragmentInputComponents;
//     uint32_t maxFragmentOutputAttachments;
//     uint32_t maxFragmentDualSrcAttachments;
//     uint32_t maxFragmentCombinedOutputResources;
//     uint32_t maxComputeSharedMemorySize;
//     uint32_t maxComputeWorkGroupCount;
//     uint32_t maxComputeWorkGroupInvocations;
//     uint32_t maxComputeWorkGroupSize;
//     uint32_t subPixelPrecisionBits;
//     uint32_t subTexelPrecisionBits;
//     uint32_t mipmapPrecisionBits;
//     uint32_t maxDrawIndexedIndexValue;
//     uint32_t maxDrawIndirectCount;
//     float maxSamplerLodBias;
//     float maxSamplerAnisotropy;
//     uint32_t maxViewports;
//     uint32_t maxViewportDimensions;
//     float viewportBoundsRange;
//     uint32_t viewportSubPixelBits;
//     size_t minMemoryMapAlignment;
//     VkDeviceSize minTexelBufferOffsetAlignment;
//     VkDeviceSize minUniformBufferOffsetAlignment;
//     VkDeviceSize minStorageBufferOffsetAlignment;
//     int32_t minTexelOffset;
//     uint32_t maxTexelOffset;
//     int32_t minTexelGatherOffset;
//     uint32_t maxTexelGatherOffset;
//     float minInterpolationOffset;
//     float maxInterpolationOffset;
//     uint32_t subPixelInterpolationOffsetBits;
//     uint32_t maxFramebufferWidth;
//     uint32_t maxFramebufferHeight;
//     uint32_t maxFramebufferLayers;
//     VkSampleCountFlags framebufferColorSampleCounts;
//     VkSampleCountFlags framebufferDepthSampleCounts;
//     VkSampleCountFlags framebufferStencilSampleCounts;
//     VkSampleCountFlags framebufferNoAttachmentsSampleCounts;
//     uint32_t maxColorAttachments;
//     VkSampleCountFlags sampledImageColorSampleCounts;
//     VkSampleCountFlags sampledImageIntegerSampleCounts;
//     VkSampleCountFlags sampledImageDepthSampleCounts;
//     VkSampleCountFlags sampledImageStencilSampleCounts;
//     VkSampleCountFlags storageImageSampleCounts;
//     uint32_t maxSampleMaskWords;
//     VkBool32 timestampComputeAndGraphics;
//     float timestampPeriod;
//     uint32_t maxClipDistances;
//     uint32_t maxCullDistances;
//     uint32_t maxCombinedClipAndCullDistances;
//     uint32_t discreteQueuePriorities;
//     float pointSizeRange;
//     float lineWidthRange;
//     float pointSizeGranularity;
//     float lineWidthGranularity;
//     VkBool32 strictLines;
//     VkBool32 standardSampleLocations;
//     VkDeviceSize optimalBufferCopyOffsetAlignment;
//     VkDeviceSize optimalBufferCopyRowPitchAlignment;
//     VkDeviceSize nonCoherentAtomSize;
// }
    public unsafe struct PhysicalDeviceLimits
    {
        public uint MaxImageDimension1D;
        public uint MaxImageDimension2D;
        public uint MaxImageDimension3D;
        public uint MaxImageDimensionCube;
        public uint MaxImageArrayLayers;
        public uint MaxTexelBufferElements;
        public uint MaxUniformBufferRange;
        public uint MaxStorageBufferRange;
        public uint MaxPushConstantsSize;
        public uint MaxMemoryAllocationCount;
        public uint MaxSamplerAllocationCount;
        public ulong BufferImageGranularity;
        public ulong SparseAddressSpaceSize;
        public uint MaxBoundDescriptorSets;
        public uint MaxPerStageDescriptorSamplers;
        public uint MaxPerStageDescriptorUniformBuffers;
        public uint MaxPerStageDescriptorStorageBuffers;
        public uint MaxPerStageDescriptorSampledImages;
        public uint MaxPerStageDescriptorStorageImages;
        public uint MaxPerStageDescriptorInputAttachments;
        public uint MaxPerStageResources;
        public uint MaxDescriptorSetSamplers;
        public uint MaxDescriptorSetUniformBuffers;
        public uint MaxDescriptorSetUniformBuffersDynamic;
        public uint MaxDescriptorSetStorageBuffers;
        public uint MaxDescriptorSetStorageBuffersDynamic;
        public uint MaxDescriptorSetSampledImages;
        public uint MaxDescriptorSetStorageImages;
        public uint MaxDescriptorSetInputAttachments;
        public uint MaxVertexInputAttributes;
        public uint MaxVertexInputBindings;
        public uint MaxVertexInputAttributeOffset;
        public uint MaxVertexInputBindingStride;
        public uint MaxVertexOutputComponents;
        public uint MaxTessellationGenerationLevel;
        public uint MaxTessellationPatchSize;
        public uint MaxTessellationControlPerVertexInputComponents;
        public uint MaxTessellationControlPerVertexOutputComponents;
        public uint MaxTessellationControlPerPatchOutputComponents;
        public uint MaxTessellationControlTotalOutputComponents;
        public uint MaxTessellationEvaluationInputComponents;
        public uint MaxTessellationEvaluationOutputComponents;
        public uint MaxGeometryShaderInvocations;
        public uint MaxGeometryInputComponents;
        public uint MaxGeometryOutputComponents;
        public uint MaxGeometryOutputVertices;
        public uint MaxGeometryTotalOutputComponents;
        public uint MaxFragmentInputComponents;
        public uint MaxFragmentOutputAttachments;
        public uint MaxFragmentDualSrcAttachments;
        public uint MaxFragmentCombinedOutputResources;
        public uint MaxComputeSharedMemorySize;
        public fixed uint MaxComputeWorkGroupCount[3];
        public uint MaxComputeWorkGroupInvocations;
        public fixed uint MaxComputeWorkGroupSize[3];
        public uint SubPixelPrecisionBits;
        public uint SubTexelPrecisionBits;
        public uint MipmapPrecisionBits;
        public uint MaxDrawIndexedIndexValue;
        public uint MaxDrawIndirectCount;
        public float MaxSamplerLodBias;
        public float MaxSamplerAnisotropy;
        public uint MaxViewports;
        public fixed uint MaxViewportDimensions[2];
        public fixed float ViewportBoundsRange[2];
        public uint ViewportSubPixelBits;
        public IntPtr MinMemoryMapAlignment;
        public ulong MinTexelBufferOffsetAlignment;
        public ulong MinUniformBufferOffsetAlignment;
        public ulong MinStorageBufferOffsetAlignment;
        public int MinTexelOffset;
        public uint MaxTexelOffset;
        public int MinTexelGatherOffset;
        public uint MaxTexelGatherOffset;
        public float MinInterpolationOffset;
        public float MaxInterpolationOffset;
        public uint SubPixelInterpolationOffsetBits;
        public uint MaxFramebufferWidth;
        public uint MaxFramebufferHeight;
        public uint MaxFramebufferLayers;
        public SampleCountFlags FramebufferColorSampleCounts;
        public SampleCountFlags FramebufferDepthSampleCounts;
        public SampleCountFlags FramebufferStencilSampleCounts;
        public SampleCountFlags FramebufferNoAttachmentsSampleCounts;
        public uint MaxColorAttachments;
        public SampleCountFlags SampledImageColorSampleCounts;
        public SampleCountFlags SampledImageIntegerSampleCounts;
        public SampleCountFlags SampledImageDepthSampleCounts;
        public SampleCountFlags SampledImageStencilSampleCounts;
        public SampleCountFlags StorageImageSampleCounts;
        public uint MaxSampleMaskWords;
        public uint TimestampComputeAndGraphics;
        public float TimestampPeriod;
        public uint MaxClipDistances;
        public uint MaxCullDistances;
        public uint MaxCombinedClipAndCullDistances;
        public uint DiscreteQueuePriorities;
        public fixed float PointSizeRange[2];
        public fixed float LineWidthRange[2];
        public float PointSizeGranularity;
        public float LineWidthGranularity;
        public uint StrictLines;
        public uint StandardSampleLocations;
        public ulong OptimalBufferCopyOffsetAlignment;
        public ulong OptimalBufferCopyRowPitchAlignment;
        public ulong NonCoherentAtomSize;
    }

// Original Definition: 
// struct VkSemaphoreCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSemaphoreCreateFlags flags;
// }
    public unsafe struct SemaphoreCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public SemaphoreCreateFlags Flags;
    }

// Original Definition: 
// struct VkQueryPoolCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkQueryPoolCreateFlags flags;
//     VkQueryType queryType;
//     uint32_t queryCount;
//     VkQueryPipelineStatisticFlags pipelineStatistics;
// }
    public unsafe struct QueryPoolCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public QueryPoolCreateFlags Flags;
        public QueryType QueryType;
        public uint QueryCount;
        public QueryPipelineStatisticFlags PipelineStatistics;
    }

// Original Definition: 
// struct VkFramebufferCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFramebufferCreateFlags flags;
//     VkRenderPass renderPass;
//     uint32_t attachmentCount;
//     VkImageView* pAttachments;
//     uint32_t width;
//     uint32_t height;
//     uint32_t layers;
// }
    public unsafe struct FramebufferCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public FramebufferCreateFlags Flags;
        public RenderPass RenderPass;
        public uint AttachmentCount;
        public ImageView* PAttachments;
        public uint Width;
        public uint Height;
        public uint Layers;
    }

// Original Definition: 
// struct VkDrawIndirectCommand 
// {
//     uint32_t vertexCount;
//     uint32_t instanceCount;
//     uint32_t firstVertex;
//     uint32_t firstInstance;
// }
    public unsafe struct DrawIndirectCommand
    {
        public uint VertexCount;
        public uint InstanceCount;
        public uint FirstVertex;
        public uint FirstInstance;
    }

// Original Definition: 
// struct VkDrawIndexedIndirectCommand 
// {
//     uint32_t indexCount;
//     uint32_t instanceCount;
//     uint32_t firstIndex;
//     int32_t vertexOffset;
//     uint32_t firstInstance;
// }
    public unsafe struct DrawIndexedIndirectCommand
    {
        public uint IndexCount;
        public uint InstanceCount;
        public uint FirstIndex;
        public int VertexOffset;
        public uint FirstInstance;
    }

// Original Definition: 
// struct VkDispatchIndirectCommand 
// {
//     uint32_t x;
//     uint32_t y;
//     uint32_t z;
// }
    public unsafe struct DispatchIndirectCommand
    {
        public uint X;
        public uint Y;
        public uint Z;
    }

// Original Definition: 
// struct VkSubmitInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t waitSemaphoreCount;
//     VkSemaphore* pWaitSemaphores;
//     VkPipelineStageFlags* pWaitDstStageMask;
//     uint32_t commandBufferCount;
//     VkCommandBuffer* pCommandBuffers;
//     uint32_t signalSemaphoreCount;
//     VkSemaphore* pSignalSemaphores;
// }
    public unsafe struct SubmitInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint WaitSemaphoreCount;
        public Semaphore* PWaitSemaphores;
        public PipelineStageFlags* PWaitDstStageMask;
        public uint CommandBufferCount;
        public CommandBuffer* PCommandBuffers;
        public uint SignalSemaphoreCount;
        public Semaphore* PSignalSemaphores;
    }

// Original Definition: 
// struct VkDisplayPropertiesKHR 
// {
//     VkDisplayKHR display;
//     char* displayName;
//     VkExtent2D physicalDimensions;
//     VkExtent2D physicalResolution;
//     VkSurfaceTransformFlagsKHR supportedTransforms;
//     VkBool32 planeReorderPossible;
//     VkBool32 persistentContent;
// }
    public unsafe struct DisplayPropertiesKhr
    {
        public DisplayKhr Display;
        public sbyte* DisplayName;
        public Extent2d PhysicalDimensions;
        public Extent2d PhysicalResolution;
        public SurfaceTransformFlagsKhr SupportedTransforms;
        public uint PlaneReorderPossible;
        public uint PersistentContent;
    }

// Original Definition: 
// struct VkDisplayPlanePropertiesKHR 
// {
//     VkDisplayKHR currentDisplay;
//     uint32_t currentStackIndex;
// }
    public unsafe struct DisplayPlanePropertiesKhr
    {
        public DisplayKhr CurrentDisplay;
        public uint CurrentStackIndex;
    }

// Original Definition: 
// struct VkDisplayModeParametersKHR 
// {
//     VkExtent2D visibleRegion;
//     uint32_t refreshRate;
// }
    public unsafe struct DisplayModeParametersKhr
    {
        public Extent2d VisibleRegion;
        public uint RefreshRate;
    }

// Original Definition: 
// struct VkDisplayModePropertiesKHR 
// {
//     VkDisplayModeKHR displayMode;
//     VkDisplayModeParametersKHR parameters;
// }
    public unsafe struct DisplayModePropertiesKhr
    {
        public DisplayModeKhr DisplayMode;
        public DisplayModeParametersKhr Parameters;
    }

// Original Definition: 
// struct VkDisplayModeCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDisplayModeCreateFlagsKHR flags;
//     VkDisplayModeParametersKHR parameters;
// }
    public unsafe struct DisplayModeCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public DisplayModeCreateFlagsKhr Flags;
        public DisplayModeParametersKhr Parameters;
    }

// Original Definition: 
// struct VkDisplayPlaneCapabilitiesKHR 
// {
//     VkDisplayPlaneAlphaFlagsKHR supportedAlpha;
//     VkOffset2D minSrcPosition;
//     VkOffset2D maxSrcPosition;
//     VkExtent2D minSrcExtent;
//     VkExtent2D maxSrcExtent;
//     VkOffset2D minDstPosition;
//     VkOffset2D maxDstPosition;
//     VkExtent2D minDstExtent;
//     VkExtent2D maxDstExtent;
// }
    public unsafe struct DisplayPlaneCapabilitiesKhr
    {
        public DisplayPlaneAlphaFlagsKhr SupportedAlpha;
        public Offset2d MinSrcPosition;
        public Offset2d MaxSrcPosition;
        public Extent2d MinSrcExtent;
        public Extent2d MaxSrcExtent;
        public Offset2d MinDstPosition;
        public Offset2d MaxDstPosition;
        public Extent2d MinDstExtent;
        public Extent2d MaxDstExtent;
    }

// Original Definition: 
// struct VkDisplaySurfaceCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDisplaySurfaceCreateFlagsKHR flags;
//     VkDisplayModeKHR displayMode;
//     uint32_t planeIndex;
//     uint32_t planeStackIndex;
//     VkSurfaceTransformFlagBitsKHR transform;
//     float globalAlpha;
//     VkDisplayPlaneAlphaFlagBitsKHR alphaMode;
//     VkExtent2D imageExtent;
// }
    public unsafe struct DisplaySurfaceCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public DisplaySurfaceCreateFlagsKhr Flags;
        public DisplayModeKhr DisplayMode;
        public uint PlaneIndex;
        public uint PlaneStackIndex;
        public SurfaceTransformFlagsKhr Transform;
        public float GlobalAlpha;
        public DisplayPlaneAlphaFlagsKhr AlphaMode;
        public Extent2d ImageExtent;
    }

// Original Definition: 
// struct VkDisplayPresentInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkRect2D srcRect;
//     VkRect2D dstRect;
//     VkBool32 persistent;
// }
    public unsafe struct DisplayPresentInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Rect2d SrcRect;
        public Rect2d DstRect;
        public uint Persistent;
    }

// Original Definition: 
// struct VkSurfaceCapabilitiesKHR 
// {
//     uint32_t minImageCount;
//     uint32_t maxImageCount;
//     VkExtent2D currentExtent;
//     VkExtent2D minImageExtent;
//     VkExtent2D maxImageExtent;
//     uint32_t maxImageArrayLayers;
//     VkSurfaceTransformFlagsKHR supportedTransforms;
//     VkSurfaceTransformFlagBitsKHR currentTransform;
//     VkCompositeAlphaFlagsKHR supportedCompositeAlpha;
//     VkImageUsageFlags supportedUsageFlags;
// }
    public unsafe struct SurfaceCapabilitiesKhr
    {
        public uint MinImageCount;
        public uint MaxImageCount;
        public Extent2d CurrentExtent;
        public Extent2d MinImageExtent;
        public Extent2d MaxImageExtent;
        public uint MaxImageArrayLayers;
        public SurfaceTransformFlagsKhr SupportedTransforms;
        public SurfaceTransformFlagsKhr CurrentTransform;
        public CompositeAlphaFlagsKhr SupportedCompositeAlpha;
        public ImageUsageFlags SupportedUsageFlags;
    }

// Original Definition: 
// struct VkAndroidSurfaceCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAndroidSurfaceCreateFlagsKHR flags;
//     ANativeWindow* window;
// }
    public unsafe struct AndroidSurfaceCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public AndroidSurfaceCreateFlagsKhr Flags;
        public IntPtr Window;
    }

// Original Definition: 
// struct VkViSurfaceCreateInfoNN 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkViSurfaceCreateFlagsNN flags;
//     void* window;
// }
    public unsafe struct ViSurfaceCreateInfoNn
    {
        public StructureType SType;
        public void* PNext;
        public ViSurfaceCreateFlagsNn Flags;
        public void* Window;
    }

// Original Definition: 
// struct VkWaylandSurfaceCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkWaylandSurfaceCreateFlagsKHR flags;
//     wl_display* display;
//     wl_surface* surface;
// }
    public unsafe struct WaylandSurfaceCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public WaylandSurfaceCreateFlagsKhr Flags;
        public IntPtr Display;
        public IntPtr Surface;
    }

// Original Definition: 
// struct VkWin32SurfaceCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkWin32SurfaceCreateFlagsKHR flags;
//     HINSTANCE hinstance;
//     HWND hwnd;
// }
    public unsafe struct Win32SurfaceCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Win32SurfaceCreateFlagsKhr Flags;
        public IntPtr Hinstance;
        public IntPtr Hwnd;
    }

// Original Definition: 
// struct VkXlibSurfaceCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkXlibSurfaceCreateFlagsKHR flags;
//     Display* dpy;
//     Window window;
// }
    public unsafe struct XlibSurfaceCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public XlibSurfaceCreateFlagsKhr Flags;
        public IntPtr Dpy;
        public IntPtr Window;
    }

// Original Definition: 
// struct VkXcbSurfaceCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkXcbSurfaceCreateFlagsKHR flags;
//     xcb_connection_t* connection;
//     xcb_window_t window;
// }
    public unsafe struct XcbSurfaceCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public XcbSurfaceCreateFlagsKhr Flags;
        public IntPtr Connection;
        public int Window;
    }

// Original Definition: 
// struct VkDirectFBSurfaceCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDirectFBSurfaceCreateFlagsEXT flags;
//     IDirectFB* dfb;
//     IDirectFBSurface* surface;
// }
    public unsafe struct DirectFBSurfaceCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public DirectFBSurfaceCreateFlagsExt Flags;
        public void* Dfb;
        public IntPtr Surface;
    }

// Original Definition: 
// struct VkImagePipeSurfaceCreateInfoFUCHSIA 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImagePipeSurfaceCreateFlagsFUCHSIA flags;
//     zx_handle_t imagePipeHandle;
// }
    public unsafe struct ImagePipeSurfaceCreateInfoFuchsia
    {
        public StructureType SType;
        public void* PNext;
        public ImagePipeSurfaceCreateFlagsFuchsia Flags;
        public uint ImagePipeHandle;
    }

// Original Definition: 
// struct VkStreamDescriptorSurfaceCreateInfoGGP 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkStreamDescriptorSurfaceCreateFlagsGGP flags;
//     GgpStreamDescriptor streamDescriptor;
// }
    public unsafe struct StreamDescriptorSurfaceCreateInfoGgp
    {
        public StructureType SType;
        public void* PNext;
        public StreamDescriptorSurfaceCreateFlagsGgp Flags;
        public uint StreamDescriptor;
    }

// Original Definition: 
// struct VkSurfaceFormatKHR 
// {
//     VkFormat format;
//     VkColorSpaceKHR colorSpace;
// }
    public unsafe struct SurfaceFormatKhr
    {
        public Format Format;
        public ColorSpaceKhr ColorSpace;
    }

// Original Definition: 
// struct VkSwapchainCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSwapchainCreateFlagsKHR flags;
//     VkSurfaceKHR surface;
//     uint32_t minImageCount;
//     VkFormat imageFormat;
//     VkColorSpaceKHR imageColorSpace;
//     VkExtent2D imageExtent;
//     uint32_t imageArrayLayers;
//     VkImageUsageFlags imageUsage;
//     VkSharingMode imageSharingMode;
//     uint32_t queueFamilyIndexCount;
//     uint32_t* pQueueFamilyIndices;
//     VkSurfaceTransformFlagBitsKHR preTransform;
//     VkCompositeAlphaFlagBitsKHR compositeAlpha;
//     VkPresentModeKHR presentMode;
//     VkBool32 clipped;
//     VkSwapchainKHR oldSwapchain;
// }
    public unsafe struct SwapchainCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public SwapchainCreateFlagsKhr Flags;
        public SurfaceKhr Surface;
        public uint MinImageCount;
        public Format ImageFormat;
        public ColorSpaceKhr ImageColorSpace;
        public Extent2d ImageExtent;
        public uint ImageArrayLayers;
        public ImageUsageFlags ImageUsage;
        public SharingMode ImageSharingMode;
        public uint QueueFamilyIndexCount;
        public uint* PQueueFamilyIndices;
        public SurfaceTransformFlagsKhr PreTransform;
        public CompositeAlphaFlagsKhr CompositeAlpha;
        public PresentModeKhr PresentMode;
        public uint Clipped;
        public SwapchainKhr OldSwapchain;
    }

// Original Definition: 
// struct VkPresentInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t waitSemaphoreCount;
//     VkSemaphore* pWaitSemaphores;
//     uint32_t swapchainCount;
//     VkSwapchainKHR* pSwapchains;
//     uint32_t* pImageIndices;
//     VkResult* pResults;
// }
    public unsafe struct PresentInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint WaitSemaphoreCount;
        public Semaphore* PWaitSemaphores;
        public uint SwapchainCount;
        public SwapchainKhr* PSwapchains;
        public uint* PImageIndices;
        public Result* PResults;
    }

// Original Definition: 
// struct VkDebugReportCallbackCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDebugReportFlagsEXT flags;
//     PFN_vkDebugReportCallbackEXT pfnCallback;
//     void* pUserData;
// }
    public unsafe struct DebugReportCallbackCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public DebugReportFlagsExt Flags;
        public void* PfnCallback;
        public void* PUserData;
    }

// Original Definition: 
// struct VkValidationFlagsEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t disabledValidationCheckCount;
//     VkValidationCheckEXT* pDisabledValidationChecks;
// }
    public unsafe struct ValidationFlagsExt
    {
        public StructureType SType;
        public void* PNext;
        public uint DisabledValidationCheckCount;
        public ValidationCheckExt* PDisabledValidationChecks;
    }

// Original Definition: 
// struct VkValidationFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t enabledValidationFeatureCount;
//     VkValidationFeatureEnableEXT* pEnabledValidationFeatures;
//     uint32_t disabledValidationFeatureCount;
//     VkValidationFeatureDisableEXT* pDisabledValidationFeatures;
// }
    public unsafe struct ValidationFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint EnabledValidationFeatureCount;
        public ValidationFeatureEnableExt* PEnabledValidationFeatures;
        public uint DisabledValidationFeatureCount;
        public ValidationFeatureDisableExt* PDisabledValidationFeatures;
    }

// Original Definition: 
// struct VkPipelineRasterizationStateRasterizationOrderAMD 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkRasterizationOrderAMD rasterizationOrder;
// }
    public unsafe struct PipelineRasterizationStateRasterizationOrderAmd
    {
        public StructureType SType;
        public void* PNext;
        public RasterizationOrderAmd RasterizationOrder;
    }

// Original Definition: 
// struct VkDebugMarkerObjectNameInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDebugReportObjectTypeEXT objectType;
//     uint64_t object;
//     char* pObjectName;
// }
    public unsafe struct DebugMarkerObjectNameInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public DebugReportObjectTypeExt ObjectType;
        public ulong Object;
        public sbyte* PObjectName;
    }

// Original Definition: 
// struct VkDebugMarkerObjectTagInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDebugReportObjectTypeEXT objectType;
//     uint64_t object;
//     uint64_t tagName;
//     size_t tagSize;
//     void* pTag;
// }
    public unsafe struct DebugMarkerObjectTagInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public DebugReportObjectTypeExt ObjectType;
        public ulong Object;
        public ulong TagName;
        public IntPtr TagSize;
        public void* PTag;
    }

// Original Definition: 
// struct VkDebugMarkerMarkerInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     char* pMarkerName;
//     float color;
// }
    public unsafe struct DebugMarkerMarkerInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public sbyte* PMarkerName;
        public fixed float Color[4];
    }

// Original Definition: 
// struct VkDedicatedAllocationImageCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 dedicatedAllocation;
// }
    public unsafe struct DedicatedAllocationImageCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public uint DedicatedAllocation;
    }

// Original Definition: 
// struct VkDedicatedAllocationBufferCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 dedicatedAllocation;
// }
    public unsafe struct DedicatedAllocationBufferCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public uint DedicatedAllocation;
    }

// Original Definition: 
// struct VkDedicatedAllocationMemoryAllocateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImage image;
//     VkBuffer buffer;
// }
    public unsafe struct DedicatedAllocationMemoryAllocateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public Image Image;
        public Buffer Buffer;
    }

// Original Definition: 
// struct VkExternalImageFormatPropertiesNV 
// {
//     VkImageFormatProperties imageFormatProperties;
//     VkExternalMemoryFeatureFlagsNV externalMemoryFeatures;
//     VkExternalMemoryHandleTypeFlagsNV exportFromImportedHandleTypes;
//     VkExternalMemoryHandleTypeFlagsNV compatibleHandleTypes;
// }
    public unsafe struct ExternalImageFormatPropertiesNv
    {
        public ImageFormatProperties ImageFormatProperties;
        public ExternalMemoryFeatureFlagsNv ExternalMemoryFeatures;
        public ExternalMemoryHandleTypeFlagsNv ExportFromImportedHandleTypes;
        public ExternalMemoryHandleTypeFlagsNv CompatibleHandleTypes;
    }

// Original Definition: 
// struct VkExternalMemoryImageCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlagsNV handleTypes;
// }
    public unsafe struct ExternalMemoryImageCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlagsNv HandleTypes;
    }

// Original Definition: 
// struct VkExportMemoryAllocateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlagsNV handleTypes;
// }
    public unsafe struct ExportMemoryAllocateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlagsNv HandleTypes;
    }

// Original Definition: 
// struct VkImportMemoryWin32HandleInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlagsNV handleType;
//     HANDLE handle;
// }
    public unsafe struct ImportMemoryWin32HandleInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlagsNv HandleType;
        public IntPtr Handle;
    }

// Original Definition: 
// struct VkExportMemoryWin32HandleInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     SECURITY_ATTRIBUTES* pAttributes;
//     DWORD dwAccess;
// }
    public unsafe struct ExportMemoryWin32HandleInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public void* PAttributes;
        public uint DwAccess;
    }

// Original Definition: 
// struct VkWin32KeyedMutexAcquireReleaseInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t acquireCount;
//     VkDeviceMemory* pAcquireSyncs;
//     uint64_t* pAcquireKeys;
//     uint32_t* pAcquireTimeoutMilliseconds;
//     uint32_t releaseCount;
//     VkDeviceMemory* pReleaseSyncs;
//     uint64_t* pReleaseKeys;
// }
    public unsafe struct Win32KeyedMutexAcquireReleaseInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public uint AcquireCount;
        public DeviceMemory* PAcquireSyncs;
        public ulong* PAcquireKeys;
        public uint* PAcquireTimeoutMilliseconds;
        public uint ReleaseCount;
        public DeviceMemory* PReleaseSyncs;
        public ulong* PReleaseKeys;
    }

// Original Definition: 
// struct VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 deviceGeneratedCommands;
// }
    public unsafe struct PhysicalDeviceDeviceGeneratedCommandsFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint DeviceGeneratedCommands;
    }

// Original Definition: 
// struct VkDevicePrivateDataCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t privateDataSlotRequestCount;
// }
    public unsafe struct DevicePrivateDataCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public uint PrivateDataSlotRequestCount;
    }

// Original Definition: 
// struct VkPrivateDataSlotCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPrivateDataSlotCreateFlagsEXT flags;
// }
    public unsafe struct PrivateDataSlotCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public PrivateDataSlotCreateFlagsExt Flags;
    }

// Original Definition: 
// struct VkPhysicalDevicePrivateDataFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 privateData;
// }
    public unsafe struct PhysicalDevicePrivateDataFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint PrivateData;
    }

// Original Definition: 
// struct VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxGraphicsShaderGroupCount;
//     uint32_t maxIndirectSequenceCount;
//     uint32_t maxIndirectCommandsTokenCount;
//     uint32_t maxIndirectCommandsStreamCount;
//     uint32_t maxIndirectCommandsTokenOffset;
//     uint32_t maxIndirectCommandsStreamStride;
//     uint32_t minSequencesCountBufferOffsetAlignment;
//     uint32_t minSequencesIndexBufferOffsetAlignment;
//     uint32_t minIndirectCommandsBufferOffsetAlignment;
// }
    public unsafe struct PhysicalDeviceDeviceGeneratedCommandsPropertiesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxGraphicsShaderGroupCount;
        public uint MaxIndirectSequenceCount;
        public uint MaxIndirectCommandsTokenCount;
        public uint MaxIndirectCommandsStreamCount;
        public uint MaxIndirectCommandsTokenOffset;
        public uint MaxIndirectCommandsStreamStride;
        public uint MinSequencesCountBufferOffsetAlignment;
        public uint MinSequencesIndexBufferOffsetAlignment;
        public uint MinIndirectCommandsBufferOffsetAlignment;
    }

// Original Definition: 
// struct VkGraphicsShaderGroupCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t stageCount;
//     VkPipelineShaderStageCreateInfo* pStages;
//     VkPipelineVertexInputStateCreateInfo* pVertexInputState;
//     VkPipelineTessellationStateCreateInfo* pTessellationState;
// }
    public unsafe struct GraphicsShaderGroupCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public uint StageCount;
        public PipelineShaderStageCreateInfo* PStages;
        public PipelineVertexInputStateCreateInfo* PVertexInputState;
        public PipelineTessellationStateCreateInfo* PTessellationState;
    }

// Original Definition: 
// struct VkGraphicsPipelineShaderGroupsCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t groupCount;
//     VkGraphicsShaderGroupCreateInfoNV* pGroups;
//     uint32_t pipelineCount;
//     VkPipeline* pPipelines;
// }
    public unsafe struct GraphicsPipelineShaderGroupsCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public uint GroupCount;
        public GraphicsShaderGroupCreateInfoNv* PGroups;
        public uint PipelineCount;
        public Pipeline* PPipelines;
    }

// Original Definition: 
// struct VkBindShaderGroupIndirectCommandNV 
// {
//     uint32_t groupIndex;
// }
    public unsafe struct BindShaderGroupIndirectCommandNv
    {
        public uint GroupIndex;
    }

// Original Definition: 
// struct VkBindIndexBufferIndirectCommandNV 
// {
//     VkDeviceAddress bufferAddress;
//     uint32_t size;
//     VkIndexType indexType;
// }
    public unsafe struct BindIndexBufferIndirectCommandNv
    {
        public ulong BufferAddress;
        public uint Size;
        public IndexType IndexType;
    }

// Original Definition: 
// struct VkBindVertexBufferIndirectCommandNV 
// {
//     VkDeviceAddress bufferAddress;
//     uint32_t size;
//     uint32_t stride;
// }
    public unsafe struct BindVertexBufferIndirectCommandNv
    {
        public ulong BufferAddress;
        public uint Size;
        public uint Stride;
    }

// Original Definition: 
// struct VkSetStateFlagsIndirectCommandNV 
// {
//     uint32_t data;
// }
    public unsafe struct SetStateFlagsIndirectCommandNv
    {
        public uint Data;
    }

// Original Definition: 
// struct VkIndirectCommandsStreamNV 
// {
//     VkBuffer buffer;
//     VkDeviceSize offset;
// }
    public unsafe struct IndirectCommandsStreamNv
    {
        public Buffer Buffer;
        public ulong Offset;
    }

// Original Definition: 
// struct VkIndirectCommandsLayoutTokenNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkIndirectCommandsTokenTypeNV tokenType;
//     uint32_t stream;
//     uint32_t offset;
//     uint32_t vertexBindingUnit;
//     VkBool32 vertexDynamicStride;
//     VkPipelineLayout pushconstantPipelineLayout;
//     VkShaderStageFlags pushconstantShaderStageFlags;
//     uint32_t pushconstantOffset;
//     uint32_t pushconstantSize;
//     VkIndirectStateFlagsNV indirectStateFlags;
//     uint32_t indexTypeCount;
//     VkIndexType* pIndexTypes;
//     uint32_t* pIndexTypeValues;
// }
    public unsafe struct IndirectCommandsLayoutTokenNv
    {
        public StructureType SType;
        public void* PNext;
        public IndirectCommandsTokenTypeNv TokenType;
        public uint Stream;
        public uint Offset;
        public uint VertexBindingUnit;
        public uint VertexDynamicStride;
        public PipelineLayout PushconstantPipelineLayout;
        public ShaderStageFlags PushconstantShaderStageFlags;
        public uint PushconstantOffset;
        public uint PushconstantSize;
        public IndirectStateFlagsNv IndirectStateFlags;
        public uint IndexTypeCount;
        public IndexType* PIndexTypes;
        public uint* PIndexTypeValues;
    }

// Original Definition: 
// struct VkIndirectCommandsLayoutCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkIndirectCommandsLayoutUsageFlagsNV flags;
//     VkPipelineBindPoint pipelineBindPoint;
//     uint32_t tokenCount;
//     VkIndirectCommandsLayoutTokenNV* pTokens;
//     uint32_t streamCount;
//     uint32_t* pStreamStrides;
// }
    public unsafe struct IndirectCommandsLayoutCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public IndirectCommandsLayoutUsageFlagsNv Flags;
        public PipelineBindPoint PipelineBindPoint;
        public uint TokenCount;
        public IndirectCommandsLayoutTokenNv* PTokens;
        public uint StreamCount;
        public uint* PStreamStrides;
    }

// Original Definition: 
// struct VkGeneratedCommandsInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineBindPoint pipelineBindPoint;
//     VkPipeline pipeline;
//     VkIndirectCommandsLayoutNV indirectCommandsLayout;
//     uint32_t streamCount;
//     VkIndirectCommandsStreamNV* pStreams;
//     uint32_t sequencesCount;
//     VkBuffer preprocessBuffer;
//     VkDeviceSize preprocessOffset;
//     VkDeviceSize preprocessSize;
//     VkBuffer sequencesCountBuffer;
//     VkDeviceSize sequencesCountOffset;
//     VkBuffer sequencesIndexBuffer;
//     VkDeviceSize sequencesIndexOffset;
// }
    public unsafe struct GeneratedCommandsInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public PipelineBindPoint PipelineBindPoint;
        public Pipeline Pipeline;
        public IndirectCommandsLayoutNv IndirectCommandsLayout;
        public uint StreamCount;
        public IndirectCommandsStreamNv* PStreams;
        public uint SequencesCount;
        public Buffer PreprocessBuffer;
        public ulong PreprocessOffset;
        public ulong PreprocessSize;
        public Buffer SequencesCountBuffer;
        public ulong SequencesCountOffset;
        public Buffer SequencesIndexBuffer;
        public ulong SequencesIndexOffset;
    }

// Original Definition: 
// struct VkGeneratedCommandsMemoryRequirementsInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineBindPoint pipelineBindPoint;
//     VkPipeline pipeline;
//     VkIndirectCommandsLayoutNV indirectCommandsLayout;
//     uint32_t maxSequencesCount;
// }
    public unsafe struct GeneratedCommandsMemoryRequirementsInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public PipelineBindPoint PipelineBindPoint;
        public Pipeline Pipeline;
        public IndirectCommandsLayoutNv IndirectCommandsLayout;
        public uint MaxSequencesCount;
    }

// Original Definition: 
// struct VkPhysicalDeviceFeatures2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPhysicalDeviceFeatures features;
// }
    public unsafe struct PhysicalDeviceFeatures2
    {
        public StructureType SType;
        public void* PNext;
        public PhysicalDeviceFeatures Features;
    }
// Original Definition: 
// struct VkPhysicalDeviceFeatures2KHR 
// {

// }
    public unsafe struct PhysicalDeviceFeatures2khr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceProperties2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPhysicalDeviceProperties properties;
// }
    public unsafe struct PhysicalDeviceProperties2
    {
        public StructureType SType;
        public void* PNext;
        public PhysicalDeviceProperties Properties;
    }
// Original Definition: 
// struct VkPhysicalDeviceProperties2KHR 
// {

// }
    public unsafe struct PhysicalDeviceProperties2khr
    {
    }

// Original Definition: 
// struct VkFormatProperties2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFormatProperties formatProperties;
// }
    public unsafe struct FormatProperties2
    {
        public StructureType SType;
        public void* PNext;
        public FormatProperties FormatProperties;
    }
// Original Definition: 
// struct VkFormatProperties2KHR 
// {

// }
    public unsafe struct FormatProperties2khr
    {
    }

// Original Definition: 
// struct VkImageFormatProperties2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageFormatProperties imageFormatProperties;
// }
    public unsafe struct ImageFormatProperties2
    {
        public StructureType SType;
        public void* PNext;
        public ImageFormatProperties ImageFormatProperties;
    }
// Original Definition: 
// struct VkImageFormatProperties2KHR 
// {

// }
    public unsafe struct ImageFormatProperties2khr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceImageFormatInfo2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFormat format;
//     VkImageType type;
//     VkImageTiling tiling;
//     VkImageUsageFlags usage;
//     VkImageCreateFlags flags;
// }
    public unsafe struct PhysicalDeviceImageFormatInfo2
    {
        public StructureType SType;
        public void* PNext;
        public Format Format;
        public ImageType Type;
        public ImageTiling Tiling;
        public ImageUsageFlags Usage;
        public ImageCreateFlags Flags;
    }
// Original Definition: 
// struct VkPhysicalDeviceImageFormatInfo2KHR 
// {

// }
    public unsafe struct PhysicalDeviceImageFormatInfo2khr
    {
    }

// Original Definition: 
// struct VkQueueFamilyProperties2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkQueueFamilyProperties queueFamilyProperties;
// }
    public unsafe struct QueueFamilyProperties2
    {
        public StructureType SType;
        public void* PNext;
        public QueueFamilyProperties QueueFamilyProperties;
    }
// Original Definition: 
// struct VkQueueFamilyProperties2KHR 
// {

// }
    public unsafe struct QueueFamilyProperties2khr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceMemoryProperties2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPhysicalDeviceMemoryProperties memoryProperties;
// }
    public unsafe struct PhysicalDeviceMemoryProperties2
    {
        public StructureType SType;
        public void* PNext;
        public PhysicalDeviceMemoryProperties MemoryProperties;
    }
// Original Definition: 
// struct VkPhysicalDeviceMemoryProperties2KHR 
// {

// }
    public unsafe struct PhysicalDeviceMemoryProperties2khr
    {
    }

// Original Definition: 
// struct VkSparseImageFormatProperties2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSparseImageFormatProperties properties;
// }
    public unsafe struct SparseImageFormatProperties2
    {
        public StructureType SType;
        public void* PNext;
        public SparseImageFormatProperties Properties;
    }
// Original Definition: 
// struct VkSparseImageFormatProperties2KHR 
// {

// }
    public unsafe struct SparseImageFormatProperties2khr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceSparseImageFormatInfo2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFormat format;
//     VkImageType type;
//     VkSampleCountFlagBits samples;
//     VkImageUsageFlags usage;
//     VkImageTiling tiling;
// }
    public unsafe struct PhysicalDeviceSparseImageFormatInfo2
    {
        public StructureType SType;
        public void* PNext;
        public Format Format;
        public ImageType Type;
        public SampleCountFlags Samples;
        public ImageUsageFlags Usage;
        public ImageTiling Tiling;
    }
// Original Definition: 
// struct VkPhysicalDeviceSparseImageFormatInfo2KHR 
// {

// }
    public unsafe struct PhysicalDeviceSparseImageFormatInfo2khr
    {
    }

// Original Definition: 
// struct VkPhysicalDevicePushDescriptorPropertiesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxPushDescriptors;
// }
    public unsafe struct PhysicalDevicePushDescriptorPropertiesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxPushDescriptors;
    }

// Original Definition: 
// struct VkConformanceVersion 
// {
//     uint8_t major;
//     uint8_t minor;
//     uint8_t subminor;
//     uint8_t patch;
// }
    public unsafe struct ConformanceVersion
    {
        public byte Major;
        public byte Minor;
        public byte Subminor;
        public byte Patch;
    }
// Original Definition: 
// struct VkConformanceVersionKHR 
// {

// }
    public unsafe struct ConformanceVersionKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceDriverProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDriverId driverID;
//     char driverName[(int)ApiConstants.MaxDriverNameSize];
//     char driverInfo[(int)ApiConstants.MaxDriverInfoSize];
//     VkConformanceVersion conformanceVersion;
// }
    public unsafe struct PhysicalDeviceDriverProperties
    {
        public StructureType SType;
        public void* PNext;
        public DriverId DriverID;
        public fixed sbyte DriverName_[(int) ApiConstants.MaxDriverNameSize];
        public fixed sbyte DriverInfo_[(int) ApiConstants.MaxDriverInfoSize];
        public ConformanceVersion ConformanceVersion;

        public string DriverName
        {
            get
            {
                fixed (sbyte* ptr = DriverName_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string DriverInfo
        {
            get
            {
                fixed (sbyte* ptr = DriverInfo_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }
// Original Definition: 
// struct VkPhysicalDeviceDriverPropertiesKHR 
// {

// }
    public unsafe struct PhysicalDeviceDriverPropertiesKhr
    {
    }

// Original Definition: 
// struct VkPresentRegionsKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t swapchainCount;
//     VkPresentRegionKHR* pRegions;
// }
    public unsafe struct PresentRegionsKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint SwapchainCount;
        public PresentRegionKhr* PRegions;
    }

// Original Definition: 
// struct VkPresentRegionKHR 
// {
//     uint32_t rectangleCount;
//     VkRectLayerKHR* pRectangles;
// }
    public unsafe struct PresentRegionKhr
    {
        public uint RectangleCount;
        public RectLayerKhr* PRectangles;
    }

// Original Definition: 
// struct VkRectLayerKHR 
// {
//     VkOffset2D offset;
//     VkExtent2D extent;
//     uint32_t layer;
// }
    public unsafe struct RectLayerKhr
    {
        public Offset2d Offset;
        public Extent2d Extent;
        public uint Layer;
    }

// Original Definition: 
// struct VkPhysicalDeviceVariablePointersFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 variablePointersStorageBuffer;
//     VkBool32 variablePointers;
// }
    public unsafe struct PhysicalDeviceVariablePointersFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint VariablePointersStorageBuffer;
        public uint VariablePointers;
    }
// Original Definition: 
// struct VkPhysicalDeviceVariablePointersFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceVariablePointersFeaturesKhr
    {
    }
// Original Definition: 
// struct VkPhysicalDeviceVariablePointerFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceVariablePointerFeaturesKhr
    {
    }
// Original Definition: 
// struct VkPhysicalDeviceVariablePointerFeatures 
// {

// }
    public unsafe struct PhysicalDeviceVariablePointerFeatures
    {
    }

// Original Definition: 
// struct VkExternalMemoryProperties 
// {
//     VkExternalMemoryFeatureFlags externalMemoryFeatures;
//     VkExternalMemoryHandleTypeFlags exportFromImportedHandleTypes;
//     VkExternalMemoryHandleTypeFlags compatibleHandleTypes;
// }
    public unsafe struct ExternalMemoryProperties
    {
        public ExternalMemoryFeatureFlags ExternalMemoryFeatures;
        public ExternalMemoryHandleTypeFlags ExportFromImportedHandleTypes;
        public ExternalMemoryHandleTypeFlags CompatibleHandleTypes;
    }
// Original Definition: 
// struct VkExternalMemoryPropertiesKHR 
// {

// }
    public unsafe struct ExternalMemoryPropertiesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceExternalImageFormatInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlagBits handleType;
// }
    public unsafe struct PhysicalDeviceExternalImageFormatInfo
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlags HandleType;
    }
// Original Definition: 
// struct VkPhysicalDeviceExternalImageFormatInfoKHR 
// {

// }
    public unsafe struct PhysicalDeviceExternalImageFormatInfoKhr
    {
    }

// Original Definition: 
// struct VkExternalImageFormatProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryProperties externalMemoryProperties;
// }
    public unsafe struct ExternalImageFormatProperties
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryProperties ExternalMemoryProperties;
    }
// Original Definition: 
// struct VkExternalImageFormatPropertiesKHR 
// {

// }
    public unsafe struct ExternalImageFormatPropertiesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceExternalBufferInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBufferCreateFlags flags;
//     VkBufferUsageFlags usage;
//     VkExternalMemoryHandleTypeFlagBits handleType;
// }
    public unsafe struct PhysicalDeviceExternalBufferInfo
    {
        public StructureType SType;
        public void* PNext;
        public BufferCreateFlags Flags;
        public BufferUsageFlags Usage;
        public ExternalMemoryHandleTypeFlags HandleType;
    }
// Original Definition: 
// struct VkPhysicalDeviceExternalBufferInfoKHR 
// {

// }
    public unsafe struct PhysicalDeviceExternalBufferInfoKhr
    {
    }

// Original Definition: 
// struct VkExternalBufferProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryProperties externalMemoryProperties;
// }
    public unsafe struct ExternalBufferProperties
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryProperties ExternalMemoryProperties;
    }
// Original Definition: 
// struct VkExternalBufferPropertiesKHR 
// {

// }
    public unsafe struct ExternalBufferPropertiesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceIDProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint8_t deviceUUID[(int)ApiConstants.UuidSize];
//     uint8_t driverUUID[(int)ApiConstants.UuidSize];
//     uint8_t deviceLUID[(int)ApiConstants.LuidSize];
//     uint32_t deviceNodeMask;
//     VkBool32 deviceLUIDValid;
// }
    public unsafe struct PhysicalDeviceIDProperties
    {
        public StructureType SType;
        public void* PNext;
        public fixed byte DeviceUUID[(int) ApiConstants.UuidSize];
        public fixed byte DriverUUID[(int) ApiConstants.UuidSize];
        public fixed byte DeviceLUID[(int) ApiConstants.LuidSize];
        public uint DeviceNodeMask;
        public uint DeviceLUIDValid;
    }
// Original Definition: 
// struct VkPhysicalDeviceIDPropertiesKHR 
// {

// }
    public unsafe struct PhysicalDeviceIDPropertiesKhr
    {
    }

// Original Definition: 
// struct VkExternalMemoryImageCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlags handleTypes;
// }
    public unsafe struct ExternalMemoryImageCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlags HandleTypes;
    }
// Original Definition: 
// struct VkExternalMemoryImageCreateInfoKHR 
// {

// }
    public unsafe struct ExternalMemoryImageCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkExternalMemoryBufferCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlags handleTypes;
// }
    public unsafe struct ExternalMemoryBufferCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlags HandleTypes;
    }
// Original Definition: 
// struct VkExternalMemoryBufferCreateInfoKHR 
// {

// }
    public unsafe struct ExternalMemoryBufferCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkExportMemoryAllocateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlags handleTypes;
// }
    public unsafe struct ExportMemoryAllocateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlags HandleTypes;
    }
// Original Definition: 
// struct VkExportMemoryAllocateInfoKHR 
// {

// }
    public unsafe struct ExportMemoryAllocateInfoKhr
    {
    }

// Original Definition: 
// struct VkImportMemoryWin32HandleInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlagBits handleType;
//     HANDLE handle;
//     LPCWSTR name;
// }
    public unsafe struct ImportMemoryWin32HandleInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlags HandleType;
        public IntPtr Handle;
        public sbyte* Name;
    }

// Original Definition: 
// struct VkExportMemoryWin32HandleInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     SECURITY_ATTRIBUTES* pAttributes;
//     DWORD dwAccess;
//     LPCWSTR name;
// }
    public unsafe struct ExportMemoryWin32HandleInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public void* PAttributes;
        public uint DwAccess;
        public sbyte* Name;
    }

// Original Definition: 
// struct VkMemoryWin32HandlePropertiesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t memoryTypeBits;
// }
    public unsafe struct MemoryWin32HandlePropertiesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint MemoryTypeBits;
    }

// Original Definition: 
// struct VkMemoryGetWin32HandleInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceMemory memory;
//     VkExternalMemoryHandleTypeFlagBits handleType;
// }
    public unsafe struct MemoryGetWin32HandleInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public DeviceMemory Memory;
        public ExternalMemoryHandleTypeFlags HandleType;
    }

// Original Definition: 
// struct VkImportMemoryFdInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlagBits handleType;
//     int fd;
// }
    public unsafe struct ImportMemoryFdInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlags HandleType;
        public IntPtr Fd;
    }

// Original Definition: 
// struct VkMemoryFdPropertiesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t memoryTypeBits;
// }
    public unsafe struct MemoryFdPropertiesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint MemoryTypeBits;
    }

// Original Definition: 
// struct VkMemoryGetFdInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceMemory memory;
//     VkExternalMemoryHandleTypeFlagBits handleType;
// }
    public unsafe struct MemoryGetFdInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public DeviceMemory Memory;
        public ExternalMemoryHandleTypeFlags HandleType;
    }

// Original Definition: 
// struct VkWin32KeyedMutexAcquireReleaseInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t acquireCount;
//     VkDeviceMemory* pAcquireSyncs;
//     uint64_t* pAcquireKeys;
//     uint32_t* pAcquireTimeouts;
//     uint32_t releaseCount;
//     VkDeviceMemory* pReleaseSyncs;
//     uint64_t* pReleaseKeys;
// }
    public unsafe struct Win32KeyedMutexAcquireReleaseInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint AcquireCount;
        public DeviceMemory* PAcquireSyncs;
        public ulong* PAcquireKeys;
        public uint* PAcquireTimeouts;
        public uint ReleaseCount;
        public DeviceMemory* PReleaseSyncs;
        public ulong* PReleaseKeys;
    }

// Original Definition: 
// struct VkPhysicalDeviceExternalSemaphoreInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalSemaphoreHandleTypeFlagBits handleType;
// }
    public unsafe struct PhysicalDeviceExternalSemaphoreInfo
    {
        public StructureType SType;
        public void* PNext;
        public ExternalSemaphoreHandleTypeFlags HandleType;
    }
// Original Definition: 
// struct VkPhysicalDeviceExternalSemaphoreInfoKHR 
// {

// }
    public unsafe struct PhysicalDeviceExternalSemaphoreInfoKhr
    {
    }

// Original Definition: 
// struct VkExternalSemaphoreProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalSemaphoreHandleTypeFlags exportFromImportedHandleTypes;
//     VkExternalSemaphoreHandleTypeFlags compatibleHandleTypes;
//     VkExternalSemaphoreFeatureFlags externalSemaphoreFeatures;
// }
    public unsafe struct ExternalSemaphoreProperties
    {
        public StructureType SType;
        public void* PNext;
        public ExternalSemaphoreHandleTypeFlags ExportFromImportedHandleTypes;
        public ExternalSemaphoreHandleTypeFlags CompatibleHandleTypes;
        public ExternalSemaphoreFeatureFlags ExternalSemaphoreFeatures;
    }
// Original Definition: 
// struct VkExternalSemaphorePropertiesKHR 
// {

// }
    public unsafe struct ExternalSemaphorePropertiesKhr
    {
    }

// Original Definition: 
// struct VkExportSemaphoreCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalSemaphoreHandleTypeFlags handleTypes;
// }
    public unsafe struct ExportSemaphoreCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ExternalSemaphoreHandleTypeFlags HandleTypes;
    }
// Original Definition: 
// struct VkExportSemaphoreCreateInfoKHR 
// {

// }
    public unsafe struct ExportSemaphoreCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkImportSemaphoreWin32HandleInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSemaphore semaphore;
//     VkSemaphoreImportFlags flags;
//     VkExternalSemaphoreHandleTypeFlagBits handleType;
//     HANDLE handle;
//     LPCWSTR name;
// }
    public unsafe struct ImportSemaphoreWin32HandleInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Semaphore Semaphore;
        public SemaphoreImportFlags Flags;
        public ExternalSemaphoreHandleTypeFlags HandleType;
        public IntPtr Handle;
        public sbyte* Name;
    }

// Original Definition: 
// struct VkExportSemaphoreWin32HandleInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     SECURITY_ATTRIBUTES* pAttributes;
//     DWORD dwAccess;
//     LPCWSTR name;
// }
    public unsafe struct ExportSemaphoreWin32HandleInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public void* PAttributes;
        public uint DwAccess;
        public sbyte* Name;
    }

// Original Definition: 
// struct VkD3D12FenceSubmitInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t waitSemaphoreValuesCount;
//     uint64_t* pWaitSemaphoreValues;
//     uint32_t signalSemaphoreValuesCount;
//     uint64_t* pSignalSemaphoreValues;
// }
    public unsafe struct D3D12FenceSubmitInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint WaitSemaphoreValuesCount;
        public ulong* PWaitSemaphoreValues;
        public uint SignalSemaphoreValuesCount;
        public ulong* PSignalSemaphoreValues;
    }

// Original Definition: 
// struct VkSemaphoreGetWin32HandleInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSemaphore semaphore;
//     VkExternalSemaphoreHandleTypeFlagBits handleType;
// }
    public unsafe struct SemaphoreGetWin32HandleInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Semaphore Semaphore;
        public ExternalSemaphoreHandleTypeFlags HandleType;
    }

// Original Definition: 
// struct VkImportSemaphoreFdInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSemaphore semaphore;
//     VkSemaphoreImportFlags flags;
//     VkExternalSemaphoreHandleTypeFlagBits handleType;
//     int fd;
// }
    public unsafe struct ImportSemaphoreFdInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Semaphore Semaphore;
        public SemaphoreImportFlags Flags;
        public ExternalSemaphoreHandleTypeFlags HandleType;
        public IntPtr Fd;
    }

// Original Definition: 
// struct VkSemaphoreGetFdInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSemaphore semaphore;
//     VkExternalSemaphoreHandleTypeFlagBits handleType;
// }
    public unsafe struct SemaphoreGetFdInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Semaphore Semaphore;
        public ExternalSemaphoreHandleTypeFlags HandleType;
    }

// Original Definition: 
// struct VkPhysicalDeviceExternalFenceInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalFenceHandleTypeFlagBits handleType;
// }
    public unsafe struct PhysicalDeviceExternalFenceInfo
    {
        public StructureType SType;
        public void* PNext;
        public ExternalFenceHandleTypeFlags HandleType;
    }
// Original Definition: 
// struct VkPhysicalDeviceExternalFenceInfoKHR 
// {

// }
    public unsafe struct PhysicalDeviceExternalFenceInfoKhr
    {
    }

// Original Definition: 
// struct VkExternalFenceProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalFenceHandleTypeFlags exportFromImportedHandleTypes;
//     VkExternalFenceHandleTypeFlags compatibleHandleTypes;
//     VkExternalFenceFeatureFlags externalFenceFeatures;
// }
    public unsafe struct ExternalFenceProperties
    {
        public StructureType SType;
        public void* PNext;
        public ExternalFenceHandleTypeFlags ExportFromImportedHandleTypes;
        public ExternalFenceHandleTypeFlags CompatibleHandleTypes;
        public ExternalFenceFeatureFlags ExternalFenceFeatures;
    }
// Original Definition: 
// struct VkExternalFencePropertiesKHR 
// {

// }
    public unsafe struct ExternalFencePropertiesKhr
    {
    }

// Original Definition: 
// struct VkExportFenceCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalFenceHandleTypeFlags handleTypes;
// }
    public unsafe struct ExportFenceCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ExternalFenceHandleTypeFlags HandleTypes;
    }
// Original Definition: 
// struct VkExportFenceCreateInfoKHR 
// {

// }
    public unsafe struct ExportFenceCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkImportFenceWin32HandleInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFence fence;
//     VkFenceImportFlags flags;
//     VkExternalFenceHandleTypeFlagBits handleType;
//     HANDLE handle;
//     LPCWSTR name;
// }
    public unsafe struct ImportFenceWin32HandleInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Fence Fence;
        public FenceImportFlags Flags;
        public ExternalFenceHandleTypeFlags HandleType;
        public IntPtr Handle;
        public sbyte* Name;
    }

// Original Definition: 
// struct VkExportFenceWin32HandleInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     SECURITY_ATTRIBUTES* pAttributes;
//     DWORD dwAccess;
//     LPCWSTR name;
// }
    public unsafe struct ExportFenceWin32HandleInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public void* PAttributes;
        public uint DwAccess;
        public sbyte* Name;
    }

// Original Definition: 
// struct VkFenceGetWin32HandleInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFence fence;
//     VkExternalFenceHandleTypeFlagBits handleType;
// }
    public unsafe struct FenceGetWin32HandleInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Fence Fence;
        public ExternalFenceHandleTypeFlags HandleType;
    }

// Original Definition: 
// struct VkImportFenceFdInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFence fence;
//     VkFenceImportFlags flags;
//     VkExternalFenceHandleTypeFlagBits handleType;
//     int fd;
// }
    public unsafe struct ImportFenceFdInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Fence Fence;
        public FenceImportFlags Flags;
        public ExternalFenceHandleTypeFlags HandleType;
        public IntPtr Fd;
    }

// Original Definition: 
// struct VkFenceGetFdInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFence fence;
//     VkExternalFenceHandleTypeFlagBits handleType;
// }
    public unsafe struct FenceGetFdInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Fence Fence;
        public ExternalFenceHandleTypeFlags HandleType;
    }

// Original Definition: 
// struct VkPhysicalDeviceMultiviewFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 multiview;
//     VkBool32 multiviewGeometryShader;
//     VkBool32 multiviewTessellationShader;
// }
    public unsafe struct PhysicalDeviceMultiviewFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint Multiview;
        public uint MultiviewGeometryShader;
        public uint MultiviewTessellationShader;
    }
// Original Definition: 
// struct VkPhysicalDeviceMultiviewFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceMultiviewFeaturesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceMultiviewProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxMultiviewViewCount;
//     uint32_t maxMultiviewInstanceIndex;
// }
    public unsafe struct PhysicalDeviceMultiviewProperties
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxMultiviewViewCount;
        public uint MaxMultiviewInstanceIndex;
    }
// Original Definition: 
// struct VkPhysicalDeviceMultiviewPropertiesKHR 
// {

// }
    public unsafe struct PhysicalDeviceMultiviewPropertiesKhr
    {
    }

// Original Definition: 
// struct VkRenderPassMultiviewCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t subpassCount;
//     uint32_t* pViewMasks;
//     uint32_t dependencyCount;
//     int32_t* pViewOffsets;
//     uint32_t correlationMaskCount;
//     uint32_t* pCorrelationMasks;
// }
    public unsafe struct RenderPassMultiviewCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint SubpassCount;
        public uint* PViewMasks;
        public uint DependencyCount;
        public int* PViewOffsets;
        public uint CorrelationMaskCount;
        public uint* PCorrelationMasks;
    }
// Original Definition: 
// struct VkRenderPassMultiviewCreateInfoKHR 
// {

// }
    public unsafe struct RenderPassMultiviewCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkSurfaceCapabilities2EXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t minImageCount;
//     uint32_t maxImageCount;
//     VkExtent2D currentExtent;
//     VkExtent2D minImageExtent;
//     VkExtent2D maxImageExtent;
//     uint32_t maxImageArrayLayers;
//     VkSurfaceTransformFlagsKHR supportedTransforms;
//     VkSurfaceTransformFlagBitsKHR currentTransform;
//     VkCompositeAlphaFlagsKHR supportedCompositeAlpha;
//     VkImageUsageFlags supportedUsageFlags;
//     VkSurfaceCounterFlagsEXT supportedSurfaceCounters;
// }
    public unsafe struct SurfaceCapabilities2ext
    {
        public StructureType SType;
        public void* PNext;
        public uint MinImageCount;
        public uint MaxImageCount;
        public Extent2d CurrentExtent;
        public Extent2d MinImageExtent;
        public Extent2d MaxImageExtent;
        public uint MaxImageArrayLayers;
        public SurfaceTransformFlagsKhr SupportedTransforms;
        public SurfaceTransformFlagsKhr CurrentTransform;
        public CompositeAlphaFlagsKhr SupportedCompositeAlpha;
        public ImageUsageFlags SupportedUsageFlags;
        public SurfaceCounterFlagsExt SupportedSurfaceCounters;
    }

// Original Definition: 
// struct VkDisplayPowerInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDisplayPowerStateEXT powerState;
// }
    public unsafe struct DisplayPowerInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public DisplayPowerStateExt PowerState;
    }

// Original Definition: 
// struct VkDeviceEventInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceEventTypeEXT deviceEvent;
// }
    public unsafe struct DeviceEventInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public DeviceEventTypeExt DeviceEvent;
    }

// Original Definition: 
// struct VkDisplayEventInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDisplayEventTypeEXT displayEvent;
// }
    public unsafe struct DisplayEventInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public DisplayEventTypeExt DisplayEvent;
    }

// Original Definition: 
// struct VkSwapchainCounterCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSurfaceCounterFlagsEXT surfaceCounters;
// }
    public unsafe struct SwapchainCounterCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public SurfaceCounterFlagsExt SurfaceCounters;
    }

// Original Definition: 
// struct VkPhysicalDeviceGroupProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t physicalDeviceCount;
//     VkPhysicalDevice physicalDevices[(int)ApiConstants.MaxDeviceGroupSize];
//     VkBool32 subsetAllocation;
// }
    public unsafe struct PhysicalDeviceGroupProperties
    {
        public StructureType SType;
        public void* PNext;
        public uint PhysicalDeviceCount;
        public fixed byte PhysicalDevices_[(int) ApiConstants.MaxDeviceGroupSize * InteropHelper.PtrSize];
        public uint SubsetAllocation;

        public PhysicalDevice[] PhysicalDevices
        {
            get
            {
                var devices = new PhysicalDevice[PhysicalDeviceCount];
                fixed (byte* bytePtr = PhysicalDevices_)
                {
                    var ptr = (IntPtr*) bytePtr;

                    for (var i = 0; i < PhysicalDeviceCount; ++i)
                        devices[i] = new PhysicalDevice {Handle = ptr[i]};
                }

                return devices;
            }
        }
    }
// Original Definition: 
// struct VkPhysicalDeviceGroupPropertiesKHR 
// {

// }
    public unsafe struct PhysicalDeviceGroupPropertiesKhr
    {
    }

// Original Definition: 
// struct VkMemoryAllocateFlagsInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkMemoryAllocateFlags flags;
//     uint32_t deviceMask;
// }
    public unsafe struct MemoryAllocateFlagsInfo
    {
        public StructureType SType;
        public void* PNext;
        public MemoryAllocateFlags Flags;
        public uint DeviceMask;
    }
// Original Definition: 
// struct VkMemoryAllocateFlagsInfoKHR 
// {

// }
    public unsafe struct MemoryAllocateFlagsInfoKhr
    {
    }

// Original Definition: 
// struct VkBindBufferMemoryInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBuffer buffer;
//     VkDeviceMemory memory;
//     VkDeviceSize memoryOffset;
// }
    public unsafe struct BindBufferMemoryInfo
    {
        public StructureType SType;
        public void* PNext;
        public Buffer Buffer;
        public DeviceMemory Memory;
        public ulong MemoryOffset;
    }
// Original Definition: 
// struct VkBindBufferMemoryInfoKHR 
// {

// }
    public unsafe struct BindBufferMemoryInfoKhr
    {
    }

// Original Definition: 
// struct VkBindBufferMemoryDeviceGroupInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t deviceIndexCount;
//     uint32_t* pDeviceIndices;
// }
    public unsafe struct BindBufferMemoryDeviceGroupInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint DeviceIndexCount;
        public uint* PDeviceIndices;
    }
// Original Definition: 
// struct VkBindBufferMemoryDeviceGroupInfoKHR 
// {

// }
    public unsafe struct BindBufferMemoryDeviceGroupInfoKhr
    {
    }

// Original Definition: 
// struct VkBindImageMemoryInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImage image;
//     VkDeviceMemory memory;
//     VkDeviceSize memoryOffset;
// }
    public unsafe struct BindImageMemoryInfo
    {
        public StructureType SType;
        public void* PNext;
        public Image Image;
        public DeviceMemory Memory;
        public ulong MemoryOffset;
    }
// Original Definition: 
// struct VkBindImageMemoryInfoKHR 
// {

// }
    public unsafe struct BindImageMemoryInfoKhr
    {
    }

// Original Definition: 
// struct VkBindImageMemoryDeviceGroupInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t deviceIndexCount;
//     uint32_t* pDeviceIndices;
//     uint32_t splitInstanceBindRegionCount;
//     VkRect2D* pSplitInstanceBindRegions;
// }
    public unsafe struct BindImageMemoryDeviceGroupInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint DeviceIndexCount;
        public uint* PDeviceIndices;
        public uint SplitInstanceBindRegionCount;
        public Rect2d* PSplitInstanceBindRegions;
    }
// Original Definition: 
// struct VkBindImageMemoryDeviceGroupInfoKHR 
// {

// }
    public unsafe struct BindImageMemoryDeviceGroupInfoKhr
    {
    }

// Original Definition: 
// struct VkDeviceGroupRenderPassBeginInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t deviceMask;
//     uint32_t deviceRenderAreaCount;
//     VkRect2D* pDeviceRenderAreas;
// }
    public unsafe struct DeviceGroupRenderPassBeginInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint DeviceMask;
        public uint DeviceRenderAreaCount;
        public Rect2d* PDeviceRenderAreas;
    }
// Original Definition: 
// struct VkDeviceGroupRenderPassBeginInfoKHR 
// {

// }
    public unsafe struct DeviceGroupRenderPassBeginInfoKhr
    {
    }

// Original Definition: 
// struct VkDeviceGroupCommandBufferBeginInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t deviceMask;
// }
    public unsafe struct DeviceGroupCommandBufferBeginInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint DeviceMask;
    }
// Original Definition: 
// struct VkDeviceGroupCommandBufferBeginInfoKHR 
// {

// }
    public unsafe struct DeviceGroupCommandBufferBeginInfoKhr
    {
    }

// Original Definition: 
// struct VkDeviceGroupSubmitInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t waitSemaphoreCount;
//     uint32_t* pWaitSemaphoreDeviceIndices;
//     uint32_t commandBufferCount;
//     uint32_t* pCommandBufferDeviceMasks;
//     uint32_t signalSemaphoreCount;
//     uint32_t* pSignalSemaphoreDeviceIndices;
// }
    public unsafe struct DeviceGroupSubmitInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint WaitSemaphoreCount;
        public uint* PWaitSemaphoreDeviceIndices;
        public uint CommandBufferCount;
        public uint* PCommandBufferDeviceMasks;
        public uint SignalSemaphoreCount;
        public uint* PSignalSemaphoreDeviceIndices;
    }
// Original Definition: 
// struct VkDeviceGroupSubmitInfoKHR 
// {

// }
    public unsafe struct DeviceGroupSubmitInfoKhr
    {
    }

// Original Definition: 
// struct VkDeviceGroupBindSparseInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t resourceDeviceIndex;
//     uint32_t memoryDeviceIndex;
// }
    public unsafe struct DeviceGroupBindSparseInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint ResourceDeviceIndex;
        public uint MemoryDeviceIndex;
    }
// Original Definition: 
// struct VkDeviceGroupBindSparseInfoKHR 
// {

// }
    public unsafe struct DeviceGroupBindSparseInfoKhr
    {
    }

// Original Definition: 
// struct VkDeviceGroupPresentCapabilitiesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t presentMask[(int)ApiConstants.MaxDeviceGroupSize];
//     VkDeviceGroupPresentModeFlagsKHR modes;
// }
    public unsafe struct DeviceGroupPresentCapabilitiesKhr
    {
        public StructureType SType;
        public void* PNext;
        public fixed uint PresentMask[(int) ApiConstants.MaxDeviceGroupSize];
        public DeviceGroupPresentModeFlagsKhr Modes;
    }

// Original Definition: 
// struct VkImageSwapchainCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSwapchainKHR swapchain;
// }
    public unsafe struct ImageSwapchainCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public SwapchainKhr Swapchain;
    }

// Original Definition: 
// struct VkBindImageMemorySwapchainInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSwapchainKHR swapchain;
//     uint32_t imageIndex;
// }
    public unsafe struct BindImageMemorySwapchainInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public SwapchainKhr Swapchain;
        public uint ImageIndex;
    }

// Original Definition: 
// struct VkAcquireNextImageInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSwapchainKHR swapchain;
//     uint64_t timeout;
//     VkSemaphore semaphore;
//     VkFence fence;
//     uint32_t deviceMask;
// }
    public unsafe struct AcquireNextImageInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public SwapchainKhr Swapchain;
        public ulong Timeout;
        public Semaphore Semaphore;
        public Fence Fence;
        public uint DeviceMask;
    }

// Original Definition: 
// struct VkDeviceGroupPresentInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t swapchainCount;
//     uint32_t* pDeviceMasks;
//     VkDeviceGroupPresentModeFlagBitsKHR mode;
// }
    public unsafe struct DeviceGroupPresentInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint SwapchainCount;
        public uint* PDeviceMasks;
        public DeviceGroupPresentModeFlagsKhr Mode;
    }

// Original Definition: 
// struct VkDeviceGroupDeviceCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t physicalDeviceCount;
//     VkPhysicalDevice* pPhysicalDevices;
// }
    public unsafe struct DeviceGroupDeviceCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint PhysicalDeviceCount;
        public PhysicalDevice* PPhysicalDevices;
    }
// Original Definition: 
// struct VkDeviceGroupDeviceCreateInfoKHR 
// {

// }
    public unsafe struct DeviceGroupDeviceCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkDeviceGroupSwapchainCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceGroupPresentModeFlagsKHR modes;
// }
    public unsafe struct DeviceGroupSwapchainCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public DeviceGroupPresentModeFlagsKhr Modes;
    }

// Original Definition: 
// struct VkDescriptorUpdateTemplateEntry 
// {
//     uint32_t dstBinding;
//     uint32_t dstArrayElement;
//     uint32_t descriptorCount;
//     VkDescriptorType descriptorType;
//     size_t offset;
//     size_t stride;
// }
    public unsafe struct DescriptorUpdateTemplateEntry
    {
        public uint DstBinding;
        public uint DstArrayElement;
        public uint DescriptorCount;
        public DescriptorType DescriptorType;
        public IntPtr Offset;
        public IntPtr Stride;
    }
// Original Definition: 
// struct VkDescriptorUpdateTemplateEntryKHR 
// {

// }
    public unsafe struct DescriptorUpdateTemplateEntryKhr
    {
    }

// Original Definition: 
// struct VkDescriptorUpdateTemplateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDescriptorUpdateTemplateCreateFlags flags;
//     uint32_t descriptorUpdateEntryCount;
//     VkDescriptorUpdateTemplateEntry* pDescriptorUpdateEntries;
//     VkDescriptorUpdateTemplateType templateType;
//     VkDescriptorSetLayout descriptorSetLayout;
//     VkPipelineBindPoint pipelineBindPoint;
//     VkPipelineLayout pipelineLayout;
//     uint32_t set;
// }
    public unsafe struct DescriptorUpdateTemplateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public DescriptorUpdateTemplateCreateFlags Flags;
        public uint DescriptorUpdateEntryCount;
        public DescriptorUpdateTemplateEntry* PDescriptorUpdateEntries;
        public DescriptorUpdateTemplateType TemplateType;
        public DescriptorSetLayout DescriptorSetLayout;
        public PipelineBindPoint PipelineBindPoint;
        public PipelineLayout PipelineLayout;
        public uint Set;
    }
// Original Definition: 
// struct VkDescriptorUpdateTemplateCreateInfoKHR 
// {

// }
    public unsafe struct DescriptorUpdateTemplateCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkXYColorEXT 
// {
//     float x;
//     float y;
// }
    public unsafe struct XYColorExt
    {
        public float X;
        public float Y;
    }

// Original Definition: 
// struct VkHdrMetadataEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkXYColorEXT displayPrimaryRed;
//     VkXYColorEXT displayPrimaryGreen;
//     VkXYColorEXT displayPrimaryBlue;
//     VkXYColorEXT whitePoint;
//     float maxLuminance;
//     float minLuminance;
//     float maxContentLightLevel;
//     float maxFrameAverageLightLevel;
// }
    public unsafe struct HdrMetadataExt
    {
        public StructureType SType;
        public void* PNext;
        public XYColorExt DisplayPrimaryRed;
        public XYColorExt DisplayPrimaryGreen;
        public XYColorExt DisplayPrimaryBlue;
        public XYColorExt WhitePoint;
        public float MaxLuminance;
        public float MinLuminance;
        public float MaxContentLightLevel;
        public float MaxFrameAverageLightLevel;
    }

// Original Definition: 
// struct VkDisplayNativeHdrSurfaceCapabilitiesAMD 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 localDimmingSupport;
// }
    public unsafe struct DisplayNativeHdrSurfaceCapabilitiesAmd
    {
        public StructureType SType;
        public void* PNext;
        public uint LocalDimmingSupport;
    }

// Original Definition: 
// struct VkSwapchainDisplayNativeHdrCreateInfoAMD 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 localDimmingEnable;
// }
    public unsafe struct SwapchainDisplayNativeHdrCreateInfoAmd
    {
        public StructureType SType;
        public void* PNext;
        public uint LocalDimmingEnable;
    }

// Original Definition: 
// struct VkRefreshCycleDurationGOOGLE 
// {
//     uint64_t refreshDuration;
// }
    public unsafe struct RefreshCycleDurationGoogle
    {
        public ulong RefreshDuration;
    }

// Original Definition: 
// struct VkPastPresentationTimingGOOGLE 
// {
//     uint32_t presentID;
//     uint64_t desiredPresentTime;
//     uint64_t actualPresentTime;
//     uint64_t earliestPresentTime;
//     uint64_t presentMargin;
// }
    public unsafe struct PastPresentationTimingGoogle
    {
        public uint PresentID;
        public ulong DesiredPresentTime;
        public ulong ActualPresentTime;
        public ulong EarliestPresentTime;
        public ulong PresentMargin;
    }

// Original Definition: 
// struct VkPresentTimesInfoGOOGLE 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t swapchainCount;
//     VkPresentTimeGOOGLE* pTimes;
// }
    public unsafe struct PresentTimesInfoGoogle
    {
        public StructureType SType;
        public void* PNext;
        public uint SwapchainCount;
        public PresentTimeGoogle* PTimes;
    }

// Original Definition: 
// struct VkPresentTimeGOOGLE 
// {
//     uint32_t presentID;
//     uint64_t desiredPresentTime;
// }
    public unsafe struct PresentTimeGoogle
    {
        public uint PresentID;
        public ulong DesiredPresentTime;
    }

// Original Definition: 
// struct VkIOSSurfaceCreateInfoMVK 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkIOSSurfaceCreateFlagsMVK flags;
//     void* pView;
// }
    public unsafe struct IOSSurfaceCreateInfoMvk
    {
        public StructureType SType;
        public void* PNext;
        public IOSSurfaceCreateFlagsMvk Flags;
        public void* PView;
    }

// Original Definition: 
// struct VkMacOSSurfaceCreateInfoMVK 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkMacOSSurfaceCreateFlagsMVK flags;
//     void* pView;
// }
    public unsafe struct MacOSSurfaceCreateInfoMvk
    {
        public StructureType SType;
        public void* PNext;
        public MacOSSurfaceCreateFlagsMvk Flags;
        public void* PView;
    }

// Original Definition: 
// struct VkMetalSurfaceCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkMetalSurfaceCreateFlagsEXT flags;
//     CAMetalLayer* pLayer;
// }
    public unsafe struct MetalSurfaceCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public MetalSurfaceCreateFlagsExt Flags;
        public IntPtr PLayer;
    }

// Original Definition: 
// struct VkViewportWScalingNV 
// {
//     float xcoeff;
//     float ycoeff;
// }
    public unsafe struct ViewportWScalingNv
    {
        public float Xcoeff;
        public float Ycoeff;
    }

// Original Definition: 
// struct VkPipelineViewportWScalingStateCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 viewportWScalingEnable;
//     uint32_t viewportCount;
//     VkViewportWScalingNV* pViewportWScalings;
// }
    public unsafe struct PipelineViewportWScalingStateCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ViewportWScalingEnable;
        public uint ViewportCount;
        public ViewportWScalingNv* PViewportWScalings;
    }

// Original Definition: 
// struct VkViewportSwizzleNV 
// {
//     VkViewportCoordinateSwizzleNV x;
//     VkViewportCoordinateSwizzleNV y;
//     VkViewportCoordinateSwizzleNV z;
//     VkViewportCoordinateSwizzleNV w;
// }
    public unsafe struct ViewportSwizzleNv
    {
        public ViewportCoordinateSwizzleNv X;
        public ViewportCoordinateSwizzleNv Y;
        public ViewportCoordinateSwizzleNv Z;
        public ViewportCoordinateSwizzleNv W;
    }

// Original Definition: 
// struct VkPipelineViewportSwizzleStateCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineViewportSwizzleStateCreateFlagsNV flags;
//     uint32_t viewportCount;
//     VkViewportSwizzleNV* pViewportSwizzles;
// }
    public unsafe struct PipelineViewportSwizzleStateCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public PipelineViewportSwizzleStateCreateFlagsNv Flags;
        public uint ViewportCount;
        public ViewportSwizzleNv* PViewportSwizzles;
    }

// Original Definition: 
// struct VkPhysicalDeviceDiscardRectanglePropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxDiscardRectangles;
// }
    public unsafe struct PhysicalDeviceDiscardRectanglePropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxDiscardRectangles;
    }

// Original Definition: 
// struct VkPipelineDiscardRectangleStateCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineDiscardRectangleStateCreateFlagsEXT flags;
//     VkDiscardRectangleModeEXT discardRectangleMode;
//     uint32_t discardRectangleCount;
//     VkRect2D* pDiscardRectangles;
// }
    public unsafe struct PipelineDiscardRectangleStateCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public PipelineDiscardRectangleStateCreateFlagsExt Flags;
        public DiscardRectangleModeExt DiscardRectangleMode;
        public uint DiscardRectangleCount;
        public Rect2d* PDiscardRectangles;
    }

// Original Definition: 
// struct VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 perViewPositionAllComponents;
// }
    public unsafe struct PhysicalDeviceMultiviewPerViewAttributesPropertiesNvx
    {
        public StructureType SType;
        public void* PNext;
        public uint PerViewPositionAllComponents;
    }

// Original Definition: 
// struct VkInputAttachmentAspectReference 
// {
//     uint32_t subpass;
//     uint32_t inputAttachmentIndex;
//     VkImageAspectFlags aspectMask;
// }
    public unsafe struct InputAttachmentAspectReference
    {
        public uint Subpass;
        public uint InputAttachmentIndex;
        public ImageAspectFlags AspectMask;
    }
// Original Definition: 
// struct VkInputAttachmentAspectReferenceKHR 
// {

// }
    public unsafe struct InputAttachmentAspectReferenceKhr
    {
    }

// Original Definition: 
// struct VkRenderPassInputAttachmentAspectCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t aspectReferenceCount;
//     VkInputAttachmentAspectReference* pAspectReferences;
// }
    public unsafe struct RenderPassInputAttachmentAspectCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint AspectReferenceCount;
        public InputAttachmentAspectReference* PAspectReferences;
    }
// Original Definition: 
// struct VkRenderPassInputAttachmentAspectCreateInfoKHR 
// {

// }
    public unsafe struct RenderPassInputAttachmentAspectCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceSurfaceInfo2KHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSurfaceKHR surface;
// }
    public unsafe struct PhysicalDeviceSurfaceInfo2khr
    {
        public StructureType SType;
        public void* PNext;
        public SurfaceKhr Surface;
    }

// Original Definition: 
// struct VkSurfaceCapabilities2KHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSurfaceCapabilitiesKHR surfaceCapabilities;
// }
    public unsafe struct SurfaceCapabilities2khr
    {
        public StructureType SType;
        public void* PNext;
        public SurfaceCapabilitiesKhr SurfaceCapabilities;
    }

// Original Definition: 
// struct VkSurfaceFormat2KHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSurfaceFormatKHR surfaceFormat;
// }
    public unsafe struct SurfaceFormat2khr
    {
        public StructureType SType;
        public void* PNext;
        public SurfaceFormatKhr SurfaceFormat;
    }

// Original Definition: 
// struct VkDisplayProperties2KHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDisplayPropertiesKHR displayProperties;
// }
    public unsafe struct DisplayProperties2khr
    {
        public StructureType SType;
        public void* PNext;
        public DisplayPropertiesKhr DisplayProperties;
    }

// Original Definition: 
// struct VkDisplayPlaneProperties2KHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDisplayPlanePropertiesKHR displayPlaneProperties;
// }
    public unsafe struct DisplayPlaneProperties2khr
    {
        public StructureType SType;
        public void* PNext;
        public DisplayPlanePropertiesKhr DisplayPlaneProperties;
    }

// Original Definition: 
// struct VkDisplayModeProperties2KHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDisplayModePropertiesKHR displayModeProperties;
// }
    public unsafe struct DisplayModeProperties2khr
    {
        public StructureType SType;
        public void* PNext;
        public DisplayModePropertiesKhr DisplayModeProperties;
    }

// Original Definition: 
// struct VkDisplayPlaneInfo2KHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDisplayModeKHR mode;
//     uint32_t planeIndex;
// }
    public unsafe struct DisplayPlaneInfo2khr
    {
        public StructureType SType;
        public void* PNext;
        public DisplayModeKhr Mode;
        public uint PlaneIndex;
    }

// Original Definition: 
// struct VkDisplayPlaneCapabilities2KHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDisplayPlaneCapabilitiesKHR capabilities;
// }
    public unsafe struct DisplayPlaneCapabilities2khr
    {
        public StructureType SType;
        public void* PNext;
        public DisplayPlaneCapabilitiesKhr Capabilities;
    }

// Original Definition: 
// struct VkSharedPresentSurfaceCapabilitiesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageUsageFlags sharedPresentSupportedUsageFlags;
// }
    public unsafe struct SharedPresentSurfaceCapabilitiesKhr
    {
        public StructureType SType;
        public void* PNext;
        public ImageUsageFlags SharedPresentSupportedUsageFlags;
    }

// Original Definition: 
// struct VkPhysicalDevice16BitStorageFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 storageBuffer16BitAccess;
//     VkBool32 uniformAndStorageBuffer16BitAccess;
//     VkBool32 storagePushConstant16;
//     VkBool32 storageInputOutput16;
// }
    public unsafe struct PhysicalDevice16BitStorageFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint StorageBuffer16BitAccess;
        public uint UniformAndStorageBuffer16BitAccess;
        public uint StoragePushConstant16;
        public uint StorageInputOutput16;
    }
// Original Definition: 
// struct VkPhysicalDevice16BitStorageFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDevice16BitStorageFeaturesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceSubgroupProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t subgroupSize;
//     VkShaderStageFlags supportedStages;
//     VkSubgroupFeatureFlags supportedOperations;
//     VkBool32 quadOperationsInAllStages;
// }
    public unsafe struct PhysicalDeviceSubgroupProperties
    {
        public StructureType SType;
        public void* PNext;
        public uint SubgroupSize;
        public ShaderStageFlags SupportedStages;
        public SubgroupFeatureFlags SupportedOperations;
        public uint QuadOperationsInAllStages;
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderSubgroupExtendedTypes;
// }
    public unsafe struct PhysicalDeviceShaderSubgroupExtendedTypesFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderSubgroupExtendedTypes;
    }
// Original Definition: 
// struct VkPhysicalDeviceShaderSubgroupExtendedTypesFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceShaderSubgroupExtendedTypesFeaturesKhr
    {
    }

// Original Definition: 
// struct VkBufferMemoryRequirementsInfo2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBuffer buffer;
// }
    public unsafe struct BufferMemoryRequirementsInfo2
    {
        public StructureType SType;
        public void* PNext;
        public Buffer Buffer;
    }
// Original Definition: 
// struct VkBufferMemoryRequirementsInfo2KHR 
// {

// }
    public unsafe struct BufferMemoryRequirementsInfo2khr
    {
    }

// Original Definition: 
// struct VkImageMemoryRequirementsInfo2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImage image;
// }
    public unsafe struct ImageMemoryRequirementsInfo2
    {
        public StructureType SType;
        public void* PNext;
        public Image Image;
    }
// Original Definition: 
// struct VkImageMemoryRequirementsInfo2KHR 
// {

// }
    public unsafe struct ImageMemoryRequirementsInfo2khr
    {
    }

// Original Definition: 
// struct VkImageSparseMemoryRequirementsInfo2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImage image;
// }
    public unsafe struct ImageSparseMemoryRequirementsInfo2
    {
        public StructureType SType;
        public void* PNext;
        public Image Image;
    }
// Original Definition: 
// struct VkImageSparseMemoryRequirementsInfo2KHR 
// {

// }
    public unsafe struct ImageSparseMemoryRequirementsInfo2khr
    {
    }

// Original Definition: 
// struct VkMemoryRequirements2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkMemoryRequirements memoryRequirements;
// }
    public unsafe struct MemoryRequirements2
    {
        public StructureType SType;
        public void* PNext;
        public MemoryRequirements MemoryRequirements;
    }
// Original Definition: 
// struct VkMemoryRequirements2KHR 
// {

// }
    public unsafe struct MemoryRequirements2khr
    {
    }

// Original Definition: 
// struct VkSparseImageMemoryRequirements2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSparseImageMemoryRequirements memoryRequirements;
// }
    public unsafe struct SparseImageMemoryRequirements2
    {
        public StructureType SType;
        public void* PNext;
        public SparseImageMemoryRequirements MemoryRequirements;
    }
// Original Definition: 
// struct VkSparseImageMemoryRequirements2KHR 
// {

// }
    public unsafe struct SparseImageMemoryRequirements2khr
    {
    }

// Original Definition: 
// struct VkPhysicalDevicePointClippingProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPointClippingBehavior pointClippingBehavior;
// }
    public unsafe struct PhysicalDevicePointClippingProperties
    {
        public StructureType SType;
        public void* PNext;
        public PointClippingBehavior PointClippingBehavior;
    }
// Original Definition: 
// struct VkPhysicalDevicePointClippingPropertiesKHR 
// {

// }
    public unsafe struct PhysicalDevicePointClippingPropertiesKhr
    {
    }

// Original Definition: 
// struct VkMemoryDedicatedRequirements 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 prefersDedicatedAllocation;
//     VkBool32 requiresDedicatedAllocation;
// }
    public unsafe struct MemoryDedicatedRequirements
    {
        public StructureType SType;
        public void* PNext;
        public uint PrefersDedicatedAllocation;
        public uint RequiresDedicatedAllocation;
    }
// Original Definition: 
// struct VkMemoryDedicatedRequirementsKHR 
// {

// }
    public unsafe struct MemoryDedicatedRequirementsKhr
    {
    }

// Original Definition: 
// struct VkMemoryDedicatedAllocateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImage image;
//     VkBuffer buffer;
// }
    public unsafe struct MemoryDedicatedAllocateInfo
    {
        public StructureType SType;
        public void* PNext;
        public Image Image;
        public Buffer Buffer;
    }
// Original Definition: 
// struct VkMemoryDedicatedAllocateInfoKHR 
// {

// }
    public unsafe struct MemoryDedicatedAllocateInfoKhr
    {
    }

// Original Definition: 
// struct VkImageViewUsageCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageUsageFlags usage;
// }
    public unsafe struct ImageViewUsageCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ImageUsageFlags Usage;
    }
// Original Definition: 
// struct VkImageViewUsageCreateInfoKHR 
// {

// }
    public unsafe struct ImageViewUsageCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkPipelineTessellationDomainOriginStateCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkTessellationDomainOrigin domainOrigin;
// }
    public unsafe struct PipelineTessellationDomainOriginStateCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public TessellationDomainOrigin DomainOrigin;
    }
// Original Definition: 
// struct VkPipelineTessellationDomainOriginStateCreateInfoKHR 
// {

// }
    public unsafe struct PipelineTessellationDomainOriginStateCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkSamplerYcbcrConversionInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSamplerYcbcrConversion conversion;
// }
    public unsafe struct SamplerYcbcrConversionInfo
    {
        public StructureType SType;
        public void* PNext;
        public SamplerYcbcrConversion Conversion;
    }
// Original Definition: 
// struct VkSamplerYcbcrConversionInfoKHR 
// {

// }
    public unsafe struct SamplerYcbcrConversionInfoKhr
    {
    }

// Original Definition: 
// struct VkSamplerYcbcrConversionCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFormat format;
//     VkSamplerYcbcrModelConversion ycbcrModel;
//     VkSamplerYcbcrRange ycbcrRange;
//     VkComponentMapping components;
//     VkChromaLocation xChromaOffset;
//     VkChromaLocation yChromaOffset;
//     VkFilter chromaFilter;
//     VkBool32 forceExplicitReconstruction;
// }
    public unsafe struct SamplerYcbcrConversionCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public Format Format;
        public SamplerYcbcrModelConversion YcbcrModel;
        public SamplerYcbcrRange YcbcrRange;
        public ComponentMapping Components;
        public ChromaLocation XChromaOffset;
        public ChromaLocation YChromaOffset;
        public Filter ChromaFilter;
        public uint ForceExplicitReconstruction;
    }
// Original Definition: 
// struct VkSamplerYcbcrConversionCreateInfoKHR 
// {

// }
    public unsafe struct SamplerYcbcrConversionCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkBindImagePlaneMemoryInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageAspectFlagBits planeAspect;
// }
    public unsafe struct BindImagePlaneMemoryInfo
    {
        public StructureType SType;
        public void* PNext;
        public ImageAspectFlags PlaneAspect;
    }
// Original Definition: 
// struct VkBindImagePlaneMemoryInfoKHR 
// {

// }
    public unsafe struct BindImagePlaneMemoryInfoKhr
    {
    }

// Original Definition: 
// struct VkImagePlaneMemoryRequirementsInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageAspectFlagBits planeAspect;
// }
    public unsafe struct ImagePlaneMemoryRequirementsInfo
    {
        public StructureType SType;
        public void* PNext;
        public ImageAspectFlags PlaneAspect;
    }
// Original Definition: 
// struct VkImagePlaneMemoryRequirementsInfoKHR 
// {

// }
    public unsafe struct ImagePlaneMemoryRequirementsInfoKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceSamplerYcbcrConversionFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 samplerYcbcrConversion;
// }
    public unsafe struct PhysicalDeviceSamplerYcbcrConversionFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint SamplerYcbcrConversion;
    }
// Original Definition: 
// struct VkPhysicalDeviceSamplerYcbcrConversionFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceSamplerYcbcrConversionFeaturesKhr
    {
    }

// Original Definition: 
// struct VkSamplerYcbcrConversionImageFormatProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t combinedImageSamplerDescriptorCount;
// }
    public unsafe struct SamplerYcbcrConversionImageFormatProperties
    {
        public StructureType SType;
        public void* PNext;
        public uint CombinedImageSamplerDescriptorCount;
    }
// Original Definition: 
// struct VkSamplerYcbcrConversionImageFormatPropertiesKHR 
// {

// }
    public unsafe struct SamplerYcbcrConversionImageFormatPropertiesKhr
    {
    }

// Original Definition: 
// struct VkTextureLODGatherFormatPropertiesAMD 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 supportsTextureGatherLODBiasAMD;
// }
    public unsafe struct TextureLODGatherFormatPropertiesAmd
    {
        public StructureType SType;
        public void* PNext;
        public uint SupportsTextureGatherLODBiasAMD;
    }

// Original Definition: 
// struct VkConditionalRenderingBeginInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBuffer buffer;
//     VkDeviceSize offset;
//     VkConditionalRenderingFlagsEXT flags;
// }
    public unsafe struct ConditionalRenderingBeginInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public Buffer Buffer;
        public ulong Offset;
        public ConditionalRenderingFlagsExt Flags;
    }

// Original Definition: 
// struct VkProtectedSubmitInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 protectedSubmit;
// }
    public unsafe struct ProtectedSubmitInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint ProtectedSubmit;
    }

// Original Definition: 
// struct VkPhysicalDeviceProtectedMemoryFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 protectedMemory;
// }
    public unsafe struct PhysicalDeviceProtectedMemoryFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint ProtectedMemory;
    }

// Original Definition: 
// struct VkPhysicalDeviceProtectedMemoryProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 protectedNoFault;
// }
    public unsafe struct PhysicalDeviceProtectedMemoryProperties
    {
        public StructureType SType;
        public void* PNext;
        public uint ProtectedNoFault;
    }

// Original Definition: 
// struct VkDeviceQueueInfo2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceQueueCreateFlags flags;
//     uint32_t queueFamilyIndex;
//     uint32_t queueIndex;
// }
    public unsafe struct DeviceQueueInfo2
    {
        public StructureType SType;
        public void* PNext;
        public DeviceQueueCreateFlags Flags;
        public uint QueueFamilyIndex;
        public uint QueueIndex;
    }

// Original Definition: 
// struct VkPipelineCoverageToColorStateCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCoverageToColorStateCreateFlagsNV flags;
//     VkBool32 coverageToColorEnable;
//     uint32_t coverageToColorLocation;
// }
    public unsafe struct PipelineCoverageToColorStateCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCoverageToColorStateCreateFlagsNv Flags;
        public uint CoverageToColorEnable;
        public uint CoverageToColorLocation;
    }

// Original Definition: 
// struct VkPhysicalDeviceSamplerFilterMinmaxProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 filterMinmaxSingleComponentFormats;
//     VkBool32 filterMinmaxImageComponentMapping;
// }
    public unsafe struct PhysicalDeviceSamplerFilterMinmaxProperties
    {
        public StructureType SType;
        public void* PNext;
        public uint FilterMinmaxSingleComponentFormats;
        public uint FilterMinmaxImageComponentMapping;
    }
// Original Definition: 
// struct VkPhysicalDeviceSamplerFilterMinmaxPropertiesEXT 
// {

// }
    public unsafe struct PhysicalDeviceSamplerFilterMinmaxPropertiesExt
    {
    }

// Original Definition: 
// struct VkSampleLocationEXT 
// {
//     float x;
//     float y;
// }
    public unsafe struct SampleLocationExt
    {
        public float X;
        public float Y;
    }

// Original Definition: 
// struct VkSampleLocationsInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSampleCountFlagBits sampleLocationsPerPixel;
//     VkExtent2D sampleLocationGridSize;
//     uint32_t sampleLocationsCount;
//     VkSampleLocationEXT* pSampleLocations;
// }
    public unsafe struct SampleLocationsInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public SampleCountFlags SampleLocationsPerPixel;
        public Extent2d SampleLocationGridSize;
        public uint SampleLocationsCount;
        public SampleLocationExt* PSampleLocations;
    }

// Original Definition: 
// struct VkAttachmentSampleLocationsEXT 
// {
//     uint32_t attachmentIndex;
//     VkSampleLocationsInfoEXT sampleLocationsInfo;
// }
    public unsafe struct AttachmentSampleLocationsExt
    {
        public uint AttachmentIndex;
        public SampleLocationsInfoExt SampleLocationsInfo;
    }

// Original Definition: 
// struct VkSubpassSampleLocationsEXT 
// {
//     uint32_t subpassIndex;
//     VkSampleLocationsInfoEXT sampleLocationsInfo;
// }
    public unsafe struct SubpassSampleLocationsExt
    {
        public uint SubpassIndex;
        public SampleLocationsInfoExt SampleLocationsInfo;
    }

// Original Definition: 
// struct VkRenderPassSampleLocationsBeginInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t attachmentInitialSampleLocationsCount;
//     VkAttachmentSampleLocationsEXT* pAttachmentInitialSampleLocations;
//     uint32_t postSubpassSampleLocationsCount;
//     VkSubpassSampleLocationsEXT* pPostSubpassSampleLocations;
// }
    public unsafe struct RenderPassSampleLocationsBeginInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public uint AttachmentInitialSampleLocationsCount;
        public AttachmentSampleLocationsExt* PAttachmentInitialSampleLocations;
        public uint PostSubpassSampleLocationsCount;
        public SubpassSampleLocationsExt* PPostSubpassSampleLocations;
    }

// Original Definition: 
// struct VkPipelineSampleLocationsStateCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 sampleLocationsEnable;
//     VkSampleLocationsInfoEXT sampleLocationsInfo;
// }
    public unsafe struct PipelineSampleLocationsStateCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public uint SampleLocationsEnable;
        public SampleLocationsInfoExt SampleLocationsInfo;
    }

// Original Definition: 
// struct VkPhysicalDeviceSampleLocationsPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSampleCountFlags sampleLocationSampleCounts;
//     VkExtent2D maxSampleLocationGridSize;
//     float sampleLocationCoordinateRange;
//     uint32_t sampleLocationSubPixelBits;
//     VkBool32 variableSampleLocations;
// }
    public unsafe struct PhysicalDeviceSampleLocationsPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public SampleCountFlags SampleLocationSampleCounts;
        public Extent2d MaxSampleLocationGridSize;
        public fixed float SampleLocationCoordinateRange[2];
        public uint SampleLocationSubPixelBits;
        public uint VariableSampleLocations;
    }

// Original Definition: 
// struct VkMultisamplePropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExtent2D maxSampleLocationGridSize;
// }
    public unsafe struct MultisamplePropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public Extent2d MaxSampleLocationGridSize;
    }

// Original Definition: 
// struct VkSamplerReductionModeCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSamplerReductionMode reductionMode;
// }
    public unsafe struct SamplerReductionModeCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public SamplerReductionMode ReductionMode;
    }
// Original Definition: 
// struct VkSamplerReductionModeCreateInfoEXT 
// {

// }
    public unsafe struct SamplerReductionModeCreateInfoExt
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 advancedBlendCoherentOperations;
// }
    public unsafe struct PhysicalDeviceBlendOperationAdvancedFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint AdvancedBlendCoherentOperations;
    }

// Original Definition: 
// struct VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t advancedBlendMaxColorAttachments;
//     VkBool32 advancedBlendIndependentBlend;
//     VkBool32 advancedBlendNonPremultipliedSrcColor;
//     VkBool32 advancedBlendNonPremultipliedDstColor;
//     VkBool32 advancedBlendCorrelatedOverlap;
//     VkBool32 advancedBlendAllOperations;
// }
    public unsafe struct PhysicalDeviceBlendOperationAdvancedPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint AdvancedBlendMaxColorAttachments;
        public uint AdvancedBlendIndependentBlend;
        public uint AdvancedBlendNonPremultipliedSrcColor;
        public uint AdvancedBlendNonPremultipliedDstColor;
        public uint AdvancedBlendCorrelatedOverlap;
        public uint AdvancedBlendAllOperations;
    }

// Original Definition: 
// struct VkPipelineColorBlendAdvancedStateCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 srcPremultiplied;
//     VkBool32 dstPremultiplied;
//     VkBlendOverlapEXT blendOverlap;
// }
    public unsafe struct PipelineColorBlendAdvancedStateCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public uint SrcPremultiplied;
        public uint DstPremultiplied;
        public BlendOverlapExt BlendOverlap;
    }

// Original Definition: 
// struct VkPhysicalDeviceInlineUniformBlockFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 inlineUniformBlock;
//     VkBool32 descriptorBindingInlineUniformBlockUpdateAfterBind;
// }
    public unsafe struct PhysicalDeviceInlineUniformBlockFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint InlineUniformBlock;
        public uint DescriptorBindingInlineUniformBlockUpdateAfterBind;
    }

// Original Definition: 
// struct VkPhysicalDeviceInlineUniformBlockPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxInlineUniformBlockSize;
//     uint32_t maxPerStageDescriptorInlineUniformBlocks;
//     uint32_t maxPerStageDescriptorUpdateAfterBindInlineUniformBlocks;
//     uint32_t maxDescriptorSetInlineUniformBlocks;
//     uint32_t maxDescriptorSetUpdateAfterBindInlineUniformBlocks;
// }
    public unsafe struct PhysicalDeviceInlineUniformBlockPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxInlineUniformBlockSize;
        public uint MaxPerStageDescriptorInlineUniformBlocks;
        public uint MaxPerStageDescriptorUpdateAfterBindInlineUniformBlocks;
        public uint MaxDescriptorSetInlineUniformBlocks;
        public uint MaxDescriptorSetUpdateAfterBindInlineUniformBlocks;
    }

// Original Definition: 
// struct VkWriteDescriptorSetInlineUniformBlockEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t dataSize;
//     void* pData;
// }
    public unsafe struct WriteDescriptorSetInlineUniformBlockExt
    {
        public StructureType SType;
        public void* PNext;
        public uint DataSize;
        public void* PData;
    }

// Original Definition: 
// struct VkDescriptorPoolInlineUniformBlockCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxInlineUniformBlockBindings;
// }
    public unsafe struct DescriptorPoolInlineUniformBlockCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxInlineUniformBlockBindings;
    }

// Original Definition: 
// struct VkPipelineCoverageModulationStateCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCoverageModulationStateCreateFlagsNV flags;
//     VkCoverageModulationModeNV coverageModulationMode;
//     VkBool32 coverageModulationTableEnable;
//     uint32_t coverageModulationTableCount;
//     float* pCoverageModulationTable;
// }
    public unsafe struct PipelineCoverageModulationStateCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCoverageModulationStateCreateFlagsNv Flags;
        public CoverageModulationModeNv CoverageModulationMode;
        public uint CoverageModulationTableEnable;
        public uint CoverageModulationTableCount;
        public IntPtr PCoverageModulationTable;
    }

// Original Definition: 
// struct VkImageFormatListCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t viewFormatCount;
//     VkFormat* pViewFormats;
// }
    public unsafe struct ImageFormatListCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint ViewFormatCount;
        public Format* PViewFormats;
    }
// Original Definition: 
// struct VkImageFormatListCreateInfoKHR 
// {

// }
    public unsafe struct ImageFormatListCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkValidationCacheCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkValidationCacheCreateFlagsEXT flags;
//     size_t initialDataSize;
//     void* pInitialData;
// }
    public unsafe struct ValidationCacheCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ValidationCacheCreateFlagsExt Flags;
        public IntPtr InitialDataSize;
        public void* PInitialData;
    }

// Original Definition: 
// struct VkShaderModuleValidationCacheCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkValidationCacheEXT validationCache;
// }
    public unsafe struct ShaderModuleValidationCacheCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ValidationCacheExt ValidationCache;
    }

// Original Definition: 
// struct VkPhysicalDeviceMaintenance3Properties 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxPerSetDescriptors;
//     VkDeviceSize maxMemoryAllocationSize;
// }
    public unsafe struct PhysicalDeviceMaintenance3Properties
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxPerSetDescriptors;
        public ulong MaxMemoryAllocationSize;
    }
// Original Definition: 
// struct VkPhysicalDeviceMaintenance3PropertiesKHR 
// {

// }
    public unsafe struct PhysicalDeviceMaintenance3PropertiesKhr
    {
    }

// Original Definition: 
// struct VkDescriptorSetLayoutSupport 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 supported;
// }
    public unsafe struct DescriptorSetLayoutSupport
    {
        public StructureType SType;
        public void* PNext;
        public uint Supported;
    }
// Original Definition: 
// struct VkDescriptorSetLayoutSupportKHR 
// {

// }
    public unsafe struct DescriptorSetLayoutSupportKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderDrawParametersFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderDrawParameters;
// }
    public unsafe struct PhysicalDeviceShaderDrawParametersFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderDrawParameters;
    }
// Original Definition: 
// struct VkPhysicalDeviceShaderDrawParameterFeatures 
// {

// }
    public unsafe struct PhysicalDeviceShaderDrawParameterFeatures
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderFloat16Int8Features 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderFloat16;
//     VkBool32 shaderInt8;
// }
    public unsafe struct PhysicalDeviceShaderFloat16Int8Features
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderFloat16;
        public uint ShaderInt8;
    }
// Original Definition: 
// struct VkPhysicalDeviceShaderFloat16Int8FeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceShaderFloat16Int8FeaturesKhr
    {
    }
// Original Definition: 
// struct VkPhysicalDeviceFloat16Int8FeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceFloat16Int8FeaturesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceFloatControlsProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkShaderFloatControlsIndependence denormBehaviorIndependence;
//     VkShaderFloatControlsIndependence roundingModeIndependence;
//     VkBool32 shaderSignedZeroInfNanPreserveFloat16;
//     VkBool32 shaderSignedZeroInfNanPreserveFloat32;
//     VkBool32 shaderSignedZeroInfNanPreserveFloat64;
//     VkBool32 shaderDenormPreserveFloat16;
//     VkBool32 shaderDenormPreserveFloat32;
//     VkBool32 shaderDenormPreserveFloat64;
//     VkBool32 shaderDenormFlushToZeroFloat16;
//     VkBool32 shaderDenormFlushToZeroFloat32;
//     VkBool32 shaderDenormFlushToZeroFloat64;
//     VkBool32 shaderRoundingModeRTEFloat16;
//     VkBool32 shaderRoundingModeRTEFloat32;
//     VkBool32 shaderRoundingModeRTEFloat64;
//     VkBool32 shaderRoundingModeRTZFloat16;
//     VkBool32 shaderRoundingModeRTZFloat32;
//     VkBool32 shaderRoundingModeRTZFloat64;
// }
    public unsafe struct PhysicalDeviceFloatControlsProperties
    {
        public StructureType SType;
        public void* PNext;
        public ShaderFloatControlsIndependence DenormBehaviorIndependence;
        public ShaderFloatControlsIndependence RoundingModeIndependence;
        public uint ShaderSignedZeroInfNanPreserveFloat16;
        public uint ShaderSignedZeroInfNanPreserveFloat32;
        public uint ShaderSignedZeroInfNanPreserveFloat64;
        public uint ShaderDenormPreserveFloat16;
        public uint ShaderDenormPreserveFloat32;
        public uint ShaderDenormPreserveFloat64;
        public uint ShaderDenormFlushToZeroFloat16;
        public uint ShaderDenormFlushToZeroFloat32;
        public uint ShaderDenormFlushToZeroFloat64;
        public uint ShaderRoundingModeRTEFloat16;
        public uint ShaderRoundingModeRTEFloat32;
        public uint ShaderRoundingModeRTEFloat64;
        public uint ShaderRoundingModeRTZFloat16;
        public uint ShaderRoundingModeRTZFloat32;
        public uint ShaderRoundingModeRTZFloat64;
    }
// Original Definition: 
// struct VkPhysicalDeviceFloatControlsPropertiesKHR 
// {

// }
    public unsafe struct PhysicalDeviceFloatControlsPropertiesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceHostQueryResetFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 hostQueryReset;
// }
    public unsafe struct PhysicalDeviceHostQueryResetFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint HostQueryReset;
    }
// Original Definition: 
// struct VkPhysicalDeviceHostQueryResetFeaturesEXT 
// {

// }
    public unsafe struct PhysicalDeviceHostQueryResetFeaturesExt
    {
    }

// Original Definition: 
// struct VkNativeBufferUsage2ANDROID 
// {
//     uint64_t consumer;
//     uint64_t producer;
// }
    public unsafe struct NativeBufferUsage2android
    {
        public ulong Consumer;
        public ulong Producer;
    }

// Original Definition: 
// struct VkNativeBufferANDROID 
// {
//     VkStructureType sType;
//     void* pNext;
//     void* handle;
//     int stride;
//     int format;
//     int usage;
//     VkNativeBufferUsage2ANDROID usage2;
// }
    public unsafe struct NativeBufferAndroid
    {
        public StructureType SType;
        public void* PNext;
        public void* Handle;
        public IntPtr Stride;
        public IntPtr Format;
        public IntPtr Usage;
        public NativeBufferUsage2android Usage2;
    }

// Original Definition: 
// struct VkSwapchainImageCreateInfoANDROID 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSwapchainImageUsageFlagsANDROID usage;
// }
    public unsafe struct SwapchainImageCreateInfoAndroid
    {
        public StructureType SType;
        public void* PNext;
        public SwapchainImageUsageFlagsAndroid Usage;
    }

// Original Definition: 
// struct VkPhysicalDevicePresentationPropertiesANDROID 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 sharedImage;
// }
    public unsafe struct PhysicalDevicePresentationPropertiesAndroid
    {
        public StructureType SType;
        public void* PNext;
        public uint SharedImage;
    }

// Original Definition: 
// struct VkShaderResourceUsageAMD 
// {
//     uint32_t numUsedVgprs;
//     uint32_t numUsedSgprs;
//     uint32_t ldsSizePerLocalWorkGroup;
//     size_t ldsUsageSizeInBytes;
//     size_t scratchMemUsageInBytes;
// }
    public unsafe struct ShaderResourceUsageAmd
    {
        public uint NumUsedVgprs;
        public uint NumUsedSgprs;
        public uint LdsSizePerLocalWorkGroup;
        public IntPtr LdsUsageSizeInBytes;
        public IntPtr ScratchMemUsageInBytes;
    }

// Original Definition: 
// struct VkShaderStatisticsInfoAMD 
// {
//     VkShaderStageFlags shaderStageMask;
//     VkShaderResourceUsageAMD resourceUsage;
//     uint32_t numPhysicalVgprs;
//     uint32_t numPhysicalSgprs;
//     uint32_t numAvailableVgprs;
//     uint32_t numAvailableSgprs;
//     uint32_t computeWorkGroupSize;
// }
    public unsafe struct ShaderStatisticsInfoAmd
    {
        public ShaderStageFlags ShaderStageMask;
        public ShaderResourceUsageAmd ResourceUsage;
        public uint NumPhysicalVgprs;
        public uint NumPhysicalSgprs;
        public uint NumAvailableVgprs;
        public uint NumAvailableSgprs;
        public fixed uint ComputeWorkGroupSize[3];
    }

// Original Definition: 
// struct VkDeviceQueueGlobalPriorityCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkQueueGlobalPriorityEXT globalPriority;
// }
    public unsafe struct DeviceQueueGlobalPriorityCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public QueueGlobalPriorityExt GlobalPriority;
    }

// Original Definition: 
// struct VkDebugUtilsObjectNameInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkObjectType objectType;
//     uint64_t objectHandle;
//     char* pObjectName;
// }
    public unsafe struct DebugUtilsObjectNameInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ObjectType ObjectType;
        public ulong ObjectHandle;
        public sbyte* PObjectName;
    }

// Original Definition: 
// struct VkDebugUtilsObjectTagInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkObjectType objectType;
//     uint64_t objectHandle;
//     uint64_t tagName;
//     size_t tagSize;
//     void* pTag;
// }
    public unsafe struct DebugUtilsObjectTagInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ObjectType ObjectType;
        public ulong ObjectHandle;
        public ulong TagName;
        public IntPtr TagSize;
        public void* PTag;
    }

// Original Definition: 
// struct VkDebugUtilsLabelEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     char* pLabelName;
//     float color;
// }
    public unsafe struct DebugUtilsLabelExt
    {
        public StructureType SType;
        public void* PNext;
        public sbyte* PLabelName;
        public float Color;
    }

// Original Definition: 
// struct VkDebugUtilsMessengerCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDebugUtilsMessengerCreateFlagsEXT flags;
//     VkDebugUtilsMessageSeverityFlagsEXT messageSeverity;
//     VkDebugUtilsMessageTypeFlagsEXT messageType;
//     PFN_vkDebugUtilsMessengerCallbackEXT pfnUserCallback;
//     void* pUserData;
// }
    public unsafe struct DebugUtilsMessengerCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public DebugUtilsMessengerCreateFlagsExt Flags;
        public DebugUtilsMessageSeverityFlagsExt MessageSeverity;
        public DebugUtilsMessageTypeFlagsExt MessageType;
        public void* PfnUserCallback;
        public void* PUserData;
    }

// Original Definition: 
// struct VkDebugUtilsMessengerCallbackDataEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDebugUtilsMessengerCallbackDataFlagsEXT flags;
//     char* pMessageIdName;
//     int32_t messageIdNumber;
//     char* pMessage;
//     uint32_t queueLabelCount;
//     VkDebugUtilsLabelEXT* pQueueLabels;
//     uint32_t cmdBufLabelCount;
//     VkDebugUtilsLabelEXT* pCmdBufLabels;
//     uint32_t objectCount;
//     VkDebugUtilsObjectNameInfoEXT* pObjects;
// }
    public unsafe struct DebugUtilsMessengerCallbackDataExt
    {
        public StructureType SType;
        public void* PNext;
        public uint Flags;
        public sbyte* PMessageIdName;
        public int MessageIdNumber;
        public sbyte* PMessage;
        public uint QueueLabelCount;
        public DebugUtilsLabelExt* PQueueLabels;
        public uint CmdBufLabelCount;
        public DebugUtilsLabelExt* PCmdBufLabels;
        public uint ObjectCount;
        public DebugUtilsObjectNameInfoExt* PObjects;
    }

// Original Definition: 
// struct VkImportMemoryHostPointerInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExternalMemoryHandleTypeFlagBits handleType;
//     void* pHostPointer;
// }
    public unsafe struct ImportMemoryHostPointerInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ExternalMemoryHandleTypeFlags HandleType;
        public void* PHostPointer;
    }

// Original Definition: 
// struct VkMemoryHostPointerPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t memoryTypeBits;
// }
    public unsafe struct MemoryHostPointerPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint MemoryTypeBits;
    }

// Original Definition: 
// struct VkPhysicalDeviceExternalMemoryHostPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceSize minImportedHostPointerAlignment;
// }
    public unsafe struct PhysicalDeviceExternalMemoryHostPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public ulong MinImportedHostPointerAlignment;
    }

// Original Definition: 
// struct VkPhysicalDeviceConservativeRasterizationPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     float primitiveOverestimationSize;
//     float maxExtraPrimitiveOverestimationSize;
//     float extraPrimitiveOverestimationSizeGranularity;
//     VkBool32 primitiveUnderestimation;
//     VkBool32 conservativePointAndLineRasterization;
//     VkBool32 degenerateTrianglesRasterized;
//     VkBool32 degenerateLinesRasterized;
//     VkBool32 fullyCoveredFragmentShaderInputVariable;
//     VkBool32 conservativeRasterizationPostDepthCoverage;
// }
    public unsafe struct PhysicalDeviceConservativeRasterizationPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public float PrimitiveOverestimationSize;
        public float MaxExtraPrimitiveOverestimationSize;
        public float ExtraPrimitiveOverestimationSizeGranularity;
        public uint PrimitiveUnderestimation;
        public uint ConservativePointAndLineRasterization;
        public uint DegenerateTrianglesRasterized;
        public uint DegenerateLinesRasterized;
        public uint FullyCoveredFragmentShaderInputVariable;
        public uint ConservativeRasterizationPostDepthCoverage;
    }

// Original Definition: 
// struct VkCalibratedTimestampInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkTimeDomainEXT timeDomain;
// }
    public unsafe struct CalibratedTimestampInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public TimeDomainExt TimeDomain;
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderCorePropertiesAMD 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t shaderEngineCount;
//     uint32_t shaderArraysPerEngineCount;
//     uint32_t computeUnitsPerShaderArray;
//     uint32_t simdPerComputeUnit;
//     uint32_t wavefrontsPerSimd;
//     uint32_t wavefrontSize;
//     uint32_t sgprsPerSimd;
//     uint32_t minSgprAllocation;
//     uint32_t maxSgprAllocation;
//     uint32_t sgprAllocationGranularity;
//     uint32_t vgprsPerSimd;
//     uint32_t minVgprAllocation;
//     uint32_t maxVgprAllocation;
//     uint32_t vgprAllocationGranularity;
// }
    public unsafe struct PhysicalDeviceShaderCorePropertiesAmd
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderEngineCount;
        public uint ShaderArraysPerEngineCount;
        public uint ComputeUnitsPerShaderArray;
        public uint SimdPerComputeUnit;
        public uint WavefrontsPerSimd;
        public uint WavefrontSize;
        public uint SgprsPerSimd;
        public uint MinSgprAllocation;
        public uint MaxSgprAllocation;
        public uint SgprAllocationGranularity;
        public uint VgprsPerSimd;
        public uint MinVgprAllocation;
        public uint MaxVgprAllocation;
        public uint VgprAllocationGranularity;
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderCoreProperties2AMD 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkShaderCorePropertiesFlagsAMD shaderCoreFeatures;
//     uint32_t activeComputeUnitCount;
// }
    public unsafe struct PhysicalDeviceShaderCoreProperties2amd
    {
        public StructureType SType;
        public void* PNext;
        public ShaderCorePropertiesFlagsAmd ShaderCoreFeatures;
        public uint ActiveComputeUnitCount;
    }

// Original Definition: 
// struct VkPipelineRasterizationConservativeStateCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineRasterizationConservativeStateCreateFlagsEXT flags;
//     VkConservativeRasterizationModeEXT conservativeRasterizationMode;
//     float extraPrimitiveOverestimationSize;
// }
    public unsafe struct PipelineRasterizationConservativeStateCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public PipelineRasterizationConservativeStateCreateFlagsExt Flags;
        public ConservativeRasterizationModeExt ConservativeRasterizationMode;
        public float ExtraPrimitiveOverestimationSize;
    }

// Original Definition: 
// struct VkPhysicalDeviceDescriptorIndexingFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderInputAttachmentArrayDynamicIndexing;
//     VkBool32 shaderUniformTexelBufferArrayDynamicIndexing;
//     VkBool32 shaderStorageTexelBufferArrayDynamicIndexing;
//     VkBool32 shaderUniformBufferArrayNonUniformIndexing;
//     VkBool32 shaderSampledImageArrayNonUniformIndexing;
//     VkBool32 shaderStorageBufferArrayNonUniformIndexing;
//     VkBool32 shaderStorageImageArrayNonUniformIndexing;
//     VkBool32 shaderInputAttachmentArrayNonUniformIndexing;
//     VkBool32 shaderUniformTexelBufferArrayNonUniformIndexing;
//     VkBool32 shaderStorageTexelBufferArrayNonUniformIndexing;
//     VkBool32 descriptorBindingUniformBufferUpdateAfterBind;
//     VkBool32 descriptorBindingSampledImageUpdateAfterBind;
//     VkBool32 descriptorBindingStorageImageUpdateAfterBind;
//     VkBool32 descriptorBindingStorageBufferUpdateAfterBind;
//     VkBool32 descriptorBindingUniformTexelBufferUpdateAfterBind;
//     VkBool32 descriptorBindingStorageTexelBufferUpdateAfterBind;
//     VkBool32 descriptorBindingUpdateUnusedWhilePending;
//     VkBool32 descriptorBindingPartiallyBound;
//     VkBool32 descriptorBindingVariableDescriptorCount;
//     VkBool32 runtimeDescriptorArray;
// }
    public unsafe struct PhysicalDeviceDescriptorIndexingFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderInputAttachmentArrayDynamicIndexing;
        public uint ShaderUniformTexelBufferArrayDynamicIndexing;
        public uint ShaderStorageTexelBufferArrayDynamicIndexing;
        public uint ShaderUniformBufferArrayNonUniformIndexing;
        public uint ShaderSampledImageArrayNonUniformIndexing;
        public uint ShaderStorageBufferArrayNonUniformIndexing;
        public uint ShaderStorageImageArrayNonUniformIndexing;
        public uint ShaderInputAttachmentArrayNonUniformIndexing;
        public uint ShaderUniformTexelBufferArrayNonUniformIndexing;
        public uint ShaderStorageTexelBufferArrayNonUniformIndexing;
        public uint DescriptorBindingUniformBufferUpdateAfterBind;
        public uint DescriptorBindingSampledImageUpdateAfterBind;
        public uint DescriptorBindingStorageImageUpdateAfterBind;
        public uint DescriptorBindingStorageBufferUpdateAfterBind;
        public uint DescriptorBindingUniformTexelBufferUpdateAfterBind;
        public uint DescriptorBindingStorageTexelBufferUpdateAfterBind;
        public uint DescriptorBindingUpdateUnusedWhilePending;
        public uint DescriptorBindingPartiallyBound;
        public uint DescriptorBindingVariableDescriptorCount;
        public uint RuntimeDescriptorArray;
    }
// Original Definition: 
// struct VkPhysicalDeviceDescriptorIndexingFeaturesEXT 
// {

// }
    public unsafe struct PhysicalDeviceDescriptorIndexingFeaturesExt
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceDescriptorIndexingProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxUpdateAfterBindDescriptorsInAllPools;
//     VkBool32 shaderUniformBufferArrayNonUniformIndexingNative;
//     VkBool32 shaderSampledImageArrayNonUniformIndexingNative;
//     VkBool32 shaderStorageBufferArrayNonUniformIndexingNative;
//     VkBool32 shaderStorageImageArrayNonUniformIndexingNative;
//     VkBool32 shaderInputAttachmentArrayNonUniformIndexingNative;
//     VkBool32 robustBufferAccessUpdateAfterBind;
//     VkBool32 quadDivergentImplicitLod;
//     uint32_t maxPerStageDescriptorUpdateAfterBindSamplers;
//     uint32_t maxPerStageDescriptorUpdateAfterBindUniformBuffers;
//     uint32_t maxPerStageDescriptorUpdateAfterBindStorageBuffers;
//     uint32_t maxPerStageDescriptorUpdateAfterBindSampledImages;
//     uint32_t maxPerStageDescriptorUpdateAfterBindStorageImages;
//     uint32_t maxPerStageDescriptorUpdateAfterBindInputAttachments;
//     uint32_t maxPerStageUpdateAfterBindResources;
//     uint32_t maxDescriptorSetUpdateAfterBindSamplers;
//     uint32_t maxDescriptorSetUpdateAfterBindUniformBuffers;
//     uint32_t maxDescriptorSetUpdateAfterBindUniformBuffersDynamic;
//     uint32_t maxDescriptorSetUpdateAfterBindStorageBuffers;
//     uint32_t maxDescriptorSetUpdateAfterBindStorageBuffersDynamic;
//     uint32_t maxDescriptorSetUpdateAfterBindSampledImages;
//     uint32_t maxDescriptorSetUpdateAfterBindStorageImages;
//     uint32_t maxDescriptorSetUpdateAfterBindInputAttachments;
// }
    public unsafe struct PhysicalDeviceDescriptorIndexingProperties
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxUpdateAfterBindDescriptorsInAllPools;
        public uint ShaderUniformBufferArrayNonUniformIndexingNative;
        public uint ShaderSampledImageArrayNonUniformIndexingNative;
        public uint ShaderStorageBufferArrayNonUniformIndexingNative;
        public uint ShaderStorageImageArrayNonUniformIndexingNative;
        public uint ShaderInputAttachmentArrayNonUniformIndexingNative;
        public uint RobustBufferAccessUpdateAfterBind;
        public uint QuadDivergentImplicitLod;
        public uint MaxPerStageDescriptorUpdateAfterBindSamplers;
        public uint MaxPerStageDescriptorUpdateAfterBindUniformBuffers;
        public uint MaxPerStageDescriptorUpdateAfterBindStorageBuffers;
        public uint MaxPerStageDescriptorUpdateAfterBindSampledImages;
        public uint MaxPerStageDescriptorUpdateAfterBindStorageImages;
        public uint MaxPerStageDescriptorUpdateAfterBindInputAttachments;
        public uint MaxPerStageUpdateAfterBindResources;
        public uint MaxDescriptorSetUpdateAfterBindSamplers;
        public uint MaxDescriptorSetUpdateAfterBindUniformBuffers;
        public uint MaxDescriptorSetUpdateAfterBindUniformBuffersDynamic;
        public uint MaxDescriptorSetUpdateAfterBindStorageBuffers;
        public uint MaxDescriptorSetUpdateAfterBindStorageBuffersDynamic;
        public uint MaxDescriptorSetUpdateAfterBindSampledImages;
        public uint MaxDescriptorSetUpdateAfterBindStorageImages;
        public uint MaxDescriptorSetUpdateAfterBindInputAttachments;
    }
// Original Definition: 
// struct VkPhysicalDeviceDescriptorIndexingPropertiesEXT 
// {

// }
    public unsafe struct PhysicalDeviceDescriptorIndexingPropertiesExt
    {
    }

// Original Definition: 
// struct VkDescriptorSetLayoutBindingFlagsCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t bindingCount;
//     VkDescriptorBindingFlags* pBindingFlags;
// }
    public unsafe struct DescriptorSetLayoutBindingFlagsCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint BindingCount;
        public DescriptorBindingFlags* PBindingFlags;
    }
// Original Definition: 
// struct VkDescriptorSetLayoutBindingFlagsCreateInfoEXT 
// {

// }
    public unsafe struct DescriptorSetLayoutBindingFlagsCreateInfoExt
    {
    }

// Original Definition: 
// struct VkDescriptorSetVariableDescriptorCountAllocateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t descriptorSetCount;
//     uint32_t* pDescriptorCounts;
// }
    public unsafe struct DescriptorSetVariableDescriptorCountAllocateInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint DescriptorSetCount;
        public uint* PDescriptorCounts;
    }
// Original Definition: 
// struct VkDescriptorSetVariableDescriptorCountAllocateInfoEXT 
// {

// }
    public unsafe struct DescriptorSetVariableDescriptorCountAllocateInfoExt
    {
    }

// Original Definition: 
// struct VkDescriptorSetVariableDescriptorCountLayoutSupport 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxVariableDescriptorCount;
// }
    public unsafe struct DescriptorSetVariableDescriptorCountLayoutSupport
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxVariableDescriptorCount;
    }
// Original Definition: 
// struct VkDescriptorSetVariableDescriptorCountLayoutSupportEXT 
// {

// }
    public unsafe struct DescriptorSetVariableDescriptorCountLayoutSupportExt
    {
    }

// Original Definition: 
// struct VkAttachmentDescription2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAttachmentDescriptionFlags flags;
//     VkFormat format;
//     VkSampleCountFlagBits samples;
//     VkAttachmentLoadOp loadOp;
//     VkAttachmentStoreOp storeOp;
//     VkAttachmentLoadOp stencilLoadOp;
//     VkAttachmentStoreOp stencilStoreOp;
//     VkImageLayout initialLayout;
//     VkImageLayout finalLayout;
// }
    public unsafe struct AttachmentDescription2
    {
        public StructureType SType;
        public void* PNext;
        public AttachmentDescriptionFlags Flags;
        public Format Format;
        public SampleCountFlags Samples;
        public AttachmentLoadOp LoadOp;
        public AttachmentStoreOp StoreOp;
        public AttachmentLoadOp StencilLoadOp;
        public AttachmentStoreOp StencilStoreOp;
        public ImageLayout InitialLayout;
        public ImageLayout FinalLayout;
    }
// Original Definition: 
// struct VkAttachmentDescription2KHR 
// {

// }
    public unsafe struct AttachmentDescription2khr
    {
    }

// Original Definition: 
// struct VkAttachmentReference2 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t attachment;
//     VkImageLayout layout;
//     VkImageAspectFlags aspectMask;
// }
    public unsafe struct AttachmentReference2
    {
        public StructureType SType;
        public void* PNext;
        public uint Attachment;
        public ImageLayout Layout;
        public ImageAspectFlags AspectMask;
    }
// Original Definition: 
// struct VkAttachmentReference2KHR 
// {

// }
    public unsafe struct AttachmentReference2khr
    {
    }

// Original Definition: 
// struct VkSubpassDescription2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSubpassDescriptionFlags flags;
//     VkPipelineBindPoint pipelineBindPoint;
//     uint32_t viewMask;
//     uint32_t inputAttachmentCount;
//     VkAttachmentReference2* pInputAttachments;
//     uint32_t colorAttachmentCount;
//     VkAttachmentReference2* pColorAttachments;
//     VkAttachmentReference2* pResolveAttachments;
//     VkAttachmentReference2* pDepthStencilAttachment;
//     uint32_t preserveAttachmentCount;
//     uint32_t* pPreserveAttachments;
// }
    public unsafe struct SubpassDescription2
    {
        public StructureType SType;
        public void* PNext;
        public SubpassDescriptionFlags Flags;
        public PipelineBindPoint PipelineBindPoint;
        public uint ViewMask;
        public uint InputAttachmentCount;
        public AttachmentReference2* PInputAttachments;
        public uint ColorAttachmentCount;
        public AttachmentReference2* PColorAttachments;
        public AttachmentReference2* PResolveAttachments;
        public AttachmentReference2* PDepthStencilAttachment;
        public uint PreserveAttachmentCount;
        public uint* PPreserveAttachments;
    }
// Original Definition: 
// struct VkSubpassDescription2KHR 
// {

// }
    public unsafe struct SubpassDescription2khr
    {
    }

// Original Definition: 
// struct VkSubpassDependency2 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t srcSubpass;
//     uint32_t dstSubpass;
//     VkPipelineStageFlags srcStageMask;
//     VkPipelineStageFlags dstStageMask;
//     VkAccessFlags srcAccessMask;
//     VkAccessFlags dstAccessMask;
//     VkDependencyFlags dependencyFlags;
//     int32_t viewOffset;
// }
    public unsafe struct SubpassDependency2
    {
        public StructureType SType;
        public void* PNext;
        public uint SrcSubpass;
        public uint DstSubpass;
        public PipelineStageFlags SrcStageMask;
        public PipelineStageFlags DstStageMask;
        public AccessFlags SrcAccessMask;
        public AccessFlags DstAccessMask;
        public DependencyFlags DependencyFlags;
        public int ViewOffset;
    }
// Original Definition: 
// struct VkSubpassDependency2KHR 
// {

// }
    public unsafe struct SubpassDependency2khr
    {
    }

// Original Definition: 
// struct VkRenderPassCreateInfo2 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkRenderPassCreateFlags flags;
//     uint32_t attachmentCount;
//     VkAttachmentDescription2* pAttachments;
//     uint32_t subpassCount;
//     VkSubpassDescription2* pSubpasses;
//     uint32_t dependencyCount;
//     VkSubpassDependency2* pDependencies;
//     uint32_t correlatedViewMaskCount;
//     uint32_t* pCorrelatedViewMasks;
// }
    public unsafe struct RenderPassCreateInfo2
    {
        public StructureType SType;
        public void* PNext;
        public RenderPassCreateFlags Flags;
        public uint AttachmentCount;
        public AttachmentDescription2* PAttachments;
        public uint SubpassCount;
        public SubpassDescription2* PSubpasses;
        public uint DependencyCount;
        public SubpassDependency2* PDependencies;
        public uint CorrelatedViewMaskCount;
        public uint* PCorrelatedViewMasks;
    }
// Original Definition: 
// struct VkRenderPassCreateInfo2KHR 
// {

// }
    public unsafe struct RenderPassCreateInfo2khr
    {
    }

// Original Definition: 
// struct VkSubpassBeginInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSubpassContents contents;
// }
    public unsafe struct SubpassBeginInfo
    {
        public StructureType SType;
        public void* PNext;
        public SubpassContents Contents;
    }
// Original Definition: 
// struct VkSubpassBeginInfoKHR 
// {

// }
    public unsafe struct SubpassBeginInfoKhr
    {
    }

// Original Definition: 
// struct VkSubpassEndInfo 
// {
//     VkStructureType sType;
//     void* pNext;
// }
    public unsafe struct SubpassEndInfo
    {
        public StructureType SType;
        public void* PNext;
    }
// Original Definition: 
// struct VkSubpassEndInfoKHR 
// {

// }
    public unsafe struct SubpassEndInfoKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceTimelineSemaphoreFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 timelineSemaphore;
// }
    public unsafe struct PhysicalDeviceTimelineSemaphoreFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint TimelineSemaphore;
    }
// Original Definition: 
// struct VkPhysicalDeviceTimelineSemaphoreFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceTimelineSemaphoreFeaturesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceTimelineSemaphoreProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint64_t maxTimelineSemaphoreValueDifference;
// }
    public unsafe struct PhysicalDeviceTimelineSemaphoreProperties
    {
        public StructureType SType;
        public void* PNext;
        public ulong MaxTimelineSemaphoreValueDifference;
    }
// Original Definition: 
// struct VkPhysicalDeviceTimelineSemaphorePropertiesKHR 
// {

// }
    public unsafe struct PhysicalDeviceTimelineSemaphorePropertiesKhr
    {
    }

// Original Definition: 
// struct VkSemaphoreTypeCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSemaphoreType semaphoreType;
//     uint64_t initialValue;
// }
    public unsafe struct SemaphoreTypeCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public SemaphoreType SemaphoreType;
        public ulong InitialValue;
    }
// Original Definition: 
// struct VkSemaphoreTypeCreateInfoKHR 
// {

// }
    public unsafe struct SemaphoreTypeCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkTimelineSemaphoreSubmitInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t waitSemaphoreValueCount;
//     uint64_t* pWaitSemaphoreValues;
//     uint32_t signalSemaphoreValueCount;
//     uint64_t* pSignalSemaphoreValues;
// }
    public unsafe struct TimelineSemaphoreSubmitInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint WaitSemaphoreValueCount;
        public ulong* PWaitSemaphoreValues;
        public uint SignalSemaphoreValueCount;
        public ulong* PSignalSemaphoreValues;
    }
// Original Definition: 
// struct VkTimelineSemaphoreSubmitInfoKHR 
// {

// }
    public unsafe struct TimelineSemaphoreSubmitInfoKhr
    {
    }

// Original Definition: 
// struct VkSemaphoreWaitInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSemaphoreWaitFlags flags;
//     uint32_t semaphoreCount;
//     VkSemaphore* pSemaphores;
//     uint64_t* pValues;
// }
    public unsafe struct SemaphoreWaitInfo
    {
        public StructureType SType;
        public void* PNext;
        public SemaphoreWaitFlags Flags;
        public uint SemaphoreCount;
        public Semaphore* PSemaphores;
        public ulong* PValues;
    }
// Original Definition: 
// struct VkSemaphoreWaitInfoKHR 
// {

// }
    public unsafe struct SemaphoreWaitInfoKhr
    {
    }

// Original Definition: 
// struct VkSemaphoreSignalInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSemaphore semaphore;
//     uint64_t value;
// }
    public unsafe struct SemaphoreSignalInfo
    {
        public StructureType SType;
        public void* PNext;
        public Semaphore Semaphore;
        public ulong Value;
    }
// Original Definition: 
// struct VkSemaphoreSignalInfoKHR 
// {

// }
    public unsafe struct SemaphoreSignalInfoKhr
    {
    }

// Original Definition: 
// struct VkVertexInputBindingDivisorDescriptionEXT 
// {
//     uint32_t binding;
//     uint32_t divisor;
// }
    public unsafe struct VertexInputBindingDivisorDescriptionExt
    {
        public uint Binding;
        public uint Divisor;
    }

// Original Definition: 
// struct VkPipelineVertexInputDivisorStateCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t vertexBindingDivisorCount;
//     VkVertexInputBindingDivisorDescriptionEXT* pVertexBindingDivisors;
// }
    public unsafe struct PipelineVertexInputDivisorStateCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public uint VertexBindingDivisorCount;
        public VertexInputBindingDivisorDescriptionExt* PVertexBindingDivisors;
    }

// Original Definition: 
// struct VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxVertexAttribDivisor;
// }
    public unsafe struct PhysicalDeviceVertexAttributeDivisorPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxVertexAttribDivisor;
    }

// Original Definition: 
// struct VkPhysicalDevicePCIBusInfoPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t pciDomain;
//     uint32_t pciBus;
//     uint32_t pciDevice;
//     uint32_t pciFunction;
// }
    public unsafe struct PhysicalDevicePCIBusInfoPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint PciDomain;
        public uint PciBus;
        public uint PciDevice;
        public uint PciFunction;
    }

// Original Definition: 
// struct VkImportAndroidHardwareBufferInfoANDROID 
// {
//     VkStructureType sType;
//     void* pNext;
//     AHardwareBuffer* buffer;
// }
    public unsafe struct ImportAndroidHardwareBufferInfoAndroid
    {
        public StructureType SType;
        public void* PNext;
        public IntPtr Buffer;
    }

// Original Definition: 
// struct VkAndroidHardwareBufferUsageANDROID 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint64_t androidHardwareBufferUsage;
// }
    public unsafe struct AndroidHardwareBufferUsageAndroid
    {
        public StructureType SType;
        public void* PNext;
        public ulong AndroidHardwareBufferUsage;
    }

// Original Definition: 
// struct VkAndroidHardwareBufferPropertiesANDROID 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceSize allocationSize;
//     uint32_t memoryTypeBits;
// }
    public unsafe struct AndroidHardwareBufferPropertiesAndroid
    {
        public StructureType SType;
        public void* PNext;
        public ulong AllocationSize;
        public uint MemoryTypeBits;
    }

// Original Definition: 
// struct VkMemoryGetAndroidHardwareBufferInfoANDROID 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceMemory memory;
// }
    public unsafe struct MemoryGetAndroidHardwareBufferInfoAndroid
    {
        public StructureType SType;
        public void* PNext;
        public DeviceMemory Memory;
    }

// Original Definition: 
// struct VkAndroidHardwareBufferFormatPropertiesANDROID 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFormat format;
//     uint64_t externalFormat;
//     VkFormatFeatureFlags formatFeatures;
//     VkComponentMapping samplerYcbcrConversionComponents;
//     VkSamplerYcbcrModelConversion suggestedYcbcrModel;
//     VkSamplerYcbcrRange suggestedYcbcrRange;
//     VkChromaLocation suggestedXChromaOffset;
//     VkChromaLocation suggestedYChromaOffset;
// }
    public unsafe struct AndroidHardwareBufferFormatPropertiesAndroid
    {
        public StructureType SType;
        public void* PNext;
        public Format Format;
        public ulong ExternalFormat;
        public FormatFeatureFlags FormatFeatures;
        public ComponentMapping SamplerYcbcrConversionComponents;
        public SamplerYcbcrModelConversion SuggestedYcbcrModel;
        public SamplerYcbcrRange SuggestedYcbcrRange;
        public ChromaLocation SuggestedXChromaOffset;
        public ChromaLocation SuggestedYChromaOffset;
    }

// Original Definition: 
// struct VkCommandBufferInheritanceConditionalRenderingInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 conditionalRenderingEnable;
// }
    public unsafe struct CommandBufferInheritanceConditionalRenderingInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public uint ConditionalRenderingEnable;
    }

// Original Definition: 
// struct VkExternalFormatANDROID 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint64_t externalFormat;
// }
    public unsafe struct ExternalFormatAndroid
    {
        public StructureType SType;
        public void* PNext;
        public ulong ExternalFormat;
    }

// Original Definition: 
// struct VkPhysicalDevice8BitStorageFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 storageBuffer8BitAccess;
//     VkBool32 uniformAndStorageBuffer8BitAccess;
//     VkBool32 storagePushConstant8;
// }
    public unsafe struct PhysicalDevice8BitStorageFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint StorageBuffer8BitAccess;
        public uint UniformAndStorageBuffer8BitAccess;
        public uint StoragePushConstant8;
    }
// Original Definition: 
// struct VkPhysicalDevice8BitStorageFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDevice8BitStorageFeaturesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceConditionalRenderingFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 conditionalRendering;
//     VkBool32 inheritedConditionalRendering;
// }
    public unsafe struct PhysicalDeviceConditionalRenderingFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint ConditionalRendering;
        public uint InheritedConditionalRendering;
    }

// Original Definition: 
// struct VkPhysicalDeviceVulkanMemoryModelFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 vulkanMemoryModel;
//     VkBool32 vulkanMemoryModelDeviceScope;
//     VkBool32 vulkanMemoryModelAvailabilityVisibilityChains;
// }
    public unsafe struct PhysicalDeviceVulkanMemoryModelFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint VulkanMemoryModel;
        public uint VulkanMemoryModelDeviceScope;
        public uint VulkanMemoryModelAvailabilityVisibilityChains;
    }
// Original Definition: 
// struct VkPhysicalDeviceVulkanMemoryModelFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceVulkanMemoryModelFeaturesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderAtomicInt64Features 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderBufferInt64Atomics;
//     VkBool32 shaderSharedInt64Atomics;
// }
    public unsafe struct PhysicalDeviceShaderAtomicInt64Features
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderBufferInt64Atomics;
        public uint ShaderSharedInt64Atomics;
    }
// Original Definition: 
// struct VkPhysicalDeviceShaderAtomicInt64FeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceShaderAtomicInt64FeaturesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderAtomicFloatFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderBufferFloat32Atomics;
//     VkBool32 shaderBufferFloat32AtomicAdd;
//     VkBool32 shaderBufferFloat64Atomics;
//     VkBool32 shaderBufferFloat64AtomicAdd;
//     VkBool32 shaderSharedFloat32Atomics;
//     VkBool32 shaderSharedFloat32AtomicAdd;
//     VkBool32 shaderSharedFloat64Atomics;
//     VkBool32 shaderSharedFloat64AtomicAdd;
//     VkBool32 shaderImageFloat32Atomics;
//     VkBool32 shaderImageFloat32AtomicAdd;
//     VkBool32 sparseImageFloat32Atomics;
//     VkBool32 sparseImageFloat32AtomicAdd;
// }
    public unsafe struct PhysicalDeviceShaderAtomicFloatFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderBufferFloat32Atomics;
        public uint ShaderBufferFloat32AtomicAdd;
        public uint ShaderBufferFloat64Atomics;
        public uint ShaderBufferFloat64AtomicAdd;
        public uint ShaderSharedFloat32Atomics;
        public uint ShaderSharedFloat32AtomicAdd;
        public uint ShaderSharedFloat64Atomics;
        public uint ShaderSharedFloat64AtomicAdd;
        public uint ShaderImageFloat32Atomics;
        public uint ShaderImageFloat32AtomicAdd;
        public uint SparseImageFloat32Atomics;
        public uint SparseImageFloat32AtomicAdd;
    }

// Original Definition: 
// struct VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 vertexAttributeInstanceRateDivisor;
//     VkBool32 vertexAttributeInstanceRateZeroDivisor;
// }
    public unsafe struct PhysicalDeviceVertexAttributeDivisorFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint VertexAttributeInstanceRateDivisor;
        public uint VertexAttributeInstanceRateZeroDivisor;
    }

// Original Definition: 
// struct VkQueueFamilyCheckpointPropertiesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineStageFlags checkpointExecutionStageMask;
// }
    public unsafe struct QueueFamilyCheckpointPropertiesNv
    {
        public StructureType SType;
        public void* PNext;
        public PipelineStageFlags CheckpointExecutionStageMask;
    }

// Original Definition: 
// struct VkCheckpointDataNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineStageFlagBits stage;
//     void* pCheckpointMarker;
// }
    public unsafe struct CheckpointDataNv
    {
        public StructureType SType;
        public void* PNext;
        public PipelineStageFlags Stage;
        public void* PCheckpointMarker;
    }

// Original Definition: 
// struct VkPhysicalDeviceDepthStencilResolveProperties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkResolveModeFlags supportedDepthResolveModes;
//     VkResolveModeFlags supportedStencilResolveModes;
//     VkBool32 independentResolveNone;
//     VkBool32 independentResolve;
// }
    public unsafe struct PhysicalDeviceDepthStencilResolveProperties
    {
        public StructureType SType;
        public void* PNext;
        public ResolveModeFlags SupportedDepthResolveModes;
        public ResolveModeFlags SupportedStencilResolveModes;
        public uint IndependentResolveNone;
        public uint IndependentResolve;
    }
// Original Definition: 
// struct VkPhysicalDeviceDepthStencilResolvePropertiesKHR 
// {

// }
    public unsafe struct PhysicalDeviceDepthStencilResolvePropertiesKhr
    {
    }

// Original Definition: 
// struct VkSubpassDescriptionDepthStencilResolve 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkResolveModeFlagBits depthResolveMode;
//     VkResolveModeFlagBits stencilResolveMode;
//     VkAttachmentReference2* pDepthStencilResolveAttachment;
// }
    public unsafe struct SubpassDescriptionDepthStencilResolve
    {
        public StructureType SType;
        public void* PNext;
        public ResolveModeFlags DepthResolveMode;
        public ResolveModeFlags StencilResolveMode;
        public AttachmentReference2* PDepthStencilResolveAttachment;
    }
// Original Definition: 
// struct VkSubpassDescriptionDepthStencilResolveKHR 
// {

// }
    public unsafe struct SubpassDescriptionDepthStencilResolveKhr
    {
    }

// Original Definition: 
// struct VkImageViewASTCDecodeModeEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFormat decodeMode;
// }
    public unsafe struct ImageViewASTCDecodeModeExt
    {
        public StructureType SType;
        public void* PNext;
        public Format DecodeMode;
    }

// Original Definition: 
// struct VkPhysicalDeviceASTCDecodeFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 decodeModeSharedExponent;
// }
    public unsafe struct PhysicalDeviceASTCDecodeFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint DecodeModeSharedExponent;
    }

// Original Definition: 
// struct VkPhysicalDeviceTransformFeedbackFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 transformFeedback;
//     VkBool32 geometryStreams;
// }
    public unsafe struct PhysicalDeviceTransformFeedbackFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint TransformFeedback;
        public uint GeometryStreams;
    }

// Original Definition: 
// struct VkPhysicalDeviceTransformFeedbackPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxTransformFeedbackStreams;
//     uint32_t maxTransformFeedbackBuffers;
//     VkDeviceSize maxTransformFeedbackBufferSize;
//     uint32_t maxTransformFeedbackStreamDataSize;
//     uint32_t maxTransformFeedbackBufferDataSize;
//     uint32_t maxTransformFeedbackBufferDataStride;
//     VkBool32 transformFeedbackQueries;
//     VkBool32 transformFeedbackStreamsLinesTriangles;
//     VkBool32 transformFeedbackRasterizationStreamSelect;
//     VkBool32 transformFeedbackDraw;
// }
    public unsafe struct PhysicalDeviceTransformFeedbackPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxTransformFeedbackStreams;
        public uint MaxTransformFeedbackBuffers;
        public ulong MaxTransformFeedbackBufferSize;
        public uint MaxTransformFeedbackStreamDataSize;
        public uint MaxTransformFeedbackBufferDataSize;
        public uint MaxTransformFeedbackBufferDataStride;
        public uint TransformFeedbackQueries;
        public uint TransformFeedbackStreamsLinesTriangles;
        public uint TransformFeedbackRasterizationStreamSelect;
        public uint TransformFeedbackDraw;
    }

// Original Definition: 
// struct VkPipelineRasterizationStateStreamCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineRasterizationStateStreamCreateFlagsEXT flags;
//     uint32_t rasterizationStream;
// }
    public unsafe struct PipelineRasterizationStateStreamCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public PipelineRasterizationStateStreamCreateFlagsExt Flags;
        public uint RasterizationStream;
    }

// Original Definition: 
// struct VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 representativeFragmentTest;
// }
    public unsafe struct PhysicalDeviceRepresentativeFragmentTestFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint RepresentativeFragmentTest;
    }

// Original Definition: 
// struct VkPipelineRepresentativeFragmentTestStateCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 representativeFragmentTestEnable;
// }
    public unsafe struct PipelineRepresentativeFragmentTestStateCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public uint RepresentativeFragmentTestEnable;
    }

// Original Definition: 
// struct VkPhysicalDeviceExclusiveScissorFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 exclusiveScissor;
// }
    public unsafe struct PhysicalDeviceExclusiveScissorFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ExclusiveScissor;
    }

// Original Definition: 
// struct VkPipelineViewportExclusiveScissorStateCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t exclusiveScissorCount;
//     VkRect2D* pExclusiveScissors;
// }
    public unsafe struct PipelineViewportExclusiveScissorStateCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ExclusiveScissorCount;
        public Rect2d* PExclusiveScissors;
    }

// Original Definition: 
// struct VkPhysicalDeviceCornerSampledImageFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 cornerSampledImage;
// }
    public unsafe struct PhysicalDeviceCornerSampledImageFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint CornerSampledImage;
    }

// Original Definition: 
// struct VkPhysicalDeviceComputeShaderDerivativesFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 computeDerivativeGroupQuads;
//     VkBool32 computeDerivativeGroupLinear;
// }
    public unsafe struct PhysicalDeviceComputeShaderDerivativesFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ComputeDerivativeGroupQuads;
        public uint ComputeDerivativeGroupLinear;
    }

// Original Definition: 
// struct VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 fragmentShaderBarycentric;
// }
    public unsafe struct PhysicalDeviceFragmentShaderBarycentricFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint FragmentShaderBarycentric;
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderImageFootprintFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 imageFootprint;
// }
    public unsafe struct PhysicalDeviceShaderImageFootprintFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ImageFootprint;
    }

// Original Definition: 
// struct VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 dedicatedAllocationImageAliasing;
// }
    public unsafe struct PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint DedicatedAllocationImageAliasing;
    }

// Original Definition: 
// struct VkShadingRatePaletteNV 
// {
//     uint32_t shadingRatePaletteEntryCount;
//     VkShadingRatePaletteEntryNV* pShadingRatePaletteEntries;
// }
    public unsafe struct ShadingRatePaletteNv
    {
        public uint ShadingRatePaletteEntryCount;
        public ShadingRatePaletteEntryNv* PShadingRatePaletteEntries;
    }

// Original Definition: 
// struct VkPipelineViewportShadingRateImageStateCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shadingRateImageEnable;
//     uint32_t viewportCount;
//     VkShadingRatePaletteNV* pShadingRatePalettes;
// }
    public unsafe struct PipelineViewportShadingRateImageStateCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ShadingRateImageEnable;
        public uint ViewportCount;
        public ShadingRatePaletteNv* PShadingRatePalettes;
    }

// Original Definition: 
// struct VkPhysicalDeviceShadingRateImageFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shadingRateImage;
//     VkBool32 shadingRateCoarseSampleOrder;
// }
    public unsafe struct PhysicalDeviceShadingRateImageFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ShadingRateImage;
        public uint ShadingRateCoarseSampleOrder;
    }

// Original Definition: 
// struct VkPhysicalDeviceShadingRateImagePropertiesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExtent2D shadingRateTexelSize;
//     uint32_t shadingRatePaletteSize;
//     uint32_t shadingRateMaxCoarseSamples;
// }
    public unsafe struct PhysicalDeviceShadingRateImagePropertiesNv
    {
        public StructureType SType;
        public void* PNext;
        public Extent2d ShadingRateTexelSize;
        public uint ShadingRatePaletteSize;
        public uint ShadingRateMaxCoarseSamples;
    }

// Original Definition: 
// struct VkCoarseSampleLocationNV 
// {
//     uint32_t pixelX;
//     uint32_t pixelY;
//     uint32_t sample;
// }
    public unsafe struct CoarseSampleLocationNv
    {
        public uint PixelX;
        public uint PixelY;
        public uint Sample;
    }

// Original Definition: 
// struct VkCoarseSampleOrderCustomNV 
// {
//     VkShadingRatePaletteEntryNV shadingRate;
//     uint32_t sampleCount;
//     uint32_t sampleLocationCount;
//     VkCoarseSampleLocationNV* pSampleLocations;
// }
    public unsafe struct CoarseSampleOrderCustomNv
    {
        public ShadingRatePaletteEntryNv ShadingRate;
        public uint SampleCount;
        public uint SampleLocationCount;
        public CoarseSampleLocationNv* PSampleLocations;
    }

// Original Definition: 
// struct VkPipelineViewportCoarseSampleOrderStateCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkCoarseSampleOrderTypeNV sampleOrderType;
//     uint32_t customSampleOrderCount;
//     VkCoarseSampleOrderCustomNV* pCustomSampleOrders;
// }
    public unsafe struct PipelineViewportCoarseSampleOrderStateCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public CoarseSampleOrderTypeNv SampleOrderType;
        public uint CustomSampleOrderCount;
        public CoarseSampleOrderCustomNv* PCustomSampleOrders;
    }

// Original Definition: 
// struct VkPhysicalDeviceMeshShaderFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 taskShader;
//     VkBool32 meshShader;
// }
    public unsafe struct PhysicalDeviceMeshShaderFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint TaskShader;
        public uint MeshShader;
    }

// Original Definition: 
// struct VkPhysicalDeviceMeshShaderPropertiesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxDrawMeshTasksCount;
//     uint32_t maxTaskWorkGroupInvocations;
//     uint32_t maxTaskWorkGroupSize;
//     uint32_t maxTaskTotalMemorySize;
//     uint32_t maxTaskOutputCount;
//     uint32_t maxMeshWorkGroupInvocations;
//     uint32_t maxMeshWorkGroupSize;
//     uint32_t maxMeshTotalMemorySize;
//     uint32_t maxMeshOutputVertices;
//     uint32_t maxMeshOutputPrimitives;
//     uint32_t maxMeshMultiviewViewCount;
//     uint32_t meshOutputPerVertexGranularity;
//     uint32_t meshOutputPerPrimitiveGranularity;
// }
    public unsafe struct PhysicalDeviceMeshShaderPropertiesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxDrawMeshTasksCount;
        public uint MaxTaskWorkGroupInvocations;
        public uint MaxTaskWorkGroupSize;
        public uint MaxTaskTotalMemorySize;
        public uint MaxTaskOutputCount;
        public uint MaxMeshWorkGroupInvocations;
        public uint MaxMeshWorkGroupSize;
        public uint MaxMeshTotalMemorySize;
        public uint MaxMeshOutputVertices;
        public uint MaxMeshOutputPrimitives;
        public uint MaxMeshMultiviewViewCount;
        public uint MeshOutputPerVertexGranularity;
        public uint MeshOutputPerPrimitiveGranularity;
    }

// Original Definition: 
// struct VkDrawMeshTasksIndirectCommandNV 
// {
//     uint32_t taskCount;
//     uint32_t firstTask;
// }
    public unsafe struct DrawMeshTasksIndirectCommandNv
    {
        public uint TaskCount;
        public uint FirstTask;
    }

// Original Definition: 
// struct VkRayTracingShaderGroupCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkRayTracingShaderGroupTypeKHR type;
//     uint32_t generalShader;
//     uint32_t closestHitShader;
//     uint32_t anyHitShader;
//     uint32_t intersectionShader;
// }
    public unsafe struct RayTracingShaderGroupCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public RayTracingShaderGroupTypeKhr Type;
        public uint GeneralShader;
        public uint ClosestHitShader;
        public uint AnyHitShader;
        public uint IntersectionShader;
    }

// Original Definition: 
// struct VkRayTracingShaderGroupCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkRayTracingShaderGroupTypeKHR type;
//     uint32_t generalShader;
//     uint32_t closestHitShader;
//     uint32_t anyHitShader;
//     uint32_t intersectionShader;
//     void* pShaderGroupCaptureReplayHandle;
// }
    public unsafe struct RayTracingShaderGroupCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public RayTracingShaderGroupTypeKhr Type;
        public uint GeneralShader;
        public uint ClosestHitShader;
        public uint AnyHitShader;
        public uint IntersectionShader;
        public void* PShaderGroupCaptureReplayHandle;
    }

// Original Definition: 
// struct VkRayTracingPipelineCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCreateFlags flags;
//     uint32_t stageCount;
//     VkPipelineShaderStageCreateInfo* pStages;
//     uint32_t groupCount;
//     VkRayTracingShaderGroupCreateInfoNV* pGroups;
//     uint32_t maxRecursionDepth;
//     VkPipelineLayout layout;
//     VkPipeline basePipelineHandle;
//     int32_t basePipelineIndex;
// }
    public unsafe struct RayTracingPipelineCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCreateFlags Flags;
        public uint StageCount;
        public PipelineShaderStageCreateInfo* PStages;
        public uint GroupCount;
        public RayTracingShaderGroupCreateInfoNv* PGroups;
        public uint MaxRecursionDepth;
        public PipelineLayout Layout;
        public Pipeline BasePipelineHandle;
        public int BasePipelineIndex;
    }

// Original Definition: 
// struct VkRayTracingPipelineCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCreateFlags flags;
//     uint32_t stageCount;
//     VkPipelineShaderStageCreateInfo* pStages;
//     uint32_t groupCount;
//     VkRayTracingShaderGroupCreateInfoKHR* pGroups;
//     uint32_t maxRecursionDepth;
//     VkPipelineLibraryCreateInfoKHR libraries;
//     VkRayTracingPipelineInterfaceCreateInfoKHR* pLibraryInterface;
//     VkPipelineLayout layout;
//     VkPipeline basePipelineHandle;
//     int32_t basePipelineIndex;
// }
    public unsafe struct RayTracingPipelineCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCreateFlags Flags;
        public uint StageCount;
        public PipelineShaderStageCreateInfo* PStages;
        public uint GroupCount;
        public RayTracingShaderGroupCreateInfoKhr* PGroups;
        public uint MaxRecursionDepth;
        public PipelineLibraryCreateInfoKhr Libraries;
        public RayTracingPipelineInterfaceCreateInfoKhr* PLibraryInterface;
        public PipelineLayout Layout;
        public Pipeline BasePipelineHandle;
        public int BasePipelineIndex;
    }

// Original Definition: 
// struct VkGeometryTrianglesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBuffer vertexData;
//     VkDeviceSize vertexOffset;
//     uint32_t vertexCount;
//     VkDeviceSize vertexStride;
//     VkFormat vertexFormat;
//     VkBuffer indexData;
//     VkDeviceSize indexOffset;
//     uint32_t indexCount;
//     VkIndexType indexType;
//     VkBuffer transformData;
//     VkDeviceSize transformOffset;
// }
    public unsafe struct GeometryTrianglesNv
    {
        public StructureType SType;
        public void* PNext;
        public Buffer VertexData;
        public ulong VertexOffset;
        public uint VertexCount;
        public ulong VertexStride;
        public Format VertexFormat;
        public Buffer IndexData;
        public ulong IndexOffset;
        public uint IndexCount;
        public IndexType IndexType;
        public Buffer TransformData;
        public ulong TransformOffset;
    }

// Original Definition: 
// struct VkGeometryAABBNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBuffer aabbData;
//     uint32_t numAABBs;
//     uint32_t stride;
//     VkDeviceSize offset;
// }
    public unsafe struct GeometryAabbnv
    {
        public StructureType SType;
        public void* PNext;
        public Buffer AabbData;
        public uint NumAABBs;
        public uint Stride;
        public ulong Offset;
    }

// Original Definition: 
// struct VkGeometryDataNV 
// {
//     VkGeometryTrianglesNV triangles;
//     VkGeometryAABBNV aabbs;
// }
    public unsafe struct GeometryDataNv
    {
        public GeometryTrianglesNv Triangles;
        public GeometryAabbnv Aabbs;
    }

// Original Definition: 
// struct VkGeometryNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkGeometryTypeKHR geometryType;
//     VkGeometryDataNV geometry;
//     VkGeometryFlagsKHR flags;
// }
    public unsafe struct GeometryNv
    {
        public StructureType SType;
        public void* PNext;
        public GeometryTypeKhr GeometryType;
        public GeometryDataNv Geometry;
        public GeometryFlagsKhr Flags;
    }

// Original Definition: 
// struct VkAccelerationStructureInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccelerationStructureTypeNV type;
//     VkBuildAccelerationStructureFlagsNV flags;
//     uint32_t instanceCount;
//     uint32_t geometryCount;
//     VkGeometryNV* pGeometries;
// }
    public unsafe struct AccelerationStructureInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public AccelerationStructureTypeKhr Type;
        public BuildAccelerationStructureFlagsKhr Flags;
        public uint InstanceCount;
        public uint GeometryCount;
        public GeometryNv* PGeometries;
    }

// Original Definition: 
// struct VkAccelerationStructureCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceSize compactedSize;
//     VkAccelerationStructureInfoNV info;
// }
    public unsafe struct AccelerationStructureCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public ulong CompactedSize;
        public AccelerationStructureInfoNv Info;
    }

// Original Definition: 
// struct VkBindAccelerationStructureMemoryInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccelerationStructureKHR accelerationStructure;
//     VkDeviceMemory memory;
//     VkDeviceSize memoryOffset;
//     uint32_t deviceIndexCount;
//     uint32_t* pDeviceIndices;
// }
    public unsafe struct BindAccelerationStructureMemoryInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public AccelerationStructureKhr AccelerationStructure;
        public DeviceMemory Memory;
        public ulong MemoryOffset;
        public uint DeviceIndexCount;
        public uint* PDeviceIndices;
    }
// Original Definition: 
// struct VkBindAccelerationStructureMemoryInfoNV 
// {

// }
    public unsafe struct BindAccelerationStructureMemoryInfoNv
    {
    }

// Original Definition: 
// struct VkWriteDescriptorSetAccelerationStructureKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t accelerationStructureCount;
//     VkAccelerationStructureKHR* pAccelerationStructures;
// }
    public unsafe struct WriteDescriptorSetAccelerationStructureKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint AccelerationStructureCount;
        public AccelerationStructureKhr* PAccelerationStructures;
    }
// Original Definition: 
// struct VkWriteDescriptorSetAccelerationStructureNV 
// {

// }
    public unsafe struct WriteDescriptorSetAccelerationStructureNv
    {
    }

// Original Definition: 
// struct VkAccelerationStructureMemoryRequirementsInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccelerationStructureMemoryRequirementsTypeKHR type;
//     VkAccelerationStructureBuildTypeKHR buildType;
//     VkAccelerationStructureKHR accelerationStructure;
// }
    public unsafe struct AccelerationStructureMemoryRequirementsInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public AccelerationStructureMemoryRequirementsTypeKhr Type;
        public AccelerationStructureBuildTypeKhr BuildType;
        public AccelerationStructureKhr AccelerationStructure;
    }

// Original Definition: 
// struct VkAccelerationStructureMemoryRequirementsInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccelerationStructureMemoryRequirementsTypeNV type;
//     VkAccelerationStructureNV accelerationStructure;
// }
    public unsafe struct AccelerationStructureMemoryRequirementsInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public AccelerationStructureMemoryRequirementsTypeKhr Type;
        public AccelerationStructureKhr AccelerationStructure;
    }

// Original Definition: 
// struct VkPhysicalDeviceRayTracingFeaturesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 rayTracing;
//     VkBool32 rayTracingShaderGroupHandleCaptureReplay;
//     VkBool32 rayTracingShaderGroupHandleCaptureReplayMixed;
//     VkBool32 rayTracingAccelerationStructureCaptureReplay;
//     VkBool32 rayTracingIndirectTraceRays;
//     VkBool32 rayTracingIndirectAccelerationStructureBuild;
//     VkBool32 rayTracingHostAccelerationStructureCommands;
//     VkBool32 rayQuery;
//     VkBool32 rayTracingPrimitiveCulling;
// }
    public unsafe struct PhysicalDeviceRayTracingFeaturesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint RayTracing;
        public uint RayTracingShaderGroupHandleCaptureReplay;
        public uint RayTracingShaderGroupHandleCaptureReplayMixed;
        public uint RayTracingAccelerationStructureCaptureReplay;
        public uint RayTracingIndirectTraceRays;
        public uint RayTracingIndirectAccelerationStructureBuild;
        public uint RayTracingHostAccelerationStructureCommands;
        public uint RayQuery;
        public uint RayTracingPrimitiveCulling;
    }

// Original Definition: 
// struct VkPhysicalDeviceRayTracingPropertiesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t shaderGroupHandleSize;
//     uint32_t maxRecursionDepth;
//     uint32_t maxShaderGroupStride;
//     uint32_t shaderGroupBaseAlignment;
//     uint64_t maxGeometryCount;
//     uint64_t maxInstanceCount;
//     uint64_t maxPrimitiveCount;
//     uint32_t maxDescriptorSetAccelerationStructures;
//     uint32_t shaderGroupHandleCaptureReplaySize;
// }
    public unsafe struct PhysicalDeviceRayTracingPropertiesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderGroupHandleSize;
        public uint MaxRecursionDepth;
        public uint MaxShaderGroupStride;
        public uint ShaderGroupBaseAlignment;
        public ulong MaxGeometryCount;
        public ulong MaxInstanceCount;
        public ulong MaxPrimitiveCount;
        public uint MaxDescriptorSetAccelerationStructures;
        public uint ShaderGroupHandleCaptureReplaySize;
    }

// Original Definition: 
// struct VkPhysicalDeviceRayTracingPropertiesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t shaderGroupHandleSize;
//     uint32_t maxRecursionDepth;
//     uint32_t maxShaderGroupStride;
//     uint32_t shaderGroupBaseAlignment;
//     uint64_t maxGeometryCount;
//     uint64_t maxInstanceCount;
//     uint64_t maxTriangleCount;
//     uint32_t maxDescriptorSetAccelerationStructures;
// }
    public unsafe struct PhysicalDeviceRayTracingPropertiesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderGroupHandleSize;
        public uint MaxRecursionDepth;
        public uint MaxShaderGroupStride;
        public uint ShaderGroupBaseAlignment;
        public ulong MaxGeometryCount;
        public ulong MaxInstanceCount;
        public ulong MaxTriangleCount;
        public uint MaxDescriptorSetAccelerationStructures;
    }

// Original Definition: 
// struct VkStridedBufferRegionKHR 
// {
//     VkBuffer buffer;
//     VkDeviceSize offset;
//     VkDeviceSize stride;
//     VkDeviceSize size;
// }
    public unsafe struct StridedBufferRegionKhr
    {
        public Buffer Buffer;
        public ulong Offset;
        public ulong Stride;
        public ulong Size;
    }

// Original Definition: 
// struct VkTraceRaysIndirectCommandKHR 
// {
//     uint32_t width;
//     uint32_t height;
//     uint32_t depth;
// }
    public unsafe struct TraceRaysIndirectCommandKhr
    {
        public uint Width;
        public uint Height;
        public uint Depth;
    }

// Original Definition: 
// struct VkDrmFormatModifierPropertiesListEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t drmFormatModifierCount;
//     VkDrmFormatModifierPropertiesEXT* pDrmFormatModifierProperties;
// }
    public unsafe struct DrmFormatModifierPropertiesListExt
    {
        public StructureType SType;
        public void* PNext;
        public uint DrmFormatModifierCount;
        public DrmFormatModifierPropertiesExt* PDrmFormatModifierProperties;
    }

// Original Definition: 
// struct VkDrmFormatModifierPropertiesEXT 
// {
//     uint64_t drmFormatModifier;
//     uint32_t drmFormatModifierPlaneCount;
//     VkFormatFeatureFlags drmFormatModifierTilingFeatures;
// }
    public unsafe struct DrmFormatModifierPropertiesExt
    {
        public ulong DrmFormatModifier;
        public uint DrmFormatModifierPlaneCount;
        public FormatFeatureFlags DrmFormatModifierTilingFeatures;
    }

// Original Definition: 
// struct VkPhysicalDeviceImageDrmFormatModifierInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint64_t drmFormatModifier;
//     VkSharingMode sharingMode;
//     uint32_t queueFamilyIndexCount;
//     uint32_t* pQueueFamilyIndices;
// }
    public unsafe struct PhysicalDeviceImageDrmFormatModifierInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ulong DrmFormatModifier;
        public SharingMode SharingMode;
        public uint QueueFamilyIndexCount;
        public uint* PQueueFamilyIndices;
    }

// Original Definition: 
// struct VkImageDrmFormatModifierListCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t drmFormatModifierCount;
//     uint64_t* pDrmFormatModifiers;
// }
    public unsafe struct ImageDrmFormatModifierListCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public uint DrmFormatModifierCount;
        public ulong* PDrmFormatModifiers;
    }

// Original Definition: 
// struct VkImageDrmFormatModifierExplicitCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint64_t drmFormatModifier;
//     uint32_t drmFormatModifierPlaneCount;
//     VkSubresourceLayout* pPlaneLayouts;
// }
    public unsafe struct ImageDrmFormatModifierExplicitCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ulong DrmFormatModifier;
        public uint DrmFormatModifierPlaneCount;
        public SubresourceLayout* PPlaneLayouts;
    }

// Original Definition: 
// struct VkImageDrmFormatModifierPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint64_t drmFormatModifier;
// }
    public unsafe struct ImageDrmFormatModifierPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public ulong DrmFormatModifier;
    }

// Original Definition: 
// struct VkImageStencilUsageCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageUsageFlags stencilUsage;
// }
    public unsafe struct ImageStencilUsageCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ImageUsageFlags StencilUsage;
    }
// Original Definition: 
// struct VkImageStencilUsageCreateInfoEXT 
// {

// }
    public unsafe struct ImageStencilUsageCreateInfoExt
    {
    }

// Original Definition: 
// struct VkDeviceMemoryOverallocationCreateInfoAMD 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkMemoryOverallocationBehaviorAMD overallocationBehavior;
// }
    public unsafe struct DeviceMemoryOverallocationCreateInfoAmd
    {
        public StructureType SType;
        public void* PNext;
        public MemoryOverallocationBehaviorAmd OverallocationBehavior;
    }

// Original Definition: 
// struct VkPhysicalDeviceFragmentDensityMapFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 fragmentDensityMap;
//     VkBool32 fragmentDensityMapDynamic;
//     VkBool32 fragmentDensityMapNonSubsampledImages;
// }
    public unsafe struct PhysicalDeviceFragmentDensityMapFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint FragmentDensityMap;
        public uint FragmentDensityMapDynamic;
        public uint FragmentDensityMapNonSubsampledImages;
    }

// Original Definition: 
// struct VkPhysicalDeviceFragmentDensityMap2FeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 fragmentDensityMapDeferred;
// }
    public unsafe struct PhysicalDeviceFragmentDensityMap2FeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint FragmentDensityMapDeferred;
    }

// Original Definition: 
// struct VkPhysicalDeviceFragmentDensityMapPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkExtent2D minFragmentDensityTexelSize;
//     VkExtent2D maxFragmentDensityTexelSize;
//     VkBool32 fragmentDensityInvocations;
// }
    public unsafe struct PhysicalDeviceFragmentDensityMapPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public Extent2d MinFragmentDensityTexelSize;
        public Extent2d MaxFragmentDensityTexelSize;
        public uint FragmentDensityInvocations;
    }

// Original Definition: 
// struct VkPhysicalDeviceFragmentDensityMap2PropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 subsampledLoads;
//     VkBool32 subsampledCoarseReconstructionEarlyAccess;
//     uint32_t maxSubsampledArrayLayers;
//     uint32_t maxDescriptorSetSubsampledSamplers;
// }
    public unsafe struct PhysicalDeviceFragmentDensityMap2PropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint SubsampledLoads;
        public uint SubsampledCoarseReconstructionEarlyAccess;
        public uint MaxSubsampledArrayLayers;
        public uint MaxDescriptorSetSubsampledSamplers;
    }

// Original Definition: 
// struct VkRenderPassFragmentDensityMapCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAttachmentReference fragmentDensityMapAttachment;
// }
    public unsafe struct RenderPassFragmentDensityMapCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public AttachmentReference FragmentDensityMapAttachment;
    }

// Original Definition: 
// struct VkPhysicalDeviceScalarBlockLayoutFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 scalarBlockLayout;
// }
    public unsafe struct PhysicalDeviceScalarBlockLayoutFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint ScalarBlockLayout;
    }
// Original Definition: 
// struct VkPhysicalDeviceScalarBlockLayoutFeaturesEXT 
// {

// }
    public unsafe struct PhysicalDeviceScalarBlockLayoutFeaturesExt
    {
    }

// Original Definition: 
// struct VkSurfaceProtectedCapabilitiesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 supportsProtected;
// }
    public unsafe struct SurfaceProtectedCapabilitiesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint SupportsProtected;
    }

// Original Definition: 
// struct VkPhysicalDeviceUniformBufferStandardLayoutFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 uniformBufferStandardLayout;
// }
    public unsafe struct PhysicalDeviceUniformBufferStandardLayoutFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint UniformBufferStandardLayout;
    }
// Original Definition: 
// struct VkPhysicalDeviceUniformBufferStandardLayoutFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceUniformBufferStandardLayoutFeaturesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceDepthClipEnableFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 depthClipEnable;
// }
    public unsafe struct PhysicalDeviceDepthClipEnableFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint DepthClipEnable;
    }

// Original Definition: 
// struct VkPipelineRasterizationDepthClipStateCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineRasterizationDepthClipStateCreateFlagsEXT flags;
//     VkBool32 depthClipEnable;
// }
    public unsafe struct PipelineRasterizationDepthClipStateCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public PipelineRasterizationDepthClipStateCreateFlagsExt Flags;
        public uint DepthClipEnable;
    }

// Original Definition: 
// struct VkPhysicalDeviceMemoryBudgetPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceSize heapBudget[(int)ApiConstants.MaxMemoryHeaps];
//     VkDeviceSize heapUsage[(int)ApiConstants.MaxMemoryHeaps];
// }
    public unsafe struct PhysicalDeviceMemoryBudgetPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public fixed ulong HeapBudget[(int) ApiConstants.MaxMemoryHeaps];
        public fixed ulong HeapUsage[(int) ApiConstants.MaxMemoryHeaps];
    }

// Original Definition: 
// struct VkPhysicalDeviceMemoryPriorityFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 memoryPriority;
// }
    public unsafe struct PhysicalDeviceMemoryPriorityFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint MemoryPriority;
    }

// Original Definition: 
// struct VkMemoryPriorityAllocateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     float priority;
// }
    public unsafe struct MemoryPriorityAllocateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public float Priority;
    }

// Original Definition: 
// struct VkPhysicalDeviceBufferDeviceAddressFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 bufferDeviceAddress;
//     VkBool32 bufferDeviceAddressCaptureReplay;
//     VkBool32 bufferDeviceAddressMultiDevice;
// }
    public unsafe struct PhysicalDeviceBufferDeviceAddressFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint BufferDeviceAddress;
        public uint BufferDeviceAddressCaptureReplay;
        public uint BufferDeviceAddressMultiDevice;
    }
// Original Definition: 
// struct VkPhysicalDeviceBufferDeviceAddressFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceBufferDeviceAddressFeaturesKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceBufferDeviceAddressFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 bufferDeviceAddress;
//     VkBool32 bufferDeviceAddressCaptureReplay;
//     VkBool32 bufferDeviceAddressMultiDevice;
// }
    public unsafe struct PhysicalDeviceBufferDeviceAddressFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint BufferDeviceAddress;
        public uint BufferDeviceAddressCaptureReplay;
        public uint BufferDeviceAddressMultiDevice;
    }
// Original Definition: 
// struct VkPhysicalDeviceBufferAddressFeaturesEXT 
// {

// }
    public unsafe struct PhysicalDeviceBufferAddressFeaturesExt
    {
    }

// Original Definition: 
// struct VkBufferDeviceAddressInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBuffer buffer;
// }
    public unsafe struct BufferDeviceAddressInfo
    {
        public StructureType SType;
        public void* PNext;
        public Buffer Buffer;
    }
// Original Definition: 
// struct VkBufferDeviceAddressInfoKHR 
// {

// }
    public unsafe struct BufferDeviceAddressInfoKhr
    {
    }
// Original Definition: 
// struct VkBufferDeviceAddressInfoEXT 
// {

// }
    public unsafe struct BufferDeviceAddressInfoExt
    {
    }

// Original Definition: 
// struct VkBufferOpaqueCaptureAddressCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint64_t opaqueCaptureAddress;
// }
    public unsafe struct BufferOpaqueCaptureAddressCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ulong OpaqueCaptureAddress;
    }
// Original Definition: 
// struct VkBufferOpaqueCaptureAddressCreateInfoKHR 
// {

// }
    public unsafe struct BufferOpaqueCaptureAddressCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkBufferDeviceAddressCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceAddress deviceAddress;
// }
    public unsafe struct BufferDeviceAddressCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ulong DeviceAddress;
    }

// Original Definition: 
// struct VkPhysicalDeviceImageViewImageFormatInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageViewType imageViewType;
// }
    public unsafe struct PhysicalDeviceImageViewImageFormatInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ImageViewType ImageViewType;
    }

// Original Definition: 
// struct VkFilterCubicImageViewImageFormatPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 filterCubic;
//     VkBool32 filterCubicMinmax;
// }
    public unsafe struct FilterCubicImageViewImageFormatPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint FilterCubic;
        public uint FilterCubicMinmax;
    }

// Original Definition: 
// struct VkPhysicalDeviceImagelessFramebufferFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 imagelessFramebuffer;
// }
    public unsafe struct PhysicalDeviceImagelessFramebufferFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint ImagelessFramebuffer;
    }
// Original Definition: 
// struct VkPhysicalDeviceImagelessFramebufferFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceImagelessFramebufferFeaturesKhr
    {
    }

// Original Definition: 
// struct VkFramebufferAttachmentsCreateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t attachmentImageInfoCount;
//     VkFramebufferAttachmentImageInfo* pAttachmentImageInfos;
// }
    public unsafe struct FramebufferAttachmentsCreateInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint AttachmentImageInfoCount;
        public FramebufferAttachmentImageInfo* PAttachmentImageInfos;
    }
// Original Definition: 
// struct VkFramebufferAttachmentsCreateInfoKHR 
// {

// }
    public unsafe struct FramebufferAttachmentsCreateInfoKhr
    {
    }

// Original Definition: 
// struct VkFramebufferAttachmentImageInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageCreateFlags flags;
//     VkImageUsageFlags usage;
//     uint32_t width;
//     uint32_t height;
//     uint32_t layerCount;
//     uint32_t viewFormatCount;
//     VkFormat* pViewFormats;
// }
    public unsafe struct FramebufferAttachmentImageInfo
    {
        public StructureType SType;
        public void* PNext;
        public ImageCreateFlags Flags;
        public ImageUsageFlags Usage;
        public uint Width;
        public uint Height;
        public uint LayerCount;
        public uint ViewFormatCount;
        public Format* PViewFormats;
    }
// Original Definition: 
// struct VkFramebufferAttachmentImageInfoKHR 
// {

// }
    public unsafe struct FramebufferAttachmentImageInfoKhr
    {
    }

// Original Definition: 
// struct VkRenderPassAttachmentBeginInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t attachmentCount;
//     VkImageView* pAttachments;
// }
    public unsafe struct RenderPassAttachmentBeginInfo
    {
        public StructureType SType;
        public void* PNext;
        public uint AttachmentCount;
        public ImageView* PAttachments;
    }
// Original Definition: 
// struct VkRenderPassAttachmentBeginInfoKHR 
// {

// }
    public unsafe struct RenderPassAttachmentBeginInfoKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 textureCompressionASTC_HDR;
// }
    public unsafe struct PhysicalDeviceTextureCompressionASTCHDRFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint TextureCompressionASTC_HDR;
    }

// Original Definition: 
// struct VkPhysicalDeviceCooperativeMatrixFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 cooperativeMatrix;
//     VkBool32 cooperativeMatrixRobustBufferAccess;
// }
    public unsafe struct PhysicalDeviceCooperativeMatrixFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint CooperativeMatrix;
        public uint CooperativeMatrixRobustBufferAccess;
    }

// Original Definition: 
// struct VkPhysicalDeviceCooperativeMatrixPropertiesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkShaderStageFlags cooperativeMatrixSupportedStages;
// }
    public unsafe struct PhysicalDeviceCooperativeMatrixPropertiesNv
    {
        public StructureType SType;
        public void* PNext;
        public ShaderStageFlags CooperativeMatrixSupportedStages;
    }

// Original Definition: 
// struct VkCooperativeMatrixPropertiesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t MSize;
//     uint32_t NSize;
//     uint32_t KSize;
//     VkComponentTypeNV AType;
//     VkComponentTypeNV BType;
//     VkComponentTypeNV CType;
//     VkComponentTypeNV DType;
//     VkScopeNV scope;
// }
    public unsafe struct CooperativeMatrixPropertiesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint MSize;
        public uint NSize;
        public uint KSize;
        public ComponentTypeNv AType;
        public ComponentTypeNv BType;
        public ComponentTypeNv CType;
        public ComponentTypeNv DType;
        public ScopeNv Scope;
    }

// Original Definition: 
// struct VkPhysicalDeviceYcbcrImageArraysFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 ycbcrImageArrays;
// }
    public unsafe struct PhysicalDeviceYcbcrImageArraysFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint YcbcrImageArrays;
    }

// Original Definition: 
// struct VkImageViewHandleInfoNVX 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageView imageView;
//     VkDescriptorType descriptorType;
//     VkSampler sampler;
// }
    public unsafe struct ImageViewHandleInfoNvx
    {
        public StructureType SType;
        public void* PNext;
        public ImageView ImageView;
        public DescriptorType DescriptorType;
        public Sampler Sampler;
    }

// Original Definition: 
// struct VkImageViewAddressPropertiesNVX 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceAddress deviceAddress;
//     VkDeviceSize size;
// }
    public unsafe struct ImageViewAddressPropertiesNvx
    {
        public StructureType SType;
        public void* PNext;
        public ulong DeviceAddress;
        public ulong Size;
    }

// Original Definition: 
// struct VkPresentFrameTokenGGP 
// {
//     VkStructureType sType;
//     void* pNext;
//     GgpFrameToken frameToken;
// }
    public unsafe struct PresentFrameTokenGgp
    {
        public StructureType SType;
        public void* PNext;
        public uint FrameToken;
    }

// Original Definition: 
// struct VkPipelineCreationFeedbackEXT 
// {
//     VkPipelineCreationFeedbackFlagsEXT flags;
//     uint64_t duration;
// }
    public unsafe struct PipelineCreationFeedbackExt
    {
        public PipelineCreationFeedbackFlagsExt Flags;
        public ulong Duration;
    }

// Original Definition: 
// struct VkPipelineCreationFeedbackCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCreationFeedbackEXT* pPipelineCreationFeedback;
//     uint32_t pipelineStageCreationFeedbackCount;
//     VkPipelineCreationFeedbackEXT* pPipelineStageCreationFeedbacks;
// }
    public unsafe struct PipelineCreationFeedbackCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCreationFeedbackExt* PPipelineCreationFeedback;
        public uint PipelineStageCreationFeedbackCount;
        public PipelineCreationFeedbackExt* PPipelineStageCreationFeedbacks;
    }

// Original Definition: 
// struct VkSurfaceFullScreenExclusiveInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFullScreenExclusiveEXT fullScreenExclusive;
// }
    public unsafe struct SurfaceFullScreenExclusiveInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public FullScreenExclusiveExt FullScreenExclusive;
    }

// Original Definition: 
// struct VkSurfaceFullScreenExclusiveWin32InfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     HMONITOR hmonitor;
// }
    public unsafe struct SurfaceFullScreenExclusiveWin32InfoExt
    {
        public StructureType SType;
        public void* PNext;
        public IntPtr Hmonitor;
    }

// Original Definition: 
// struct VkSurfaceCapabilitiesFullScreenExclusiveEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 fullScreenExclusiveSupported;
// }
    public unsafe struct SurfaceCapabilitiesFullScreenExclusiveExt
    {
        public StructureType SType;
        public void* PNext;
        public uint FullScreenExclusiveSupported;
    }

// Original Definition: 
// struct VkPhysicalDevicePerformanceQueryFeaturesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 performanceCounterQueryPools;
//     VkBool32 performanceCounterMultipleQueryPools;
// }
    public unsafe struct PhysicalDevicePerformanceQueryFeaturesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint PerformanceCounterQueryPools;
        public uint PerformanceCounterMultipleQueryPools;
    }

// Original Definition: 
// struct VkPhysicalDevicePerformanceQueryPropertiesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 allowCommandBufferQueryCopies;
// }
    public unsafe struct PhysicalDevicePerformanceQueryPropertiesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint AllowCommandBufferQueryCopies;
    }

// Original Definition: 
// struct VkPerformanceCounterKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPerformanceCounterUnitKHR unit;
//     VkPerformanceCounterScopeKHR scope;
//     VkPerformanceCounterStorageKHR storage;
//     uint8_t uuid[(int)ApiConstants.UuidSize];
// }
    public unsafe struct PerformanceCounterKhr
    {
        public StructureType SType;
        public void* PNext;
        public PerformanceCounterUnitKhr Unit;
        public PerformanceCounterScopeKhr Scope;
        public PerformanceCounterStorageKhr Storage;
        public fixed byte Uuid[(int) ApiConstants.UuidSize];
    }

// Original Definition: 
// struct VkPerformanceCounterDescriptionKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPerformanceCounterDescriptionFlagsKHR flags;
//     char name[(int)ApiConstants.MaxDescriptionSize];
//     char category[(int)ApiConstants.MaxDescriptionSize];
//     char description[(int)ApiConstants.MaxDescriptionSize];
// }
    public unsafe struct PerformanceCounterDescriptionKhr
    {
        public StructureType SType;
        public void* PNext;
        public PerformanceCounterDescriptionFlagsKhr Flags;
        public fixed sbyte Name_[(int) ApiConstants.MaxDescriptionSize];
        public fixed sbyte Category_[(int) ApiConstants.MaxDescriptionSize];
        public fixed sbyte Description_[(int) ApiConstants.MaxDescriptionSize];

        public string Name
        {
            get
            {
                fixed (sbyte* ptr = Name_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string Category
        {
            get
            {
                fixed (sbyte* ptr = Category_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string Description
        {
            get
            {
                fixed (sbyte* ptr = Description_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }

// Original Definition: 
// struct VkQueryPoolPerformanceCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t queueFamilyIndex;
//     uint32_t counterIndexCount;
//     uint32_t* pCounterIndices;
// }
    public unsafe struct QueryPoolPerformanceCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint QueueFamilyIndex;
        public uint CounterIndexCount;
        public uint* PCounterIndices;
    }

    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct PerformanceCounterResultKhr
    {
        [FieldOffset(0)] public int Int32;
        [FieldOffset(0)] public long Int64;
        [FieldOffset(0)] public uint Uint32;
        [FieldOffset(0)] public ulong Uint64;
        [FieldOffset(0)] public float Float32;
        [FieldOffset(0)] public IntPtr Float64;
    }

// Original Definition: 
// struct VkAcquireProfilingLockInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAcquireProfilingLockFlagsKHR flags;
//     uint64_t timeout;
// }
    public unsafe struct AcquireProfilingLockInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public AcquireProfilingLockFlagsKhr Flags;
        public ulong Timeout;
    }

// Original Definition: 
// struct VkPerformanceQuerySubmitInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t counterPassIndex;
// }
    public unsafe struct PerformanceQuerySubmitInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint CounterPassIndex;
    }

// Original Definition: 
// struct VkHeadlessSurfaceCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkHeadlessSurfaceCreateFlagsEXT flags;
// }
    public unsafe struct HeadlessSurfaceCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public HeadlessSurfaceCreateFlagsExt Flags;
    }

// Original Definition: 
// struct VkPhysicalDeviceCoverageReductionModeFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 coverageReductionMode;
// }
    public unsafe struct PhysicalDeviceCoverageReductionModeFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint CoverageReductionMode;
    }

// Original Definition: 
// struct VkPipelineCoverageReductionStateCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCoverageReductionStateCreateFlagsNV flags;
//     VkCoverageReductionModeNV coverageReductionMode;
// }
    public unsafe struct PipelineCoverageReductionStateCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCoverageReductionStateCreateFlagsNv Flags;
        public CoverageReductionModeNv CoverageReductionMode;
    }

// Original Definition: 
// struct VkFramebufferMixedSamplesCombinationNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkCoverageReductionModeNV coverageReductionMode;
//     VkSampleCountFlagBits rasterizationSamples;
//     VkSampleCountFlags depthStencilSamples;
//     VkSampleCountFlags colorSamples;
// }
    public unsafe struct FramebufferMixedSamplesCombinationNv
    {
        public StructureType SType;
        public void* PNext;
        public CoverageReductionModeNv CoverageReductionMode;
        public SampleCountFlags RasterizationSamples;
        public SampleCountFlags DepthStencilSamples;
        public SampleCountFlags ColorSamples;
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderIntegerFunctions2;
// }
    public unsafe struct PhysicalDeviceShaderIntegerFunctions2FeaturesIntel
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderIntegerFunctions2;
    }

    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct PerformanceValueDataIntel
    {
        [FieldOffset(0)] public uint Value32;
        [FieldOffset(0)] public ulong Value64;
        [FieldOffset(0)] public float ValueFloat;
        [FieldOffset(0)] public uint ValueBool;
        [FieldOffset(0)] public sbyte* ValueString;
    }

// Original Definition: 
// struct VkPerformanceValueINTEL 
// {
//     VkPerformanceValueTypeINTEL type;
//     VkPerformanceValueDataINTEL data;
// }
    public unsafe struct PerformanceValueIntel
    {
        public PerformanceValueTypeIntel Type;
        public PerformanceValueDataIntel Data;
    }

// Original Definition: 
// struct VkInitializePerformanceApiInfoINTEL 
// {
//     VkStructureType sType;
//     void* pNext;
//     void* pUserData;
// }
    public unsafe struct InitializePerformanceApiInfoIntel
    {
        public StructureType SType;
        public void* PNext;
        public void* PUserData;
    }

// Original Definition: 
// struct VkQueryPoolPerformanceQueryCreateInfoINTEL 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkQueryPoolSamplingModeINTEL performanceCountersSampling;
// }
    public unsafe struct QueryPoolPerformanceQueryCreateInfoIntel
    {
        public StructureType SType;
        public void* PNext;
        public QueryPoolSamplingModeIntel PerformanceCountersSampling;
    }
// Original Definition: 
// struct VkQueryPoolCreateInfoINTEL 
// {

// }
    public unsafe struct QueryPoolCreateInfoIntel
    {
    }

// Original Definition: 
// struct VkPerformanceMarkerInfoINTEL 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint64_t marker;
// }
    public unsafe struct PerformanceMarkerInfoIntel
    {
        public StructureType SType;
        public void* PNext;
        public ulong Marker;
    }

// Original Definition: 
// struct VkPerformanceStreamMarkerInfoINTEL 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t marker;
// }
    public unsafe struct PerformanceStreamMarkerInfoIntel
    {
        public StructureType SType;
        public void* PNext;
        public uint Marker;
    }

// Original Definition: 
// struct VkPerformanceOverrideInfoINTEL 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPerformanceOverrideTypeINTEL type;
//     VkBool32 enable;
//     uint64_t parameter;
// }
    public unsafe struct PerformanceOverrideInfoIntel
    {
        public StructureType SType;
        public void* PNext;
        public PerformanceOverrideTypeIntel Type;
        public uint Enable;
        public ulong Parameter;
    }

// Original Definition: 
// struct VkPerformanceConfigurationAcquireInfoINTEL 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPerformanceConfigurationTypeINTEL type;
// }
    public unsafe struct PerformanceConfigurationAcquireInfoIntel
    {
        public StructureType SType;
        public void* PNext;
        public PerformanceConfigurationTypeIntel Type;
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderClockFeaturesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderSubgroupClock;
//     VkBool32 shaderDeviceClock;
// }
    public unsafe struct PhysicalDeviceShaderClockFeaturesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderSubgroupClock;
        public uint ShaderDeviceClock;
    }

// Original Definition: 
// struct VkPhysicalDeviceIndexTypeUint8FeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 indexTypeUint8;
// }
    public unsafe struct PhysicalDeviceIndexTypeUint8FeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint IndexTypeUint8;
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderSMBuiltinsPropertiesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t shaderSMCount;
//     uint32_t shaderWarpsPerSM;
// }
    public unsafe struct PhysicalDeviceShaderSMBuiltinsPropertiesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderSMCount;
        public uint ShaderWarpsPerSM;
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderSMBuiltinsFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderSMBuiltins;
// }
    public unsafe struct PhysicalDeviceShaderSMBuiltinsFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderSMBuiltins;
    }

// Original Definition: 
// struct VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 fragmentShaderSampleInterlock;
//     VkBool32 fragmentShaderPixelInterlock;
//     VkBool32 fragmentShaderShadingRateInterlock;
// }
    public unsafe struct PhysicalDeviceFragmentShaderInterlockFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint FragmentShaderSampleInterlock;
        public uint FragmentShaderPixelInterlock;
        public uint FragmentShaderShadingRateInterlock;
    }

// Original Definition: 
// struct VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 separateDepthStencilLayouts;
// }
    public unsafe struct PhysicalDeviceSeparateDepthStencilLayoutsFeatures
    {
        public StructureType SType;
        public void* PNext;
        public uint SeparateDepthStencilLayouts;
    }
// Original Definition: 
// struct VkPhysicalDeviceSeparateDepthStencilLayoutsFeaturesKHR 
// {

// }
    public unsafe struct PhysicalDeviceSeparateDepthStencilLayoutsFeaturesKhr
    {
    }

// Original Definition: 
// struct VkAttachmentReferenceStencilLayout 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageLayout stencilLayout;
// }
    public unsafe struct AttachmentReferenceStencilLayout
    {
        public StructureType SType;
        public void* PNext;
        public ImageLayout StencilLayout;
    }
// Original Definition: 
// struct VkAttachmentReferenceStencilLayoutKHR 
// {

// }
    public unsafe struct AttachmentReferenceStencilLayoutKhr
    {
    }

// Original Definition: 
// struct VkAttachmentDescriptionStencilLayout 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkImageLayout stencilInitialLayout;
//     VkImageLayout stencilFinalLayout;
// }
    public unsafe struct AttachmentDescriptionStencilLayout
    {
        public StructureType SType;
        public void* PNext;
        public ImageLayout StencilInitialLayout;
        public ImageLayout StencilFinalLayout;
    }
// Original Definition: 
// struct VkAttachmentDescriptionStencilLayoutKHR 
// {

// }
    public unsafe struct AttachmentDescriptionStencilLayoutKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 pipelineExecutableInfo;
// }
    public unsafe struct PhysicalDevicePipelineExecutablePropertiesFeaturesKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint PipelineExecutableInfo;
    }

// Original Definition: 
// struct VkPipelineInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipeline pipeline;
// }
    public unsafe struct PipelineInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Pipeline Pipeline;
    }

// Original Definition: 
// struct VkPipelineExecutablePropertiesKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkShaderStageFlags stages;
//     char name[(int)ApiConstants.MaxDescriptionSize];
//     char description[(int)ApiConstants.MaxDescriptionSize];
//     uint32_t subgroupSize;
// }
    public unsafe struct PipelineExecutablePropertiesKhr
    {
        public StructureType SType;
        public void* PNext;
        public ShaderStageFlags Stages;
        public fixed sbyte Name_[(int) ApiConstants.MaxDescriptionSize];
        public fixed sbyte Description_[(int) ApiConstants.MaxDescriptionSize];
        public uint SubgroupSize;

        public string Name
        {
            get
            {
                fixed (sbyte* ptr = Name_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string Description
        {
            get
            {
                fixed (sbyte* ptr = Description_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }

// Original Definition: 
// struct VkPipelineExecutableInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipeline pipeline;
//     uint32_t executableIndex;
// }
    public unsafe struct PipelineExecutableInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public Pipeline Pipeline;
        public uint ExecutableIndex;
    }

    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct PipelineExecutableStatisticValueKhr
    {
        [FieldOffset(0)] public uint B32;
        [FieldOffset(0)] public long I64;
        [FieldOffset(0)] public ulong U64;
        [FieldOffset(0)] public IntPtr F64;
    }

// Original Definition: 
// struct VkPipelineExecutableStatisticKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     char name[(int)ApiConstants.MaxDescriptionSize];
//     char description[(int)ApiConstants.MaxDescriptionSize];
//     VkPipelineExecutableStatisticFormatKHR format;
//     VkPipelineExecutableStatisticValueKHR value;
// }
    public unsafe struct PipelineExecutableStatisticKhr
    {
        public StructureType SType;
        public void* PNext;
        public fixed sbyte Name_[(int) ApiConstants.MaxDescriptionSize];
        public fixed sbyte Description_[(int) ApiConstants.MaxDescriptionSize];
        public PipelineExecutableStatisticFormatKhr Format;
        public PipelineExecutableStatisticValueKhr Value;

        public string Name
        {
            get
            {
                fixed (sbyte* ptr = Name_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string Description
        {
            get
            {
                fixed (sbyte* ptr = Description_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }

// Original Definition: 
// struct VkPipelineExecutableInternalRepresentationKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     char name[(int)ApiConstants.MaxDescriptionSize];
//     char description[(int)ApiConstants.MaxDescriptionSize];
//     VkBool32 isText;
//     size_t dataSize;
//     void* pData;
// }
    public unsafe struct PipelineExecutableInternalRepresentationKhr
    {
        public StructureType SType;
        public void* PNext;
        public fixed sbyte Name_[(int) ApiConstants.MaxDescriptionSize];
        public fixed sbyte Description_[(int) ApiConstants.MaxDescriptionSize];
        public uint IsText;
        public IntPtr DataSize;
        public void* PData;

        public string Name
        {
            get
            {
                fixed (sbyte* ptr = Name_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string Description
        {
            get
            {
                fixed (sbyte* ptr = Description_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }

// Original Definition: 
// struct VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 shaderDemoteToHelperInvocation;
// }
    public unsafe struct PhysicalDeviceShaderDemoteToHelperInvocationFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint ShaderDemoteToHelperInvocation;
    }

// Original Definition: 
// struct VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 texelBufferAlignment;
// }
    public unsafe struct PhysicalDeviceTexelBufferAlignmentFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint TexelBufferAlignment;
    }

// Original Definition: 
// struct VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceSize storageTexelBufferOffsetAlignmentBytes;
//     VkBool32 storageTexelBufferOffsetSingleTexelAlignment;
//     VkDeviceSize uniformTexelBufferOffsetAlignmentBytes;
//     VkBool32 uniformTexelBufferOffsetSingleTexelAlignment;
// }
    public unsafe struct PhysicalDeviceTexelBufferAlignmentPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public ulong StorageTexelBufferOffsetAlignmentBytes;
        public uint StorageTexelBufferOffsetSingleTexelAlignment;
        public ulong UniformTexelBufferOffsetAlignmentBytes;
        public uint UniformTexelBufferOffsetSingleTexelAlignment;
    }

// Original Definition: 
// struct VkPhysicalDeviceSubgroupSizeControlFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 subgroupSizeControl;
//     VkBool32 computeFullSubgroups;
// }
    public unsafe struct PhysicalDeviceSubgroupSizeControlFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint SubgroupSizeControl;
        public uint ComputeFullSubgroups;
    }

// Original Definition: 
// struct VkPhysicalDeviceSubgroupSizeControlPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t minSubgroupSize;
//     uint32_t maxSubgroupSize;
//     uint32_t maxComputeWorkgroupSubgroups;
//     VkShaderStageFlags requiredSubgroupSizeStages;
// }
    public unsafe struct PhysicalDeviceSubgroupSizeControlPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint MinSubgroupSize;
        public uint MaxSubgroupSize;
        public uint MaxComputeWorkgroupSubgroups;
        public ShaderStageFlags RequiredSubgroupSizeStages;
    }

// Original Definition: 
// struct VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t requiredSubgroupSize;
// }
    public unsafe struct PipelineShaderStageRequiredSubgroupSizeCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public uint RequiredSubgroupSize;
    }

// Original Definition: 
// struct VkMemoryOpaqueCaptureAddressAllocateInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint64_t opaqueCaptureAddress;
// }
    public unsafe struct MemoryOpaqueCaptureAddressAllocateInfo
    {
        public StructureType SType;
        public void* PNext;
        public ulong OpaqueCaptureAddress;
    }
// Original Definition: 
// struct VkMemoryOpaqueCaptureAddressAllocateInfoKHR 
// {

// }
    public unsafe struct MemoryOpaqueCaptureAddressAllocateInfoKhr
    {
    }

// Original Definition: 
// struct VkDeviceMemoryOpaqueCaptureAddressInfo 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceMemory memory;
// }
    public unsafe struct DeviceMemoryOpaqueCaptureAddressInfo
    {
        public StructureType SType;
        public void* PNext;
        public DeviceMemory Memory;
    }
// Original Definition: 
// struct VkDeviceMemoryOpaqueCaptureAddressInfoKHR 
// {

// }
    public unsafe struct DeviceMemoryOpaqueCaptureAddressInfoKhr
    {
    }

// Original Definition: 
// struct VkPhysicalDeviceLineRasterizationFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 rectangularLines;
//     VkBool32 bresenhamLines;
//     VkBool32 smoothLines;
//     VkBool32 stippledRectangularLines;
//     VkBool32 stippledBresenhamLines;
//     VkBool32 stippledSmoothLines;
// }
    public unsafe struct PhysicalDeviceLineRasterizationFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint RectangularLines;
        public uint BresenhamLines;
        public uint SmoothLines;
        public uint StippledRectangularLines;
        public uint StippledBresenhamLines;
        public uint StippledSmoothLines;
    }

// Original Definition: 
// struct VkPhysicalDeviceLineRasterizationPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t lineSubPixelPrecisionBits;
// }
    public unsafe struct PhysicalDeviceLineRasterizationPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint LineSubPixelPrecisionBits;
    }

// Original Definition: 
// struct VkPipelineRasterizationLineStateCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkLineRasterizationModeEXT lineRasterizationMode;
//     VkBool32 stippledLineEnable;
//     uint32_t lineStippleFactor;
//     uint16_t lineStipplePattern;
// }
    public unsafe struct PipelineRasterizationLineStateCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public LineRasterizationModeExt LineRasterizationMode;
        public uint StippledLineEnable;
        public uint LineStippleFactor;
        public ushort LineStipplePattern;
    }

// Original Definition: 
// struct VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 pipelineCreationCacheControl;
// }
    public unsafe struct PhysicalDevicePipelineCreationCacheControlFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint PipelineCreationCacheControl;
    }

// Original Definition: 
// struct VkPhysicalDeviceVulkan11Features 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 storageBuffer16BitAccess;
//     VkBool32 uniformAndStorageBuffer16BitAccess;
//     VkBool32 storagePushConstant16;
//     VkBool32 storageInputOutput16;
//     VkBool32 multiview;
//     VkBool32 multiviewGeometryShader;
//     VkBool32 multiviewTessellationShader;
//     VkBool32 variablePointersStorageBuffer;
//     VkBool32 variablePointers;
//     VkBool32 protectedMemory;
//     VkBool32 samplerYcbcrConversion;
//     VkBool32 shaderDrawParameters;
// }
    public unsafe struct PhysicalDeviceVulkan11Features
    {
        public StructureType SType;
        public void* PNext;
        public uint StorageBuffer16BitAccess;
        public uint UniformAndStorageBuffer16BitAccess;
        public uint StoragePushConstant16;
        public uint StorageInputOutput16;
        public uint Multiview;
        public uint MultiviewGeometryShader;
        public uint MultiviewTessellationShader;
        public uint VariablePointersStorageBuffer;
        public uint VariablePointers;
        public uint ProtectedMemory;
        public uint SamplerYcbcrConversion;
        public uint ShaderDrawParameters;
    }

// Original Definition: 
// struct VkPhysicalDeviceVulkan11Properties 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint8_t deviceUUID[(int)ApiConstants.UuidSize];
//     uint8_t driverUUID[(int)ApiConstants.UuidSize];
//     uint8_t deviceLUID[(int)ApiConstants.LuidSize];
//     uint32_t deviceNodeMask;
//     VkBool32 deviceLUIDValid;
//     uint32_t subgroupSize;
//     VkShaderStageFlags subgroupSupportedStages;
//     VkSubgroupFeatureFlags subgroupSupportedOperations;
//     VkBool32 subgroupQuadOperationsInAllStages;
//     VkPointClippingBehavior pointClippingBehavior;
//     uint32_t maxMultiviewViewCount;
//     uint32_t maxMultiviewInstanceIndex;
//     VkBool32 protectedNoFault;
//     uint32_t maxPerSetDescriptors;
//     VkDeviceSize maxMemoryAllocationSize;
// }
    public unsafe struct PhysicalDeviceVulkan11Properties
    {
        public StructureType SType;
        public void* PNext;
        public fixed byte DeviceUUID[(int) ApiConstants.UuidSize];
        public fixed byte DriverUUID[(int) ApiConstants.UuidSize];
        public fixed byte DeviceLUID[(int) ApiConstants.LuidSize];
        public uint DeviceNodeMask;
        public uint DeviceLUIDValid;
        public uint SubgroupSize;
        public ShaderStageFlags SubgroupSupportedStages;
        public SubgroupFeatureFlags SubgroupSupportedOperations;
        public uint SubgroupQuadOperationsInAllStages;
        public PointClippingBehavior PointClippingBehavior;
        public uint MaxMultiviewViewCount;
        public uint MaxMultiviewInstanceIndex;
        public uint ProtectedNoFault;
        public uint MaxPerSetDescriptors;
        public ulong MaxMemoryAllocationSize;
    }

// Original Definition: 
// struct VkPhysicalDeviceVulkan12Features 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 samplerMirrorClampToEdge;
//     VkBool32 drawIndirectCount;
//     VkBool32 storageBuffer8BitAccess;
//     VkBool32 uniformAndStorageBuffer8BitAccess;
//     VkBool32 storagePushConstant8;
//     VkBool32 shaderBufferInt64Atomics;
//     VkBool32 shaderSharedInt64Atomics;
//     VkBool32 shaderFloat16;
//     VkBool32 shaderInt8;
//     VkBool32 descriptorIndexing;
//     VkBool32 shaderInputAttachmentArrayDynamicIndexing;
//     VkBool32 shaderUniformTexelBufferArrayDynamicIndexing;
//     VkBool32 shaderStorageTexelBufferArrayDynamicIndexing;
//     VkBool32 shaderUniformBufferArrayNonUniformIndexing;
//     VkBool32 shaderSampledImageArrayNonUniformIndexing;
//     VkBool32 shaderStorageBufferArrayNonUniformIndexing;
//     VkBool32 shaderStorageImageArrayNonUniformIndexing;
//     VkBool32 shaderInputAttachmentArrayNonUniformIndexing;
//     VkBool32 shaderUniformTexelBufferArrayNonUniformIndexing;
//     VkBool32 shaderStorageTexelBufferArrayNonUniformIndexing;
//     VkBool32 descriptorBindingUniformBufferUpdateAfterBind;
//     VkBool32 descriptorBindingSampledImageUpdateAfterBind;
//     VkBool32 descriptorBindingStorageImageUpdateAfterBind;
//     VkBool32 descriptorBindingStorageBufferUpdateAfterBind;
//     VkBool32 descriptorBindingUniformTexelBufferUpdateAfterBind;
//     VkBool32 descriptorBindingStorageTexelBufferUpdateAfterBind;
//     VkBool32 descriptorBindingUpdateUnusedWhilePending;
//     VkBool32 descriptorBindingPartiallyBound;
//     VkBool32 descriptorBindingVariableDescriptorCount;
//     VkBool32 runtimeDescriptorArray;
//     VkBool32 samplerFilterMinmax;
//     VkBool32 scalarBlockLayout;
//     VkBool32 imagelessFramebuffer;
//     VkBool32 uniformBufferStandardLayout;
//     VkBool32 shaderSubgroupExtendedTypes;
//     VkBool32 separateDepthStencilLayouts;
//     VkBool32 hostQueryReset;
//     VkBool32 timelineSemaphore;
//     VkBool32 bufferDeviceAddress;
//     VkBool32 bufferDeviceAddressCaptureReplay;
//     VkBool32 bufferDeviceAddressMultiDevice;
//     VkBool32 vulkanMemoryModel;
//     VkBool32 vulkanMemoryModelDeviceScope;
//     VkBool32 vulkanMemoryModelAvailabilityVisibilityChains;
//     VkBool32 shaderOutputViewportIndex;
//     VkBool32 shaderOutputLayer;
//     VkBool32 subgroupBroadcastDynamicId;
// }
    public unsafe struct PhysicalDeviceVulkan12Features
    {
        public StructureType SType;
        public void* PNext;
        public uint SamplerMirrorClampToEdge;
        public uint DrawIndirectCount;
        public uint StorageBuffer8BitAccess;
        public uint UniformAndStorageBuffer8BitAccess;
        public uint StoragePushConstant8;
        public uint ShaderBufferInt64Atomics;
        public uint ShaderSharedInt64Atomics;
        public uint ShaderFloat16;
        public uint ShaderInt8;
        public uint DescriptorIndexing;
        public uint ShaderInputAttachmentArrayDynamicIndexing;
        public uint ShaderUniformTexelBufferArrayDynamicIndexing;
        public uint ShaderStorageTexelBufferArrayDynamicIndexing;
        public uint ShaderUniformBufferArrayNonUniformIndexing;
        public uint ShaderSampledImageArrayNonUniformIndexing;
        public uint ShaderStorageBufferArrayNonUniformIndexing;
        public uint ShaderStorageImageArrayNonUniformIndexing;
        public uint ShaderInputAttachmentArrayNonUniformIndexing;
        public uint ShaderUniformTexelBufferArrayNonUniformIndexing;
        public uint ShaderStorageTexelBufferArrayNonUniformIndexing;
        public uint DescriptorBindingUniformBufferUpdateAfterBind;
        public uint DescriptorBindingSampledImageUpdateAfterBind;
        public uint DescriptorBindingStorageImageUpdateAfterBind;
        public uint DescriptorBindingStorageBufferUpdateAfterBind;
        public uint DescriptorBindingUniformTexelBufferUpdateAfterBind;
        public uint DescriptorBindingStorageTexelBufferUpdateAfterBind;
        public uint DescriptorBindingUpdateUnusedWhilePending;
        public uint DescriptorBindingPartiallyBound;
        public uint DescriptorBindingVariableDescriptorCount;
        public uint RuntimeDescriptorArray;
        public uint SamplerFilterMinmax;
        public uint ScalarBlockLayout;
        public uint ImagelessFramebuffer;
        public uint UniformBufferStandardLayout;
        public uint ShaderSubgroupExtendedTypes;
        public uint SeparateDepthStencilLayouts;
        public uint HostQueryReset;
        public uint TimelineSemaphore;
        public uint BufferDeviceAddress;
        public uint BufferDeviceAddressCaptureReplay;
        public uint BufferDeviceAddressMultiDevice;
        public uint VulkanMemoryModel;
        public uint VulkanMemoryModelDeviceScope;
        public uint VulkanMemoryModelAvailabilityVisibilityChains;
        public uint ShaderOutputViewportIndex;
        public uint ShaderOutputLayer;
        public uint SubgroupBroadcastDynamicId;
    }

// Original Definition: 
// struct VkPhysicalDeviceVulkan12Properties 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDriverId driverID;
//     char driverName[(int)ApiConstants.MaxDriverNameSize];
//     char driverInfo[(int)ApiConstants.MaxDriverInfoSize];
//     VkConformanceVersion conformanceVersion;
//     VkShaderFloatControlsIndependence denormBehaviorIndependence;
//     VkShaderFloatControlsIndependence roundingModeIndependence;
//     VkBool32 shaderSignedZeroInfNanPreserveFloat16;
//     VkBool32 shaderSignedZeroInfNanPreserveFloat32;
//     VkBool32 shaderSignedZeroInfNanPreserveFloat64;
//     VkBool32 shaderDenormPreserveFloat16;
//     VkBool32 shaderDenormPreserveFloat32;
//     VkBool32 shaderDenormPreserveFloat64;
//     VkBool32 shaderDenormFlushToZeroFloat16;
//     VkBool32 shaderDenormFlushToZeroFloat32;
//     VkBool32 shaderDenormFlushToZeroFloat64;
//     VkBool32 shaderRoundingModeRTEFloat16;
//     VkBool32 shaderRoundingModeRTEFloat32;
//     VkBool32 shaderRoundingModeRTEFloat64;
//     VkBool32 shaderRoundingModeRTZFloat16;
//     VkBool32 shaderRoundingModeRTZFloat32;
//     VkBool32 shaderRoundingModeRTZFloat64;
//     uint32_t maxUpdateAfterBindDescriptorsInAllPools;
//     VkBool32 shaderUniformBufferArrayNonUniformIndexingNative;
//     VkBool32 shaderSampledImageArrayNonUniformIndexingNative;
//     VkBool32 shaderStorageBufferArrayNonUniformIndexingNative;
//     VkBool32 shaderStorageImageArrayNonUniformIndexingNative;
//     VkBool32 shaderInputAttachmentArrayNonUniformIndexingNative;
//     VkBool32 robustBufferAccessUpdateAfterBind;
//     VkBool32 quadDivergentImplicitLod;
//     uint32_t maxPerStageDescriptorUpdateAfterBindSamplers;
//     uint32_t maxPerStageDescriptorUpdateAfterBindUniformBuffers;
//     uint32_t maxPerStageDescriptorUpdateAfterBindStorageBuffers;
//     uint32_t maxPerStageDescriptorUpdateAfterBindSampledImages;
//     uint32_t maxPerStageDescriptorUpdateAfterBindStorageImages;
//     uint32_t maxPerStageDescriptorUpdateAfterBindInputAttachments;
//     uint32_t maxPerStageUpdateAfterBindResources;
//     uint32_t maxDescriptorSetUpdateAfterBindSamplers;
//     uint32_t maxDescriptorSetUpdateAfterBindUniformBuffers;
//     uint32_t maxDescriptorSetUpdateAfterBindUniformBuffersDynamic;
//     uint32_t maxDescriptorSetUpdateAfterBindStorageBuffers;
//     uint32_t maxDescriptorSetUpdateAfterBindStorageBuffersDynamic;
//     uint32_t maxDescriptorSetUpdateAfterBindSampledImages;
//     uint32_t maxDescriptorSetUpdateAfterBindStorageImages;
//     uint32_t maxDescriptorSetUpdateAfterBindInputAttachments;
//     VkResolveModeFlags supportedDepthResolveModes;
//     VkResolveModeFlags supportedStencilResolveModes;
//     VkBool32 independentResolveNone;
//     VkBool32 independentResolve;
//     VkBool32 filterMinmaxSingleComponentFormats;
//     VkBool32 filterMinmaxImageComponentMapping;
//     uint64_t maxTimelineSemaphoreValueDifference;
//     VkSampleCountFlags framebufferIntegerColorSampleCounts;
// }
    public unsafe struct PhysicalDeviceVulkan12Properties
    {
        public StructureType SType;
        public void* PNext;
        public DriverId DriverID;
        public fixed sbyte DriverName_[(int) ApiConstants.MaxDriverNameSize];
        public fixed sbyte DriverInfo_[(int) ApiConstants.MaxDriverInfoSize];
        public ConformanceVersion ConformanceVersion;
        public ShaderFloatControlsIndependence DenormBehaviorIndependence;
        public ShaderFloatControlsIndependence RoundingModeIndependence;
        public uint ShaderSignedZeroInfNanPreserveFloat16;
        public uint ShaderSignedZeroInfNanPreserveFloat32;
        public uint ShaderSignedZeroInfNanPreserveFloat64;
        public uint ShaderDenormPreserveFloat16;
        public uint ShaderDenormPreserveFloat32;
        public uint ShaderDenormPreserveFloat64;
        public uint ShaderDenormFlushToZeroFloat16;
        public uint ShaderDenormFlushToZeroFloat32;
        public uint ShaderDenormFlushToZeroFloat64;
        public uint ShaderRoundingModeRTEFloat16;
        public uint ShaderRoundingModeRTEFloat32;
        public uint ShaderRoundingModeRTEFloat64;
        public uint ShaderRoundingModeRTZFloat16;
        public uint ShaderRoundingModeRTZFloat32;
        public uint ShaderRoundingModeRTZFloat64;
        public uint MaxUpdateAfterBindDescriptorsInAllPools;
        public uint ShaderUniformBufferArrayNonUniformIndexingNative;
        public uint ShaderSampledImageArrayNonUniformIndexingNative;
        public uint ShaderStorageBufferArrayNonUniformIndexingNative;
        public uint ShaderStorageImageArrayNonUniformIndexingNative;
        public uint ShaderInputAttachmentArrayNonUniformIndexingNative;
        public uint RobustBufferAccessUpdateAfterBind;
        public uint QuadDivergentImplicitLod;
        public uint MaxPerStageDescriptorUpdateAfterBindSamplers;
        public uint MaxPerStageDescriptorUpdateAfterBindUniformBuffers;
        public uint MaxPerStageDescriptorUpdateAfterBindStorageBuffers;
        public uint MaxPerStageDescriptorUpdateAfterBindSampledImages;
        public uint MaxPerStageDescriptorUpdateAfterBindStorageImages;
        public uint MaxPerStageDescriptorUpdateAfterBindInputAttachments;
        public uint MaxPerStageUpdateAfterBindResources;
        public uint MaxDescriptorSetUpdateAfterBindSamplers;
        public uint MaxDescriptorSetUpdateAfterBindUniformBuffers;
        public uint MaxDescriptorSetUpdateAfterBindUniformBuffersDynamic;
        public uint MaxDescriptorSetUpdateAfterBindStorageBuffers;
        public uint MaxDescriptorSetUpdateAfterBindStorageBuffersDynamic;
        public uint MaxDescriptorSetUpdateAfterBindSampledImages;
        public uint MaxDescriptorSetUpdateAfterBindStorageImages;
        public uint MaxDescriptorSetUpdateAfterBindInputAttachments;
        public ResolveModeFlags SupportedDepthResolveModes;
        public ResolveModeFlags SupportedStencilResolveModes;
        public uint IndependentResolveNone;
        public uint IndependentResolve;
        public uint FilterMinmaxSingleComponentFormats;
        public uint FilterMinmaxImageComponentMapping;
        public ulong MaxTimelineSemaphoreValueDifference;
        public SampleCountFlags FramebufferIntegerColorSampleCounts;

        public string DriverName
        {
            get
            {
                fixed (sbyte* ptr = DriverName_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string DriverInfo
        {
            get
            {
                fixed (sbyte* ptr = DriverInfo_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }

// Original Definition: 
// struct VkPipelineCompilerControlCreateInfoAMD 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkPipelineCompilerControlFlagsAMD compilerControlFlags;
// }
    public unsafe struct PipelineCompilerControlCreateInfoAmd
    {
        public StructureType SType;
        public void* PNext;
        public PipelineCompilerControlFlagsAmd CompilerControlFlags;
    }

// Original Definition: 
// struct VkPhysicalDeviceCoherentMemoryFeaturesAMD 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 deviceCoherentMemory;
// }
    public unsafe struct PhysicalDeviceCoherentMemoryFeaturesAmd
    {
        public StructureType SType;
        public void* PNext;
        public uint DeviceCoherentMemory;
    }

// Original Definition: 
// struct VkPhysicalDeviceToolPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     char name[(int)ApiConstants.MaxExtensionNameSize];
//     char version[(int)ApiConstants.MaxExtensionNameSize];
//     VkToolPurposeFlagsEXT purposes;
//     char description[(int)ApiConstants.MaxDescriptionSize];
//     char layer[(int)ApiConstants.MaxExtensionNameSize];
// }
    public unsafe struct PhysicalDeviceToolPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public fixed sbyte Name_[(int) ApiConstants.MaxExtensionNameSize];
        public fixed sbyte Version_[(int) ApiConstants.MaxExtensionNameSize];
        public ToolPurposeFlagsExt Purposes;
        public fixed sbyte Description_[(int) ApiConstants.MaxDescriptionSize];
        public fixed sbyte Layer_[(int) ApiConstants.MaxExtensionNameSize];

        public string Name
        {
            get
            {
                fixed (sbyte* ptr = Name_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string Version
        {
            get
            {
                fixed (sbyte* ptr = Version_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string Description
        {
            get
            {
                fixed (sbyte* ptr = Description_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }

        public string Layer
        {
            get
            {
                fixed (sbyte* ptr = Layer_)
                    return Marshal.PtrToStringAnsi((IntPtr) ptr);
            }
        }
    }

// Original Definition: 
// struct VkSamplerCustomBorderColorCreateInfoEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkClearColorValue customBorderColor;
//     VkFormat format;
// }
    public unsafe struct SamplerCustomBorderColorCreateInfoExt
    {
        public StructureType SType;
        public void* PNext;
        public ClearColorValue CustomBorderColor;
        public Format Format;
    }

// Original Definition: 
// struct VkPhysicalDeviceCustomBorderColorPropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxCustomBorderColorSamplers;
// }
    public unsafe struct PhysicalDeviceCustomBorderColorPropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxCustomBorderColorSamplers;
    }

// Original Definition: 
// struct VkPhysicalDeviceCustomBorderColorFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 customBorderColors;
//     VkBool32 customBorderColorWithoutFormat;
// }
    public unsafe struct PhysicalDeviceCustomBorderColorFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint CustomBorderColors;
        public uint CustomBorderColorWithoutFormat;
    }

    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct DeviceOrHostAddressKhr
    {
        [FieldOffset(0)] public ulong DeviceAddress;
        [FieldOffset(0)] public void* HostAddress;
    }

    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct DeviceOrHostAddressConstKhr
    {
        [FieldOffset(0)] public ulong DeviceAddress;
        [FieldOffset(0)] public void* HostAddress;
    }

// Original Definition: 
// struct VkAccelerationStructureGeometryTrianglesDataKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkFormat vertexFormat;
//     VkDeviceOrHostAddressConstKHR vertexData;
//     VkDeviceSize vertexStride;
//     VkIndexType indexType;
//     VkDeviceOrHostAddressConstKHR indexData;
//     VkDeviceOrHostAddressConstKHR transformData;
// }
    public unsafe struct AccelerationStructureGeometryTrianglesDataKhr
    {
        public StructureType SType;
        public void* PNext;
        public Format VertexFormat;
        public DeviceOrHostAddressConstKhr VertexData;
        public ulong VertexStride;
        public IndexType IndexType;
        public DeviceOrHostAddressConstKhr IndexData;
        public DeviceOrHostAddressConstKhr TransformData;
    }

// Original Definition: 
// struct VkAccelerationStructureGeometryAabbsDataKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceOrHostAddressConstKHR data;
//     VkDeviceSize stride;
// }
    public unsafe struct AccelerationStructureGeometryAabbsDataKhr
    {
        public StructureType SType;
        public void* PNext;
        public DeviceOrHostAddressConstKhr Data;
        public ulong Stride;
    }

// Original Definition: 
// struct VkAccelerationStructureGeometryInstancesDataKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 arrayOfPointers;
//     VkDeviceOrHostAddressConstKHR data;
// }
    public unsafe struct AccelerationStructureGeometryInstancesDataKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint ArrayOfPointers;
        public DeviceOrHostAddressConstKhr Data;
    }

    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct AccelerationStructureGeometryDataKhr
    {
        [FieldOffset(0)] public AccelerationStructureGeometryTrianglesDataKhr Triangles;
        [FieldOffset(0)] public AccelerationStructureGeometryAabbsDataKhr Aabbs;
        [FieldOffset(0)] public AccelerationStructureGeometryInstancesDataKhr Instances;
    }

// Original Definition: 
// struct VkAccelerationStructureGeometryKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkGeometryTypeKHR geometryType;
//     VkAccelerationStructureGeometryDataKHR geometry;
//     VkGeometryFlagsKHR flags;
// }
    public unsafe struct AccelerationStructureGeometryKhr
    {
        public StructureType SType;
        public void* PNext;
        public GeometryTypeKhr GeometryType;
        public AccelerationStructureGeometryDataKhr Geometry;
        public GeometryFlagsKhr Flags;
    }

// Original Definition: 
// struct VkAccelerationStructureBuildGeometryInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccelerationStructureTypeKHR type;
//     VkBuildAccelerationStructureFlagsKHR flags;
//     VkBool32 update;
//     VkAccelerationStructureKHR srcAccelerationStructure;
//     VkAccelerationStructureKHR dstAccelerationStructure;
//     VkBool32 geometryArrayOfPointers;
//     uint32_t geometryCount;
//     VkAccelerationStructureGeometryKHR* const* ppGeometries;
//     VkDeviceOrHostAddressKHR scratchData;
// }
    public unsafe struct AccelerationStructureBuildGeometryInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public AccelerationStructureTypeKhr Type;
        public BuildAccelerationStructureFlagsKhr Flags;
        public uint Update;
        public AccelerationStructureKhr SrcAccelerationStructure;
        public AccelerationStructureKhr DstAccelerationStructure;
        public uint GeometryArrayOfPointers;
        public uint GeometryCount;
        public AccelerationStructureGeometryKhr* PpGeometries;
        public DeviceOrHostAddressKhr ScratchData;
    }

// Original Definition: 
// struct VkAccelerationStructureBuildOffsetInfoKHR 
// {
//     uint32_t primitiveCount;
//     uint32_t primitiveOffset;
//     uint32_t firstVertex;
//     uint32_t transformOffset;
// }
    public unsafe struct AccelerationStructureBuildOffsetInfoKhr
    {
        public uint PrimitiveCount;
        public uint PrimitiveOffset;
        public uint FirstVertex;
        public uint TransformOffset;
    }

// Original Definition: 
// struct VkAccelerationStructureCreateGeometryTypeInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkGeometryTypeKHR geometryType;
//     uint32_t maxPrimitiveCount;
//     VkIndexType indexType;
//     uint32_t maxVertexCount;
//     VkFormat vertexFormat;
//     VkBool32 allowsTransforms;
// }
    public unsafe struct AccelerationStructureCreateGeometryTypeInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public GeometryTypeKhr GeometryType;
        public uint MaxPrimitiveCount;
        public IndexType IndexType;
        public uint MaxVertexCount;
        public Format VertexFormat;
        public uint AllowsTransforms;
    }

// Original Definition: 
// struct VkAccelerationStructureCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceSize compactedSize;
//     VkAccelerationStructureTypeKHR type;
//     VkBuildAccelerationStructureFlagsKHR flags;
//     uint32_t maxGeometryCount;
//     VkAccelerationStructureCreateGeometryTypeInfoKHR* pGeometryInfos;
//     VkDeviceAddress deviceAddress;
// }
    public unsafe struct AccelerationStructureCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public ulong CompactedSize;
        public AccelerationStructureTypeKhr Type;
        public BuildAccelerationStructureFlagsKhr Flags;
        public uint MaxGeometryCount;
        public AccelerationStructureCreateGeometryTypeInfoKhr* PGeometryInfos;
        public ulong DeviceAddress;
    }

// Original Definition: 
// struct VkAabbPositionsKHR 
// {
//     float minX;
//     float minY;
//     float minZ;
//     float maxX;
//     float maxY;
//     float maxZ;
// }
    public unsafe struct AabbPositionsKhr
    {
        public float MinX;
        public float MinY;
        public float MinZ;
        public float MaxX;
        public float MaxY;
        public float MaxZ;
    }
// Original Definition: 
// struct VkAabbPositionsNV 
// {

// }
    public unsafe struct AabbPositionsNv
    {
    }

// Original Definition: 
// struct VkTransformMatrixKHR 
// {
//     float matrix;
// }
    public unsafe struct TransformMatrixKhr
    {
        public float Matrix;
    }
// Original Definition: 
// struct VkTransformMatrixNV 
// {

// }
    public unsafe struct TransformMatrixNv
    {
    }

// Original Definition: 
// struct VkAccelerationStructureInstanceKHR 
// {
//     VkTransformMatrixKHR transform;
//     uint32_t instanceCustomIndex;
//     uint32_t mask;
//     uint32_t instanceShaderBindingTableRecordOffset;
//     VkGeometryInstanceFlagsKHR flags;
//     uint64_t accelerationStructureReference;
// }
    public unsafe struct AccelerationStructureInstanceKhr
    {
        public TransformMatrixKhr Transform;
        public uint InstanceCustomIndex;
        public uint Mask;
        public uint InstanceShaderBindingTableRecordOffset;
        public GeometryInstanceFlagsKhr Flags;
        public ulong AccelerationStructureReference;
    }
// Original Definition: 
// struct VkAccelerationStructureInstanceNV 
// {

// }
    public unsafe struct AccelerationStructureInstanceNv
    {
    }

// Original Definition: 
// struct VkAccelerationStructureDeviceAddressInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccelerationStructureKHR accelerationStructure;
// }
    public unsafe struct AccelerationStructureDeviceAddressInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public AccelerationStructureKhr AccelerationStructure;
    }

// Original Definition: 
// struct VkAccelerationStructureVersionKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint8_t* versionData;
// }
    public unsafe struct AccelerationStructureVersionKhr
    {
        public StructureType SType;
        public void* PNext;
        public byte* VersionData;
    }

// Original Definition: 
// struct VkCopyAccelerationStructureInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccelerationStructureKHR src;
//     VkAccelerationStructureKHR dst;
//     VkCopyAccelerationStructureModeKHR mode;
// }
    public unsafe struct CopyAccelerationStructureInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public AccelerationStructureKhr Src;
        public AccelerationStructureKhr Dst;
        public CopyAccelerationStructureModeKhr Mode;
    }

// Original Definition: 
// struct VkCopyAccelerationStructureToMemoryInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkAccelerationStructureKHR src;
//     VkDeviceOrHostAddressKHR dst;
//     VkCopyAccelerationStructureModeKHR mode;
// }
    public unsafe struct CopyAccelerationStructureToMemoryInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public AccelerationStructureKhr Src;
        public DeviceOrHostAddressKhr Dst;
        public CopyAccelerationStructureModeKhr Mode;
    }

// Original Definition: 
// struct VkCopyMemoryToAccelerationStructureInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceOrHostAddressConstKHR src;
//     VkAccelerationStructureKHR dst;
//     VkCopyAccelerationStructureModeKHR mode;
// }
    public unsafe struct CopyMemoryToAccelerationStructureInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public DeviceOrHostAddressConstKhr Src;
        public AccelerationStructureKhr Dst;
        public CopyAccelerationStructureModeKhr Mode;
    }

// Original Definition: 
// struct VkRayTracingPipelineInterfaceCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t maxPayloadSize;
//     uint32_t maxAttributeSize;
//     uint32_t maxCallableSize;
// }
    public unsafe struct RayTracingPipelineInterfaceCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint MaxPayloadSize;
        public uint MaxAttributeSize;
        public uint MaxCallableSize;
    }

// Original Definition: 
// struct VkDeferredOperationInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeferredOperationKHR operationHandle;
// }
    public unsafe struct DeferredOperationInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public DeferredOperationKhr OperationHandle;
    }

// Original Definition: 
// struct VkPipelineLibraryCreateInfoKHR 
// {
//     VkStructureType sType;
//     void* pNext;
//     uint32_t libraryCount;
//     VkPipeline* pLibraries;
// }
    public unsafe struct PipelineLibraryCreateInfoKhr
    {
        public StructureType SType;
        public void* PNext;
        public uint LibraryCount;
        public Pipeline* PLibraries;
    }

// Original Definition: 
// struct VkPhysicalDeviceExtendedDynamicStateFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 extendedDynamicState;
// }
    public unsafe struct PhysicalDeviceExtendedDynamicStateFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint ExtendedDynamicState;
    }

// Original Definition: 
// struct VkRenderPassTransformBeginInfoQCOM 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSurfaceTransformFlagBitsKHR transform;
// }
    public unsafe struct RenderPassTransformBeginInfoQcom
    {
        public StructureType SType;
        public void* PNext;
        public SurfaceTransformFlagsKhr Transform;
    }

// Original Definition: 
// struct VkCommandBufferInheritanceRenderPassTransformInfoQCOM 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkSurfaceTransformFlagBitsKHR transform;
//     VkRect2D renderArea;
// }
    public unsafe struct CommandBufferInheritanceRenderPassTransformInfoQcom
    {
        public StructureType SType;
        public void* PNext;
        public SurfaceTransformFlagsKhr Transform;
        public Rect2d RenderArea;
    }

// Original Definition: 
// struct VkPhysicalDeviceDiagnosticsConfigFeaturesNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 diagnosticsConfig;
// }
    public unsafe struct PhysicalDeviceDiagnosticsConfigFeaturesNv
    {
        public StructureType SType;
        public void* PNext;
        public uint DiagnosticsConfig;
    }

// Original Definition: 
// struct VkDeviceDiagnosticsConfigCreateInfoNV 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceDiagnosticsConfigFlagsNV flags;
// }
    public unsafe struct DeviceDiagnosticsConfigCreateInfoNv
    {
        public StructureType SType;
        public void* PNext;
        public DeviceDiagnosticsConfigFlagsNv Flags;
    }

// Original Definition: 
// struct VkPhysicalDeviceRobustness2FeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 robustBufferAccess2;
//     VkBool32 robustImageAccess2;
//     VkBool32 nullDescriptor;
// }
    public unsafe struct PhysicalDeviceRobustness2FeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint RobustBufferAccess2;
        public uint RobustImageAccess2;
        public uint NullDescriptor;
    }

// Original Definition: 
// struct VkPhysicalDeviceRobustness2PropertiesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkDeviceSize robustStorageBufferAccessSizeAlignment;
//     VkDeviceSize robustUniformBufferAccessSizeAlignment;
// }
    public unsafe struct PhysicalDeviceRobustness2PropertiesExt
    {
        public StructureType SType;
        public void* PNext;
        public ulong RobustStorageBufferAccessSizeAlignment;
        public ulong RobustUniformBufferAccessSizeAlignment;
    }

// Original Definition: 
// struct VkPhysicalDeviceImageRobustnessFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 robustImageAccess;
// }
    public unsafe struct PhysicalDeviceImageRobustnessFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint RobustImageAccess;
    }

// Original Definition: 
// struct VkPhysicalDevice4444FormatsFeaturesEXT 
// {
//     VkStructureType sType;
//     void* pNext;
//     VkBool32 formatA4R4G4B4;
//     VkBool32 formatA4B4G4R4;
// }
    public unsafe struct PhysicalDevice4444FormatsFeaturesExt
    {
        public StructureType SType;
        public void* PNext;
        public uint FormatA4R4G4B4;
        public uint FormatA4B4G4R4;
    }
}
