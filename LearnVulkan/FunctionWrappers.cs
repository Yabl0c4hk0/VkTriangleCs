using System;
using LearnVulkan.Vulkan.Binding.Native;
using Buffer = LearnVulkan.Vulkan.Binding.Native.Buffer;

namespace LearnVulkan
{
    public static unsafe class Vkh
    {
        public static PhysicalDevice[] EnumeratePhysicalDevices(Instance instance)
        {
            uint count;
            Vk.EnumeratePhysicalDevices(instance, &count, null);
            var values = new PhysicalDevice[count];
            fixed (PhysicalDevice* ptr = values)
                Vk.EnumeratePhysicalDevices(instance, &count, ptr);
            return values;
        }

        public static QueueFamilyProperties[] GetPhysicalDeviceQueueFamilyProperties(PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceQueueFamilyProperties(physicalDevice, &count, null);
            var values = new QueueFamilyProperties[count];
            fixed (QueueFamilyProperties* ptr = values)
                Vk.GetPhysicalDeviceQueueFamilyProperties(physicalDevice, &count, ptr);
            return values;
        }

        public static LayerProperties[] EnumerateInstanceLayerProperties()
        {
            uint count;
            Vk.EnumerateInstanceLayerProperties(&count, null);
            var values = new LayerProperties[count];
            fixed (LayerProperties* ptr = values)
                Vk.EnumerateInstanceLayerProperties(&count, ptr);
            return values;
        }

        public static ExtensionProperties[] EnumerateInstanceExtensionProperties(sbyte* pLayerName)
        {
            uint count;
            Vk.EnumerateInstanceExtensionProperties(pLayerName, &count, null);
            var values = new ExtensionProperties[count];
            fixed (ExtensionProperties* ptr = values)
                Vk.EnumerateInstanceExtensionProperties(pLayerName, &count, ptr);
            return values;
        }

        public static LayerProperties[] EnumerateDeviceLayerProperties(PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.EnumerateDeviceLayerProperties(physicalDevice, &count, null);
            var values = new LayerProperties[count];
            fixed (LayerProperties* ptr = values)
                Vk.EnumerateDeviceLayerProperties(physicalDevice, &count, ptr);
            return values;
        }

        public static ExtensionProperties[] EnumerateDeviceExtensionProperties(PhysicalDevice physicalDevice,
            sbyte* pLayerName)
        {
            uint count;
            Vk.EnumerateDeviceExtensionProperties(physicalDevice, pLayerName, &count, null);
            var values = new ExtensionProperties[count];
            fixed (ExtensionProperties* ptr = values)
                Vk.EnumerateDeviceExtensionProperties(physicalDevice, pLayerName, &count, ptr);
            return values;
        }

        public static SparseImageMemoryRequirements[] GetImageSparseMemoryRequirements(Device device, Image image)
        {
            uint count;
            Vk.GetImageSparseMemoryRequirements(device, image, &count, null);
            var values = new SparseImageMemoryRequirements[count];
            fixed (SparseImageMemoryRequirements* ptr = values)
                Vk.GetImageSparseMemoryRequirements(device, image, &count, ptr);
            return values;
        }

        public static SparseImageFormatProperties[] GetPhysicalDeviceSparseImageFormatProperties(
            PhysicalDevice physicalDevice, Format format, ImageType type, SampleCountFlags samples,
            ImageUsageFlags usage, ImageTiling tiling)
        {
            uint count;
            Vk.GetPhysicalDeviceSparseImageFormatProperties(physicalDevice, format, type, samples, usage, tiling,
                &count, null);
            var values = new SparseImageFormatProperties[count];
            fixed (SparseImageFormatProperties* ptr = values)
                Vk.GetPhysicalDeviceSparseImageFormatProperties(physicalDevice, format, type, samples, usage, tiling,
                    &count, ptr);
            return values;
        }

        public static DisplayPropertiesKhr[] GetPhysicalDeviceDisplayPropertiesKhr(PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceDisplayPropertiesKhr(physicalDevice, &count, null);
            var values = new DisplayPropertiesKhr[count];
            fixed (DisplayPropertiesKhr* ptr = values)
                Vk.GetPhysicalDeviceDisplayPropertiesKhr(physicalDevice, &count, ptr);
            return values;
        }

        public static DisplayPlanePropertiesKhr[] GetPhysicalDeviceDisplayPlanePropertiesKhr(
            PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceDisplayPlanePropertiesKhr(physicalDevice, &count, null);
            var values = new DisplayPlanePropertiesKhr[count];
            fixed (DisplayPlanePropertiesKhr* ptr = values)
                Vk.GetPhysicalDeviceDisplayPlanePropertiesKhr(physicalDevice, &count, ptr);
            return values;
        }

        public static DisplayKhr[] GetDisplayPlaneSupportedDisplaysKhr(PhysicalDevice physicalDevice, uint planeIndex)
        {
            uint count;
            Vk.GetDisplayPlaneSupportedDisplaysKhr(physicalDevice, planeIndex, &count, null);
            var values = new DisplayKhr[count];
            fixed (DisplayKhr* ptr = values)
                Vk.GetDisplayPlaneSupportedDisplaysKhr(physicalDevice, planeIndex, &count, ptr);
            return values;
        }

        public static DisplayModePropertiesKhr[] GetDisplayModePropertiesKhr(PhysicalDevice physicalDevice,
            DisplayKhr display)
        {
            uint count;
            Vk.GetDisplayModePropertiesKhr(physicalDevice, display, &count, null);
            var values = new DisplayModePropertiesKhr[count];
            fixed (DisplayModePropertiesKhr* ptr = values)
                Vk.GetDisplayModePropertiesKhr(physicalDevice, display, &count, ptr);
            return values;
        }

        public static SurfaceFormatKhr[] GetPhysicalDeviceSurfaceFormatsKhr(PhysicalDevice physicalDevice,
            SurfaceKhr surface)
        {
            uint count;
            Vk.GetPhysicalDeviceSurfaceFormatsKhr(physicalDevice, surface, &count, null);
            var values = new SurfaceFormatKhr[count];
            fixed (SurfaceFormatKhr* ptr = values)
                Vk.GetPhysicalDeviceSurfaceFormatsKhr(physicalDevice, surface, &count, ptr);
            return values;
        }

        public static PresentModeKhr[] GetPhysicalDeviceSurfacePresentModesKhr(PhysicalDevice physicalDevice,
            SurfaceKhr surface)
        {
            uint count;
            Vk.GetPhysicalDeviceSurfacePresentModesKhr(physicalDevice, surface, &count, null);
            var values = new PresentModeKhr[count];
            fixed (PresentModeKhr* ptr = values)
                Vk.GetPhysicalDeviceSurfacePresentModesKhr(physicalDevice, surface, &count, ptr);
            return values;
        }

        public static Image[] GetSwapchainImagesKhr(Device device, SwapchainKhr swapchain)
        {
            uint count;
            Vk.GetSwapchainImagesKhr(device, swapchain, &count, null);
            var values = new Image[count];
            fixed (Image* ptr = values)
                Vk.GetSwapchainImagesKhr(device, swapchain, &count, ptr);
            return values;
        }

        public static QueueFamilyProperties2[] GetPhysicalDeviceQueueFamilyProperties2(PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceQueueFamilyProperties2(physicalDevice, &count, null);
            var values = new QueueFamilyProperties2[count];
            fixed (QueueFamilyProperties2* ptr = values)
                Vk.GetPhysicalDeviceQueueFamilyProperties2(physicalDevice, &count, ptr);
            return values;
        }

        public static SparseImageFormatProperties2[] GetPhysicalDeviceSparseImageFormatProperties2(
            PhysicalDevice physicalDevice, PhysicalDeviceSparseImageFormatInfo2* pFormatInfo)
        {
            uint count;
            Vk.GetPhysicalDeviceSparseImageFormatProperties2(physicalDevice, pFormatInfo, &count, null);
            var values = new SparseImageFormatProperties2[count];
            fixed (SparseImageFormatProperties2* ptr = values)
                Vk.GetPhysicalDeviceSparseImageFormatProperties2(physicalDevice, pFormatInfo, &count, ptr);
            return values;
        }

        public static PhysicalDeviceGroupProperties[] EnumeratePhysicalDeviceGroups(Instance instance)
        {
            uint count;
            Vk.EnumeratePhysicalDeviceGroups(instance, &count, null);
            var values = new PhysicalDeviceGroupProperties[count];
            fixed (PhysicalDeviceGroupProperties* ptr = values)
                Vk.EnumeratePhysicalDeviceGroups(instance, &count, ptr);
            return values;
        }

        public static Rect2d[] GetPhysicalDevicePresentRectanglesKhr(PhysicalDevice physicalDevice, SurfaceKhr surface)
        {
            uint count;
            Vk.GetPhysicalDevicePresentRectanglesKhr(physicalDevice, surface, &count, null);
            var values = new Rect2d[count];
            fixed (Rect2d* ptr = values)
                Vk.GetPhysicalDevicePresentRectanglesKhr(physicalDevice, surface, &count, ptr);
            return values;
        }

        public static PastPresentationTimingGoogle[] GetPastPresentationTimingGoogle(Device device,
            SwapchainKhr swapchain)
        {
            uint count;
            Vk.GetPastPresentationTimingGoogle(device, swapchain, &count, null);
            var values = new PastPresentationTimingGoogle[count];
            fixed (PastPresentationTimingGoogle* ptr = values)
                Vk.GetPastPresentationTimingGoogle(device, swapchain, &count, ptr);
            return values;
        }

        public static SurfaceFormat2khr[] GetPhysicalDeviceSurfaceFormats2khr(PhysicalDevice physicalDevice,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo)
        {
            uint count;
            Vk.GetPhysicalDeviceSurfaceFormats2khr(physicalDevice, pSurfaceInfo, &count, null);
            var values = new SurfaceFormat2khr[count];
            fixed (SurfaceFormat2khr* ptr = values)
                Vk.GetPhysicalDeviceSurfaceFormats2khr(physicalDevice, pSurfaceInfo, &count, ptr);
            return values;
        }

        public static DisplayProperties2khr[] GetPhysicalDeviceDisplayProperties2khr(PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceDisplayProperties2khr(physicalDevice, &count, null);
            var values = new DisplayProperties2khr[count];
            fixed (DisplayProperties2khr* ptr = values)
                Vk.GetPhysicalDeviceDisplayProperties2khr(physicalDevice, &count, ptr);
            return values;
        }

        public static DisplayPlaneProperties2khr[] GetPhysicalDeviceDisplayPlaneProperties2khr(
            PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceDisplayPlaneProperties2khr(physicalDevice, &count, null);
            var values = new DisplayPlaneProperties2khr[count];
            fixed (DisplayPlaneProperties2khr* ptr = values)
                Vk.GetPhysicalDeviceDisplayPlaneProperties2khr(physicalDevice, &count, ptr);
            return values;
        }

        public static DisplayModeProperties2khr[] GetDisplayModeProperties2khr(PhysicalDevice physicalDevice,
            DisplayKhr display)
        {
            uint count;
            Vk.GetDisplayModeProperties2khr(physicalDevice, display, &count, null);
            var values = new DisplayModeProperties2khr[count];
            fixed (DisplayModeProperties2khr* ptr = values)
                Vk.GetDisplayModeProperties2khr(physicalDevice, display, &count, ptr);
            return values;
        }

        public static SparseImageMemoryRequirements2[] GetImageSparseMemoryRequirements2(Device device,
            ImageSparseMemoryRequirementsInfo2* pInfo)
        {
            uint count;
            Vk.GetImageSparseMemoryRequirements2(device, pInfo, &count, null);
            var values = new SparseImageMemoryRequirements2[count];
            fixed (SparseImageMemoryRequirements2* ptr = values)
                Vk.GetImageSparseMemoryRequirements2(device, pInfo, &count, ptr);
            return values;
        }

        public static TimeDomainExt[] GetPhysicalDeviceCalibrateableTimeDomainsExt(PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceCalibrateableTimeDomainsExt(physicalDevice, &count, null);
            var values = new TimeDomainExt[count];
            fixed (TimeDomainExt* ptr = values)
                Vk.GetPhysicalDeviceCalibrateableTimeDomainsExt(physicalDevice, &count, ptr);
            return values;
        }

        public static CooperativeMatrixPropertiesNv[] GetPhysicalDeviceCooperativeMatrixPropertiesNv(
            PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceCooperativeMatrixPropertiesNv(physicalDevice, &count, null);
            var values = new CooperativeMatrixPropertiesNv[count];
            fixed (CooperativeMatrixPropertiesNv* ptr = values)
                Vk.GetPhysicalDeviceCooperativeMatrixPropertiesNv(physicalDevice, &count, ptr);
            return values;
        }

        public static PresentModeKhr[] GetPhysicalDeviceSurfacePresentModes2ext(PhysicalDevice physicalDevice,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo)
        {
            uint count;
            Vk.GetPhysicalDeviceSurfacePresentModes2ext(physicalDevice, pSurfaceInfo, &count, null);
            var values = new PresentModeKhr[count];
            fixed (PresentModeKhr* ptr = values)
                Vk.GetPhysicalDeviceSurfacePresentModes2ext(physicalDevice, pSurfaceInfo, &count, ptr);
            return values;
        }

        public static PerformanceCounterDescriptionKhr[] EnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKhr(
            PhysicalDevice physicalDevice, uint queueFamilyIndex, PerformanceCounterKhr* pCounters)
        {
            uint count;
            Vk.EnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKhr(physicalDevice, queueFamilyIndex, &count,
                pCounters, null);
            var values = new PerformanceCounterDescriptionKhr[count];
            fixed (PerformanceCounterDescriptionKhr* ptr = values)
                Vk.EnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKhr(physicalDevice, queueFamilyIndex,
                    &count, pCounters, ptr);
            return values;
        }

        public static FramebufferMixedSamplesCombinationNv[]
            GetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNv(PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNv(physicalDevice, &count, null);
            var values = new FramebufferMixedSamplesCombinationNv[count];
            fixed (FramebufferMixedSamplesCombinationNv* ptr = values)
                Vk.GetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNv(physicalDevice, &count, ptr);
            return values;
        }

        public static PipelineExecutablePropertiesKhr[] GetPipelineExecutablePropertiesKhr(Device device,
            PipelineInfoKhr* pPipelineInfo)
        {
            uint count;
            Vk.GetPipelineExecutablePropertiesKhr(device, pPipelineInfo, &count, null);
            var values = new PipelineExecutablePropertiesKhr[count];
            fixed (PipelineExecutablePropertiesKhr* ptr = values)
                Vk.GetPipelineExecutablePropertiesKhr(device, pPipelineInfo, &count, ptr);
            return values;
        }

        public static PipelineExecutableStatisticKhr[] GetPipelineExecutableStatisticsKhr(Device device,
            PipelineExecutableInfoKhr* pExecutableInfo)
        {
            uint count;
            Vk.GetPipelineExecutableStatisticsKhr(device, pExecutableInfo, &count, null);
            var values = new PipelineExecutableStatisticKhr[count];
            fixed (PipelineExecutableStatisticKhr* ptr = values)
                Vk.GetPipelineExecutableStatisticsKhr(device, pExecutableInfo, &count, ptr);
            return values;
        }

        public static PipelineExecutableInternalRepresentationKhr[] GetPipelineExecutableInternalRepresentationsKhr(
            Device device, PipelineExecutableInfoKhr* pExecutableInfo)
        {
            uint count;
            Vk.GetPipelineExecutableInternalRepresentationsKhr(device, pExecutableInfo, &count, null);
            var values = new PipelineExecutableInternalRepresentationKhr[count];
            fixed (PipelineExecutableInternalRepresentationKhr* ptr = values)
                Vk.GetPipelineExecutableInternalRepresentationsKhr(device, pExecutableInfo, &count, ptr);
            return values;
        }

        public static PhysicalDeviceToolPropertiesExt[] GetPhysicalDeviceToolPropertiesExt(
            PhysicalDevice physicalDevice)
        {
            uint count;
            Vk.GetPhysicalDeviceToolPropertiesExt(physicalDevice, &count, null);
            var values = new PhysicalDeviceToolPropertiesExt[count];
            fixed (PhysicalDeviceToolPropertiesExt* ptr = values)
                Vk.GetPhysicalDeviceToolPropertiesExt(physicalDevice, &count, ptr);
            return values;
        }

        public static PhysicalDeviceProperties GetPhysicalDeviceProperties(PhysicalDevice physicalDevice)
        {
            PhysicalDeviceProperties value;
            Vk.GetPhysicalDeviceProperties(physicalDevice, &value);
            return value;
        }

        public static QueueFamilyProperties GetPhysicalDeviceQueueFamilyProperties(PhysicalDevice physicalDevice,
            uint* pQueueFamilyPropertyCount)
        {
            QueueFamilyProperties value;
            Vk.GetPhysicalDeviceQueueFamilyProperties(physicalDevice, pQueueFamilyPropertyCount, &value);
            return value;
        }

        public static PhysicalDeviceMemoryProperties GetPhysicalDeviceMemoryProperties(PhysicalDevice physicalDevice)
        {
            PhysicalDeviceMemoryProperties value;
            Vk.GetPhysicalDeviceMemoryProperties(physicalDevice, &value);
            return value;
        }

        public static PhysicalDeviceFeatures GetPhysicalDeviceFeatures(PhysicalDevice physicalDevice)
        {
            PhysicalDeviceFeatures value;
            Vk.GetPhysicalDeviceFeatures(physicalDevice, &value);
            return value;
        }

        public static FormatProperties GetPhysicalDeviceFormatProperties(PhysicalDevice physicalDevice, Format format)
        {
            FormatProperties value;
            Vk.GetPhysicalDeviceFormatProperties(physicalDevice, format, &value);
            return value;
        }

        public static ImageFormatProperties GetPhysicalDeviceImageFormatProperties(PhysicalDevice physicalDevice,
            Format format, ImageType type, ImageTiling tiling, ImageUsageFlags usage, ImageCreateFlags flags)
        {
            ImageFormatProperties value;
            Vk.GetPhysicalDeviceImageFormatProperties(physicalDevice, format, type, tiling, usage, flags, &value);
            return value;
        }

        public static Queue GetDeviceQueue(Device device, uint queueFamilyIndex, uint queueIndex)
        {
            Queue value;
            Vk.GetDeviceQueue(device, queueFamilyIndex, queueIndex, &value);
            return value;
        }

        public static ulong GetDeviceMemoryCommitment(Device device, DeviceMemory memory)
        {
            ulong value;
            Vk.GetDeviceMemoryCommitment(device, memory, &value);
            return value;
        }

        public static MemoryRequirements GetBufferMemoryRequirements(Device device, Buffer buffer)
        {
            MemoryRequirements value;
            Vk.GetBufferMemoryRequirements(device, buffer, &value);
            return value;
        }

        public static MemoryRequirements GetImageMemoryRequirements(Device device, Image image)
        {
            MemoryRequirements value;
            Vk.GetImageMemoryRequirements(device, image, &value);
            return value;
        }

        public static SparseImageMemoryRequirements GetImageSparseMemoryRequirements(Device device, Image image,
            uint* pSparseMemoryRequirementCount)
        {
            SparseImageMemoryRequirements value;
            Vk.GetImageSparseMemoryRequirements(device, image, pSparseMemoryRequirementCount, &value);
            return value;
        }

        public static SparseImageFormatProperties GetPhysicalDeviceSparseImageFormatProperties(
            PhysicalDevice physicalDevice, Format format, ImageType type, SampleCountFlags samples,
            ImageUsageFlags usage, ImageTiling tiling, uint* pPropertyCount)
        {
            SparseImageFormatProperties value;
            Vk.GetPhysicalDeviceSparseImageFormatProperties(physicalDevice, format, type, samples, usage, tiling,
                pPropertyCount, &value);
            return value;
        }

        public static SubresourceLayout GetImageSubresourceLayout(Device device, Image image,
            ImageSubresource* pSubresource)
        {
            SubresourceLayout value;
            Vk.GetImageSubresourceLayout(device, image, pSubresource, &value);
            return value;
        }

        public static Extent2d GetRenderAreaGranularity(Device device, RenderPass renderPass)
        {
            Extent2d value;
            Vk.GetRenderAreaGranularity(device, renderPass, &value);
            return value;
        }

        public static DisplayPropertiesKhr GetPhysicalDeviceDisplayPropertiesKhr(PhysicalDevice physicalDevice,
            uint* pPropertyCount)
        {
            DisplayPropertiesKhr value;
            Vk.GetPhysicalDeviceDisplayPropertiesKhr(physicalDevice, pPropertyCount, &value);
            return value;
        }

        public static DisplayPlanePropertiesKhr GetPhysicalDeviceDisplayPlanePropertiesKhr(
            PhysicalDevice physicalDevice, uint* pPropertyCount)
        {
            DisplayPlanePropertiesKhr value;
            Vk.GetPhysicalDeviceDisplayPlanePropertiesKhr(physicalDevice, pPropertyCount, &value);
            return value;
        }

        public static DisplayKhr GetDisplayPlaneSupportedDisplaysKhr(PhysicalDevice physicalDevice, uint planeIndex,
            uint* pDisplayCount)
        {
            DisplayKhr value;
            Vk.GetDisplayPlaneSupportedDisplaysKhr(physicalDevice, planeIndex, pDisplayCount, &value);
            return value;
        }

        public static DisplayModePropertiesKhr GetDisplayModePropertiesKhr(PhysicalDevice physicalDevice,
            DisplayKhr display, uint* pPropertyCount)
        {
            DisplayModePropertiesKhr value;
            Vk.GetDisplayModePropertiesKhr(physicalDevice, display, pPropertyCount, &value);
            return value;
        }

        public static DisplayPlaneCapabilitiesKhr GetDisplayPlaneCapabilitiesKhr(PhysicalDevice physicalDevice,
            DisplayModeKhr mode, uint planeIndex)
        {
            DisplayPlaneCapabilitiesKhr value;
            Vk.GetDisplayPlaneCapabilitiesKhr(physicalDevice, mode, planeIndex, &value);
            return value;
        }

        public static uint GetPhysicalDeviceSurfaceSupportKhr(PhysicalDevice physicalDevice, uint queueFamilyIndex,
            SurfaceKhr surface)
        {
            uint value;
            Vk.GetPhysicalDeviceSurfaceSupportKhr(physicalDevice, queueFamilyIndex, surface, &value);
            return value;
        }

        public static SurfaceCapabilitiesKhr GetPhysicalDeviceSurfaceCapabilitiesKhr(PhysicalDevice physicalDevice,
            SurfaceKhr surface)
        {
            SurfaceCapabilitiesKhr value;
            Vk.GetPhysicalDeviceSurfaceCapabilitiesKhr(physicalDevice, surface, &value);
            return value;
        }

        public static SurfaceFormatKhr GetPhysicalDeviceSurfaceFormatsKhr(PhysicalDevice physicalDevice,
            SurfaceKhr surface, uint* pSurfaceFormatCount)
        {
            SurfaceFormatKhr value;
            Vk.GetPhysicalDeviceSurfaceFormatsKhr(physicalDevice, surface, pSurfaceFormatCount, &value);
            return value;
        }

        public static PresentModeKhr GetPhysicalDeviceSurfacePresentModesKhr(PhysicalDevice physicalDevice,
            SurfaceKhr surface, uint* pPresentModeCount)
        {
            PresentModeKhr value;
            Vk.GetPhysicalDeviceSurfacePresentModesKhr(physicalDevice, surface, pPresentModeCount, &value);
            return value;
        }

        public static Image GetSwapchainImagesKhr(Device device, SwapchainKhr swapchain, uint* pSwapchainImageCount)
        {
            Image value;
            Vk.GetSwapchainImagesKhr(device, swapchain, pSwapchainImageCount, &value);
            return value;
        }

        public static ExternalImageFormatPropertiesNv GetPhysicalDeviceExternalImageFormatPropertiesNv(
            PhysicalDevice physicalDevice, Format format, ImageType type, ImageTiling tiling, ImageUsageFlags usage,
            ImageCreateFlags flags, ExternalMemoryHandleTypeFlagsNv externalHandleType)
        {
            ExternalImageFormatPropertiesNv value;
            Vk.GetPhysicalDeviceExternalImageFormatPropertiesNv(physicalDevice, format, type, tiling, usage, flags,
                externalHandleType, &value);
            return value;
        }

        public static MemoryRequirements2 GetGeneratedCommandsMemoryRequirementsNv(Device device,
            GeneratedCommandsMemoryRequirementsInfoNv* pInfo)
        {
            MemoryRequirements2 value;
            Vk.GetGeneratedCommandsMemoryRequirementsNv(device, pInfo, &value);
            return value;
        }

        public static PhysicalDeviceFeatures2 GetPhysicalDeviceFeatures2(PhysicalDevice physicalDevice)
        {
            PhysicalDeviceFeatures2 value;
            Vk.GetPhysicalDeviceFeatures2(physicalDevice, &value);
            return value;
        }

        public static PhysicalDeviceProperties2 GetPhysicalDeviceProperties2(PhysicalDevice physicalDevice)
        {
            PhysicalDeviceProperties2 value;
            Vk.GetPhysicalDeviceProperties2(physicalDevice, &value);
            return value;
        }

        public static FormatProperties2 GetPhysicalDeviceFormatProperties2(PhysicalDevice physicalDevice, Format format)
        {
            FormatProperties2 value;
            Vk.GetPhysicalDeviceFormatProperties2(physicalDevice, format, &value);
            return value;
        }

        public static ImageFormatProperties2 GetPhysicalDeviceImageFormatProperties2(PhysicalDevice physicalDevice,
            PhysicalDeviceImageFormatInfo2* pImageFormatInfo)
        {
            ImageFormatProperties2 value;
            Vk.GetPhysicalDeviceImageFormatProperties2(physicalDevice, pImageFormatInfo, &value);
            return value;
        }

        public static QueueFamilyProperties2 GetPhysicalDeviceQueueFamilyProperties2(PhysicalDevice physicalDevice,
            uint* pQueueFamilyPropertyCount)
        {
            QueueFamilyProperties2 value;
            Vk.GetPhysicalDeviceQueueFamilyProperties2(physicalDevice, pQueueFamilyPropertyCount, &value);
            return value;
        }

        public static PhysicalDeviceMemoryProperties2 GetPhysicalDeviceMemoryProperties2(PhysicalDevice physicalDevice)
        {
            PhysicalDeviceMemoryProperties2 value;
            Vk.GetPhysicalDeviceMemoryProperties2(physicalDevice, &value);
            return value;
        }

        public static SparseImageFormatProperties2 GetPhysicalDeviceSparseImageFormatProperties2(
            PhysicalDevice physicalDevice, PhysicalDeviceSparseImageFormatInfo2* pFormatInfo, uint* pPropertyCount)
        {
            SparseImageFormatProperties2 value;
            Vk.GetPhysicalDeviceSparseImageFormatProperties2(physicalDevice, pFormatInfo, pPropertyCount, &value);
            return value;
        }

        public static ExternalBufferProperties GetPhysicalDeviceExternalBufferProperties(PhysicalDevice physicalDevice,
            PhysicalDeviceExternalBufferInfo* pExternalBufferInfo)
        {
            ExternalBufferProperties value;
            Vk.GetPhysicalDeviceExternalBufferProperties(physicalDevice, pExternalBufferInfo, &value);
            return value;
        }

        public static int GetMemoryFdKhr(Device device, MemoryGetFdInfoKhr* pGetFdInfo)
        {
            int value;
            Vk.GetMemoryFdKhr(device, pGetFdInfo, &value);
            return value;
        }

        public static MemoryFdPropertiesKhr GetMemoryFdPropertiesKhr(Device device,
            ExternalMemoryHandleTypeFlags handleType, int fd)
        {
            MemoryFdPropertiesKhr value;
            Vk.GetMemoryFdPropertiesKhr(device, handleType, fd, &value);
            return value;
        }

        public static ExternalSemaphoreProperties GetPhysicalDeviceExternalSemaphoreProperties(
            PhysicalDevice physicalDevice, PhysicalDeviceExternalSemaphoreInfo* pExternalSemaphoreInfo)
        {
            ExternalSemaphoreProperties value;
            Vk.GetPhysicalDeviceExternalSemaphoreProperties(physicalDevice, pExternalSemaphoreInfo, &value);
            return value;
        }

        public static int GetSemaphoreFdKhr(Device device, SemaphoreGetFdInfoKhr* pGetFdInfo)
        {
            int value;
            Vk.GetSemaphoreFdKhr(device, pGetFdInfo, &value);
            return value;
        }

        public static ExternalFenceProperties GetPhysicalDeviceExternalFenceProperties(PhysicalDevice physicalDevice,
            PhysicalDeviceExternalFenceInfo* pExternalFenceInfo)
        {
            ExternalFenceProperties value;
            Vk.GetPhysicalDeviceExternalFenceProperties(physicalDevice, pExternalFenceInfo, &value);
            return value;
        }

        public static int GetFenceFdKhr(Device device, FenceGetFdInfoKhr* pGetFdInfo)
        {
            int value;
            Vk.GetFenceFdKhr(device, pGetFdInfo, &value);
            return value;
        }

        public static ulong GetSwapchainCounterExt(Device device, SwapchainKhr swapchain,
            SurfaceCounterFlagsExt counter)
        {
            ulong value;
            Vk.GetSwapchainCounterExt(device, swapchain, counter, &value);
            return value;
        }

        public static SurfaceCapabilities2ext GetPhysicalDeviceSurfaceCapabilities2ext(PhysicalDevice physicalDevice,
            SurfaceKhr surface)
        {
            SurfaceCapabilities2ext value;
            Vk.GetPhysicalDeviceSurfaceCapabilities2ext(physicalDevice, surface, &value);
            return value;
        }

        public static PeerMemoryFeatureFlags GetDeviceGroupPeerMemoryFeatures(Device device, uint heapIndex,
            uint localDeviceIndex, uint remoteDeviceIndex)
        {
            PeerMemoryFeatureFlags value;
            Vk.GetDeviceGroupPeerMemoryFeatures(device, heapIndex, localDeviceIndex, remoteDeviceIndex, &value);
            return value;
        }

        public static DeviceGroupPresentCapabilitiesKhr GetDeviceGroupPresentCapabilitiesKhr(Device device)
        {
            DeviceGroupPresentCapabilitiesKhr value;
            Vk.GetDeviceGroupPresentCapabilitiesKhr(device, &value);
            return value;
        }

        public static DeviceGroupPresentModeFlagsKhr GetDeviceGroupSurfacePresentModesKhr(Device device,
            SurfaceKhr surface)
        {
            DeviceGroupPresentModeFlagsKhr value;
            Vk.GetDeviceGroupSurfacePresentModesKhr(device, surface, &value);
            return value;
        }

        public static Rect2d GetPhysicalDevicePresentRectanglesKhr(PhysicalDevice physicalDevice, SurfaceKhr surface,
            uint* pRectCount)
        {
            Rect2d value;
            Vk.GetPhysicalDevicePresentRectanglesKhr(physicalDevice, surface, pRectCount, &value);
            return value;
        }

        public static RefreshCycleDurationGoogle GetRefreshCycleDurationGoogle(Device device, SwapchainKhr swapchain)
        {
            RefreshCycleDurationGoogle value;
            Vk.GetRefreshCycleDurationGoogle(device, swapchain, &value);
            return value;
        }

        public static PastPresentationTimingGoogle GetPastPresentationTimingGoogle(Device device,
            SwapchainKhr swapchain, uint* pPresentationTimingCount)
        {
            PastPresentationTimingGoogle value;
            Vk.GetPastPresentationTimingGoogle(device, swapchain, pPresentationTimingCount, &value);
            return value;
        }

        public static MultisamplePropertiesExt GetPhysicalDeviceMultisamplePropertiesExt(PhysicalDevice physicalDevice,
            SampleCountFlags samples)
        {
            MultisamplePropertiesExt value;
            Vk.GetPhysicalDeviceMultisamplePropertiesExt(physicalDevice, samples, &value);
            return value;
        }

        public static SurfaceCapabilities2khr GetPhysicalDeviceSurfaceCapabilities2khr(PhysicalDevice physicalDevice,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo)
        {
            SurfaceCapabilities2khr value;
            Vk.GetPhysicalDeviceSurfaceCapabilities2khr(physicalDevice, pSurfaceInfo, &value);
            return value;
        }

        public static SurfaceFormat2khr GetPhysicalDeviceSurfaceFormats2khr(PhysicalDevice physicalDevice,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo, uint* pSurfaceFormatCount)
        {
            SurfaceFormat2khr value;
            Vk.GetPhysicalDeviceSurfaceFormats2khr(physicalDevice, pSurfaceInfo, pSurfaceFormatCount, &value);
            return value;
        }

        public static DisplayProperties2khr GetPhysicalDeviceDisplayProperties2khr(PhysicalDevice physicalDevice,
            uint* pPropertyCount)
        {
            DisplayProperties2khr value;
            Vk.GetPhysicalDeviceDisplayProperties2khr(physicalDevice, pPropertyCount, &value);
            return value;
        }

        public static DisplayPlaneProperties2khr GetPhysicalDeviceDisplayPlaneProperties2khr(
            PhysicalDevice physicalDevice, uint* pPropertyCount)
        {
            DisplayPlaneProperties2khr value;
            Vk.GetPhysicalDeviceDisplayPlaneProperties2khr(physicalDevice, pPropertyCount, &value);
            return value;
        }

        public static DisplayModeProperties2khr GetDisplayModeProperties2khr(PhysicalDevice physicalDevice,
            DisplayKhr display, uint* pPropertyCount)
        {
            DisplayModeProperties2khr value;
            Vk.GetDisplayModeProperties2khr(physicalDevice, display, pPropertyCount, &value);
            return value;
        }

        public static DisplayPlaneCapabilities2khr GetDisplayPlaneCapabilities2khr(PhysicalDevice physicalDevice,
            DisplayPlaneInfo2khr* pDisplayPlaneInfo)
        {
            DisplayPlaneCapabilities2khr value;
            Vk.GetDisplayPlaneCapabilities2khr(physicalDevice, pDisplayPlaneInfo, &value);
            return value;
        }

        public static MemoryRequirements2 GetBufferMemoryRequirements2(Device device,
            BufferMemoryRequirementsInfo2* pInfo)
        {
            MemoryRequirements2 value;
            Vk.GetBufferMemoryRequirements2(device, pInfo, &value);
            return value;
        }

        public static MemoryRequirements2 GetImageMemoryRequirements2(Device device,
            ImageMemoryRequirementsInfo2* pInfo)
        {
            MemoryRequirements2 value;
            Vk.GetImageMemoryRequirements2(device, pInfo, &value);
            return value;
        }

        public static SparseImageMemoryRequirements2 GetImageSparseMemoryRequirements2(Device device,
            ImageSparseMemoryRequirementsInfo2* pInfo, uint* pSparseMemoryRequirementCount)
        {
            SparseImageMemoryRequirements2 value;
            Vk.GetImageSparseMemoryRequirements2(device, pInfo, pSparseMemoryRequirementCount, &value);
            return value;
        }

        public static Queue GetDeviceQueue2(Device device, DeviceQueueInfo2* pQueueInfo)
        {
            Queue value;
            Vk.GetDeviceQueue2(device, pQueueInfo, &value);
            return value;
        }

        public static DescriptorSetLayoutSupport GetDescriptorSetLayoutSupport(Device device,
            DescriptorSetLayoutCreateInfo* pCreateInfo)
        {
            DescriptorSetLayoutSupport value;
            Vk.GetDescriptorSetLayoutSupport(device, pCreateInfo, &value);
            return value;
        }

        public static ulong GetSwapchainGrallocUsage2android(Device device, Format format, ImageUsageFlags imageUsage,
            SwapchainImageUsageFlagsAndroid swapchainImageUsage, ulong* grallocConsumerUsage)
        {
            ulong value;
            Vk.GetSwapchainGrallocUsage2android(device, format, imageUsage, swapchainImageUsage, grallocConsumerUsage,
                &value);
            return value;
        }

        public static TimeDomainExt GetPhysicalDeviceCalibrateableTimeDomainsExt(PhysicalDevice physicalDevice,
            uint* pTimeDomainCount)
        {
            TimeDomainExt value;
            Vk.GetPhysicalDeviceCalibrateableTimeDomainsExt(physicalDevice, pTimeDomainCount, &value);
            return value;
        }

        public static ulong GetCalibratedTimestampsExt(Device device, uint timestampCount,
            CalibratedTimestampInfoExt* pTimestampInfos, ulong* pTimestamps)
        {
            ulong value;
            Vk.GetCalibratedTimestampsExt(device, timestampCount, pTimestampInfos, pTimestamps, &value);
            return value;
        }

        public static MemoryHostPointerPropertiesExt GetMemoryHostPointerPropertiesExt(Device device,
            ExternalMemoryHandleTypeFlags handleType, void* pHostPointer)
        {
            MemoryHostPointerPropertiesExt value;
            Vk.GetMemoryHostPointerPropertiesExt(device, handleType, pHostPointer, &value);
            return value;
        }

        public static ulong GetSemaphoreCounterValue(Device device, Semaphore semaphore)
        {
            ulong value;
            Vk.GetSemaphoreCounterValue(device, semaphore, &value);
            return value;
        }

        public static AndroidHardwareBufferPropertiesAndroid GetAndroidHardwareBufferPropertiesAndroid(Device device,
            IntPtr buffer)
        {
            AndroidHardwareBufferPropertiesAndroid value;
            Vk.GetAndroidHardwareBufferPropertiesAndroid(device, buffer, &value);
            return value;
        }

        public static CheckpointDataNv GetQueueCheckpointDataNv(Queue queue, uint* pCheckpointDataCount)
        {
            CheckpointDataNv value;
            Vk.GetQueueCheckpointDataNv(queue, pCheckpointDataCount, &value);
            return value;
        }

        public static MemoryRequirements2 GetAccelerationStructureMemoryRequirementsKhr(Device device,
            AccelerationStructureMemoryRequirementsInfoKhr* pInfo)
        {
            MemoryRequirements2 value;
            Vk.GetAccelerationStructureMemoryRequirementsKhr(device, pInfo, &value);
            return value;
        }

        public static MemoryRequirements2khr GetAccelerationStructureMemoryRequirementsNv(Device device,
            AccelerationStructureMemoryRequirementsInfoNv* pInfo)
        {
            MemoryRequirements2khr value;
            Vk.GetAccelerationStructureMemoryRequirementsNv(device, pInfo, &value);
            return value;
        }

        public static CooperativeMatrixPropertiesNv GetPhysicalDeviceCooperativeMatrixPropertiesNv(
            PhysicalDevice physicalDevice, uint* pPropertyCount)
        {
            CooperativeMatrixPropertiesNv value;
            Vk.GetPhysicalDeviceCooperativeMatrixPropertiesNv(physicalDevice, pPropertyCount, &value);
            return value;
        }

        public static AccelerationStructureVersionKhr GetDeviceAccelerationStructureCompatibilityKhr(Device device)
        {
            AccelerationStructureVersionKhr value;
            Vk.GetDeviceAccelerationStructureCompatibilityKhr(device, &value);
            return value;
        }

        public static ImageViewHandleInfoNvx GetImageViewHandleNvx(Device device)
        {
            ImageViewHandleInfoNvx value;
            Vk.GetImageViewHandleNvx(device, &value);
            return value;
        }

        public static ImageViewAddressPropertiesNvx GetImageViewAddressNvx(Device device, ImageView imageView)
        {
            ImageViewAddressPropertiesNvx value;
            Vk.GetImageViewAddressNvx(device, imageView, &value);
            return value;
        }

        public static PresentModeKhr GetPhysicalDeviceSurfacePresentModes2ext(PhysicalDevice physicalDevice,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo, uint* pPresentModeCount)
        {
            PresentModeKhr value;
            Vk.GetPhysicalDeviceSurfacePresentModes2ext(physicalDevice, pSurfaceInfo, pPresentModeCount, &value);
            return value;
        }

        public static DeviceGroupPresentModeFlagsKhr GetDeviceGroupSurfacePresentModes2ext(Device device,
            PhysicalDeviceSurfaceInfo2khr* pSurfaceInfo)
        {
            DeviceGroupPresentModeFlagsKhr value;
            Vk.GetDeviceGroupSurfacePresentModes2ext(device, pSurfaceInfo, &value);
            return value;
        }

        public static uint GetPhysicalDeviceQueueFamilyPerformanceQueryPassesKhr(PhysicalDevice physicalDevice,
            QueryPoolPerformanceCreateInfoKhr* pPerformanceQueryCreateInfo)
        {
            uint value;
            Vk.GetPhysicalDeviceQueueFamilyPerformanceQueryPassesKhr(physicalDevice, pPerformanceQueryCreateInfo,
                &value);
            return value;
        }

        public static ImageDrmFormatModifierPropertiesExt GetImageDrmFormatModifierPropertiesExt(Device device,
            Image image)
        {
            ImageDrmFormatModifierPropertiesExt value;
            Vk.GetImageDrmFormatModifierPropertiesExt(device, image, &value);
            return value;
        }

        public static BufferDeviceAddressInfo GetBufferOpaqueCaptureAddress(Device device)
        {
            BufferDeviceAddressInfo value;
            Vk.GetBufferOpaqueCaptureAddress(device, &value);
            return value;
        }

        public static BufferDeviceAddressInfo GetBufferDeviceAddress(Device device)
        {
            BufferDeviceAddressInfo value;
            Vk.GetBufferDeviceAddress(device, &value);
            return value;
        }

        public static FramebufferMixedSamplesCombinationNv
            GetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNv(PhysicalDevice physicalDevice,
                uint* pCombinationCount)
        {
            FramebufferMixedSamplesCombinationNv value;
            Vk.GetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNv(physicalDevice, pCombinationCount,
                &value);
            return value;
        }

        public static PerformanceValueIntel GetPerformanceParameterIntel(Device device,
            PerformanceParameterTypeIntel parameter)
        {
            PerformanceValueIntel value;
            Vk.GetPerformanceParameterIntel(device, parameter, &value);
            return value;
        }

        public static DeviceMemoryOpaqueCaptureAddressInfo GetDeviceMemoryOpaqueCaptureAddress(Device device)
        {
            DeviceMemoryOpaqueCaptureAddressInfo value;
            Vk.GetDeviceMemoryOpaqueCaptureAddress(device, &value);
            return value;
        }

        public static PipelineExecutablePropertiesKhr GetPipelineExecutablePropertiesKhr(Device device,
            PipelineInfoKhr* pPipelineInfo, uint* pExecutableCount)
        {
            PipelineExecutablePropertiesKhr value;
            Vk.GetPipelineExecutablePropertiesKhr(device, pPipelineInfo, pExecutableCount, &value);
            return value;
        }

        public static PipelineExecutableStatisticKhr GetPipelineExecutableStatisticsKhr(Device device,
            PipelineExecutableInfoKhr* pExecutableInfo, uint* pStatisticCount)
        {
            PipelineExecutableStatisticKhr value;
            Vk.GetPipelineExecutableStatisticsKhr(device, pExecutableInfo, pStatisticCount, &value);
            return value;
        }

        public static PipelineExecutableInternalRepresentationKhr GetPipelineExecutableInternalRepresentationsKhr(
            Device device, PipelineExecutableInfoKhr* pExecutableInfo, uint* pInternalRepresentationCount)
        {
            PipelineExecutableInternalRepresentationKhr value;
            Vk.GetPipelineExecutableInternalRepresentationsKhr(device, pExecutableInfo, pInternalRepresentationCount,
                &value);
            return value;
        }

        public static PhysicalDeviceToolPropertiesExt GetPhysicalDeviceToolPropertiesExt(PhysicalDevice physicalDevice,
            uint* pToolCount)
        {
            PhysicalDeviceToolPropertiesExt value;
            Vk.GetPhysicalDeviceToolPropertiesExt(physicalDevice, pToolCount, &value);
            return value;
        }

        public static AccelerationStructureDeviceAddressInfoKhr GetAccelerationStructureDeviceAddressKhr(Device device)
        {
            AccelerationStructureDeviceAddressInfoKhr value;
            Vk.GetAccelerationStructureDeviceAddressKhr(device, &value);
            return value;
        }

        public static ulong GetPrivateDataExt(Device device, ObjectType objectType, ulong objectHandle,
            PrivateDataSlotExt privateDataSlot)
        {
            ulong value;
            Vk.GetPrivateDataExt(device, objectType, objectHandle, privateDataSlot, &value);
            return value;
        }

        public static Instance CreateInstance(InstanceCreateInfo pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            Instance retObject;
            var result = Vk.CreateInstance(&pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Device CreateDevice(PhysicalDevice physicalDevice, DeviceCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            Device retObject;
            var result = Vk.CreateDevice(physicalDevice, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Fence CreateFence(Device device, FenceCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            Fence retObject;
            var result = Vk.CreateFence(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Semaphore CreateSemaphore(Device device, SemaphoreCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            Semaphore retObject;
            var result = Vk.CreateSemaphore(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Event CreateEvent(Device device, EventCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            Event retObject;
            var result = Vk.CreateEvent(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static QueryPool CreateQueryPool(Device device, QueryPoolCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            QueryPool retObject;
            var result = Vk.CreateQueryPool(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Buffer CreateBuffer(Device device, BufferCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            Buffer retObject;
            var result = Vk.CreateBuffer(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static BufferView CreateBufferView(Device device, BufferViewCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            BufferView retObject;
            var result = Vk.CreateBufferView(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Image CreateImage(Device device, ImageCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            Image retObject;
            var result = Vk.CreateImage(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static ImageView CreateImageView(Device device, ImageViewCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            ImageView retObject;
            var result = Vk.CreateImageView(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static ShaderModule CreateShaderModule(Device device, ShaderModuleCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            ShaderModule retObject;
            var result = Vk.CreateShaderModule(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static PipelineCache CreatePipelineCache(Device device, PipelineCacheCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            PipelineCache retObject;
            var result = Vk.CreatePipelineCache(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Pipeline CreateGraphicsPipelines(Device device, PipelineCache pipelineCache, uint createInfoCount,
            GraphicsPipelineCreateInfo* pCreateInfos, AllocationCallbacks* pAllocator = null)
        {
            Pipeline retObject;
            var result = Vk.CreateGraphicsPipelines(device, pipelineCache, createInfoCount, pCreateInfos, pAllocator,
                &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Pipeline CreateComputePipelines(Device device, PipelineCache pipelineCache, uint createInfoCount,
            ComputePipelineCreateInfo* pCreateInfos, AllocationCallbacks* pAllocator = null)
        {
            Pipeline retObject;
            var result = Vk.CreateComputePipelines(device, pipelineCache, createInfoCount, pCreateInfos, pAllocator,
                &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static PipelineLayout CreatePipelineLayout(Device device, PipelineLayoutCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            PipelineLayout retObject;
            var result = Vk.CreatePipelineLayout(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Sampler CreateSampler(Device device, SamplerCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            Sampler retObject;
            var result = Vk.CreateSampler(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static DescriptorSetLayout CreateDescriptorSetLayout(Device device,
            DescriptorSetLayoutCreateInfo pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            DescriptorSetLayout retObject;
            var result = Vk.CreateDescriptorSetLayout(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static DescriptorPool CreateDescriptorPool(Device device, DescriptorPoolCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            DescriptorPool retObject;
            var result = Vk.CreateDescriptorPool(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Framebuffer CreateFramebuffer(Device device, FramebufferCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            Framebuffer retObject;
            var result = Vk.CreateFramebuffer(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static RenderPass CreateRenderPass(Device device, RenderPassCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            RenderPass retObject;
            var result = Vk.CreateRenderPass(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static CommandPool CreateCommandPool(Device device, CommandPoolCreateInfo pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            CommandPool retObject;
            var result = Vk.CreateCommandPool(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateAndroidSurfaceKhr(Instance instance, AndroidSurfaceCreateInfoKhr pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateAndroidSurfaceKhr(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static DisplayModeKhr CreateDisplayModeKhr(PhysicalDevice physicalDevice, DisplayKhr display,
            DisplayModeCreateInfoKhr pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            DisplayModeKhr retObject;
            var result = Vk.CreateDisplayModeKhr(physicalDevice, display, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateDisplayPlaneSurfaceKhr(Instance instance,
            DisplaySurfaceCreateInfoKhr pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateDisplayPlaneSurfaceKhr(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SwapchainKhr CreateSharedSwapchainsKhr(Device device, uint swapchainCount,
            SwapchainCreateInfoKhr* pCreateInfos, AllocationCallbacks* pAllocator = null)
        {
            SwapchainKhr retObject;
            var result = Vk.CreateSharedSwapchainsKhr(device, swapchainCount, pCreateInfos, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SwapchainKhr CreateSwapchainKhr(Device device, SwapchainCreateInfoKhr pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SwapchainKhr retObject;
            var result = Vk.CreateSwapchainKhr(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateViSurfaceNn(Instance instance, ViSurfaceCreateInfoNn pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateViSurfaceNn(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateWaylandSurfaceKhr(Instance instance, WaylandSurfaceCreateInfoKhr pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateWaylandSurfaceKhr(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateWin32SurfaceKhr(Instance instance, Win32SurfaceCreateInfoKhr pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateWin32SurfaceKhr(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateXlibSurfaceKhr(Instance instance, XlibSurfaceCreateInfoKhr pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateXlibSurfaceKhr(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateXcbSurfaceKhr(Instance instance, XcbSurfaceCreateInfoKhr pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateXcbSurfaceKhr(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateDirectFBSurfaceExt(Instance instance, DirectFBSurfaceCreateInfoExt pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateDirectFBSurfaceExt(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateImagePipeSurfaceFuchsia(Instance instance,
            ImagePipeSurfaceCreateInfoFuchsia pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateImagePipeSurfaceFuchsia(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateStreamDescriptorSurfaceGgp(Instance instance,
            StreamDescriptorSurfaceCreateInfoGgp pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateStreamDescriptorSurfaceGgp(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static DebugReportCallbackExt CreateDebugReportCallbackExt(Instance instance,
            DebugReportCallbackCreateInfoExt pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            DebugReportCallbackExt retObject;
            var result = Vk.CreateDebugReportCallbackExt(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static IndirectCommandsLayoutNv CreateIndirectCommandsLayoutNv(Device device,
            IndirectCommandsLayoutCreateInfoNv pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            IndirectCommandsLayoutNv retObject;
            var result = Vk.CreateIndirectCommandsLayoutNv(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static DescriptorUpdateTemplate CreateDescriptorUpdateTemplate(Device device,
            DescriptorUpdateTemplateCreateInfo pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            DescriptorUpdateTemplate retObject;
            var result = Vk.CreateDescriptorUpdateTemplate(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateIOSSurfaceMvk(Instance instance, IOSSurfaceCreateInfoMvk pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateIOSSurfaceMvk(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateMacOSSurfaceMvk(Instance instance, MacOSSurfaceCreateInfoMvk pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateMacOSSurfaceMvk(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateMetalSurfaceExt(Instance instance, MetalSurfaceCreateInfoExt pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateMetalSurfaceExt(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SamplerYcbcrConversion CreateSamplerYcbcrConversion(Device device,
            SamplerYcbcrConversionCreateInfo pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            SamplerYcbcrConversion retObject;
            var result = Vk.CreateSamplerYcbcrConversion(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static ValidationCacheExt CreateValidationCacheExt(Device device,
            ValidationCacheCreateInfoExt pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            ValidationCacheExt retObject;
            var result = Vk.CreateValidationCacheExt(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static DebugUtilsMessengerExt CreateDebugUtilsMessengerExt(Instance instance,
            DebugUtilsMessengerCreateInfoExt pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            DebugUtilsMessengerExt retObject;
            var result = Vk.CreateDebugUtilsMessengerExt(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static RenderPass CreateRenderPass2(Device device, RenderPassCreateInfo2 pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            RenderPass retObject;
            var result = Vk.CreateRenderPass2(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static AccelerationStructureKhr CreateAccelerationStructureNv(Device device,
            AccelerationStructureCreateInfoNv pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            AccelerationStructureKhr retObject;
            var result = Vk.CreateAccelerationStructureNv(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Pipeline CreateRayTracingPipelinesNv(Device device, PipelineCache pipelineCache,
            uint createInfoCount, RayTracingPipelineCreateInfoNv* pCreateInfos, AllocationCallbacks* pAllocator = null)
        {
            Pipeline retObject;
            var result = Vk.CreateRayTracingPipelinesNv(device, pipelineCache, createInfoCount, pCreateInfos,
                pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static Pipeline CreateRayTracingPipelinesKhr(Device device, PipelineCache pipelineCache,
            uint createInfoCount, RayTracingPipelineCreateInfoKhr* pCreateInfos, AllocationCallbacks* pAllocator = null)
        {
            Pipeline retObject;
            var result = Vk.CreateRayTracingPipelinesKhr(device, pipelineCache, createInfoCount, pCreateInfos,
                pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static SurfaceKhr CreateHeadlessSurfaceExt(Instance instance, HeadlessSurfaceCreateInfoExt pCreateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            SurfaceKhr retObject;
            var result = Vk.CreateHeadlessSurfaceExt(instance, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static AccelerationStructureKhr CreateAccelerationStructureKhr(Device device,
            AccelerationStructureCreateInfoKhr pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            AccelerationStructureKhr retObject;
            var result = Vk.CreateAccelerationStructureKhr(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static DeferredOperationKhr CreateDeferredOperationKhr(Device device,
            AllocationCallbacks* pAllocator = null)
        {
            DeferredOperationKhr retObject;
            var result = Vk.CreateDeferredOperationKhr(device, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static PrivateDataSlotExt CreatePrivateDataSlotExt(Device device,
            PrivateDataSlotCreateInfoExt pCreateInfo, AllocationCallbacks* pAllocator = null)
        {
            PrivateDataSlotExt retObject;
            var result = Vk.CreatePrivateDataSlotExt(device, &pCreateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static CommandBuffer[] AllocateCommandBuffers(Device device, CommandBufferAllocateInfo info)
        {
            var commandBuffers = new CommandBuffer[info.CommandBufferCount];
            fixed (CommandBuffer* ptr = commandBuffers)
            {
                var result = Vk.AllocateCommandBuffers(device, &info, ptr);
                if (result != Result.Success) throw new ApplicationException(result.ToString());
            }

            return commandBuffers;
        }

        public static DeviceMemory AllocateMemory(Device device, MemoryAllocateInfo allocateInfo,
            AllocationCallbacks* pAllocator = null)
        {
            DeviceMemory retObject;
            var result = Vk.AllocateMemory(device, &allocateInfo, pAllocator, &retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }

        public static IntPtr MapMemory(Device device, DeviceMemory memory, ulong offset, ulong size, uint flags)
        {
            IntPtr ptr;
            Vk.MapMemory(device, memory, offset, size, flags, (IntPtr) (&ptr));
            return ptr;
        }

        public static DescriptorSet[] AllocateDescriptorSets(Device device, DescriptorSetAllocateInfo info)
        {
            var descriptorSets = new DescriptorSet[info.DescriptorSetCount];
            fixed (DescriptorSet* ptr = descriptorSets)
            {
                var result = Vk.AllocateDescriptorSets(device, &info, ptr);
                if (result != Result.Success) throw new ApplicationException(result.ToString());
            }

            return descriptorSets;
        }
    }
}