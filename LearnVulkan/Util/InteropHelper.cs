namespace LearnVulkan.Util
{
    public class InteropHelper
    {
#if X64
            public const int PtrSize = 8;
#else
        public const int PtrSize = 4;
#endif
    }
}