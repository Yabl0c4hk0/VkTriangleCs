using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace LearnVulkan
{
    public static unsafe class Helper
    {
        public static T* Ptr<T>(T t) where T : unmanaged => &t;

        public static uint MakeVersion(uint major, uint minor, uint patch) =>
            (major << 22) | (minor << 12) | patch;

        public static T* ArrPtr<T>(IEnumerable<T> arr) where T : unmanaged
        {
            fixed (T* ptr = arr.ToArray())
                return ptr;
        }

        public static sbyte* StrPtr(string s) =>
            (sbyte*) ArrPtr(Encoding.UTF8.GetBytes(s + "\0"));

        public static sbyte** StrArrPtr(IReadOnlyList<string> strings) =>
            (sbyte**) new CStringArray(strings).Ptr;

        public static string Ptr2Str(IntPtr ptr) => 
            Marshal.PtrToStringUTF8(ptr);
    }

    public class CStringArray
    {
        public IntPtr Ptr;
        public int Length;

        public unsafe CStringArray(IReadOnlyList<string> strings)
        {
            var totalLength = strings.Sum(t => t.Length + 1);

            Ptr = Marshal.AllocHGlobal(totalLength + strings.Count * sizeof(void*));
            Length = strings.Count;
            var arrMem = (sbyte**) Ptr;
            var stringsMem = (sbyte*) (arrMem + strings.Count);

            for (var i = 0; i < strings.Count; ++i)
            {
                var str = strings[i];
                arrMem[i] = stringsMem;
                foreach (var t in str)
                    *stringsMem++ = (sbyte) t;
                *stringsMem++ = 0;
            }
        }

        ~CStringArray() => Marshal.FreeHGlobal(Ptr);
    }
}