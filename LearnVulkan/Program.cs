using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GLFW;
using LearnVulkan.Vulkan.Binding.Native;
using Image = LearnVulkan.Vulkan.Binding.Native.Image;

namespace LearnVulkan
{
    internal struct QueueFamilyIndices
    {
        public uint? GraphicsFamily;
        public uint? PresentFamily;

        public bool IsComplete()
        {
            return GraphicsFamily.HasValue && PresentFamily.HasValue;
        }
    }

    internal struct SwapChainSupportDetails
    {
        public SurfaceCapabilitiesKhr Capabilities;
        public SurfaceFormatKhr[] Formats;
        public PresentModeKhr[] PresentModes;
    }

    internal static class Program
    {
        private const int Width = 800;
        private const int Height = 600;

        private static readonly string[] validationLayers =
        {
            "VK_LAYER_KHRONOS_validation"
        };

        private static readonly string[] deviceExtensions =
        {
            "VK_KHR_swapchain"
        };

#if DEBUG
        private const bool EnableValidationLayers = true;
#else
        private const bool EnableValidationLayers = false;
#endif

        private static Window _window;

        private static Instance _instance;
        private static SurfaceKhr _surface;

        private static PhysicalDevice _physicalDevice = new() {Handle = IntPtr.Zero};
        private static Device _device;

        private static Queue _graphicsQueue;
        private static Queue _presentQueue;

        private static SwapchainKhr _swapChain;
        private static Image[] _swapChainImages;
        private static Format _swapChainImageFormat;
        private static Extent2d _swapChainExtent;
        private static ImageView[] _swapChainImageViews;
        private static Framebuffer[] _swapChainFramebuffers;

        private static RenderPass _renderPass;
        private static PipelineLayout _pipelineLayout;
        private static Pipeline _graphicsPipeline;

        private static CommandPool _commandPool;
        private static CommandBuffer[] _commandBuffers;
        private static Semaphore _imageAvailableSemaphore;
        private static Semaphore _renderFinishedSemaphore;
        
        private static unsafe void Main(string[] args)
        {
            Glfw.Init();
            Glfw.WindowHint(Hint.ClientApi, ClientApi.None);
            Glfw.WindowHint(Hint.Resizable, Constants.False);
            _window = Glfw.CreateWindow(Width, Height, "Vulkan", Monitor.None, Window.None);


            if (EnableValidationLayers && !validationLayers
                .Select(layerName => Vkh.EnumerateInstanceLayerProperties()
                    .Any(layerProperties => layerProperties.LayerName == layerName))
                .All(layerFound => layerFound))
                throw new ApplicationException("validation layers requested, but not available!");
            _instance = Vkh.CreateInstance(new InstanceCreateInfo
            {
                SType = StructureType.InstanceCreateInfo,
                PApplicationInfo = Helper.Ptr(
                    new ApplicationInfo
                    {
                        SType = StructureType.ApplicationInfo,
                        PApplicationName = Helper.StrPtr("Hello Triangle"),
                        ApplicationVersion = Helper.MakeVersion(1, 0, 0),
                        PEngineName = Helper.StrPtr("No Engine"),
                        EngineVersion = Helper.MakeVersion(1, 0, 0),
                        ApiVersion = Helper.MakeVersion(1, 0, 0)
                    }),
                PpEnabledExtensionNames = GlfwVk.GetRequiredInstanceExtensions(out var glfwExtensionCount),
                EnabledExtensionCount = glfwExtensionCount,
                EnabledLayerCount = EnableValidationLayers ? (uint) validationLayers.Length : 0,
                PpEnabledLayerNames = EnableValidationLayers ? Helper.StrArrPtr(validationLayers) : null,
            });
            _surface = GlfwVkh.CreateWindowSurface(_instance, _window);
            PickPhysicalDevice();
            CreateLogicalDevice();
            CreateSwapChain();
            CreateImageViews();
            CreateRenderPass();
            CreateGraphicsPipeline();
            CreateFramebuffers();
            CreateCommandPool();
            CreateCommandBuffers();
            CreateSemaphores();

            while (!Glfw.WindowShouldClose(_window))
            {
                Glfw.PollEvents();
                DrawFrame();
            }

            Vk.DestroySemaphore(_device, _renderFinishedSemaphore);
            Vk.DestroySemaphore(_device, _imageAvailableSemaphore);
            foreach (var framebuffer in _swapChainFramebuffers)
                Vk.DestroyFramebuffer(_device, framebuffer);
            Vk.DestroyPipeline(_device, _graphicsPipeline);
            Vk.DestroyPipelineLayout(_device, _pipelineLayout);
            Vk.DestroyRenderPass(_device, _renderPass);
            foreach (var imageView in _swapChainImageViews)
                Vk.DestroyImageView(_device, imageView);
            Vk.DestroySwapchainKhr(_device, _swapChain);
            Vk.DestroyDevice(_device);
            Vk.DestroySurfaceKhr(_instance, _surface);
            Vk.DestroyInstance(_instance);

            Glfw.DestroyWindow(_window);
            Glfw.Terminate();
        }

        private static void PickPhysicalDevice()
        {
            foreach (var device in Vkh.EnumeratePhysicalDevices(_instance))
                if (IsDeviceSuitable(device))
                {
                    _physicalDevice = device;
                    break;
                }

            if (_physicalDevice.Handle == IntPtr.Zero)
                throw new ApplicationException("failed to find a suitable GPU");
        }

        private static unsafe void CreateLogicalDevice()
        {
            var indices = FindQueueFamilies(_physicalDevice);

            var queuePriority = 1.0f;
            var pQueuePriority = &queuePriority;

            _device = Vkh.CreateDevice(_physicalDevice, new DeviceCreateInfo
            {
                SType = StructureType.DeviceCreateInfo,
                PQueueCreateInfos = Helper.ArrPtr(
                    new SortedSet<uint> {indices.GraphicsFamily.Value, indices.PresentFamily.Value}
                        .Select(queueFamily => new DeviceQueueCreateInfo
                        {
                            SType = StructureType.DeviceQueueCreateInfo,
                            QueueFamilyIndex = queueFamily,
                            QueueCount = 1,
                            PQueuePriorities = pQueuePriority
                        }).ToArray()),
                QueueCreateInfoCount = 1,
                PEnabledFeatures = Helper.Ptr(new PhysicalDeviceFeatures()),
                EnabledExtensionCount = (uint) deviceExtensions.Length,
                PpEnabledExtensionNames = Helper.StrArrPtr(deviceExtensions),
                EnabledLayerCount = EnableValidationLayers ? (uint) validationLayers.Length : 0,
                PpEnabledLayerNames = EnableValidationLayers ? Helper.StrArrPtr(validationLayers) : null
            });
            _graphicsQueue = Vkh.GetDeviceQueue(_device, indices.GraphicsFamily.Value, 0);
            _presentQueue = Vkh.GetDeviceQueue(_device, indices.PresentFamily.Value, 0);
        }

        private static unsafe void CreateSwapChain()
        {
            var swapChainSupport = QuerySwapChainSupport(_physicalDevice);
            var surfaceFormat = ChooseSwapSurfaceFormat(swapChainSupport.Formats);
            var presentMode = ChooseSwapPresentMode(swapChainSupport.PresentModes);
            var extent = ChooseSwapExtent(swapChainSupport.Capabilities);

            var imageCount = swapChainSupport.Capabilities.MinImageCount + 1;
            if (swapChainSupport.Capabilities.MaxImageCount > 0 &&
                imageCount > swapChainSupport.Capabilities.MaxImageCount)
                imageCount = swapChainSupport.Capabilities.MaxImageCount;

            var indices = FindQueueFamilies(_physicalDevice);
            var concurrent = indices.GraphicsFamily != indices.PresentFamily;
            var createInfo = new SwapchainCreateInfoKhr
            {
                SType = StructureType.SwapchainCreateInfoKhr,
                Surface = _surface,
                MinImageCount = imageCount,
                ImageFormat = surfaceFormat.Format,
                ImageColorSpace = surfaceFormat.ColorSpace,
                ImageExtent = extent,
                ImageArrayLayers = 1,
                ImageUsage = ImageUsageFlags.ColorAttachmentBit,
                PreTransform = swapChainSupport.Capabilities.CurrentTransform,
                CompositeAlpha = CompositeAlphaFlagsKhr.OpaqueBitKhr,
                PresentMode = presentMode,
                Clipped = (uint) ApiConstants.True,
                OldSwapchain = new SwapchainKhr {Handle = IntPtr.Zero},
                ImageSharingMode = concurrent ? SharingMode.Concurrent : SharingMode.Exclusive,
                QueueFamilyIndexCount = concurrent ? 2 : default,
                PQueueFamilyIndices = concurrent
                    ? Helper.ArrPtr(new[] {indices.GraphicsFamily.Value, indices.PresentFamily.Value})
                    : default
            };

            _swapChain = Vkh.CreateSwapchainKhr(_device, createInfo);
            _swapChainImages = Vkh.GetSwapchainImagesKhr(_device, _swapChain);
            _swapChainImageFormat = surfaceFormat.Format;
            _swapChainExtent = extent;
        }

        private static unsafe void CreateImageViews()
        {
            _swapChainImageViews = new ImageView[_swapChainImages.Length];

            for (var i = 0; i < _swapChainImages.Length; i++)
            {
                _swapChainImageViews[i] = Vkh.CreateImageView(_device, new ImageViewCreateInfo
                {
                    SType = StructureType.ImageViewCreateInfo,
                    Image = _swapChainImages[i],
                    ViewType = ImageViewType.N2d,
                    Format = _swapChainImageFormat,
                    Components = new ComponentMapping
                    {
                        R = ComponentSwizzle.Identity,
                        A = ComponentSwizzle.Identity,
                        G = ComponentSwizzle.Identity,
                        B = ComponentSwizzle.Identity,
                    },
                    SubresourceRange = new ImageSubresourceRange
                    {
                        AspectMask = ImageAspectFlags.ColorBit,
                        BaseMipLevel = 0,
                        LevelCount = 1,
                        BaseArrayLayer = 0,
                        LayerCount = 1,
                    }
                });
            }
        }

        private static unsafe void CreateRenderPass()
        {
            var colorAttachmentRef = new AttachmentReference
            {
                Attachment = 0,
                Layout = ImageLayout.ColorAttachmentOptimal
            };

            _renderPass = Vkh.CreateRenderPass(_device, new RenderPassCreateInfo
            {
                SType = StructureType.RenderPassCreateInfo,
                AttachmentCount = 1,
                PAttachments = Helper.Ptr(new AttachmentDescription
                {
                    Format = _swapChainImageFormat,
                    Samples = SampleCountFlags.N1Bit,
                    LoadOp = AttachmentLoadOp.Clear,
                    StoreOp = AttachmentStoreOp.Store,
                    StencilLoadOp = AttachmentLoadOp.DontCare,
                    StencilStoreOp = AttachmentStoreOp.DontCare,
                    InitialLayout = ImageLayout.Undefined,
                    FinalLayout = ImageLayout.PresentSrcKhr
                }),
                SubpassCount = 1,
                PSubpasses = Helper.Ptr(new SubpassDescription
                {
                    PipelineBindPoint = PipelineBindPoint.Graphics,
                    ColorAttachmentCount = 1,
                    PColorAttachments = &colorAttachmentRef,
                }),
                DependencyCount = 1,
                PDependencies = Helper.Ptr(new SubpassDependency
                {
                    SrcSubpass = (uint) ApiConstants.SubpassExternal,
                    DstSubpass = 0,
                    SrcStageMask = PipelineStageFlags.ColorAttachmentOutputBit,
                    SrcAccessMask = 0,
                    DstStageMask = PipelineStageFlags.ColorAttachmentOutputBit,
                    DstAccessMask = AccessFlags.ColorAttachmentWriteBit
                })
            });
        }

        private static unsafe void CreateGraphicsPipeline()
        {
            var vertShaderModule = CreateShaderModule("vert.spv");
            var fragShaderModule = CreateShaderModule("frag.spv");

            _pipelineLayout = Vkh.CreatePipelineLayout(_device, new PipelineLayoutCreateInfo
            {
                SType = StructureType.PipelineLayoutCreateInfo,
                SetLayoutCount = 0,
                PushConstantRangeCount = 0,
            });

            _graphicsPipeline =
                Vkh.CreateGraphicsPipelines(_device, new PipelineCache {Handle = IntPtr.Zero}, 1, Helper.Ptr(
                    new GraphicsPipelineCreateInfo
                    {
                        SType = StructureType.GraphicsPipelineCreateInfo,
                        StageCount = 2,
                        PStages = Helper.ArrPtr(new[]
                        {
                            new PipelineShaderStageCreateInfo
                            {
                                SType = StructureType.PipelineShaderStageCreateInfo,
                                Stage = ShaderStageFlags.VertexBit,
                                Module = vertShaderModule,
                                PName = Helper.StrPtr("main")
                            },
                            new PipelineShaderStageCreateInfo
                            {
                                SType = StructureType.PipelineShaderStageCreateInfo,
                                Stage = ShaderStageFlags.FragmentBit,
                                Module = fragShaderModule,
                                PName = Helper.StrPtr("main")
                            }
                        }),
                        PVertexInputState = Helper.Ptr(new PipelineVertexInputStateCreateInfo
                        {
                            SType = StructureType.PipelineVertexInputStateCreateInfo,
                            VertexBindingDescriptionCount = 0,
                            VertexAttributeDescriptionCount = 0,
                        }),
                        PInputAssemblyState = Helper.Ptr(new PipelineInputAssemblyStateCreateInfo
                        {
                            SType = StructureType.PipelineInputAssemblyStateCreateInfo,
                            Topology = PrimitiveTopology.TriangleList,
                            PrimitiveRestartEnable = (uint) ApiConstants.False,
                        }),
                        PViewportState = Helper.Ptr(new PipelineViewportStateCreateInfo
                        {
                            SType = StructureType.PipelineViewportStateCreateInfo,
                            ViewportCount = 1,
                            PViewports = Helper.Ptr(new Viewport
                            {
                                X = 0.0f,
                                Y = 0.0f,
                                Width = _swapChainExtent.Width,
                                Height = _swapChainExtent.Height,
                                MinDepth = 0.0f,
                                MaxDepth = 1.0f
                            }),
                            ScissorCount = 1,
                            PScissors =
                                Helper.Ptr(new Rect2d
                                    {Offset = new Offset2d {X = 0, Y = 0}, Extent = _swapChainExtent}),
                        }),
                        PRasterizationState = Helper.Ptr(new PipelineRasterizationStateCreateInfo
                        {
                            SType = StructureType.PipelineRasterizationStateCreateInfo,
                            DepthClampEnable = (uint) ApiConstants.False,
                            RasterizerDiscardEnable = (uint) ApiConstants.False,
                            PolygonMode = PolygonMode.Fill,
                            LineWidth = 1.0f,
                            CullMode = CullModeFlags.BackBit,
                            FrontFace = FrontFace.Clockwise,
                            DepthBiasEnable = (uint) ApiConstants.False,
                        }),
                        PMultisampleState = Helper.Ptr(new PipelineMultisampleStateCreateInfo
                        {
                            SType = StructureType.PipelineMultisampleStateCreateInfo,
                            SampleShadingEnable = (uint) ApiConstants.False,
                            RasterizationSamples = SampleCountFlags.N1Bit,
                        }),
                        PColorBlendState = Helper.Ptr(new PipelineColorBlendStateCreateInfo
                        {
                            SType = StructureType.PipelineColorBlendStateCreateInfo,
                            LogicOpEnable = (uint) ApiConstants.False,
                            LogicOp = LogicOp.Copy,
                            AttachmentCount = 1,
                            PAttachments = Helper.Ptr(new PipelineColorBlendAttachmentState
                            {
                                ColorWriteMask = ColorComponentFlags.RBit | ColorComponentFlags.GBit |
                                                 ColorComponentFlags.BBit | ColorComponentFlags.ABit,
                                BlendEnable = (uint) ApiConstants.False
                            }),
                            BlendConstants4 = Helper.ArrPtr(new[] {0.0f, 0.0f, 0.0f, 0.0f})
                        }),
                        Layout = _pipelineLayout,
                        RenderPass = _renderPass,
                        Subpass = 0,
                        BasePipelineHandle = new Pipeline {Handle = IntPtr.Zero},
                    }));

            Vk.DestroyShaderModule(_device, fragShaderModule);
            Vk.DestroyShaderModule(_device, vertShaderModule);
        }

        private static unsafe void CreateFramebuffers()
        {
            _swapChainFramebuffers = new Framebuffer[_swapChainImageViews.Length];

            for (var i = 0; i < _swapChainImageViews.Length; i++)
                _swapChainFramebuffers[i] = Vkh.CreateFramebuffer(_device, new FramebufferCreateInfo
                {
                    SType = StructureType.FramebufferCreateInfo,
                    RenderPass = _renderPass,
                    AttachmentCount = 1,
                    PAttachments = Helper.ArrPtr(new[] {_swapChainImageViews[i]}),
                    Width = _swapChainExtent.Width,
                    Height = _swapChainExtent.Height,
                    Layers = 1,
                });
        }

        private static unsafe void CreateCommandPool()
        {
            _commandPool = Vkh.CreateCommandPool(_device, new CommandPoolCreateInfo
            {
                SType = StructureType.CommandPoolCreateInfo,
                QueueFamilyIndex = FindQueueFamilies(_physicalDevice).GraphicsFamily.Value
            });
        }

        private static unsafe void CreateCommandBuffers()
        {
            _commandBuffers = Vkh.AllocateCommandBuffers(_device, new CommandBufferAllocateInfo
            {
                SType = StructureType.CommandBufferAllocateInfo,
                CommandPool = _commandPool,
                Level = CommandBufferLevel.Primary,
                CommandBufferCount = (uint) _swapChainFramebuffers.Length,
            });

            for (var i = 0; i < _commandBuffers.Length; i++)
            {
                if (Vk.BeginCommandBuffer(_commandBuffers[i],
                        Helper.Ptr(new CommandBufferBeginInfo {SType = StructureType.CommandBufferBeginInfo})) !=
                    Result.Success)
                    throw new ApplicationException("failed to begin recording command buffer!");

                Vk.CmdBeginRenderPass(_commandBuffers[i], Helper.Ptr(new RenderPassBeginInfo
                {
                    SType = StructureType.RenderPassBeginInfo,
                    RenderPass = _renderPass,
                    Framebuffer = _swapChainFramebuffers[i],
                    RenderArea = new Rect2d {Offset = new Offset2d {X = 0, Y = 0}, Extent = _swapChainExtent},
                    ClearValueCount = 1,
                    PClearValues = (ClearValue*) Helper.ArrPtr(new[] {0.0f, 0.0f, 0.0f, 1.0f})
                }), SubpassContents.Inline);
                Vk.CmdBindPipeline(_commandBuffers[i], PipelineBindPoint.Graphics, _graphicsPipeline);
                Vk.CmdDraw(_commandBuffers[i], 3, 1, 0, 0);
                Vk.CmdEndRenderPass(_commandBuffers[i]);
                if (Vk.EndCommandBuffer(_commandBuffers[i]) != Result.Success)
                    throw new ApplicationException("failed to record command buffer!");
            }
        }

        private static unsafe void CreateSemaphores()
        {
            var semaphoreInfo = new SemaphoreCreateInfo {SType = StructureType.SemaphoreCreateInfo};
            _imageAvailableSemaphore = Vkh.CreateSemaphore(_device, semaphoreInfo);
            _renderFinishedSemaphore = Vkh.CreateSemaphore(_device, semaphoreInfo);
        }

        private static unsafe void DrawFrame()
        {
            Vk.AcquireNextImageKhr(_device, _swapChain, ulong.MaxValue, _imageAvailableSemaphore,
                new Fence {Handle = IntPtr.Zero}, out var imageIndex);

            var commandBuffer = _commandBuffers[imageIndex];
            if (Vk.QueueSubmit(_graphicsQueue, 1, Helper.Ptr(new SubmitInfo
            {
                SType = StructureType.SubmitInfo,
                WaitSemaphoreCount = 1,
                PWaitSemaphores = Helper.ArrPtr(new[] {_imageAvailableSemaphore}),
                PWaitDstStageMask = Helper.ArrPtr(new[] {PipelineStageFlags.ColorAttachmentOutputBit}),
                CommandBufferCount = 1,
                PCommandBuffers = &commandBuffer,
                SignalSemaphoreCount = 1,
                PSignalSemaphores = Helper.ArrPtr(new[] {_renderFinishedSemaphore})
            }), new Fence {Handle = IntPtr.Zero}) != Result.Success)
                throw new ApplicationException("failed to submit draw command buffer!");

            Vk.QueuePresentKhr(_presentQueue, Helper.Ptr(new PresentInfoKhr
            {
                SType = StructureType.PresentInfoKhr,
                WaitSemaphoreCount = 1,
                PWaitSemaphores = Helper.ArrPtr(new[] {_renderFinishedSemaphore}),
                SwapchainCount = 1,
                PSwapchains = Helper.ArrPtr(new[] {_swapChain}),
                PImageIndices = &imageIndex
            }));
            Vk.QueueWaitIdle(_presentQueue);
        }

        private static unsafe ShaderModule CreateShaderModule(string path)
        {
            var bytes = File.ReadAllBytes(path);
            return Vkh.CreateShaderModule(_device, new ShaderModuleCreateInfo
            {
                SType = StructureType.ShaderModuleCreateInfo,
                CodeSize = (IntPtr) bytes.Length,
                PCode = (uint*) Helper.ArrPtr(bytes)
            });
        }

        private static SurfaceFormatKhr ChooseSwapSurfaceFormat(SurfaceFormatKhr[] availableFormats)
        {
            foreach (var availableFormat in availableFormats)
                if (availableFormat.Format == Format.B8g8r8a8Srgb &&
                    availableFormat.ColorSpace == ColorSpaceKhr.ColorSpaceSrgbNonlinearKhr)
                    return availableFormat;
            return availableFormats[0];
        }

        private static PresentModeKhr ChooseSwapPresentMode(PresentModeKhr[] availablePresentModes)
        {
            foreach (var availablePresentMode in availablePresentModes)
                if (availablePresentMode == PresentModeKhr.MailboxKhr)
                    return availablePresentMode;
            return PresentModeKhr.FifoKhr;
        }

        private static Extent2d ChooseSwapExtent(SurfaceCapabilitiesKhr capabilities)
        {
            if (capabilities.CurrentExtent.Width != uint.MaxValue)
                return capabilities.CurrentExtent;
            Glfw.GetFramebufferSize(_window, out var width, out var height);
            return new Extent2d
            {
                Width = (uint) Math.Max(capabilities.MinImageExtent.Width,
                    Math.Min(capabilities.MaxImageExtent.Width, width)),
                Height = (uint) Math.Max(capabilities.MinImageExtent.Height,
                    Math.Min(capabilities.MaxImageExtent.Height, height))
            };
        }

        private static SwapChainSupportDetails QuerySwapChainSupport(PhysicalDevice device) => new()
        {
            Capabilities = Vkh.GetPhysicalDeviceSurfaceCapabilitiesKhr(device, _surface),
            Formats = Vkh.GetPhysicalDeviceSurfaceFormatsKhr(device, _surface),
            PresentModes = Vkh.GetPhysicalDeviceSurfacePresentModesKhr(device, _surface)
        };

        private static bool IsDeviceSuitable(PhysicalDevice device)
        {
            var swapChainSupport = QuerySwapChainSupport(device);
            return FindQueueFamilies(device).IsComplete() && CheckDeviceExtensionSupport(device) &&
                   swapChainSupport.Formats.Length != 0 && swapChainSupport.PresentModes.Length != 0;
        }

        private static unsafe bool CheckDeviceExtensionSupport(PhysicalDevice device)
        {
            var requiredExtensions = new SortedSet<string>(deviceExtensions);
            foreach (var extension in Vkh.EnumerateDeviceExtensionProperties(device, null))
                requiredExtensions.Remove(extension.ExtensionName);

            return requiredExtensions.Count == 0;
        }

        private static QueueFamilyIndices FindQueueFamilies(PhysicalDevice device)
        {
            var indices = new QueueFamilyIndices();

            uint i = 0;
            foreach (var queueFamily in Vkh.GetPhysicalDeviceQueueFamilyProperties(device))
            {
                if ((queueFamily.QueueFlags & QueueFlags.GraphicsBit) != 0)
                    indices.GraphicsFamily = i;
                if (Vkh.GetPhysicalDeviceSurfaceSupportKhr(device, i, _surface) == (ulong) ApiConstants.True)
                    indices.PresentFamily = i;
                if (indices.IsComplete())
                    break;

                i++;
            }

            return indices;
        }
    }
}