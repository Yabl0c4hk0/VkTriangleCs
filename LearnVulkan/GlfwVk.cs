using System;
using System.Runtime.InteropServices;
using GLFW;
using LearnVulkan.Vulkan.Binding.Native;

namespace LearnVulkan
{
    public static unsafe class GlfwVk
    {
        public const string Library = "glfw";

        /// <summary>
        ///     Returns whether the Vulkan loader and any minimally functional ICD have been found.
        /// </summary>
        /// <param name="count">Where to store the number of extensions in the returned array. This is set to zero if an error occurred.</param>
        /// <returns></returns>
        [DllImport(Library, EntryPoint = "glfwGetRequiredInstanceExtensions",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern sbyte** GetRequiredInstanceExtensions(out uint count);

        /// <summary>
        ///     Creates a Vulkan surface for the specified window.
        /// </summary>
        /// <param name="instance">The Vulkan instance to create the surface in.</param>
        /// <param name="window">The window to create the surface for.</param>
        /// <param name="allocator">The allocator to use, or NULL to use the default allocator.</param>
        /// <param name="surface">Where to store the handle of the surface. This is set to VK_NULL_HANDLE if an error occurred.</param>
        /// <returns></returns>
        [DllImport(Library, EntryPoint = "glfwCreateWindowSurface",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern Result CreateWindowSurface(Instance instance, Window window,
            AllocationCallbacks* allocator, out SurfaceKhr surface);
    }
    
    public static unsafe class GlfwVkh
    {
        public static SurfaceKhr CreateWindowSurface(Instance instance, Window window,
            AllocationCallbacks* allocator = null)
        {
            var result = GlfwVk.CreateWindowSurface(instance, window, allocator, out var retObject);
            if (result != Result.Success) throw new ApplicationException(result.ToString());
            return retObject;
        }
    }
}